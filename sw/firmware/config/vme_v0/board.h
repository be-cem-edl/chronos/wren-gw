#include "vme.h"
#include "si5340-load.h"
#include "dco.h"

#define BOARD vme

#define SI5340_USE_SPI

#define MAIN_SI5340_ADDR 0
#define MAIN_SI5340_FREQ 10000000
#define MAIN_SI5340_FILE "vme_v0-main-regs.h"

#define BOARD_MENU			\
  { "pll-main", do_main_si5340 },	\
  { "dco", do_dco },			\
  { "vme", do_vme },

