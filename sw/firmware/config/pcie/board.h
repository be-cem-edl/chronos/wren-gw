#include "si5340-load.h"
#include "pcie-core.h"

#define BOARD pcie

#define SI5340_USE_SPI

#define MAIN_SI5340_ADDR 0
#define MAIN_SI5340_FREQ 48000000
#define MAIN_SI5340_FILE "pcie-main-regs.h"
//#define MAIN_SI5340_FILE "spexi7u_si5341_main.h"


void board_pcie_pll_init(void);
void board_pcie_init(void);

void do_dco(char *args);

void dco_init(void);

#define BOARD_MENU			\
  { "pll-main", do_main_si5340 },	\
  { "dco", do_dco }, \
  PCIE_MENU
