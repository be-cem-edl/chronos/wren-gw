#include "lib.h"
#include "board.h"
#include "i2c.h"
#include "pcie-core.h"
#include "si5340-load.h"

void board_pcie_pll_init(void)
{
    /* Deassert PLL rstn */
    pll_reset();

    si5340_init();
}

void board_pcie_init(void)
{
    dco_init();
    i2c_init();
    pcie_init ();
}
