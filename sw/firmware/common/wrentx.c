#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "lib.h"
#include "trace.h"
#include "wren.h"
#include "wrentx-data.h"
#include "wren-hw.h"
#include "wren-mb-defs.h"
#include "nic.h"
#include "wren/wren-packet.h"
#include "board_map.h"
#include "wrentx.h"
#include "versions.h"

#define WRENTX_NBR_TABLES 16

struct wren_tx_source wren_tx_sources[MAX_TX_SOURCES];

#define TABLE_STATE_UNUSED 0
#define TABLE_STATE_LOADED 1
#define TABLE_STATE_RUN    2

struct wren_tx_table {
    uint8_t pc;
    uint8_t state;
    uint8_t src_idx;
    uint8_t nbr_insns;

    uint16_t nbr_data;

    uint32_t repeat;
    uint32_t data[WRENTX_MAX_INSNS + WRENTX_MAX_DATA];
};

static struct wren_tx_table wren_tx_tables[WRENTX_NBR_TABLES];

void
get_now_packet_ts(struct wren_packet_ts *now)
{
  volatile struct board_map *regs =
    (volatile struct board_map *)BOARD_MAP_BASE;
  unsigned lo;

  lo = regs->tm_tai_lo;

  do {
    now->sec = lo;
    now->nsec = regs->tm_cycles << 4;
    lo = regs->tm_tai_lo;
  } while (lo != now->sec);
}

void
cmd_tx_get_config (uint32_t *data, uint32_t len)
{
  struct wren_mb_tx_get_config_reply *res;
  unsigned i;
  unsigned nbr;

  res = (struct wren_mb_tx_get_config_reply *)mb_get_reply
    (CMD_TX_GET_CONFIG | CMD_REPLY, sizeof(*res) / 4);
  res->sw_version = WREN_FW_VERSION;
  res->max_sources = MAX_TX_SOURCES;

  nbr = 0;
  for (i = 0; i < MAX_TX_SOURCES; i++)
    if (wren_tx_sources[i].proto.proto != WREN_PROTO_NONE)
      nbr++;
  res->nbr_sources = nbr;

  mb_send_reply();
}

void
cmd_tx_set_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_tx_set_source *arg = (struct wren_mb_tx_set_source *)data;
    uint32_t source_idx = arg->source_idx;
    struct wren_tx_source *src;

    if (source_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
	return;
    }

    if (arg->proto.proto != WREN_PROTO_ETHERNET) {
	mb_reply_error(WRENTX_ERR_BAD_PROTOCOL);
	return;
    }

    src = &wren_tx_sources[source_idx];
    src->proto = arg->proto;
    src->max_delay_us_p2 = arg->max_delay_us_p2;
    src->seq_id = 0;

    mb_reply_ok();
}

void
cmd_tx_get_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *arg = (struct wren_mb_arg1 *)data;
    uint32_t source_idx = arg->arg1;
    struct wren_tx_source *src;
    struct wren_protocol *res;

    if (source_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
	return;
    }

    src = &wren_tx_sources[source_idx];

    res = (struct wren_protocol *)mb_get_reply (CMD_REPLY, sizeof(*res) / 4);

    w32cpy (res, &src->proto, sizeof(src->proto) / 4);
    //    *res = src->proto;

    mb_send_reply();
}

void
w32cpy(void *dst, void *src, unsigned wlen)
{
  volatile uint32_t *d = (volatile uint32_t *)dst;
  uint32_t *s = (uint32_t *)src;

  while (wlen--)
    *d++ = *s++;
}

void
cmd_tx_send_packet (uint32_t *data, uint32_t len)
{
  uint32_t src_idx;
  struct wren_tx_source *src;
  union {
    uint8_t b[20];
    uint32_t w[5];
  } eth_hdr;
  unsigned pkt_len;
  unsigned off;
  union {
    struct wren_packet_hdr_v1 p;
    uint32_t w[sizeof(struct wren_packet_hdr_v1) / 4];
  } phdr;
  uint8_t *buf;
  struct wren_packet_ts now;

  /* Check source */
  if (len < 1) {
      mb_reply_failure(CMD_ERROR_LENGTH);
      return;
  }

  src_idx = *data;
  if (src_idx >= MAX_TX_SOURCES) {
      mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
      return;
  }
  src = &wren_tx_sources[src_idx];
  if (src->proto.proto != WREN_PROTO_ETHERNET) {
    mb_reply_error(WRENTX_ERR_BAD_PROTOCOL);
    return;
  }

  /* Remove source (it's the first byte of the data)  */
  len--;

  /* Allocate a nic buffer.
     The first 2 bytes are reserved.  */
  pkt_len = 2 + ETH_HDR_LEN + sizeof(struct wren_packet_hdr_v1) + len * 4;
  if (src->proto.u.eth.flags & WREN_PROTO_ETH_FLAGS_VLAN)
    pkt_len += 4;
  buf = (uint8_t*)nic_get_tx_buf (pkt_len);
  if (buf == NULL) {
    uart_printf("send packet: no tx buf\n");
    mb_reply_error(WRENTX_ERR_QUEUE_FULL);
    return;
  }

  /* Write protocol header (TODO: vlan + length) */
  memcpy (eth_hdr.b + 2, src->proto.u.eth.mac, 6);
  off = 2 + 6 + 6;
  if (src->proto.u.eth.flags & WREN_PROTO_ETH_FLAGS_VLAN) {
    eth_hdr.b[off + 0] = 0x81;
    eth_hdr.b[off + 1] = 0;
    eth_hdr.b[off + 2] = src->proto.u.eth.vlan >> 8;
    eth_hdr.b[off + 3] = src->proto.u.eth.vlan;
    off += 4;
  }

  eth_hdr.b[off + 0] = src->proto.u.eth.ethertype >> 8;
  eth_hdr.b[off + 1] = src->proto.u.eth.ethertype;
  off += 2;

  w32cpy(buf, eth_hdr.w, off / 4);

  /* Write packet header */
  phdr.p.version = PKT_VERSION_v1;
  phdr.p.domain = src->proto.u.eth.domain_id;
  phdr.p.xmit = src->proto.u.eth.xmit_id;
  phdr.p.next = src->max_delay_us_p2;
  phdr.p.len = len + sizeof(struct wren_packet_hdr_v1) / 4;
  phdr.p.seq_id = src->seq_id;
  src->seq_id = (src->seq_id + 4) & PKT_SEQ_MASK;

  get_now_packet_ts(&now);
  phdr.p.snd_ts = ((now.sec & 3) << 22) | (now.nsec >> 8);

  w32cpy(buf + off, phdr.w, sizeof(phdr) / 4);

  /* Copy data */
  w32cpy(buf + off + sizeof(struct wren_packet_hdr_v1), data + 1, len);

  if (log_flags & LOG_TX) {
    uart_printf("send src=%u, buf=%x, pkt_len=0x%x\n",
		(unsigned)src_idx, (unsigned)buf, pkt_len);
    dump_w32(buf, pkt_len > 64 ? 64 : pkt_len);
  }

  /* Send packet */
  nic_send_tx_buf();
  mb_reply_ok();
  return;
}

void
cmd_tx_set_table (uint32_t *data, uint32_t len)
{
    struct wren_mb_tx_set_table *arg = (struct wren_mb_tx_set_table *)data;
    unsigned hdr_len = (sizeof(*arg) - sizeof(arg->data)) / 4;
    struct wren_tx_table *tbl;
    unsigned src_idx;

    if (len < hdr_len
	|| arg->nbr_insns > WRENTX_MAX_INSNS
	|| arg->nbr_data > WRENTX_MAX_DATA
	|| len != hdr_len + arg->nbr_insns + arg->nbr_data) {
	mb_reply_error(CMD_ERROR_LENGTH);
	return;
    }

    src_idx = arg->src_idx;
    if (src_idx >= MAX_TX_SOURCES
	|| wren_tx_sources[src_idx].proto.proto == WREN_PROTO_NONE) {
	mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
	return;
    }

    if (arg->table_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_TABLE_IDX);
	return;
    }

    tbl = &wren_tx_tables[arg->table_idx];
    if (tbl->state != TABLE_STATE_UNUSED) {
	mb_reply_error(WRENTX_ERR_BUSY_TABLE);
	return;
    }

    /* Copy */
    tbl->src_idx = arg->src_idx;
    tbl->nbr_insns = arg->nbr_insns;
    tbl->nbr_data = arg->nbr_data;
    tbl->repeat = arg->repeat;
    w32cpy(tbl->data, arg->data, tbl->nbr_insns + tbl->nbr_data);

    tbl->state = TABLE_STATE_LOADED;
    tbl->pc = 0;

    /* TODO: link table to source, prevent delete source if table */

    mb_reply_ok();
    return;
}
