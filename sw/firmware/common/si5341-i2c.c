#include "board.h"
#include "i2c.h"
#include "lib.h"
#include "si5341.h"

#ifdef SI5340_USE_I2C

int si5341_write_raw(unsigned i2c_addr, uint8_t reg, uint8_t val)
{
  uint8_t buffer[2];

  buffer[0] = reg;
  buffer[1] = val;

  return i2c_send(buffer, 2, i2c_addr);
}

int si5341_read_raw(unsigned i2c_addr,
		    uint16_t addr, uint8_t *data, unsigned size)
{
  uint8_t buffer[1];

  /* Read register */
  buffer[0] = (0x00FF & addr);
  if (i2c_send(buffer, 1, i2c_addr) != 0)
    return -1;
  if (i2c_recv(data, size, i2c_addr) != 0)
    return -1;
  return 0;
}

void si5341_wait_done(void)
{
#if 0
    /* Wait until bus is idle */
    while (i2c->status & I2C_STATUS_BA)
	;
#endif
}

int si5341_init_hw(void)
{
	i2c_init();
	return 0;
}
#endif
