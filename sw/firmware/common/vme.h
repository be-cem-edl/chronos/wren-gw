#ifndef __VME__H__
#define __VME__H__

void board_vme_pll_init(void);
void board_vme_init(void);

void do_vme(char *args);

#endif /* __VME__H__ */
