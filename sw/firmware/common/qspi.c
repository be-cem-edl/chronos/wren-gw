#include <stdint.h>
#include "qspi.h"
#include "lib.h"

struct qspi_regs {
    uint32_t config;
    uint32_t isr;
    uint32_t ier;
    uint32_t idr;

    uint32_t qspi_imask;
    uint32_t en_reg;
    uint32_t delay;
    uint32_t txd0;

    uint32_t rx_data;
    uint32_t slave_idle_count;
    uint32_t tx_thres;
    uint32_t rx_thres;

    uint32_t gpio;
    uint32_t pad34;
    uint32_t lpbk_dly_adj;
    uint32_t pad3c;

    uint32_t pad40[16];

    /* 0x80 */
    uint32_t txd1;
    uint32_t txd2;
    uint32_t txd3;
    uint32_t pad8c;

    uint32_t pad90[4];

    /* 0xa0 */
    uint32_t lqspi_cfg;
    uint32_t lqspi_sts;
    uint32_t pada8;
    uint32_t padac;

    uint32_t padb0[4];

    /* 0xc0 */
    uint32_t cmnd;
    uint32_t transfer_size;
    uint32_t dummy_cycle_en;
    uint32_t padcc;

    uint32_t padd0[8];

    uint32_t padf0;
    uint32_t padf4;
    uint32_t padf8;
    uint32_t mod_id;

    /* 0x100 */
    uint32_t gqspi_cfg;
    uint32_t gqspi_isr;
    uint32_t gqspi_ier;
    uint32_t gqspi_idr;

    uint32_t gqspi_imask;
    uint32_t gqspi_en;
    uint32_t pad118;
    uint32_t gqspi_txd;
    
    uint32_t gqspi_rxd;
    uint32_t pad124;
    uint32_t gqspi_tx_thresh;
    uint32_t gqspi_rx_thresh;

    /* 0x130 */
    uint32_t gqspi_gpio;
    uint32_t pad134;
    uint32_t gqspi_lpbk_dly_adj;
    uint32_t pad13c;

    uint32_t gqspi_gen_fifo;
    uint32_t gqspi_sel;
    uint32_t pad148;
    uint32_t gqspi_fifo_ctrl;
};

extern int chksz[sizeof(struct qspi_regs) == 0x150 ? 1 : - 1];

#define QSPI 0xff0f0000

static volatile struct qspi_regs *const qspi =
    (volatile struct qspi_regs *)QSPI;

# define GQSPI_ISR_RX_FIFO_EMPTY (1 << 11)
# define GQSPI_ISR_TX_FIFO_EMPTY (1 << 8)
# define GQSPI_ISR_GEN_FIFO_EMPTY (1 << 7)
# define GQSPI_ISR_RX_FIFO_NOT_EMPTY (1 << 4)
# define GQSPI_ISR_TX_FIFO_FULL (1 << 3)

# define GQSPI_GEN_FIFO_TFR (1 << 8)
# define GQSPI_GEN_FIFO_EXP (1 << 9)
# define GQSPI_GEN_FIFO_SPI  (0x1 << 10)
# define GQSPI_GEN_FIFO_DSPI (0x2 << 10)
# define GQSPI_GEN_FIFO_QSPI (0x3 << 10)
# define GQSPI_GEN_FIFO_CS  12
# define GQSPI_GEN_FIFO_CS_L (0x1 << GQSPI_GEN_FIFO_CS)
# define GQSPI_GEN_FIFO_CS_U (0x2 << GQSPI_GEN_FIFO_CS)
# define GQSPI_GEN_FIFO_CS_B (0x3 << GQSPI_GEN_FIFO_CS)
# define GQSPI_GEN_FIFO_BUS 14
# define GQSPI_GEN_FIFO_BUS_L (0x1 << GQSPI_GEN_FIFO_BUS)
# define GQSPI_GEN_FIFO_BUS_U (0x2 << GQSPI_GEN_FIFO_BUS)
# define GQSPI_GEN_FIFO_BUS_B (0x3 << GQSPI_GEN_FIFO_BUS)
# define GQSPI_GEN_FIFO_TX (1 << 16)
# define GQSPI_GEN_FIFO_RX (1 << 17)
# define GQSPI_GEN_FIFO_STRIPE (1 << 18)
# define GQSPI_GEN_FIFO_POLL (1 << 19)

# define GQSPI_FIFO_CTRL_RST_GEN_FIFO (1 << 0)
# define GQSPI_FIFO_CTRL_RST_TX_FIFO (1 << 1)
# define GQSPI_FIFO_CTRL_RST_RX_FIFO (1 << 2)

/* Large (32-bit address) SPI flash commands */
#define FLASH_EN4B 0xB7 /* Enable */
#define FLASH_EX4B 0xE9 /* Exit */

#define FLASH_RDID 0x9F

void
qspi_init(void)
{
    qspi->gqspi_sel = 1; /* Select QSPI */

    // unsigned cfg = qspi->gqspi_cfg;
    // uart_printf("GQSPI CFG: %08x\n", cfg);

    /* IO mode, manual start, rate=/4 */
    qspi->gqspi_cfg = 0x20000010;
    unsigned status = qspi->isr;
    // uart_printf("ISR=%08x\n", status);

    while (status & GQSPI_ISR_RX_FIFO_NOT_EMPTY) {
	qspi->gqspi_rxd;
	status = qspi->isr;
    }

    /* Reset FIFOs */
    qspi->gqspi_fifo_ctrl = (GQSPI_FIFO_CTRL_RST_TX_FIFO
			     | GQSPI_FIFO_CTRL_RST_RX_FIFO
			     | GQSPI_FIFO_CTRL_RST_GEN_FIFO);

    // uart_printf("FIFO CTRL: %08x\n", (unsigned)qspi->gqspi_fifo_ctrl);

    qspi->gqspi_en = 1; /* Enable device */
}

static void sys_write_cmd(uint32_t data)
{
    qspi->gqspi_gen_fifo = data;
}

static uint32_t zynqus_qspi_read_fifo_word(void)
{
    while (1) {
	unsigned status = qspi->gqspi_isr;
	//printf("GQSPI ISR=%08x\n", status);
	if (status & GQSPI_ISR_RX_FIFO_NOT_EMPTY) {
	    uint32_t d = qspi->gqspi_rxd;
	    // uart_printf("GQSPI data: %08x\n", (unsigned)d);
	    return d;
	}
    }
}

/* Read LEN bytes from qspi rx fifo */
static int zynqus_qspi_read_fifo(uint8_t *ptr, unsigned len)
{
    while (len > 0) {
	uint32_t d = zynqus_qspi_read_fifo_word();
	for (unsigned i = 4; i > 0 && len > 0; i--, len--) {
	    *ptr++ = d & 0xff;
	    d >>= 8;
	}
    }

    unsigned status = qspi->gqspi_isr;
    if ((status & GQSPI_ISR_RX_FIFO_EMPTY)
	&& (status & GQSPI_ISR_TX_FIFO_EMPTY)
	&& (status & GQSPI_ISR_GEN_FIFO_EMPTY))
	return 0;
    uart_printf("zynqus_qspi: fifos not empty\n");
    return -1;
}

static void zynqus_qspi_set_cs(unsigned cs, int en)
{
    sys_write_cmd((cs << GQSPI_GEN_FIFO_BUS)
		  | (en ? (cs << GQSPI_GEN_FIFO_CS) : 0)
		  | GQSPI_GEN_FIFO_SPI
		  | 0x04); /* 0x4 is CS setup time */
}

static void zynqus_qspi_write8(unsigned cs, unsigned val)
{
    sys_write_cmd(GQSPI_GEN_FIFO_TX
		  | (cs << GQSPI_GEN_FIFO_BUS)
		  | (cs << GQSPI_GEN_FIFO_CS)
		  | GQSPI_GEN_FIFO_SPI
		  | val);
}


/* flags is GQSPI_GEN_FIFO_TX or GQSPI_GEN_FIFO_RX
   ored with GQSPI_GEN_FIFO_SPI or GQSPI_GEN_FIFO_QSPI */
static void zynqus_qspi_set_len(unsigned cs,
				unsigned len, uint32_t flags)
{
    if (len > 255) {
	for (unsigned i = 30; i >= 8; i--) {
	    if (!(len & (1 << i)))
		continue;
	    sys_write_cmd(flags
			  | (cs << GQSPI_GEN_FIFO_BUS)
			  | (cs << GQSPI_GEN_FIFO_CS)
			  | GQSPI_GEN_FIFO_EXP
			  | GQSPI_GEN_FIFO_TFR
			  | i);
	}
    }
    if (len & 0xff)
	sys_write_cmd(flags
		      | (cs << GQSPI_GEN_FIFO_BUS)
		      | (cs << GQSPI_GEN_FIFO_CS)
		      | GQSPI_GEN_FIFO_TFR
		      | (len & 0xff));
}

static void zynqus_qspi_set_read_len(unsigned cs, unsigned len)
{
    zynqus_qspi_set_len(cs, len, GQSPI_GEN_FIFO_RX | GQSPI_GEN_FIFO_SPI);
}

static void zynqus_qspi_exec(void)
{
    qspi->gqspi_cfg = 0x3000010;
}

static void zynqus_qspi_dummy(unsigned cs, unsigned len)
{
    sys_write_cmd((cs << GQSPI_GEN_FIFO_BUS)
		  | (cs << GQSPI_GEN_FIFO_CS)
		  | GQSPI_GEN_FIFO_SPI
		  | GQSPI_GEN_FIFO_TFR
		  | len);
}

static void zynqus_qspi_addr_sz(unsigned cs,
				uint32_t addr,
				unsigned addr_4b)
{
    if (addr_4b)
	zynqus_qspi_write8(cs, (addr >> 24) & 0xff);

    zynqus_qspi_write8(cs, (addr >> 16) & 0xff);
    zynqus_qspi_write8(cs, (addr >> 8) & 0xff);
    zynqus_qspi_write8(cs, addr & 0xff);
}

static void zynqus_qspi_address_mode(unsigned cs, int cmd)
{
    zynqus_qspi_set_cs(cs, 1);
    zynqus_qspi_write8(cs, cmd);
    zynqus_qspi_set_cs(cs, 0);
    zynqus_qspi_exec();
}

static int zynqus_qspi_read_otp(unsigned cs,
				unsigned char *dest, unsigned len)
{
    zynqus_qspi_address_mode(cs, FLASH_EX4B);

    zynqus_qspi_set_cs(cs, 1);
    zynqus_qspi_write8(cs, 0x4b);
    zynqus_qspi_addr_sz(cs, 0, 0);
    zynqus_qspi_dummy(cs, 8);
    zynqus_qspi_set_read_len(cs, len);
    zynqus_qspi_set_cs(cs, 0);
    
    zynqus_qspi_exec();
    
    return zynqus_qspi_read_fifo(dest, len);
}

static int zynqus_qspi_read_reg(unsigned cs,
				unsigned cmd, unsigned char *dest, unsigned len)
{
	zynqus_qspi_set_cs(cs, 1);
	zynqus_qspi_write8(cs, cmd);
	zynqus_qspi_set_read_len(cs, len);
	zynqus_qspi_set_cs(cs, 0);

	zynqus_qspi_exec();

	return zynqus_qspi_read_fifo(dest, len);
}

void
qspi_dump(void)
{
    unsigned char data[64];
    unsigned i;

    if (0)
	zynqus_qspi_read_reg(1, FLASH_RDID, data, 20);

    zynqus_qspi_read_otp(1, data, sizeof(data));

    for (i = 0; i < sizeof(data); i += 16) {
	unsigned j;
	if (i % 16 == 0)
	    uart_printf("OTP[%02u]:", i);
	for (j = i; j < sizeof(data) && j < i + 16; j++)
	    uart_printf (" %02x", data[j]);
	for (; j < i + 16; j++)
	    uart_printf ("   ");
	uart_printf ("  ");

	for (j = i; j < sizeof(data) && j < i + 16; j++) {
	    unsigned char c = data[j];
	    uart_printf ("%c", c >= ' ' && c < 0x7f ? c : '.');
	}
	uart_printf("\n");
    }
}
