#include <stdint.h>
#include "wren/wren-common.h"

/* TX part */

#define MAX_TX_SOURCES 8

struct wren_tx_source {
  struct wren_protocol proto;
  uint16_t seq_id;
  uint32_t max_delay_us_p2;
};

extern struct wren_tx_source wren_tx_sources[MAX_TX_SOURCES];
