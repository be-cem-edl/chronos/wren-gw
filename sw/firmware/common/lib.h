#ifndef _LIB_H_
#define _LIB_H_

#include <stdint.h>
#include <stdarg.h>

static inline uint32_t read32(unsigned addr)
{
  return *(volatile uint32_t *)addr;
}

static inline uint16_t read16(unsigned addr)
{
  return *(volatile uint16_t *)addr;
}

static inline uint8_t read8(unsigned addr)
{
  return *(volatile uint8_t *)addr;
}

static inline void write32(unsigned addr, uint32_t val)
{
  *(volatile uint32_t *)addr = val;
}


void uart_printf (const char *fmt, ...) __attribute__((format(printf, 1, 2)));
void uart_vprintf (const char *fmt, va_list args);

void uart_putc(unsigned c);
void uart_puts(const char *s);
unsigned uart_getc(void);
unsigned uart_getc_next(void);

/* Copy to DEST the at most the MAXLEN last characters send to the console.
   Return the actual number of characters copied.
   If CLEAR is set, the buffer is emptied */
unsigned uart_get_log (unsigned maxlen, unsigned clear, char *dest);

/* Return 1 if a byte can be read from uart.  */
unsigned uart_can_read(void);

/* Read *available* byte from uart. */
unsigned uart_raw_getc(void);

void uart_history_add (const char *line);

int uart_readline(const char *prompt,
		  unsigned (*getchar)(void),
		  char *buf,
		  unsigned maxlen);

struct menu_item
{
  const char *name;
  void (*cmd)(char *args);
};

/* Extract the next argument.  Return -1 in case of error.
   Update LINE and set it to NULL when the end of the string is reached */
int arg_next_number (char **line, unsigned *val);
int arg_next_string(char **line, char **s);

/* Return 0 if OK, < 0 in case of error */
int strtou (char *l, unsigned *val);

void menu_execute (char *line, const struct menu_item *menu);

void dump_w32(void *addr, unsigned len);

#define barrier asm volatile("": : :"memory")

/* Limited to < 2 min */
void usleep(unsigned us);
unsigned get_cpu_cycles(void);

#define ASSERT(X)

#endif
