#include <stddef.h>

/* Data Cache Clean by MVA to poC */
static inline void
r5_dccmvac(unsigned addr)
{
    addr &= ~0x1f;
    /*                P   op1, rt, CRn, CRm, op2 */
    asm volatile("mcr 15, 0,   %0, c7,  c10, 1" : : "r" (addr) : "memory");
}

/* Data Cache Clean and invalidate by MVA to poC */
static inline void
r5_dccimvac(unsigned addr)
{
    addr &= ~0x1f;
    /*                P   op1, rt, CRn, CRm, op2 */
    asm volatile("mcr 15, 0,   %0, c7,  c14, 1" : : "r" (addr) : "memory");
}

/* Instruction Cache invalidate by MVA to PoU */
static inline void
r5_icimvau(unsigned addr)
{
    addr &= ~0x1f;
    /*                P   op1, rt, CRn, CRm, op2 */
    asm volatile("mcr 15, 0,   %0, c7,  c5, 1" : : "r" (addr) : "memory");
}

#define cpu_dsb() asm volatile ("dsb")
#define cpu_isb() asm volatile ("isb")

static inline uint32_t
r5_read_actlr(void)
{
    unsigned val;

    /*                P   op1, rt, CRn, CRm, op2 */
    asm volatile("mrc 15, 0,   %0, c1,  c0, 1" : "=r" (val));

    return val;
}

static inline void
r5_write_actlr(uint32_t val)
{
    /*                P   op1, rt, CRn, CRm, op2 */
    asm volatile("mcr 15, 0,   %0, c1,  c0, 1" : "=r" (val));
}
