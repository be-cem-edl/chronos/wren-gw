#include <stddef.h>
#include <string.h>
#include "lib.h"
#include "wren-hw.h"
#include "dco.h"
#include "board_map.h"

static volatile struct board_map * const regs =
    (volatile struct board_map *)BOARD_MAP_BASE;

void
dco_init(void)
{
    /* Setup si5340 DCO (0 is helper, 1 is main) */
    regs->si5340.pll[0].dac = 0x8000;
    regs->si5340.pll[1].dac = 0x5800;
    regs->si5340.ctrl = SI5340_CTRL_MAP_CTRL_DAC_UPDATE;

#ifndef USE_HELPER
    /* Helper pll: 25Mhz +/- 100 ppm.
       The initial bias = N2_NUM
       and we choose a ppm of 1/(2**13) */
    const uint64_t num0 = 0x03600000000; /* 44b */
    const uint32_t ppm0 = num0 >> 13;
    regs->si5340.pll[0].bias_h = num0 >> 32;
    regs->si5340.pll[0].bias_l = (num0 & 0xffffffffUL) + ppm0;
    regs->si5340.pll[0].mul = ppm0 * 2;
    regs->si5340.pll[0].force = 0x8000;

    /* Main pll: 10Mhz +/- 10 ppm
       The initial bias = N0_NUM
       and we choose a ppm of 1/(2**17) */
    const uint64_t num1 = 0x00d80000000; /* 44b, 11 digits */
    const uint32_t ppm1 = num1 >> 17;
    regs->si5340.pll[1].bias_h = num1 >> 32;
    regs->si5340.pll[1].bias_l = (num1 & 0xffffffffUL) + ppm1;
    regs->si5340.pll[1].mul = ppm1 * 2;
    regs->si5340.pll[1].force = 0x8000;

    /* Enable DCO, and force initial value */
    regs->si5340.ctrl =
	  SI5340_CTRL_MAP_CTRL_SPI_EN   /* SPI controlled by DCO */
	| SI5340_CTRL_MAP_CTRL_DAC_EN	/* DAC controlled by DCO */
	| SI5340_CTRL_MAP_CTRL_HELPER_SEL /* Select helper clock (unused) */
	| SI5340_CTRL_MAP_CTRL_EN0        /* Enable update */
	| SI5340_CTRL_MAP_CTRL_EN1
	| SI5340_CTRL_MAP_CTRL_FORCE0 /* Force DAC value */
	| SI5340_CTRL_MAP_CTRL_FORCE1;
#endif
}


void
do_dco(char *args)
{
    char *cmd;

    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	uart_printf("ctrl:   %02x\n", (unsigned)regs->si5340.ctrl);
	uart_printf("status: %02x\n", (unsigned)regs->si5340.status);
	uart_printf("pll 0 (helper):\n");
	uart_printf(" dac:   0x%04x\n", (unsigned)regs->si5340.pll[0].dac);
	uart_printf(" input: 0x%04x\n", (unsigned)regs->si5340.pll[0].inp);
	uart_printf(" value: 0x%04x%08x\n",
		    (unsigned)regs->si5340.pll[0].value_h,
		    (unsigned)regs->si5340.pll[0].value_l);
	uart_printf("pll 1 (main):\n");
	uart_printf(" dac:   0x%04x\n", (unsigned)regs->si5340.pll[1].dac);
	uart_printf(" input: 0x%04x\n", (unsigned)regs->si5340.pll[1].inp);
	uart_printf(" value: 0x%04x%08x\n",
		    (unsigned)regs->si5340.pll[1].value_h,
		    (unsigned)regs->si5340.pll[1].value_l);
	return;
    }

    if (!strcmp(cmd, "off"))
	regs->si5340.ctrl &= ~(SI5340_CTRL_MAP_CTRL_SPI_EN
			       | SI5340_CTRL_MAP_CTRL_DAC_EN);
    else if (!strcmp(cmd, "on"))
	regs->si5340.ctrl |= (SI5340_CTRL_MAP_CTRL_SPI_EN
			      | SI5340_CTRL_MAP_CTRL_DAC_EN);
    else if (!strcmp(cmd, "force0")) {
	unsigned val;
	if (arg_next_number(&args, &val) != 0)
	    uart_printf("missing number\n");
	else {
	    regs->si5340.pll[0].force = val;
	    regs->si5340.ctrl |= SI5340_CTRL_MAP_CTRL_FORCE0;
	}
    }
    else if (!strcmp(cmd, "dac")) {
	unsigned val;
	if (arg_next_number(&args, &val) != 0)
	    uart_printf("missing number\n");
	else {
	    regs->si5340.pll[1].dac = val;
	    regs->si5340.ctrl |= SI5340_CTRL_MAP_CTRL_DAC_UPDATE;
	}
    }
    else if (!strcmp(cmd, "hsel")) {
	regs->si5340.ctrl ^= SI5340_CTRL_MAP_CTRL_HELPER_SEL;
    }
    else
	uart_printf("usage: dco [on|off|hsel|force0 VAL|dac VAL]\n");
}
