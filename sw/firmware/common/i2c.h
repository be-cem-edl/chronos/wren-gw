#include <stdint.h>

void i2c_init(void);
int i2c_send(uint8_t *buf, unsigned len, unsigned i2c_addr);
int i2c_recv(uint8_t *buf, unsigned len, unsigned i2c_addr);
