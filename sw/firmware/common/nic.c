#include <stddef.h>
#include "lib.h"
#include "wren/wren-packet.h"
#include "board_map.h"
#include "wr_nic.h"
#include "rx_desc.h"
#include "tx_desc.h"
#include "wren-hw.h"
#include "nic.h"
#include "wrenrx-core.h"
#include "evlog.h"
#include "trace.h"

unsigned nic_rx_promisc;

static volatile struct nic * const nic = (volatile struct nic *)
    (BOARD_MAP_BASE + BOARD_MAP_WRNIC);
static volatile struct rx_desc * const nic_rx_desc =
  (volatile struct rx_desc *)nic->drx;
static volatile struct tx_desc * const nic_tx_desc =
  (volatile struct tx_desc *)nic->dtx;
static unsigned char * const nic_mem =
  (unsigned char *)nic->mem;

/* Next descriptor to use (if free) */
static unsigned nic_rxd;
static unsigned nic_txd;

#define NBR_TX_DESC 8
#define NBR_RX_DESC 8

/* Number of free descriptors for tx.  */
static unsigned nic_txnum;

/* Last busy descriptor that was not recycled. */
static unsigned nic_txbusy;

void
nic_init(void)
{
    unsigned i;

    /* Initialize descriptors.  */
    nic->CR = NIC_CR_SW_RST;

    /* 2KB per descriptor (so 16KB in total). */
    for (i = 0; i < NBR_RX_DESC; i++) {
	nic_rx_desc[i].buf = ((1536 + 8) << RX_DESC_BUF_LEN_SHIFT)
	    | (i << 11);
	nic_rx_desc[i].flags = RX_DESC_FLAGS_EMPTY;
    }

    for (i = 0; i < NBR_TX_DESC; i++) {
	nic_tx_desc[i].flags = 0;
	nic_tx_desc[i].dest = 1;
    }

    nic_rxd = 0;
    nic_txd = 0;
    nic_txnum = NBR_TX_DESC;
    nic_txbusy = 0;

    /* Enable interrupts (so that the status is updated). */
    nic->eic.eic_ier = NIC_EIC_EIC_IER_TCOMP
	| NIC_EIC_EIC_IER_TXERR
    	| NIC_EIC_EIC_IER_RCOMP;

    /* Enable RX and TX  */
    nic->CR = NIC_CR_RX_EN | NIC_CR_TX_EN;
}

uint32_t *
nic_get_tx_buf(unsigned len)
{
    unsigned off = (1 << 14) + (nic_txd << 11);

    /* Check descriptor is free.  */
    if (nic_txnum == 0)
	return NULL;

    nic_txnum--;
    
    nic_tx_desc[nic_txd].buf = (len << TX_DESC_BUF_LEN_SHIFT)
	| (off << TX_DESC_BUF_OFFSET_SHIFT);

    return (uint32_t *)(nic_mem + off);
}

void
nic_send_tx_buf(void)
{
    nic_tx_desc[nic_txd].flags = TX_DESC_FLAGS_READY
	| TX_DESC_FLAGS_PAD_E;
    nic_txd = (nic_txd + 1) % NBR_TX_DESC;
}

int
nic_send(const void *buf, unsigned len)
{
    void *b;
    unsigned i;

    b = nic_get_tx_buf(len);
    if (b == NULL)
	return -1;
	    
    uart_printf("nic_send @%08x+%u to 0x%08x\n",
		(unsigned)buf, len, (unsigned)b);

    /* Word copy */
    for (i = 0; i >= 4 && i < len; i += 4)
	*(unsigned *)(b + i) = *(const unsigned *)(buf + i);

    /* Byte copy */
    for (; i < len; i++)
	*(unsigned char *)(b + i) = *(const unsigned char *)(buf + i);

    uart_printf("write txd %u\n", nic_txd);
    /* Write len.  */
    nic_send_tx_buf();

    return 0;
}

static void
disp_packet(unsigned char *b, unsigned l)
{
    for (unsigned j = 0; j < 72 && j < l + 2; j += 1) {
	switch (j) {
	case 0:
	    break;
	case 2:  // eth daddr
	case 2 + 6: // eth saddr
	case 2 + 6 + 6:  // eth type
	case 2 + 6 + 6 + 2: // eth payload (ip hdr + frag)
	case 2 + 14 + 12: // ip daddr
	case 2 + 14 + 16: // ip saddr
	case 2 + 14 + 20: // ip payload
	    uart_putc('.');
	    break;
	case 2 + 14 + 8:  // ip protocol
	case 48:
	    uart_puts("\n ");
	    break;
	default:
	    uart_putc(' ');
	    break;
	}
	uart_printf ("%02x", *(unsigned char *)(b + j));
    }
    uart_putc('\n');
}

static void
nic_rx_packet(unsigned char *b, unsigned l)
{
    int src;
    struct wren_pkt_buf buf;

    buf.buf = b;
    buf.len = l;
    buf.frame_off = 2;

    if (nic_rx_promisc)
	wrenrx_handle_promisc(&buf);

    src = wrenrx_filter_packet(&buf);
    if (src < 0) {
	if (log_flags & LOG_DISCARD) {
	    uart_printf("discard packet\n");
	    disp_packet(b, l);
	}
	return;
    }

    /* Log packet header */
    /* TODO: vlan */
    evlog_hdr(EVLOG_TYPE_RECV,
	      EVLOG_TS_SIZE + 14 + sizeof(struct wren_packet_hdr_v1) + 1);
    evlog_push_ts ();
    evlog_push_u8 (src);
    evlog_push_bytes (b + 2, 14 + sizeof(struct wren_packet_hdr_v1));

    wrenrx_handle_packet (&buf, src);
}

void
nic_rx_handler(void)
{
    unsigned sr;
    unsigned cur_rx;

    /* Handle all packets.  */
    while (1) {
	sr = nic->SR;

	/* Check status bit */
	if (!(sr & (NIC_SR_BNA | NIC_SR_REC)))
	    break;

	/* Clear REC bits.  */
	nic->SR = sr & NIC_SR_REC;

	cur_rx = (sr & NIC_SR_CUR_RX_DESC_MASK) >> NIC_SR_CUR_RX_DESC_SHIFT;

	while (1) {
	    /* Disp packet.  */
	    unsigned rxd_buf = nic_rx_desc[nic_rxd].buf;
	    unsigned char *b = (nic_mem + (rxd_buf & RX_DESC_BUF_OFFSET_MASK));
	    unsigned l =
		(rxd_buf & RX_DESC_BUF_LEN_MASK) >> RX_DESC_BUF_LEN_SHIFT;

	    if (log_flags & LOG_RX) {
		uart_printf ("SR: %08x, rx-desc: %u ", sr, nic_rxd);
		uart_printf("flags: %08x, buf: %08x (adr=%08x, l=%u)\n ",
			    (unsigned)nic_rx_desc[nic_rxd].flags, rxd_buf,
			    (unsigned)b, l);
	    }

	    nic_rx_packet(b, l);

	    /* Reset descriptor.  */
	    nic_rx_desc[nic_rxd].buf = ((1536 + 8) << RX_DESC_BUF_LEN_SHIFT)
		| (nic_rxd << 11);
	    nic_rx_desc[nic_rxd].flags = RX_DESC_FLAGS_EMPTY;

	    nic_rxd = (nic_rxd + 1) & 7;

	    if (nic_rxd == cur_rx)
	      break;
	}
    }
}

void
nic_tx_handler (void)
{
    while (1) {
	unsigned cur_tx;
	unsigned sr;

	/* Check status bits */
	sr = nic->SR;

	if (0)
	    uart_printf("nic_tx_handler: sr=0x%08x, txnum=%u\n",
			sr, nic_txnum);

	/* Clear status bits.  */
	nic->SR = sr & (NIC_SR_TX_DONE | NIC_SR_TX_ERROR);

	/* Exit if no buffer to free */
	if (nic_txnum == NBR_TX_DESC)
	    break;

	cur_tx = (sr & NIC_SR_CUR_TX_DESC_MASK) >> NIC_SR_CUR_TX_DESC_SHIFT;

	while (nic_txbusy != cur_tx) {
	    if (0) {
		uart_printf ("SR: %08x, desc: %u@0x%08x, ",
			     (unsigned)sr,
			     nic_txbusy, (unsigned)&nic_tx_desc[nic_txbusy]);
		uart_printf("flags: %08x, buf: %08x\n",
			    (unsigned)nic_tx_desc[nic_txbusy].flags,
			    (unsigned)nic_tx_desc[nic_txbusy].buf);
	    }

	    nic_txbusy = (nic_txbusy + 1) & 7;
	    nic_txnum++;
	}
    }
}

void
do_nic (char *args)
{
    uart_printf ("NIC %08x SR: %08x, CR: %08x\n",
		 (unsigned)nic, (unsigned)nic->SR, (unsigned)nic->CR);
    uart_printf ("tx_desc: %u, rx_desc: %u\n", nic_txd, nic_rxd);
}

