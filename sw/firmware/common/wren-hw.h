#include "pci_map.h"

#define PL_MAP_BASE 0x80000000
#define BTCM_MAP_BASE 0xf0000000
#define ATCM_MAP_BASE 0xf0020000 /* 128KB apart to handle combined TCM */
#define BOARD_MAP_BASE (PL_MAP_BASE + PCI_MAP_BOARD)
#define HOST_MAP_BASE  (PL_MAP_BASE + PCI_MAP_HOST)

uint32_t *mb_get_reply(unsigned cmd, unsigned len);
void mb_send_reply(void);
// void mb_reply_word(unsigned cmd, unsigned data);

/* User error */
void mb_reply_error(int err);
/* Internal error */
void mb_reply_failure(unsigned err);
/* No error */
void mb_reply_ok(void);

