#include "lib.h"
#include "i2c.h"
#include "board.h"
#include "si5341.h"
#include "vme_regs_map.h"

static volatile struct vme_regs_map * const vme_regs =
  (volatile struct vme_regs_map *) 0x90000000;

void board_vme_pll_init(void)
{
    /* Deassert PLL rstn */
    pll_reset();

    si5340_init();
}

void board_vme_init(void)
{
    dco_init();
    i2c_init();
}

void do_vme(char *args)
{
    /* Be sure to be master */
    vme_regs->sysc = VME_REGS_MAP_SYSC_SYSC;

    /* Use br/bg 3 */
    vme_regs->brl = 3;

    /* Map 16MB of am 0x39 (A24) */
    vme_regs->xlat_lp[1].vector =
	(0x39 << VME_REGS_MAP_XLAT_LP_VECTOR_AM_SHIFT)
	| (3 << VME_REGS_MAP_XLAT_LP_VECTOR_WIDTH_SHIFT);
}

