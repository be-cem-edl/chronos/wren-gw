#include <stddef.h>
#include <string.h>
#include "lib.h"
#include "wren-hw.h"
#include "board_map.h"
#include "si5341.h"
#include "si5340_ctrl_map.h"
#include "si5340-load.h"
#include "board.h"

#include MAIN_SI5340_FILE

#ifdef HELPER_SI5340_ADDR
#include HELPER_SI5340_FILE
#endif

int si5340_init(void)
{
    unsigned val;
    si5341_init_hw();

    // Wait for ready
    while (si5341_read_reg(MAIN_SI5340_ADDR, 0xfe) != 0x0f)
	uart_printf("main Si5340 not ready\n");
#ifdef HELPER_SI5340_ADDR
    while (si5341_read_reg(HELPER_SI5340_ADDR, 0xfe) != 0x0f)
	uart_printf("helper Si5340 not ready\n");
#endif

    if (!si534x_is_locked(MAIN_SI5340_ADDR)) {
	uart_printf("Programming Si5340 main PLL\n");
	si5341_program(si5340_main_regs, SI5340_MAIN_NUM_REGS,
		       MAIN_SI5340_ADDR);
    }
#ifdef HELPER_SI5340_ADDR
    if (!si534x_is_locked(HELPER_SI5340_ADDR)) {
	uart_printf("Programming Si5340 helper PLL\n");
	si5341_program(si5340_helper_regs, SI5340_HELPER_NUM_REGS,
		       HELPER_SI5340_ADDR);
    }
#endif

    while ((val = si5341_read_reg(MAIN_SI5340_ADDR, 0xfe)) != 0x0f) {
	uart_printf("main Si5340 not ready, [fe]=%02x\n", val);
	usleep(200000); /* 10ms */
    }

    //disp_si534x(MAIN_SI5340_ADDR, 0);
    //dump_regs(MAIN_SI5340_ADDR, 0, 16);

    while (1) {
	int main_lk = si534x_is_locked(MAIN_SI5340_ADDR);
#ifdef HELPER_SI5340_ADDR
	int helper_lk = si534x_is_locked(HELPER_SI5340_ADDR);
#else
	int helper_lk = 1;
#endif
	if (main_lk && helper_lk)
	    break;

	uart_printf ("PLL: waiting for");
	if (!main_lk)
	    uart_printf (" main");
	if (!helper_lk)
	    uart_printf (" helper");
       uart_printf (" to lock\n");
       usleep(200000); /* 10ms */
    }

    return 0;
}

void
do_main_si5340(char *args)
{
    do_si534x (args, MAIN_SI5340_ADDR, 0, MAIN_SI5340_FREQ);
}

#ifdef HELPER_SI5340_ADDR
void
do_helper_si5340(char *args)
{
    do_si534x (args, HELPER_SI5340_ADDR, 0, HELPER_SI5340_FREQ);
}
#endif

/* WR_CLK_RSTN */
#define MASK_DATA_2_LSW 0x00FF0A0010
#define GPIO_DIRM_2     0x00FF0A0284
#define GPIO_OEN_2      0x00FF0A0288
#define MIO_MST_TRI1    0x00ff180208

void pll_reset(void)
{
    /* Deassert PLL rstn */
    unsigned v;
    
    /* output */
    v = read32(GPIO_DIRM_2);
    write32(GPIO_DIRM_2, v | (1<<0));

    /* enable */
    v = read32(GPIO_OEN_2);
    write32(GPIO_OEN_2, v | (1<<0));

    /* Disable master tri state */
    v = read32(MIO_MST_TRI1);
    v &= ~(1 << (52 - 32));
    write32(MIO_MST_TRI1, v);

    /* Assert */
    write32(MASK_DATA_2_LSW, (0xfffe << 16) | 0x0000);

    usleep(5000);

    /* Deassert */
    write32(MASK_DATA_2_LSW, (0xfffe << 16) | 0x0001);
}
