#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "lib.h"
#include "wrenrx-data.h"
#include "wren-hw.h"
#include "wren.h"
#include "wren-mb-defs.h"
#include "wren/wren-packet.h"
#include "board_map.h"
#include "mb_map.h"
#include "pulser_group_map.h"
#include "wrenrx-core.h"
#include "wren/wren-hw.h"
#include "trace.h"
#include "versions.h"
#include "nic.h"
#include "armr5.h"

static volatile struct board_map * const regs =
    (volatile struct board_map *)BOARD_MAP_BASE;

static volatile struct mb_map * const mb =
    (volatile struct mb_map *)BTCM_MAP_BASE;

#define NS_PER_SEC 1000000000

#define STATIC_CHECK(NAME, COND) \
    extern unsigned char NAME[(COND) ? 0 : - 1];

/* Still some hard-wired values in alloc_comparator_for_pulser */
STATIC_CHECK(check1, WREN_COMPARATORS_PER_PULSER == 8);

/* Return index in the group of a free comparator for PULSER_IDX.
   Return < 0 in case of error (no free comparator). */
static int
alloc_comparator_for_pulser(unsigned pulser_idx)
{
    volatile struct pulser_group_map *grp;
    unsigned grp_idx = pulser_idx / WREN_PULSERS_PER_GROUP;
    unsigned idx = pulser_idx % WREN_PULSERS_PER_GROUP;
    uint32_t status, status_orig;
    unsigned done;
    unsigned cmp_idx;

    grp = &regs->pulser_group[grp_idx].el;
    status = grp->pulsers[idx].comp_busy;
    status_orig = status;

    /* Comparators that could be recycled. */
    done = grp->pulsers[idx].done_comp;
    if (done) {
	/* The comparators are released only if they didn't generate a pulse */
	uint32_t tcomp;
	tcomp = grp->pulsers[idx].pulses;
	done &= ~tcomp;
	if (done) {
	    grp->pulsers[idx].comp_busy = done;
	    status &= ~done;
	    WREN_LOG(LOG_COMP,
		     "release comps %08x for pulser %u (tcomp=%08x)\n",
		     done, pulser_idx, (unsigned)tcomp);
	}
    }

    /* First comparator for the pulser in the group */
    cmp_idx = (pulser_idx % WREN_PULSERS_PER_GROUP)
      * WREN_COMPARATORS_PER_PULSER;

    /* Find a comparator */
    if ((status & 0xf) == 0x0f) {
	cmp_idx += 4;
        status >>= 4;
    }
    if ((status & 0x3) == 0x03) {
	cmp_idx += 2;
        status >>= 2;
    }
    if ((status & 1) != 0) {
        if (status == 3) {
	    WREN_LOG(LOG_ERR,
		     "all comparators busy for pulser %u\n", pulser_idx);
            return -1;
        }
        cmp_idx++;
    }

    WREN_LOG(LOG_COMP, "alloc comparator %u for pulser %u, status: %x\n",
	     cmp_idx, pulser_idx, (unsigned)status_orig);

    return cmp_idx;
}


#define ASYNC_WSIZE (sizeof(mb->async_data) / 4)
static int
async_alloc(unsigned len, unsigned *off)
{
    unsigned b_off = mb->async_board_off;
    unsigned h_off = mb->async_host_off;
    int used;
    unsigned avail;

    used = b_off - h_off;
    if (used < 0)
	used = ASYNC_WSIZE + used;
    avail = ASYNC_WSIZE - used;

    if (len > avail) {
	uart_printf("async_alloc failed (len=%u, avail=%u, b_off=%u, h_off=%u)\n",
		    len, avail, b_off, h_off);
	return -1;
    }
    // uart_printf("async_alloc: off=%u, len=%u\n", b_off, len);
    *off = b_off;
    return 0;
}

static unsigned
async_write32(unsigned off, uint32_t val)
{
    // uart_printf("async[%u] = %08x\n", off, (unsigned)val);
    mb->async_data[off] = val;
    return (off + 1) & (ASYNC_WSIZE - 1);
}

static void
async_update(unsigned off)
{
    // uart_printf("async_update: off=%u\n", off);
    mb->async_board_off = off;
    host_async_doorbell();
}

void
cmd_rx_get_config (uint32_t *data, uint32_t len)
{
  struct wren_mb_rx_get_config_reply *res;

  res = (struct wren_mb_rx_get_config_reply *)mb_get_reply
    (CMD_RX_GET_CONFIG | CMD_REPLY, sizeof(*res) / 4);
  res->sw_version = WREN_FW_VERSION;
  res->max_sources = MAX_RX_SOURCES;
  res->max_conds = MAX_RX_CONDS;
  res->max_actions = MAX_RX_ACTIONS;

  mb_send_reply();
}

/* Program and enable a comparator */
static void
setup_comparator (volatile struct comparators *cmp,
                  struct wren_mb_pulser_config *cfg)

{
    unsigned start = WREN_MB_PULSER_GET_START(cfg->inputs);
    unsigned stop = WREN_MB_PULSER_GET_STOP(cfg->inputs);
    unsigned clock = WREN_MB_PULSER_GET_CLOCK(cfg->inputs);
    unsigned repeat = cfg->flags & WREN_MB_PULSER_FLAG_REPEAT;
    unsigned immediat = cfg->flags & WREN_MB_PULSER_FLAG_IMMEDIAT;
    unsigned int_en = cfg->flags & WREN_MB_PULSER_FLAG_INT_EN;
    unsigned out_en = cfg->flags & WREN_MB_PULSER_FLAG_OUT_EN;
    cmp->high = cfg->width;
    cmp->npulses = cfg->npulses;
    cmp->period = cfg->period;
    cmp->idelay = cfg->idelay;
    cmp->conf1 =
        ((start << PULSER_GROUP_MAP_COMPARATORS_CONF1_START_SHIFT)
         & PULSER_GROUP_MAP_COMPARATORS_CONF1_START_MASK)
        | ((stop << PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_SHIFT)
           & PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_MASK)
        | ((clock << PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_SHIFT)
           & PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_MASK)
        | (repeat ? PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT : 0)
        | (immediat ? PULSER_GROUP_MAP_COMPARATORS_CONF1_IMMEDIAT : 0)
        | (out_en ? PULSER_GROUP_MAP_COMPARATORS_CONF1_OUT_EN : 0)
        | (int_en ? PULSER_GROUP_MAP_COMPARATORS_CONF1_INT_EN : 0)
	| PULSER_GROUP_MAP_COMPARATORS_CONF1_ENABLE;
}

void
cmd_rx_get_comparator (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_comparator *res;
    uint32_t comp_idx = cmd->arg1;
    unsigned grp_idx = comp_idx / WREN_COMPARATORS_PER_GROUP;
    unsigned sub_idx = comp_idx & (WREN_COMPARATORS_PER_GROUP - 1);
    volatile struct comparators *cmp;
    volatile struct pulser_group_map *grp;
    unsigned val;
    unsigned inp;

    /* Check index. */
    if (comp_idx >= WREN_NBR_COMPARATORS) {
        mb_reply_error(WRENRX_ERR_COMPID_TOO_LARGE);
        return;
    }
    grp = &regs->pulser_group[grp_idx].el;

    cmp = &grp->comparators[sub_idx];

    res = (struct wren_mb_comparator *)mb_get_reply
        (CMD_REPLY, sizeof(*res) / 4);

    res->conf.pulser_idx = comp_idx / WREN_COMPARATORS_PER_PULSER;

    res->ts.sec = cmp->time_sec;
    res->ts.nsec = cmp->time_nsec;

    val = cmp->conf1;
    res->conf.inputs = 0;

    inp = (val & PULSER_GROUP_MAP_COMPARATORS_CONF1_START_MASK)
	>> PULSER_GROUP_MAP_COMPARATORS_CONF1_START_SHIFT;
    res->conf.inputs |= (inp << WREN_MB_PULSER_INPUT_START_SH);

    inp = (val & PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_MASK)
	>> PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_SHIFT;
    res->conf.inputs |= (inp << WREN_MB_PULSER_INPUT_STOP_SH);

    inp = (val & PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_MASK)
	>> PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_SHIFT;
    res->conf.inputs |= (inp << WREN_MB_PULSER_INPUT_CLOCK_SH);

    res->conf.flags =
	  ((val & PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT) ?
	   WREN_MB_PULSER_FLAG_REPEAT : 0)
	| ((val & PULSER_GROUP_MAP_COMPARATORS_CONF1_IMMEDIAT) ?
	   WREN_MB_PULSER_FLAG_IMMEDIAT : 0)
	| ((val & PULSER_GROUP_MAP_COMPARATORS_CONF1_OUT_EN) ?
	   WREN_MB_PULSER_FLAG_OUT_EN : 0)
	| ((val & PULSER_GROUP_MAP_COMPARATORS_CONF1_INT_EN) ?
	   WREN_MB_PULSER_FLAG_INT_EN : 0)
	| ((val & PULSER_GROUP_MAP_COMPARATORS_CONF1_ENABLE) ?
	   WREN_MB_PULSER_FLAG_ENABLE : 0);

    res->conf.width = cmp->high;
    res->conf.period = cmp->period;
    res->conf.npulses = cmp->npulses;
    res->conf.idelay = cmp->idelay;

    mb_send_reply();
}

void
cmd_rx_set_comparator (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_comparator *arg =
        (struct wren_mb_rx_set_comparator *)data;
    uint32_t idx = arg->comp_idx;
    unsigned busy;
    volatile struct comparators *cmp;
    volatile struct pulser_group_map *grp;

    /* Check index. */
    if (idx >= WREN_NBR_COMPARATORS) {
        mb_reply_error(WRENRX_ERR_COMPID_TOO_LARGE);
        return;
    }

    grp = &regs->pulser_group[idx / WREN_COMPARATORS_PER_GROUP].el;
    idx &= WREN_COMPARATORS_PER_GROUP - 1;

    /* Check busy */
    busy = grp->pulsers[idx / WREN_COMPARATORS_PER_PULSER].comp_busy;
    if (busy & (1 << (idx % WREN_COMPARATORS_PER_PULSER))) {
        mb_reply_error(WRENRX_ERR_COMPARATOR_BUSY);
        return;
    }

    cmp = &grp->comparators[idx];

    cmp->time_sec = arg->cfg.ts.sec;
    cmp->time_nsec = arg->cfg.ts.nsec;
    setup_comparator(cmp, &arg->cfg.conf);

    mb_reply_ok();
}

void
cmd_rx_abort_comparator (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd =(struct wren_mb_arg1 *)data;
    unsigned comp_idx = cmd->arg1;
    unsigned grp_idx = comp_idx / WREN_COMPARATORS_PER_GROUP;
    unsigned sub_idx = comp_idx & (WREN_COMPARATORS_PER_GROUP - 1);
    volatile struct comparators *cmp;
    volatile struct pulser_group_map *grp;

    /* Check index. */
    if (comp_idx >= WREN_NBR_COMPARATORS) {
        mb_reply_error(WRENRX_ERR_COMPID_TOO_LARGE);
        return;
    }

    grp = &regs->pulser_group[grp_idx].el;
    cmp = &grp->comparators[sub_idx];

    cmp->conf1 = (cmp->conf1 & ~PULSER_GROUP_MAP_COMPARATORS_CONF1_ENABLE)
      | PULSER_GROUP_MAP_COMPARATORS_CONF1_ABORT;

    mb_reply_ok();
}

void
cmd_rx_get_pulsers_status (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_arg_reply *rep;
    unsigned grp_idx = cmd->arg1;
    volatile struct pulser_group_map *grp;

    if (grp_idx >= WREN_NBR_GROUPS) {
        mb_reply_error(WRENRX_ERR_PULSERID_TOO_LARGE);
        return;
    }

    grp = &regs->pulser_group[grp_idx].el;

    rep = (struct wren_mb_arg_reply *)mb_get_reply
        (CMD_REPLY, sizeof(*rep) / 4);
    rep->arg1 = grp->pulsers_running;
    rep->arg2 = grp->pulsers_loaded;

    mb_send_reply();
}

void
cmd_rx_get_pulser (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_rx_get_pulser_reply *rep;
    unsigned idx = cmd->arg1;
    volatile struct pulser_group_map *grp;
    volatile struct pulsers *pulser;

    if (idx >= WREN_NBR_PULSERS) {
        mb_reply_error(WRENRX_ERR_PULSERID_TOO_LARGE);
        return;
    }
    grp = &regs->pulser_group[idx / WREN_PULSERS_PER_GROUP].el;

    idx &= WREN_PULSERS_PER_GROUP - 1;
    pulser = &grp->pulsers[idx];

    rep = (struct wren_mb_rx_get_pulser_reply *)mb_get_reply
        (CMD_RX_GET_PULSER | CMD_REPLY, sizeof(*rep) / 4);
    rep->state = pulser->state;
    rep->pulses = pulser->pulses;
    rep->ts = pulser->ts;
    rep->done_comp = pulser->done_comp;
    rep->current_comp = pulser->current_comp;
    rep->loaded_comp = pulser->loaded_comp;
    rep->late_comp = pulser->comp_late;
    rep->busy_comp = pulser->comp_busy;

    mb_send_reply();
}

void
cmd_rx_abort_pulsers (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg2 *arg =(struct wren_mb_arg2 *)data;
    uint32_t grp_idx = arg->arg1;
    volatile struct pulser_group_map *grp;

    /* Check index. */
    if (grp_idx >= WREN_NBR_GROUPS) {
        mb_reply_error(WRENRX_ERR_PULSERID_TOO_LARGE);
        return;
    }

    grp = &regs->pulser_group[grp_idx].el;

    grp->pulsers_abort = arg->arg2;

    mb_reply_ok();
}

void
cmd_rx_get_out_cfg (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_out_cfg *rep;
    unsigned out_idx = cmd->arg1;
    unsigned bit_idx;
    unsigned cfg;
    volatile struct pulser_group_map *grp;

    if (out_idx >= WREN_NBR_PINS) {
        mb_reply_error(WRENRX_ERR_OUTPUTID_TOO_LARGE);
        return;
    }

    bit_idx = out_idx & (WREN_PULSERS_PER_GROUP - 1);
    grp = &regs->pulser_group[out_idx / WREN_PULSERS_PER_GROUP].el;
    cfg = grp->out_cfg[bit_idx].config;

    rep = (struct wren_mb_out_cfg  *)mb_get_reply
        (CMD_REPLY, sizeof(*rep) / 4);
    rep->out_idx = out_idx;
    rep->mask = (cfg & PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_MASK)
        >> PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_SHIFT;
    rep->inv_in = (cfg & PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_IN) ? 1 : 0;
    rep->inv_out = (cfg & PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_OUT) ? 1 : 0;

#if 0
    rep->oe = (regs->lemo_oe >> bit_idx) & 1;
    rep->term = (regs->lemo_term >> bit_idx) & 1;
#endif

    mb_send_reply();
}

void
cmd_rx_set_out_cfg (uint32_t *data, uint32_t len)
{
    struct wren_mb_out_cfg *cmd = (struct wren_mb_out_cfg *)data;
    unsigned out_idx = cmd->out_idx;
    unsigned cfg;
    volatile struct pulser_group_map *grp;

    if (out_idx >= WREN_NBR_PINS) {
        mb_reply_error(WRENRX_ERR_OUTPUTID_TOO_LARGE);
        return;
    }

    grp = &regs->pulser_group[out_idx / WREN_PULSERS_PER_GROUP].el;

    /* Set combinatorial output configuration */
    cfg = ((cmd->mask) << PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_SHIFT)
        & PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_MASK;
    cfg |= cmd->inv_in ? PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_IN : 0;
    cfg |= cmd->inv_out ? PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_OUT : 0;

    grp->out_cfg[out_idx & (WREN_PULSERS_PER_GROUP - 1)].config = cfg;

#if 0
    /* Control OE + TERM */
    oe = regs->lemo_oe;
    term = regs->lemo_term;

    mask = 1 << out_idx;

    if (cmd->oe)
        oe |= mask;
    else
        oe &= ~mask;

    if (cmd->term)
        term |= mask;
    else
        term &= ~mask;

    regs->lemo_oe = oe;
    regs->lemo_term = term;
#endif

    mb_reply_ok();
}

void
cmd_rx_get_pin_cfg (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_pin_cfg *rep;
    unsigned pin_idx = cmd->arg1;

    if (pin_idx >= WREN_NBR_PINS) {
	mb_reply_error(WRENRX_ERR_PINID_TOO_LARGE);
	return;
    }

    rep = (struct wren_mb_pin_cfg *)mb_get_reply
	(CMD_REPLY, sizeof(*rep) / 4);
    rep->pin_idx = pin_idx;
    rep->oe = (regs->fp_oe >> pin_idx) & 1;
    rep->term = (regs->fp_term >> pin_idx) & 1;
    rep->inv = (regs->fp_inv >> pin_idx) & 1;

    mb_send_reply();
}

void
cmd_rx_set_pin_cfg (uint32_t *data, uint32_t len)
{
    struct wren_mb_pin_cfg *cmd = (struct wren_mb_pin_cfg *)data;
    unsigned pin_idx = cmd->pin_idx;
    uint32_t term, oe, inv, mask;

    if (pin_idx >= WREN_NBR_PINS) {
	mb_reply_error(WRENRX_ERR_PINID_TOO_LARGE);
	return;
    }

    /* Control OE + TERM */
    oe = regs->fp_oe;
    term = regs->fp_term;
    inv = regs->fp_inv;

    mask = 1 << pin_idx;

    if (cmd->oe)
	oe |= mask;
    else
	oe &= ~mask;

    if (cmd->term)
	term |= mask;
    else
	term &= ~mask;

    if (cmd->inv)
	inv |= mask;
    else
	inv &= ~mask;

    regs->fp_oe = oe;
    regs->fp_term = term;
    regs->fp_inv = inv;

    mb_reply_ok();
}

void
cmd_rx_set_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_source *arg = (struct wren_mb_rx_set_source *)data;
    struct wren_rx_source *src;
    unsigned idx = arg->idx;

    /* Check index. */
    if (len != sizeof(*arg)/4) {
	mb_reply_failure(CMD_ERROR_LENGTH);
	return;
    }
    if (idx >= MAX_RX_SOURCES) {
	mb_reply_error(WRENRX_ERR_SRCID_TOO_LARGE);
	return;
    }

    src = &wren_rx_sources[idx];

    /* Disable this source entry while it is modified.  */
    src->proto.proto = WREN_PROTO_NONE;
    barrier;

    src->proto.u = arg->cfg.proto.u;

    src->dest = arg->cfg.dest;

    src->subsample = arg->cfg.subsample;
    src->subsample_cnt = 0;

    src->nbr_frames = 0;
    src->next_seqid = PKT_SEQ_SYNC;
    memset(&src->subscribed_map, 0, sizeof(src->subscribed_map));

    barrier;
    src->proto.proto = arg->cfg.proto.proto;

    mb_reply_ok();
}

void
cmd_rx_get_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_rx_get_source_reply *res;
    struct wren_rx_source *src;
    unsigned idx = cmd->arg1;

    /* Check index. */
    if (idx >= MAX_RX_SOURCES) {
	mb_reply_error(WRENRX_ERR_SRCID_TOO_LARGE);
	return;
    }

    src = &wren_rx_sources[idx];

    res = (struct wren_mb_rx_get_source_reply *)mb_get_reply
	(CMD_REPLY, sizeof(*res) / 4);

    res->cfg.dest = src->dest;
    res->cfg.subsample = src->subsample;
    res->cfg.proto = src->proto;
    res->cond_idx = src->conds;

    mb_send_reply();
}

void
cmd_rx_set_cond (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_cond *arg = (struct wren_mb_rx_set_cond *)data;
    unsigned idx = arg->cond_idx;
    unsigned src_idx = arg->cond.src_idx;
    unsigned skip_op;
    struct wren_rx_cond *cond;

    /* Check index */
    if (idx >= MAX_RX_CONDS) {
	mb_reply_error(WRENRX_ERR_CONDID_TOO_LARGE);
	return;
    }
    if (src_idx >= MAX_RX_SOURCES) {
	mb_reply_error(WRENRX_ERR_SRCID_TOO_LARGE);
	return;
    }

    /* Check condition is not used */
    cond = &wren_rx_conds[idx];
    if (cond->cond.evt_id != WREN_EVENT_ID_INVALID) {
	mb_reply_error(WRENRX_ERR_COND_BUSY);
	return;
    }

    /* Fill condition */
    cond->cond.evt_id = arg->cond.evt_id;
    cond->cond.src_idx = src_idx;
    cond->act_idx = NO_RX_ACT_IDX;
    cond->cond.len = arg->cond.len;
    cond->log = 0;
    cond->nbr_act = 0;
    cond->nparam = 0;
    skip_op = 0;
    for (unsigned i = 0; i < cond->cond.len; i++) {
	union wren_rx_cond_word w = arg->cond.ops[i];
	/* Copy */
	cond->cond.ops[i] = w;
	/* And update nparam */
	if (skip_op)
	    skip_op--;
	else if (w.op.op < WREN_OP_NE) {
	    cond->nparam++;
	    skip_op++;
	}
    }

    /* Chain condition */
    cond->next = wren_rx_sources[src_idx].conds;
    wren_rx_sources[src_idx].conds = idx;

    mb_reply_ok();
}

void
cmd_rx_get_cond (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_rx_cond *res;
    struct wren_rx_cond *cond;
    unsigned idx = cmd->arg1;

    /* Check index. */
    if (idx >= MAX_RX_CONDS) {
	mb_reply_error(WRENRX_ERR_CONDID_TOO_LARGE);
	return;
    }

    cond = &wren_rx_conds[idx];

    res = (struct wren_rx_cond *)mb_get_reply(CMD_REPLY, sizeof(*res) / 4);

    *res = *cond;

    mb_send_reply();
}

void
cmd_rx_del_cond (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_rx_cond *cond;
    unsigned idx = cmd->arg1;
    unsigned src_idx;
    unsigned chain;

    /* Check index. */
    if (idx >= MAX_RX_CONDS) {
	mb_reply_error(WRENRX_ERR_CONDID_TOO_LARGE);
	return;
    }

    cond = &wren_rx_conds[idx];

    if (cond->cond.evt_id == WREN_EVENT_ID_INVALID) {
	/* Condition is free */
	mb_reply_error(WRENRX_ERR_COND_UNUSED);
    }

    if (cond->act_idx != NO_RX_ACT_IDX) {
	/* Condition is used by an action */
	mb_reply_error(WRENRX_ERR_COND_BUSY);
    }

    src_idx = cond->cond.src_idx;

    /* Chain condition */
    chain = wren_rx_sources[src_idx].conds;
    if (chain == idx)
	wren_rx_sources[src_idx].conds = cond->next;
    else {
	while (1) {
	    unsigned next = wren_rx_conds[chain].next;
	    if (next == idx) {
		wren_rx_conds[chain].next = cond->next;
		break;
	    }
	    chain = next;
	    if (chain == NO_RX_COND_IDX) {
		/* Should not happen: condition not found! */
		mb_reply_failure(CMD_ERROR_CORRUPT);
		return;
	    }
	}
    }

    /* Mark the cond as free. */
    cond->cond.evt_id = WREN_EVENT_ID_INVALID;

    mb_reply_ok();
}

void
cmd_rx_set_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_action *arg = (struct wren_mb_rx_set_action *)data;
    struct wren_rx_action *act;
    struct wren_rx_cond *cond;
    unsigned idx = arg->act_idx;
    unsigned cond_idx = arg->cond_idx;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_error(WRENRX_ERR_ACTIONID_TOO_LARGE);
	return;
    }
    if (cond_idx >= MAX_RX_CONDS) {
	mb_reply_error(WRENRX_ERR_CONDID_TOO_LARGE);
	return;
    }
    if (arg->conf.pulser_idx >= WREN_NBR_PULSERS) {
	mb_reply_error(WRENRX_ERR_PULSERID_TOO_LARGE);
	return;
    }

    act = &wren_rx_actions[idx];
    if (act->cond_idx != NO_RX_COND_IDX)  {
	mb_reply_error(WRENRX_ERR_COND_BUSY);
	return;
    }

    cond = &wren_rx_conds[cond_idx];

    act->cond_idx = cond_idx;

    memcpy (&act->conf, &arg->conf, sizeof (struct wren_mb_pulser_config));

    /* Put the action in the condition chain */
    act->next = cond->act_idx;
    cond->act_idx = idx;
    cond->nbr_act++;

    mb_reply_ok();
}

static void
apply_immediate_action(struct wren_rx_action *act,
		       struct wren_mb_pulser_config *conf,
		       struct wren_ts *ts,
		       unsigned overwrite)
{
    unsigned pulser_idx = conf->pulser_idx;
    unsigned grp_idx = pulser_idx / WREN_PULSERS_PER_GROUP;
    volatile struct pulser_group_map *grp;
    volatile struct comparators *cmp;
    int cmp_idx;
    uint32_t *res;

    cmp_idx = alloc_comparator_for_pulser(pulser_idx);
    if (cmp_idx < 0) {
	mb_reply_error(WRENRX_ERR_COMPARATOR_BUSY);
	return;
    }

    if (overwrite) {
	/* Reserve the action (but without a condition).
	   In the case of an update, this is a no-op */
	act->cond_idx = INV_RX_COND_IDX;

	/* As the action has no condition, mark the link as invalid */
	act->next = NO_RX_ACT_IDX;

	memcpy (&act->conf, conf, sizeof (struct wren_mb_pulser_config));
    }

    /* Load comparator */
    grp = &regs->pulser_group[grp_idx].el;
    cmp = (volatile struct comparators *)grp->comparators;
    cmp = &cmp[cmp_idx];

    if (ts) {
	cmp->time_sec = ts->sec;
	cmp->time_nsec = ts->nsec;
    }

    setup_comparator(cmp, conf);

    res = (uint32_t *)mb_get_reply(CMD_REPLY, sizeof(*res) / 4);
    *res = grp_idx * WREN_COMPARATORS_PER_GROUP + cmp_idx;
    mb_send_reply();
}

void
cmd_rx_imm_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_imm_action *arg = (struct wren_mb_rx_imm_action *)data;
    struct wren_rx_action *act;
    unsigned idx = arg->act_idx;
    unsigned pulser_idx = arg->cmp.conf.pulser_idx;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_error(WRENRX_ERR_ACTIONID_TOO_LARGE);
	return;
    }
    if (pulser_idx >= WREN_NBR_PULSERS) {
	mb_reply_error(WRENRX_ERR_PULSERID_TOO_LARGE);
	return;
    }

    act = &wren_rx_actions[idx];
    if (act->cond_idx != NO_RX_COND_IDX)  {
	mb_reply_error(WRENRX_ERR_ACTION_BUSY);
	return;
    }

    apply_immediate_action(act, &arg->cmp.conf, &arg->cmp.ts, 1);
}

void
cmd_rx_del_action (uint32_t *data, uint32_t len, unsigned notify)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_rx_action *act;
    unsigned idx = cmd->arg1;
    struct wren_rx_cond *cond;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_error(WRENRX_ERR_ACTIONID_TOO_LARGE);
	return;
    }

    /* Check action is allocated */
    act = &wren_rx_actions[idx];
    if (act->cond_idx == NO_RX_COND_IDX)  {
	mb_reply_error(WRENRX_ERR_ACTION_UNUSED);
	return;
    }

    /* Unlink action from conditions chain */
    if (act->cond_idx != INV_RX_COND_IDX) {
	cond = &wren_rx_conds[act->cond_idx];
	if (cond->act_idx == idx)
	    cond->act_idx = act->next;
	else {
	    struct wren_rx_action *link = &wren_rx_actions[cond->act_idx];
	    while (link->next != idx)
		link = &wren_rx_actions[link->next];
	    link->next = act->next;
	}
	cond->nbr_act--;
    }
    act->cond_idx = NO_RX_COND_IDX;

    /* TODO: remove the action on the hardware (in case of repeat or very
       long train) */
    {
	unsigned pulser_idx = act->conf.pulser_idx;
	unsigned grp_idx = pulser_idx / WREN_PULSERS_PER_GROUP;
	unsigned idx = pulser_idx % WREN_PULSERS_PER_GROUP;
	uint32_t mask;
	unsigned j;
	volatile struct pulser_group_map *grp;
	volatile struct pulsers *pulser;

	grp = &regs->pulser_group[grp_idx].el;
	pulser = &grp->pulsers[idx];

	/* Abort comparators */
	for (j = 0; j < WREN_COMPARATORS_PER_PULSER; j++)
	    grp->comparators[j + idx * WREN_COMPARATORS_PER_PULSER].conf1 =
		PULSER_GROUP_MAP_COMPARATORS_CONF1_ABORT;
	mask = (1 << WREN_COMPARATORS_PER_PULSER) - 1;
	pulser->comp_busy = mask;

	/* Abort pulser */
	grp->pulsers_abort = 1 << idx;
	regs->pulsers_int = 1 << idx;

	pulser->pulses = mask;
    }

    /* Send a message to release the action.
       It will be handled after the remaining messages concerning this action
       (if any) */
    if (notify) {
	union wren_capsule_hdr_un hdr;
	unsigned off;

	if (async_alloc(2, &off) < 0)
	    return;
	hdr.hdr.typ = CMD_ASYNC_REL_ACT;
	hdr.hdr.pad = 0;
	hdr.hdr.len = 2;
	off = async_write32(off, hdr.u32);
	off = async_write32(off, idx);
	async_update(off);
    }

    mb_reply_ok();
}

void
cmd_rx_mod_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_mod_action *cmd = (struct wren_mb_rx_mod_action *)data;
    struct wren_rx_action *act;
    unsigned idx = cmd->act_idx;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_error(WRENRX_ERR_ACTIONID_TOO_LARGE);
	return;
    }

    /* Check action is allocated */
    act = &wren_rx_actions[idx];
    if (act->cond_idx == NO_RX_COND_IDX)  {
	mb_reply_error(WRENRX_ERR_ACTION_UNUSED);
	return;
    }

    if (act->cond_idx == INV_RX_COND_IDX) {
	/* Immediate action, apply it */
	cmd->conf.flags |= WREN_MB_PULSER_FLAG_IMMEDIAT;
	apply_immediate_action(act, &cmd->conf, NULL, 1);
    }
    else {
	memcpy (&act->conf, &cmd->conf, sizeof (struct wren_mb_pulser_config));
	mb_reply_ok();
    }
}

void
cmd_rx_force_pulser (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_force_pulser *cmd =
	(struct wren_mb_rx_force_pulser *)data;
    struct wren_rx_action *act;
    unsigned idx = cmd->act_idx;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_error(WRENRX_ERR_ACTIONID_TOO_LARGE);
	return;
    }

    /* Check action is allocated */
    act = &wren_rx_actions[idx];
    if (act->cond_idx == NO_RX_COND_IDX)  {
	mb_reply_error(WRENRX_ERR_ACTION_UNUSED);
	return;
    }

    /* Apply it immediately */
    cmd->conf.flags |= WREN_MB_PULSER_FLAG_IMMEDIAT;
    apply_immediate_action(act, &cmd->conf, NULL, 0);
}

void
cmd_rx_get_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_rx_action *res;
    struct wren_rx_action *act;
    unsigned idx = cmd->arg1;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_error(WRENRX_ERR_ACTIONID_TOO_LARGE);
	return;
    }

    act = &wren_rx_actions[idx];

    if (act->cond_idx == NO_RX_COND_IDX)  {
	mb_reply_error(WRENRX_ERR_ACTION_UNUSED);
	return;
    }

    res = (struct wren_rx_action *)mb_get_reply(CMD_REPLY, sizeof(*res) / 4);

    *res = *act;

    mb_send_reply();
}

static void
cmd_rx_change_subscribe (uint32_t *data, uint32_t len, unsigned cmd)
{
    struct wren_mb_rx_subscribe *msg = (struct wren_mb_rx_subscribe *)data;
    unsigned src_idx = msg->src_idx;
    unsigned ev_id = msg->ev_id;
    struct wren_rx_source *src;
    unsigned mask;

    /* Check index. */
    if (src_idx >= MAX_RX_SOURCES) {
	mb_reply_error(WRENRX_ERR_SRCID_TOO_LARGE);
	return;
    }
    if (ev_id >= 32 * RX_SUBSCRIBE_MAP_LEN) {
	mb_reply_error(WRENRX_ERR_EVID_TOO_LARGE);
	return;
    }

    src = &wren_rx_sources[src_idx];
    if (src->proto.proto == WREN_PROTO_NONE) {
	mb_reply_error(WRENRX_ERR_BAD_PROTOCOL);
	return;
    }

    mask = 1 << (ev_id & 0x1f);
    if (cmd == CMD_RX_SUBSCRIBE)
	src->subscribed_map[ev_id >> 5] |= mask;
    else
	src->subscribed_map[ev_id >> 5] &= ~mask;

    mb_reply_ok();
}

void
cmd_rx_subscribe (uint32_t *data, uint32_t len, int on_off)
{
    cmd_rx_change_subscribe (data, len, CMD_RX_SUBSCRIBE);
}

void
cmd_rx_unsubscribe (uint32_t *data, uint32_t len, int on_off)
{
    cmd_rx_change_subscribe (data, len, CMD_RX_UNSUBSCRIBE);
}

void
cmd_rx_get_subscribed (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_get_subscribed_reply *res;
    struct wren_rx_source *src;
    unsigned src_idx = cmd->arg1;

    /* Check index. */
    if (src_idx >= MAX_RX_SOURCES) {
	mb_reply_error(WRENRX_ERR_SRCID_TOO_LARGE);
	return;
    }

    src = &wren_rx_sources[src_idx];

    res = (struct wren_mb_get_subscribed_reply *)mb_get_reply
	(CMD_REPLY, sizeof(*res) / 4);

    memcpy (res->map, src->subscribed_map, sizeof(res->map));

    mb_send_reply();
}

#define LOG_BASE 0x40000000 /* Logs start at 1GB */

/* Number of address bits for log entries of an input (or a pulser)
   1GB for the logs / 128 inputs  */
#define LOG_ADDR_BITS (30 - 7)

void
cmd_rx_log_read (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_get_log *cmd = (struct wren_mb_rx_get_log *)data;
    uint32_t *rep;
    uint32_t nentries = cmd->nentries;
    unsigned log_idx = cmd->log_idx;
    uint32_t addr;

    if (log_idx >= WREN_NBR_PULSERS + WREN_NBR_PINS) {
	mb_reply_error(WRENRX_ERR_LOGID_TOO_LARGE);
	return;
    }
    if (nentries == 0) {
	mb_reply_error(WRENRX_ERR_BAD_NENTRIES);
	return;
    }

    rep = (uint32_t *)mb_get_reply(CMD_REPLY, nentries);

    /* Set the address to the entry before the index */
    addr = LOG_BASE | (log_idx << LOG_ADDR_BITS);
    addr |= (cmd->entry_idx - 4) & ((1 << LOG_ADDR_BITS) - 4);

    /* Clear cache line */
    r5_dccimvac(addr);
    cpu_dsb();

    while (1) {
	/* Copy the entry */
	*rep++ = *(uint32_t *)addr;
	if (--nentries == 0)
	    break;

	/* Previous entry */
	if ((addr & ((1 << LOG_ADDR_BITS) - 1)) == 0)
	    addr |= ((1 << LOG_ADDR_BITS) - 1) & ~3;
	else
	    addr -= 4;

	/* If the entry is on a new cache line, invalidate the line */
	if ((addr & 0x1f) == 0x1c) {
	    r5_dccimvac(addr);
	    cpu_dsb();
	}
    }

    mb_send_reply();
}

void
cmd_rx_reset (uint32_t *data, uint32_t len)
{
    unsigned i;

    wrenrx_init();

    /* Abort comparators, abort pulsers, reset output */
    for (i = 0; i < WREN_NBR_GROUPS; i++) {
	volatile struct pulser_group_map *grp = &regs->pulser_group[i].el;
	unsigned j;

	for (j = 0; j < WREN_COMPARATORS_PER_GROUP; j++)
	    grp->comparators[j].conf1 =
		PULSER_GROUP_MAP_COMPARATORS_CONF1_ABORT;
	grp->pulsers_abort = (1 << WREN_PULSERS_PER_GROUP) - 1;
	for (j = 0; j < WREN_PULSERS_PER_GROUP; j++)
	    grp->out_cfg[j].config = 0;
    }

    /* Reset pins */
    regs->fp_oe = 0;
    regs->fp_term = 0;
    regs->fp_inv = 0;

    mb_reply_ok();
}

void
cmd_rx_set_promisc (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    nic_rx_promisc = cmd->arg1;
    mb_reply_ok();
}

void
cmd_rx_inject_packet (uint32_t *data, uint32_t len)
{
    uint32_t src_idx;
    struct wren_pkt_buf frame;

    /* Check source */
    if (len < 1 + WREN_PACKET_HDR_WORDS) {
	mb_reply_failure(CMD_ERROR_LENGTH);
	return;
    }

    src_idx = *data;
    if (src_idx >= MAX_RX_SOURCES) {
	mb_reply_error(WRENRX_ERR_BAD_SOURCE);
	return;
    }

    /* Remove source */
    len--;

    frame.buf = (unsigned char *)(data + 1);
    frame.len = len * 4;
    frame.frame_off = 0;
    frame.pkt_off = 0;

    wrenrx_handle_packet(&frame, src_idx);

    mb_reply_ok();
    return;
}

void
wrenrx_run_event(unsigned src_idx, struct wren_capsule_event_hdr *evt)
{
    unsigned hlen = sizeof(struct wren_capsule_event_hdr) / 4;
    unsigned len = evt->hdr.len;
    unsigned i;
    union wren_capsule_hdr_un hdr;
    uint32_t *data;
    unsigned off;

    if (async_alloc(len, &off) < 0)
	return;

    WREN_LOG(LOG_EVENT, "send event %u\n", evt->ev_id);

    hdr.hdr.typ = CMD_ASYNC_EVENT;
    hdr.hdr.pad = src_idx;
    hdr.hdr.len = len;
    off = async_write32(off, hdr.u32);
    off = async_write32(off, evt->ev_id | (evt->ctxt_id << 16));
    off = async_write32(off, evt->ts.nsec);
    off = async_write32(off, evt->ts.sec);

    data = (uint32_t *)(evt + 1);
    for (i = 0; i < len - hlen; i++)
	off = async_write32(off, data[i]);
    async_update(off);
}

void
wrenrx_run_context(unsigned src_idx, struct wren_rx_context *ctxt)
{
    unsigned len = 1 + 1 + 2 + ctxt->len;
    unsigned i;
    union wren_capsule_hdr_un hdr;
    unsigned off;

    if (async_alloc(len, &off) < 0)
	return;

    WREN_LOG(LOG_CTXT, "send context %u\n", ctxt->ctxt_id);

    hdr.hdr.typ = CMD_ASYNC_CONTEXT;
    hdr.hdr.pad = src_idx;
    hdr.hdr.len = len;
    /* Capsule header */
    off = async_write32(off, hdr.u32);
    off = async_write32(off, ctxt->ctxt_id);
    off = async_write32(off, ctxt->valid_from.nsec);
    off = async_write32(off, ctxt->valid_from.sec);
    /* Parameters */
    for (i = 0; i < ctxt->len; i++)
	off = async_write32(off, ctxt->params[i]);
    async_update(off);
}

void wrenrx_handle_promisc(struct wren_pkt_buf *buf)
{
    unsigned len = 1 + (buf->len + 3) / 4;
    union wren_capsule_hdr_un hdr;
    unsigned off;

    if (async_alloc(len, &off) < 0)
	return;

    hdr.hdr.typ = CMD_ASYNC_PROMISC;
    hdr.hdr.pad = 0;
    hdr.hdr.len = len;
    /* Header */
    off = async_write32(off, hdr.u32);
    /* Data */
    for (unsigned i = 0; i < len - 1; i++) {
	uint32_t data = 0;
	for (unsigned j = 0; j < 4; j++)
	    data |= buf->buf[buf->frame_off + i * 4 + j] << (j << 3);
	off = async_write32(off, data);
    }
    async_update(off);
}

void
wrenrx_run_action (unsigned src_idx, unsigned act_idx, struct wren_capsule_event_hdr *evt)
{
    struct wren_rx_action *act = &wren_rx_actions[act_idx];
    volatile struct pulser_group_map *grp;
    unsigned pulser_idx = act->conf.pulser_idx;
    unsigned grp_idx = pulser_idx / WREN_PULSERS_PER_GROUP;
    volatile struct comparators *cmp;
    union wren_capsule_hdr_un hdr;
    int cmp_grp_idx; /* Comparator index in the group */
    unsigned cmp_abs_idx; /* Absolute comparator index */
    uint32_t due_sec;
    int32_t due_nsec;
    unsigned off;

    grp = &regs->pulser_group[grp_idx].el;

    cmp_grp_idx = alloc_comparator_for_pulser(pulser_idx);
    if (cmp_grp_idx < 0)
	return;

    /* Compute due time */
    due_sec = evt->ts.sec + act->conf.load_off_sec;
    due_nsec = evt->ts.nsec + act->conf.load_off_nsec;
    if (due_nsec < 0) {
	due_sec--;
	due_nsec += NS_PER_SEC;
    }
    else if (due_nsec > NS_PER_SEC) {
	due_sec++;
	due_nsec -= NS_PER_SEC;
    }

    /* Load comparator */
    cmp = (volatile struct comparators *)grp->comparators;
    cmp = &cmp[cmp_grp_idx];

    cmp->time_sec = due_sec;
    cmp->time_nsec = due_nsec;
    setup_comparator(cmp, &act->conf);

    cmp_abs_idx = grp_idx * WREN_COMPARATORS_PER_GROUP + cmp_grp_idx;
    WREN_LOG(LOG_COMP, "action: load comp %u\n", cmp_abs_idx);

    /* Send config to the host */
    if (async_alloc(2, &off) < 0)
	return;
    hdr.hdr.typ = CMD_ASYNC_CONFIG;
    hdr.hdr.pad = src_idx;
    hdr.hdr.len = 2;
    off = async_write32(off, hdr.u32);
    off = async_write32(off, act_idx | (cmp_abs_idx << 16));
    async_update(off);
}

void
pulses_handler(unsigned pulses)
{
    unsigned off;
    union wren_capsule_hdr_un hdr;

    /* Ack interrupts */
    regs->pulsers_int = pulses;

    WREN_LOG(LOG_PULSE, "Pulses: %08x\n", pulses);

    while (1) {
	/* Extract a pulser index.  */
	unsigned pulser_idx = __builtin_ffs(pulses);
	if (pulser_idx == 0)
	    break;
	pulser_idx--;

	pulses &= ~(1 << pulser_idx);

	unsigned grp_idx = pulser_idx / WREN_PULSERS_PER_GROUP;
	volatile struct pulser_group_map *grp = &regs->pulser_group[grp_idx].el;
	unsigned sub_idx = pulser_idx % WREN_PULSERS_PER_GROUP;
	volatile struct pulsers *pulser = &grp->pulsers[sub_idx];
	unsigned comps = pulser->pulses;
	unsigned ts = pulser->ts;
	uint32_t tai = regs->tm_tai_lo;

	/* Expand time-stamp */
	if ((ts >> 30) == ((tai - 1) & 3))
	    tai--;
	else if ((ts >> 30) != (tai & 3))
	    tai = 0;
	ts = ts & 0x3fffffff;

	/* Clear bits */
	pulser->pulses = comps;
	pulser->loaded_comp = comps;

	while (comps != 0) {
	    unsigned sub_comp = __builtin_ffs(comps) - 1;

	    /* Absolute comparator index */
	    unsigned comp_id =
	      pulser_idx * WREN_COMPARATORS_PER_PULSER + sub_comp;

	    comps &= ~(1 << sub_comp);

	    /* Send comparator idx to the host */
	    if (async_alloc(4, &off) < 0)
		return;
	    hdr.hdr.typ = CMD_ASYNC_PULSE;
	    hdr.hdr.pad = 0;
	    hdr.hdr.len = 4;
	    off = async_write32(off, hdr.u32);
	    off = async_write32(off, comp_id);
	    off = async_write32(off, tai);
	    off = async_write32(off, ts);
	    async_update(off);

	    WREN_LOG(LOG_COMP, "pulse by comp %u\n", comp_id);
	}

    }
}

void
wrenrx_now(unsigned *sec, unsigned *nsec)
{
    do {
	*sec = regs->tm_tai_lo;
	*nsec = regs->tm_cycles << 4;
    } while (*sec != regs->tm_tai_lo);
}

void
wren_log (const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    uart_vprintf(fmt, args);
    va_end(args);
}
