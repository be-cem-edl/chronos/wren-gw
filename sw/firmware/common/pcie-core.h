void pcie_init(void);

/* For debug: disp ingress registers.  */
void do_pcie_ingress(char *args);
void do_pcie_egress(char *args);

/* For debug: do init again */
void do_pcie_reinit(char *args);

void do_pcie_int (char *args);

#define PCIE_MENU \
   {"pcie-in", do_pcie_ingress}, \
   {"pcie-out", do_pcie_egress},		\
   {"pcie-int", do_pcie_int}, \
   {"pcie-reinit", do_pcie_reinit },
