#include <stdint.h>
#include "spi.h"
#include "lib.h"
#include "board.h"


#ifdef SI5340_USE_SPI

#if 0

#include "xparameters.h"	/* EDK generated parameters */
#include "xspips.h"		/* SPI device driver */

/*
 * The instances to support the device drivers are global such that the
 * are initialized to zero each time the program runs.  They could be local
 * but should at least be static so they are zeroed.
 */
static XSpiPs SpiInstance;

int
si5341_init_hw(void)
{
	XSpiPs_Config *SpiConfig;

	int Status;

	SpiConfig = XSpiPs_LookupConfig(XPAR_XSPIPS_0_DEVICE_ID);
	if (NULL == SpiConfig) {
	  uart_printf("XSPI lookupconfig failure\n");
		return XST_FAILURE;
	}

	Status = XSpiPs_CfgInitialize(&SpiInstance, SpiConfig,
				      SpiConfig->BaseAddress);
	if (Status != XST_SUCCESS) {
	  uart_printf("XSPI cfg initialize failure\n");
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to check hardware build
	 */
	Status = XSpiPs_SelfTest(&SpiInstance);
	if (Status != XST_SUCCESS) {
	  uart_printf("XSPI self test failure\n");
		return XST_FAILURE;
	}

	/*
	 * Set the Spi device as a master. External loopback is required.
	 */
	XSpiPs_SetOptions(&SpiInstance, XSPIPS_MASTER_OPTION |
			  XSPIPS_FORCE_SSELECT_OPTION);

	XSpiPs_SetClkPrescaler(&SpiInstance, XSPIPS_CLK_PRESCALE_64);

	uart_printf("XSPI initialized\n");
	return 0;
}

static int si5341_write2(unsigned cs, unsigned cmd, unsigned val)
{
  unsigned char buf[2];

  buf[0] = cmd;
  buf[1] = val;

  XSpiPs_SetSlaveSelect(&SpiInstance, cs);

  XSpiPs_PolledTransfer(&SpiInstance, buf, NULL, 2);

  return 0;
}

static int si5341_read1(unsigned cs, unsigned cmd)
{
  unsigned char buf[2];
  unsigned char res[2];

  buf[0] = cmd;
  buf[1] = 0;

  XSpiPs_SetSlaveSelect(&SpiInstance, cs);

  XSpiPs_PolledTransfer(&SpiInstance, buf, res, 2);
  return res[1];
}

#else

#define SPI0 0x00FF040000
#define SPI1 0x00FF050000

struct spi_regs {
  uint32_t config;    //Config	0x0000000000	SPI configuration
  uint32_t isr;       //ISR	0x0000000004	SPI interrupt status
  uint32_t ier;       //IER	0x0000000008	Interrupt Enable
  uint32_t idr;       //IDR	0x000000000C	Interrupt disable

  uint32_t imr;       //IMR	0x0000000010	Interrupt mask
  uint32_t enable;    //Enable	0x0000000014	SPI_Enable
  uint32_t delay;     //Delay	0x0000000018	Clock Delay
  uint32_t tx_data;   //Tx_data	0x000000001C	Transmit Data.

  uint32_t rx_data;   //Rx_data	0x0000000020	Receive Data
  uint32_t idlecnt;   //Slave_Id	0x0000000024	Slave Idle Count
  uint32_t tx_thres;  //TX_thres	0x0000000028	TX FIFO Threshold
  uint32_t rx_thres;  //RX_thres	0x000000002C	RX FIFO Threshold
};

#define SPI_CONFIG_MODE_SEL (1 << 0)  // 1: master
#define SPI_CONFIG_CLK_POL  (1 << 1)  // 1: SPI clock is quiescent hi
#define SPI_CONFIG_CLK_PH   (1 << 2)  // 1: SPI clock is inactive outside word
#define SPI_CONFIG_BAUD_RATE_DIV_16 (3 << 3)
#define SPI_CONFIG_BAUD_RATE_DIV_64 (5 << 3)
#define SPI_CONFIG_REF_CLK  (1 << 8)  //  Must be 0
#define SPI_CONFIG_PERI_SEL (1 << 9)  //  1: external 3-to-8 decoder
#define SPI_CONFIG_CS_shift 10
#define SPI_CONFIG_CS_0     (0 << SPI_CONFIG_CS_shift)
#define SPI_CONFIG_CS_1     (1 << SPI_CONFIG_CS_shift)
#define SPI_CONFIG_CS_NONE  (15 << SPI_CONFIG_CS_shift)
#define SPI_CONFIG_MANUAL_CS (1 << 14)
#define SPI_CONFIG_MANUAL_START_EN   (1 << 15)
#define SPI_CONFIG_MANUAL_START_COM  (1 << 16)
#define SPI_CONFIG_MODEFAIL_GEN_EN   (1 << 17)

#define SPI_ENABLE_EN (1 << 0)

#define SPI_INT_RX_OVERFLOW (1 << 0)
#define SPI_INT_MODE_FAIL   (1 << 1)
#define SPI_INT_TX_FIFO_NOT_FULL (1 << 2)
#define SPI_INT_TX_FIFO_FULL (1 << 3)
#define SPI_INT_RX_FIFO_NOT_EMPTY (1 << 4)
#define SPI_INT_RX_FIFO_FULL (1 << 5)
#define SPI_INT_RX_FIFO_UNDERFLOW (1 << 6)

static volatile struct spi_regs *const spi = (volatile struct spi_regs *)SPI0;

int
si5341_init_hw(void)
{
  /* Note: SPI_REF_CLK is 200Mhz, Si5340 max SCLK freq is 20Mhz */
  spi->config =
    SPI_CONFIG_MODE_SEL /* Master */
    | SPI_CONFIG_CLK_POL
    | SPI_CONFIG_CLK_PH
    | SPI_CONFIG_BAUD_RATE_DIV_64 /* TODO: _16 should be OK */
    | SPI_CONFIG_MANUAL_CS
    | SPI_CONFIG_MANUAL_START_EN
    | SPI_CONFIG_CS_NONE;

#if 0
  uart_printf("spi init: config [%08x]=%08x, isr=%08x\n",
	      (unsigned)&spi->config, (unsigned)spi->config,
	      (unsigned)spi->isr);
#endif

  return 0;
}

static void si5341_set_cs(unsigned cs)
{
    unsigned v = ((~(1 << cs)) << SPI_CONFIG_CS_shift) & SPI_CONFIG_CS_NONE;

    spi->config = (spi->config & ~SPI_CONFIG_CS_NONE) | v;

#if 0
    uart_printf("SPI cs: cs=%u, v=%08x, config=%08x, isr=%08x\n", cs, v,
		(unsigned)spi->config, (unsigned)spi->isr);
#endif
}

static int si5341_write2(unsigned cs, unsigned cmd, unsigned val)
{
  unsigned rx_cnt;

  /* Enable */
  spi->enable = SPI_ENABLE_EN;

  si5341_set_cs(cs);

  spi->tx_data = cmd;
  spi->tx_data = val;

  spi->config |= SPI_CONFIG_MANUAL_START_COM;

#if 0
  uart_printf("SPI: cfg=%08x, isr=%08x\n",
	      (unsigned)spi->config, (unsigned)spi->isr);
#endif

  for (rx_cnt = 0; rx_cnt < 2; rx_cnt++) {
    while (!(spi->isr & SPI_INT_RX_FIFO_NOT_EMPTY))
      ;
    (void)spi->rx_data;
  }

  spi->config |= SPI_CONFIG_CS_NONE;

  spi->enable = 0;

  return 0;
}

static int si5341_read1(unsigned cs, unsigned cmd)
{
  unsigned rx_cnt;
  unsigned v;

  /* Enable */
  spi->enable = SPI_ENABLE_EN;

  si5341_set_cs(cs);

  spi->tx_data = cmd;
  spi->tx_data = 0;

  spi->config |= SPI_CONFIG_MANUAL_START_COM;

  for (rx_cnt = 0; rx_cnt < 2; rx_cnt++) {

    while (!(spi->isr & SPI_INT_RX_FIFO_NOT_EMPTY))
      ;
    v = spi->rx_data;
  }

  spi->config |= SPI_CONFIG_CS_NONE;

  spi->enable = 0;
  return v & 0xff;
}

#endif

#define SET_ADDRESS 0x00
#define WRITE_DATA  0x40
#define READ_DATA   0x80
#define WRITE_DATA_INC 0x60
#define READ_DATA_INC  0xa0

int si5341_write_raw(unsigned cs, uint8_t reg, uint8_t val)
{
  if (si5341_write2(cs, SET_ADDRESS, reg) < 0)
    return -1;
  if (si5341_write2(cs, WRITE_DATA, val) < 0)
    return -1;
  return 0;
}

int si5341_read_raw(unsigned cs,
		    uint16_t addr, uint8_t *data, unsigned size)
{
  unsigned rx_cnt;

  if (si5341_write2(cs, SET_ADDRESS, addr & 0xff) < 0)
    return -1;

  for (rx_cnt = 0; rx_cnt < size; rx_cnt++) {
    int v = si5341_read1(cs, READ_DATA_INC);
    if (v < 0)
      return -1;
    data[rx_cnt] = v;
  }

  return 0;
}

void si5341_wait_done(void)
{
}

#endif
