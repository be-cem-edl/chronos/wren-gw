extern void wren_log (const char *fmt, ...) __attribute__((format(printf, 1, 2)));

extern unsigned log_flags;

enum trace_t {
#define def_trace(NAME, VAL) LOG_##NAME = (1 << VAL),
#include "trace.def"
#undef def_trace
};

#define WREN_LOG(FLAGS, FMT, ...) \
  do { if ((FLAGS) & log_flags) wren_log(FMT, __VA_ARGS__); } while (0)
