#ifndef __SI5341__H__
#define __SI5341__H__

#include <stdint.h>

typedef struct
{
	uint16_t address; /* 16-bit register address */
	uint8_t  value; /* 8-bit register data */
} si5341_revd_register_t;


/* Low-level interface; page is not selected */
int si5341_write_raw(unsigned cs, uint8_t reg, uint8_t val);

int si5341_read_raw(unsigned cs,
		    uint16_t addr, uint8_t *data, unsigned size);

void si5341_wait_done(void);

int si5341_init_hw(void);

/* For CLI */
void do_si534x(char *args, unsigned cs, int is41, unsigned ifreq);

/* High-level: to be used by board initialization code */
int si5341_program(const si5341_revd_register_t *regs,
		   unsigned num_regs,
		   unsigned cs);

int si534x_is_locked(unsigned cs);
int si5341_read_reg(unsigned cs, uint16_t addr);

#endif /* __SI5341__H__ */
