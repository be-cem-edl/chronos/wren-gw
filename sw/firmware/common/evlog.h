#ifndef __EVLOG__H__
#define __EVLOG__H__

/* Entry definition */
#include "wren-mb-defs.h"

/* Event log */

/* Size of a timestamp */
#define EVLOG_TS_SIZE 8

void evlog_reset(void);

/* Create an entry step by step */
void evlog_hdr(unsigned type, unsigned dlen);

/* Extend the size of the current entry */
void evlog_hdr_extend(unsigned dlen);

/* Timestamp is only allowed as the first field */
void evlog_push_ts(void);

void evlog_push_u8(unsigned char v);
void evlog_push_u16(uint16_t v);
void evlog_push_bytes(const unsigned char *d, unsigned len);

void cmd_evlog_index (uint32_t *data, uint32_t len);
void cmd_evlog_read (uint32_t *data, uint32_t len);

#endif /* __EVLOG__H__ */
