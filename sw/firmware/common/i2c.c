#include <xparameters.h>

#include "board.h"
#include "i2c.h"
#include "lib.h"

#define I2C0 0xff020000
#define I2C1 0xff030000

struct i2c_regs {
    uint32_t control;  /* 0x320f: div_a=1, div_b=50+1 */
    uint32_t status;
    uint32_t i2c_address;
    uint32_t i2c_data;

    uint32_t interrupt_status;
    uint32_t transfer_size;
    uint32_t slave_mon_pause;
    uint32_t time_out;

    uint32_t intrpt_mask;
    uint32_t intrpt_enable;
    uint32_t intrpt_disable;
    uint32_t glitch_filter;
};

#define I2C_CONTROL_DIVISOR_A_SHIFT 14
#define I2C_CONTROL_DIVISOR_B_SHIFT 8
#define I2C_CONTROL_CLR_FIFO (1 << 6)
#define I2C_CONTROL_SLVMON   (1 << 5)
#define I2C_CONTROL_HOLD     (1 << 4)
#define I2C_CONTROL_ACK_EN   (1 << 3)
#define I2C_CONTROL_NEA      (1 << 2)
#define I2C_CONTROL_MS       (1 << 1)
#define I2C_CONTROL_RW       (1 << 0)

#define I2C_STATUS_BA        (1 << 8)
#define I2C_STATUS_RXDV      (1 << 5)

#define I2C_INTERRUPT_ARB_LOST  (1 << 9)
#define I2C_INTERRUPT_RX_UNF    (1 << 7)
#define I2C_INTERRUPT_TX_OVF    (1 << 6)
#define I2C_INTERRUPT_RX_OVF    (1 << 5)
#define I2C_INTERRUPT_SLV_RDY   (1 << 4)
#define I2C_INTERRUPT_TO        (1 << 3)
#define I2C_INTERRUPT_NACK      (1 << 2)
#define I2C_INTERRUPT_DATA      (1 << 1)
#define I2C_INTERRUPT_COMP      (1 << 0)

static volatile struct i2c_regs * const i2c = (volatile struct i2c_regs *)I2C0;

void
i2c_init(void)
{
    /*  Div = (bus clock) / (22 * i2c clk) - 1
	i2c clk = 400Khz, bus clock = 500Mhz */
    i2c->control = (50 << I2C_CONTROL_DIVISOR_B_SHIFT)
	| I2C_CONTROL_CLR_FIFO
	| I2C_CONTROL_ACK_EN
	| I2C_CONTROL_NEA
	| I2C_CONTROL_MS;

    i2c->interrupt_status = 0xffff;
    i2c->time_out = 0x1f;
    i2c->glitch_filter = 5;

#if 0
    uart_printf("i2c_init: done\n");
#endif
}

int
i2c_send(uint8_t *buf, unsigned len, unsigned i2c_addr)
{
    unsigned l;

    uint16_t control = i2c->control;

    /* Set HOLD if send more data than the size of the FIFO */
    if (len > 16)
	control |= I2C_CONTROL_HOLD;

    /* Setup master.  */
    control |= I2C_CONTROL_CLR_FIFO;
    control &= ~I2C_CONTROL_RW;
    i2c->control = control;

    /* Clear ISR (arb_lost, nack, tx_ovr) */
    i2c->interrupt_status = i2c->interrupt_status;

    /* Fill FIFO */
    for (l = 0; l < len && l < 16; l++) {
	i2c->i2c_data = *buf;
	buf++;
    }

    /* Set address to start transfer */
    i2c->i2c_address = i2c_addr;

    /* Wait until fifo is empty (SR TXDV), then send more data */
    while (1) {
	uint32_t status = i2c->interrupt_status;
#if 0
	uart_printf("i2c_send: loop len=%u, status=%04x\n",
			len, (unsigned)status);
#endif
	if (status & (I2C_INTERRUPT_ARB_LOST
		      | I2C_INTERRUPT_NACK
		      | I2C_INTERRUPT_TO))
	    return -1;
	if (status & I2C_INTERRUPT_COMP) {
	    len -= l;

	    if (len == 0) {
		/* Clear HOLD if needed */
		i2c->control &= ~I2C_CONTROL_HOLD;
		return 0;
	    }
	    else {
		for (l = 0; l < len && l < 16; l++) {
		    i2c->i2c_data = *buf;
		    buf++;
		}
	    }
	}
    }
}

int
i2c_recv(uint8_t *buf, unsigned len, unsigned i2c_addr)
{
    unsigned l;
    uint16_t control = i2c->control;

    /* Set HOLD if send more data than the size of the FIFO */
    l = len >= 250 ? 250 : len;
    if (len > l)
	control |= I2C_CONTROL_HOLD;

    /* Setup master.  */
    control |= I2C_CONTROL_CLR_FIFO | I2C_CONTROL_RW;
    i2c->control = control;

    /* Clear ISR (arb_lost, nack, tx_ovr) */
    i2c->interrupt_status = i2c->interrupt_status;

    /* Set transfer size.  */
    i2c->transfer_size = l;

    /* Set address to start transfer */
    i2c->i2c_address = i2c_addr;

    /* Wait until fifo is empty (SR TXDV), then send more data */
    while (1) {
	uint32_t status = i2c->interrupt_status;
#if 0
	uart_printf("i2c_recv: loop len=%u, status=%04x\n",
		    len, (unsigned)status);
#endif
	if (status & (I2C_INTERRUPT_ARB_LOST
		      | I2C_INTERRUPT_NACK
		      | I2C_INTERRUPT_TO))
	    return -1;
	if (status & I2C_INTERRUPT_COMP)
	    return 0;
	/* FIXME: to rework */
	if (i2c->status & I2C_STATUS_RXDV) {
	    *buf++ = i2c->i2c_data;
	    len--;
	    l--;

	    if (l == 0 && len != 0) {
		l = len > 250 ? 250 : len;
		if (len < 250)
		    i2c->control &= ~I2C_CONTROL_HOLD;
	    }
	}
    }
}
