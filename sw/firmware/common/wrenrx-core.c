#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "wrenrx-data.h"
#include "wren-hw.h"
#include "wren-mb-defs.h"
#include "wren/wren-packet.h"
#include "wrenrx-core.h"

#ifdef __linux__
#define WREN_LOG(...)
#else
#define WITH_LOG
#include "trace.h"
#define EVLOG
#include "evlog.h"
#endif

struct wren_rx_source wren_rx_sources[MAX_RX_SOURCES];
struct wren_rx_context wren_rx_contexts[MAX_RX_SOURCES][MAX_RX_CONTEXTS];
struct wren_rx_cond wren_rx_conds[MAX_RX_CONDS];
struct wren_rx_action wren_rx_actions[MAX_RX_ACTIONS];

static int
wrenrx_filter_ethernet(const struct wren_proto_eth *eth,
		       struct wren_pkt_buf *frame)
{
    const unsigned char *etp;
    unsigned ethertyp;
    const unsigned char *ver;
    unsigned hdr_len;

    /* MAC address */
    if (eth->flags & WREN_PROTO_ETH_FLAGS_MAC) {
	const unsigned char *da = frame->buf + frame->frame_off;
	for (unsigned j = 0; j < 6; j++)
	    if (eth->mac[j] != da[j])
		return 0;
    }

    etp = frame->buf + frame->frame_off + 12;
    ethertyp = (etp[0] << 8) | etp[1];
    hdr_len = 14;

    WREN_LOG(LOG_FILTER, "rxfilt: ethtyp: %04x, foff=%u, buf:%p\n",
	     ethertyp, frame->frame_off, frame->buf);

    /* vlan */
    if (ethertyp == 0x8100) {
	etp += 4;
	ethertyp = (etp[0] << 8) | etp[1];
	hdr_len += 4;
    }

    /* Check ethertype */
    if (ethertyp != eth->ethertype) {
	WREN_LOG(LOG_FILTER, "rxfilt: bad ethtyp: %04x vs %04x\n",
		 ethertyp, eth->ethertype);
	return 0;
    }

    frame->pkt_off = frame->frame_off + hdr_len;
    ver = frame->buf + frame->pkt_off;

    if (*ver != PKT_VERSION_v1) {
	WREN_LOG(LOG_FILTER, "rxfilt: unknown version %02x\n", *ver);
	return 0;
    }
    
    const struct wren_packet_hdr_v1 *hdr;
    hdr = (const struct wren_packet_hdr_v1 *)(frame->buf + frame->pkt_off);

    if (hdr->domain != eth->domain_id) {
	WREN_LOG(LOG_FILTER, "rxfilt: bad domain: %02x vs %02x\n",
		 hdr->domain, eth->domain_id);
	return 0;
    }

    /* Packet accepted */
    return 1;
}

int
wrenrx_filter_packet(struct wren_pkt_buf *frame)
{
    for (unsigned i = 0; i < MAX_RX_SOURCES; i++) {
	const struct wren_rx_source *src = &wren_rx_sources[i];

	if (src->proto.proto == WREN_PROTO_NONE) {
	    /* Source is not configured. */
	    continue;
	}

	if (src->proto.proto == WREN_PROTO_ETHERNET) {
	    if (wrenrx_filter_ethernet (&src->proto.u.eth, frame))
		return i;
	}
    }

    return -1;
}

#ifdef WITH_LOG
static void
wrenrx_dump_params(uint32_t *param, unsigned len)
{
    for (unsigned i = 0; i < len;) {
	uint32_t hdr = param[i];
	unsigned t = WREN_PACKET_PARAM_GET_TYP(hdr);
	unsigned l = WREN_PACKET_PARAM_GET_LEN(hdr);
	wren_log("  param %u (l=%u) %u\n", t, l, (unsigned)param[i+1]);
	i += l;
    }
}
#endif

static void
wrenrx_handle_context(const char *pkt, int src_idx)
{
    const struct wren_capsule_ctxt_hdr *hdr =
	(const struct wren_capsule_ctxt_hdr *)pkt;
    struct wren_rx_context *ctxt =
	&wren_rx_contexts[src_idx][hdr->ctxt_id % MAX_RX_CONTEXTS];
    unsigned param_len = hdr->hdr.len - sizeof(*hdr) / 4;
    unsigned id = hdr->ctxt_id;

    WREN_LOG(LOG_CTXT | LOG_DUMP_CTXT,
	     "Got context %u for src %u\n", id, src_idx);
#ifdef WITH_LOG
    if (log_flags & LOG_DUMP_CTXT) {
	wrenrx_dump_params((uint32_t *)(pkt + sizeof(*hdr)), param_len);
    }
#endif

#ifdef EVLOG
    evlog_hdr(EVLOG_TYPE_CTXT, hdr->hdr.len * 4);
    evlog_push_bytes ((void *)hdr, hdr->hdr.len * 4);
#endif

    /* Check length */
    if (param_len > WREN_PACKET_DATA_MAX_WORDS) {
	WREN_LOG(LOG_ERR,
		 "context %u is too long (plen=%u)\n", id, param_len);
	return;
    }

    /* Save the context */
    ctxt->ctxt_id = id;
    ctxt->len = param_len;
    ctxt->valid_from = hdr->valid_from;
    memcpy(ctxt->params, pkt + sizeof(*hdr), param_len * 4);

    /* Send it to the host */
    wrenrx_run_context(src_idx, ctxt);
}

/* Return the address of parameter PARAM_ID in PARAM.
   Return NULL if not found. */
static uint32_t *
wrenrx_read_param(uint16_t param_id,
		  uint32_t *param,
		  unsigned len)
{
    for (unsigned i = 0; i < len;) {
	uint32_t hdr = param[i];
	if (WREN_PACKET_PARAM_GET_TYP(hdr) == param_id)
	    return &param[i];
	i += WREN_PACKET_PARAM_GET_LEN(hdr);
    }
    return NULL;
}

static int
wrenrx_eval_cond(const struct wren_mb_cond *cond,
		 int src_idx,
		 struct wren_rx_context *ctxt,
		 struct wren_capsule_event_hdr *evt)
{
    unsigned clen = cond->len;
    unsigned i;
    unsigned res = 1;
    uint32_t eval_param = 0;

    for (i = 0; i < clen; i++) {
	struct wren_rx_cond_op op = cond->ops[i].op;
	if (op.op <= WREN_OP_NE) {
	    uint32_t *param;
	    unsigned plen;
	    uint32_t *p;

	    /* Get parameter. */
	    p = NULL;
	    if (op.param_orig & WRENRX_PARAM_EVENT) {
		param = (uint32_t *)(evt + 1);
		plen = evt->hdr.len
		    - sizeof (struct wren_capsule_event_hdr) / 4;
		p = wrenrx_read_param (op.param_id, param, plen);
	    }
	    if (p == NULL && (op.param_orig & WRENRX_PARAM_CONTEXT)) {
		param = ctxt->params;
		plen = ctxt->len;
		p = wrenrx_read_param (op.param_id, param, plen);
	    }
	    if (p == NULL) {
		WREN_LOG(LOG_COND, "no parameter %u\n", op.param_id);
#ifdef EVLOG
		evlog_push_u8 (0xff);
		evlog_push_u16 (op.param_id);
#endif
		return 0;
	    }

	    res <<= 1;

	    switch (op.op) {
	    case WREN_OP_EQ:
		res |= (p[1] == cond->ops[i + 1].vu32);
		break;
	    case WREN_OP_NE:
		res |= (p[1] != cond->ops[i + 1].vu32);
		break;
	    }

	    WREN_LOG(LOG_COND, "parameter %u=%u, res=%04x\n",
		     op.param_id, (unsigned)p[1], res);

	    eval_param = (eval_param << 1) | (res & 1);

	    /* Skip value */
	    i++;
	}
	else
	    switch (op.op) {
	    case WREN_OP_AND:
		res = (res >> 1) & (res | 0xffffffe);
		break;
	    case WREN_OP_OR:
		res = (res >> 1) | (res & 1);
		break;
	    case WREN_OP_NOT:
		res ^= 1;
		break;
	    }
    }

    res &= 1;

#ifdef EVLOG
    evlog_push_u8 (res);
    evlog_push_u16 (eval_param);
#endif

    return res;
}

static void
wrenrx_handle_event(struct wren_capsule_event_hdr *evt, int src_idx)
{
    uint16_t ev_id = evt->ev_id;
    struct wren_rx_source *src = &wren_rx_sources[src_idx];
    uint16_t cond_idx = src->conds;
    uint16_t ctxt_idx = evt->ctxt_id;
    struct wren_rx_context *ctxt;
    unsigned sent = 0;

    WREN_LOG(LOG_EVENT,
	     "Get event %u for src_idx %u (ctxt %u, first_cond: %u)\n",
	     ev_id, src_idx, ctxt_idx, cond_idx);

#ifdef WITH_LOG
    if (log_flags & LOG_DUMP_EVT) {
	wrenrx_dump_params((uint32_t *)(evt + 1),
			   evt->hdr.len
			   - sizeof (struct wren_capsule_event_hdr) / 4);
    }
#endif

#ifdef EVLOG
    evlog_hdr(EVLOG_TYPE_EVNT, evt->hdr.len * 4);
    evlog_push_bytes ((void *)evt, evt->hdr.len * 4);
#endif

    /* Get context.
       TODO: find context if no id. */
    ctxt = &wren_rx_contexts[src_idx][ctxt_idx % MAX_RX_CONTEXTS];
    if (ctxt->ctxt_id != ctxt_idx) {
	/* TODO: report error */
	WREN_LOG(LOG_EVENT,
		 "but mismatch context (has %u)\n", ctxt->ctxt_id);
	return;
    }

    if (ev_id < 32 * RX_SUBSCRIBE_MAP_LEN
	&& (src->subscribed_map[ev_id >> 5] & (1 << (ev_id & 0x1f))) != 0) {
	wrenrx_run_event(src_idx, evt);
	sent = 1;
    }

    while (cond_idx != NO_RX_COND_IDX) {
	struct wren_rx_cond *cond = &wren_rx_conds[cond_idx];

	WREN_LOG(LOG_EVENT,
		 "check with cond %u (ev=%u)\n", cond_idx, cond->cond.evt_id);

	if (cond->cond.evt_id == ev_id) {
#ifdef EVLOG
	    /* 2 for cond_idx, 1 for nparam, 1 for result, 2 for params */
	    evlog_hdr(EVLOG_TYPE_COND, 2 + 1 + 1 + 2);
	    evlog_push_u16 (cond_idx);
	    evlog_push_u8 (cond->nparam);
#endif
	    if (wrenrx_eval_cond(&cond->cond, src_idx, ctxt, evt)) {
		uint16_t act_idx;

		if (cond->log) {
		    unsigned now_sec, now_nsec;
		    wrenrx_now(&now_sec, &now_nsec);

		    WREN_LOG(LOG_EVENT_TS,
			     "Ev %u src_idx %u, now %u+%u, ev_ts: %u+%u\n",
			     ev_id, src_idx,
			     now_sec, now_nsec,
			     (unsigned)evt->ts.sec, (unsigned)evt->ts.nsec);
		}

#ifdef EVLOG
		/* Extend evlog for the action-id pushed below */
		evlog_hdr_extend(cond->nbr_act * 2);
#endif
		act_idx = cond->act_idx;
		while (act_idx != NO_RX_ACT_IDX) {
		    struct wren_rx_action *act = &wren_rx_actions[act_idx];
		    if (!sent) {
			wrenrx_run_event(src_idx, evt);
			sent = 1;
		    }
#ifdef EVLOG
		    evlog_push_u16(act_idx);
#endif
		    wrenrx_run_action (src_idx, act_idx, evt);
		    act_idx = act->next;
		}
	    }
	}

	cond_idx = cond->next;
    }
}

void
wrenrx_handle_packet(struct wren_pkt_buf *frame, int src_idx)
{
    struct wren_rx_source *src = &wren_rx_sources[src_idx];
    const struct wren_packet_hdr_v1 *hdr =
	(const struct wren_packet_hdr_v1 *)(frame->buf + frame->pkt_off);

    /* A new packet has been received.  */
    src->nbr_frames++;

    /* Check sequence id. */
    uint16_t seqid = hdr->seq_id;
    uint16_t next_seqid = src->next_seqid;

    if (seqid != PKT_SEQ_SYNC
	&& next_seqid != PKT_SEQ_SYNC
	&& seqid != next_seqid) {
	/* Missing frame. */
	WREN_LOG(LOG_ERR, "missing frame for src %u (seqid=%04x, next=%04x)\n",
		 src_idx, seqid, next_seqid);
    }
    src->next_seqid = (seqid + PKT_SEQ_INC) & PKT_SEQ_MASK;

    /* Sub-sampling.  */
    if (src->subsample != 0) {
	src->subsample_cnt++;
	if (src->subsample_cnt < src->subsample) {
	    /* Discard. */
	    return;
	}
	src->subsample_cnt = 0;
    }

    /* TODO: latency */

    /* Check length.  */
    if (hdr->len * 4 > (frame->len - (frame->pkt_off - frame->frame_off))) {
	WREN_LOG(LOG_ERR, "truncated frame (len=%u)\n", hdr->len * 4);
	return;
    }

    /* Decode capsule */
    const char *payload = (const char *)(hdr + 1);
    for (unsigned off = WREN_PACKET_HDR_WORDS; off < hdr->len; ) {
	struct wren_capsule_hdr *hdr = (struct wren_capsule_hdr *)payload;

	if (hdr->typ == PKT_CTXT)
	    wrenrx_handle_context(payload, src_idx);
	else if (hdr->typ == PKT_EVENT)
	    wrenrx_handle_event((struct wren_capsule_event_hdr *)hdr, src_idx);
	else {
	    WREN_LOG(LOG_ERR, "unknown capsule %u\n", hdr->typ);
	    break;
	}

	payload += hdr->len * 4;
	off += hdr->len;
    }
}

void
wrenrx_init(void)
{
    for (unsigned i = 0; i < MAX_RX_SOURCES; i++)
	wren_rx_sources[i].proto.proto = WREN_PROTO_NONE;

    for (unsigned i = 0; i < MAX_RX_SOURCES; i++)
	wren_rx_sources[i].conds = NO_RX_COND_IDX;

    for (unsigned i = 0; i < MAX_RX_CONDS; i++)
	wren_rx_conds[i].cond.evt_id = WREN_EVENT_ID_INVALID;

    for (unsigned i = 0; i < MAX_RX_ACTIONS; i++)
	wren_rx_actions[i].cond_idx = NO_RX_COND_IDX;
}
