/*
 * Copyright (C) 2017 CERN (www.cern.ch)
 * Author: Tristan Gingold <tristan.gingold@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 *
 * test_vme: program to test the vme64xcore (to be used with the
 * svec_vmecore_test design).
 */

#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include "libvmebus.h"

static struct vme_mapping data_map;
static volatile unsigned int *ptr32;

static unsigned read32(unsigned off)
{
  unsigned v = ptr32[off / 4];

  return __builtin_bswap32(v);
}

static void write32(unsigned off, unsigned val)
{
  val = __builtin_bswap32(val);
  ptr32[off / 4] = val;
}

int
main (int argc, char **argv)
{
  int am = 0x39;
  unsigned block = 0;
  unsigned addr = 0xb00000; /* Block 0 */
  int c;
  char *action = NULL;

  while ((c = getopt (argc, argv, "b:ha:m:")) != -1)
    switch (c)
      {
      case 'b':
	block = atoi(optarg);
	if (block < 0 || block > 1) {
	  fprintf(stderr, "bad block (0 or 1)\n");
	  return 1;
	}
	break;
      case 'm':
	am = strtol (optarg, NULL, 0);
	break;
      case 'a':
	action = optarg;
	break;
      case 'h':
	printf ("usage: %s [-h] [-m AM] [-a ACTION] -s SLOT\n", argv[0]);
	return 0;
      case '?':
	fprintf (stderr, "incorrect option, try %s -h\n", argv[0]);
	return 1;
      }

  /* MAP A24.  */
  addr |= (block << 16);

  data_map.am = am;
  data_map.data_width = 32;
  data_map.sizel = 0x10000;
  data_map.vme_addrl = addr;

  printf ("INFO: Map VME 0x%08x AM 0x%02x\n",
          data_map.vme_addrl, data_map.am);

  /* Do mmap */
  ptr32 = (volatile unsigned int *)vme_map(&data_map, 1);
  if (!ptr32)
    {
      fprintf (stderr, "cannot map vme space\n");
      exit (3);
    }

  if (action == NULL)
    action = "regs";

  if (strcmp (action, "regs") == 0) {
    printf ("control=%02x\n", read32(0x10) & 0xffff);
  }
  else if (strcmp (action, "en-dpram") == 0) {
    unsigned ctrl = read32(0x10) & 0xffff;
    ctrl |= (1 << 2);
    write32(0x10, ctrl);
  }
  else if (strcmp (action, "err") == 0) {
    unsigned se, de, rd = 0;
    unsigned old_se = -1, old_de = -1, old_rd = -1;
    while (1) {
      se = read32(0x40) & 0xffff;
      de = read32(0x44) & 0xffff;
      rd = read32(0x48) & 0xffff;
      if (se == old_se && de == old_de /* && rd == old_rd */) {
	usleep (1000);
	continue;
      }
      printf ("single err: %-5u  double err: %-5u  ready err: %-5u\n",
	      se, de, rd);
      old_se = se;
      old_de = de;
      old_rd = rd;
    }
  }
  else if (strcmp (action, "clear-err") == 0) {
    write32(0x40, 0);
    write32(0x44, 0);
    write32(0x48, 0);
  }
  else if (strcmp (action, "turn") == 0) {
    unsigned tc, old_tc = -1;
    while (1) {
      tc = read32(0x50) & 0xffffff;
      if (tc != old_tc) {
	printf ("turn count: %-6u\n", tc);
	old_tc = tc;
      }
      else
	usleep (1000);
    }
  }
  else if (strcmp (action, "dpram") == 0) {
    unsigned i, j;

    printf ("addr ");
    for (i = 0; i < 16; i++)
      printf ("  %x", i);
    printf ("\n");
    for (i = 0; i < 256; i += 16) {
      printf ("%02x:  ", i);
      for (j = i; j < i + 16; j++)
	printf (" %02x", read32(0x800 + j * 4) & 0xff);
      printf("\n");
    }
  }
  else
    printf ("unknown action\n");

  return 0;
}
