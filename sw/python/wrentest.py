import sys
import ctypes
import time
from wren_mb import *

NS_PER_SEC = 1_000_000_000

def ts_add_ns(l, ns):
    """Add :param ns: nanoseconds to wr_time :param l:"""
    res = struct_wren_ts()
    sec = ns // NS_PER_SEC
    ns -= sec * NS_PER_SEC
    res.sec = l.sec + sec
    res.nsec = l.nsec + ns
    if res.nsec > NS_PER_SEC:
        res.sec += 1
        res.nsec -= NS_PER_SEC
    return res


class WrenBoard:
    def __init__(self, path):
        self.wrenfd = wrenctl_open(path)
        assert(self.wrenfd > 0)

    def get_wr_time(self):
        ts = struct_wren_ts()
        if wren_wren_get_time(self.wrenfd, byref(ts)) < 0:
            raise RuntimeError("cannot get time")
        return ts

    def wait_until(self, ts):
        if wren_wren_wait_until(self.wrenfd, byref(ts)) < 0:
            raise RuntimeError("cannot wait")

    def read_pulser_log(self, idx, num):
        entries = (num * struct_wrenrx_pulser_log_entry)()
        entries_p = ctypes.cast(entries, ctypes.POINTER(struct_wrenrx_pulser_log_entry))
        if wrenctl_rx_read_pulser_log(self.wrenfd, idx, num, entries_p) < 0:
            raise RuntimeError("cannot get pulser log")
        return entries

    def set_comparator(self, idx, cfg):
        if wrenctl_rx_set_comparator(self.wrenfd, idx, byref(cfg)) < 0:
            raise RuntimeError("cannot set comparator")

    def get_comparator_group_status(self, grp):
        busy = uint32_t()
        late = uint32_t()
        if wrenctl_rx_comparator_status(self.wrenfd, grp,
                                        byref(busy), byref(late)) < 0:
            raise RuntimeError("cannot get comparator status")
        return busy.value

    def is_comparator_busy(self, idx):
        busy = self.get_comparator_group_status(idx // 32)
        return ((busy >> (idx & 31)) & 1) == 1

    def get_pulser_group_status(self, grp):
        loaded = uint32_t()
        running = uint32_t()
        if wrenctl_rx_pulsers_status(self.wrenfd, grp, byref(loaded), byref(running)) < 0:
            raise RuntimeError("cannot get pulser status")
        return (loaded.value, running.value)

    def abort_pulser(self, idx):
        if wrenctl_rx_abort_pulsers(self.wrenfd, idx // 8, 1 << (idx & 7)) < 0:
            raise RuntimeError("cannot abort pulser")

    def is_pulser_free(self, idx):
        loaded, running = self.get_pulser_group_status(idx // 8)
        return (((loaded | running) >> (idx & 7)) & 1) == 0

    def disp_log(self, entries):
        sec = entries[0].sec
        for i, e in enumerate(entries):
            if e.sec == 0:
                continue
            print("{:03}: {:4}sec + {:-6}us: {} {} {} {}".format(
                i, e.sec - sec, e.usec,
                "load" if e.load else "    ",
                "start" if e.start else "     ",
                "pulse" if e.pulse else "     ",
                "idle" if e.idle else "    "))


def check_log_pulses(log, npulses):
    """Check :param log: contains a train of :param npulses:"""
    assert log[0].idle == 1
    if log[0].pulse == 0:
        off = 1
    else:
        off = 0
    for i in range(npulses):
        assert log[i + off].pulse == 1
    assert log[npulses].load == 1 or log[npulses+1].load == 1


def test_log(board, pulser_idx, comp_idx):
    assert not board.is_comparator_busy(comp_idx)
    assert board.is_pulser_free(pulser_idx)
    # Get time
    ts = board.get_wr_time()
    # Program pulser for now + 1sec
    ts.sec += 1
    #
    npulses = 5
    cfg = struct_wren_mb_comparator()
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 0
    cfg.trig.width = 20
    cfg.trig.period = 1000  # 1us
    cfg.trig.npulses = npulses
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(comp_idx, cfg)
    # Check comparator status
    assert board.is_comparator_busy(comp_idx)
    # Check pulser status
    assert board.is_pulser_free(pulser_idx)
    # Wait until time
    ts2 = ts_add_ns(ts, (npulses + 2) * 1000)
    board.wait_until(ts)
    # Check comparator status
    assert not board.is_comparator_busy(comp_idx)
    # Check logs
    log = board.read_pulser_log(pulser_idx, npulses + 2)
    board.disp_log(log)
    check_log_pulses(log, npulses)


def test_max_train(board, pulser_idx, comp_idx):
    """Check the maximum pulses train (2**32)"""
    assert not board.is_comparator_busy(comp_idx)
    assert board.is_pulser_free(pulser_idx)
    # Get time
    ts = board.get_wr_time()
    # Program pulser for now + 1sec
    ts.sec += 1
    #
    npulses = 4294967295 # 2**32-1
    cfg = struct_wren_mb_comparator()
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 0
    cfg.trig.width = 8
    cfg.trig.period = 16
    cfg.trig.npulses = npulses
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(comp_idx, cfg)
    # Wait until time
    board.wait_until(ts)
    assert not board.is_comparator_busy(comp_idx)
    assert not board.is_pulser_free(pulser_idx)
    t_ns = npulses * 16 # in ns
    ts2 = ts_add_ns(ts, t_ns)
    print("Long wait ({}s)".format(t_ns // NS_PER_SEC))
    board.wait_until(ts2)
    assert board.is_pulser_free(pulser_idx)

def test_infinite_train(board, pulser_idx, comp_idx):
    """Check the infinite train"""
    assert not board.is_comparator_busy(comp_idx)
    assert board.is_pulser_free(pulser_idx)
    # Get time
    ts = board.get_wr_time()
    # Program pulser for now + 1sec
    ts.sec += 1
    #
    npulses = 4294967295 # 2**32-1
    cfg = struct_wren_mb_comparator()
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 0
    cfg.trig.width = 8
    cfg.trig.period = 16
    cfg.trig.npulses = 0 # Infinite
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(comp_idx, cfg)
    # Wait until time
    board.wait_until(ts)
    assert not board.is_comparator_busy(comp_idx)
    assert not board.is_pulser_free(pulser_idx)
    t_ns = npulses * 16 # in ns
    ts2 = ts_add_ns(ts, t_ns)
    print("Long wait ({}s)".format(t_ns // NS_PER_SEC))
    board.wait_until(ts)
    # Should still be running
    assert not board.is_pulser_free(pulser_idx)
    # Abort by programming a new pulse
    ts2.sec += 1
    cfg.sec = ts2.sec
    cfg.nsec = ts2.nsec
    cfg.npulses = 1
    board.set_comparator(comp_idx, cfg)
    board.wait_until(ts)
    assert board.is_comparator_busy(comp_idx)
    assert board.is_pulser_free(pulser_idx)


def test_start_stop(board, pulser_idx, start_idx, stop_idx):
    """Check start and stop"""
    # Comparators
    pulser_cmp = pulser_idx * 4
    start_cmp = start_idx * 4
    stop_cmp = stop_idx * 4
    assert not board.is_comparator_busy(pulser_cmp)
    assert board.is_pulser_free(pulser_idx)
    assert board.is_pulser_free(start_idx)
    assert board.is_pulser_free(stop_idx)
    # Get time
    ts = board.get_wr_time()
    # Program pulser for now + 1sec
    ts.sec += 1
    #
    npulses = 4294967295 # 2**32-1
    cfg = struct_wren_mb_comparator()
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_PULSER_0 + (start_idx % 8)
    cfg.trig.stop = WRENRX_INPUT_PULSER_0 + (stop_idx % 8)
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 0
    cfg.trig.width = 100
    cfg.trig.period = 1000
    cfg.trig.npulses = 0 # Infinite
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(pulser_cmp, cfg)

    ts_start = ts_add_ns(ts, 12_000)
    cfg.sec = ts_start.sec
    cfg.nsec = ts_start.nsec
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.pulser_idx = start_idx
    cfg.trig.npulses = 1
    board.set_comparator(start_cmp, cfg)

    ts_stop = ts_add_ns(ts, 20_000)
    cfg.sec = ts_stop.sec
    cfg.nsec = ts_stop.nsec
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.pulser_idx = stop_idx
    cfg.trig.npulses = 1
    board.set_comparator(stop_cmp, cfg)

    # Wait until stop
    board.wait_until(ts_stop)
    assert board.is_pulser_free(pulser_idx)
    assert board.is_pulser_free(start_idx)
    assert board.is_pulser_free(stop_idx)

    print("Log of pulser:")
    log = board.read_pulser_log(pulser_idx, 12)
    board.disp_log(log)
    check_log_pulses(log, 8)
    print("Log of start pulser (load@ {} ns):".format(ts_start.nsec))
    log = board.read_pulser_log(start_idx, 4)
    board.disp_log(log)
    check_log_pulses(log, 1)
    print("Log of stop pulser (load@ {} ns):".format(ts_stop.nsec))
    log = board.read_pulser_log(stop_idx, 4)
    board.disp_log(log)
    check_log_pulses(log, 1)


def test_idelay(board, pulser_idx, idelay=15):
    """Check start and stop"""
    # Comparators
    pulser_cmp = (pulser_idx * 4) + 1
    assert not board.is_comparator_busy(pulser_cmp)
    assert board.is_pulser_free(pulser_idx)
    # Get time
    ts = board.get_wr_time()
    # Program pulser for now + 1sec
    ts.sec += 1
    #
    cfg = struct_wren_mb_comparator()
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_CLOCK_1KHZ
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 0
    cfg.trig.width = 16
    cfg.trig.period = 50
    cfg.trig.npulses = 1
    cfg.trig.idelay = idelay * 1000
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(pulser_cmp, cfg)

    # Wait for 1Khz + 15us
    ets = ts_add_ns(ts, 1_000_000 + idelay * 1000)
    board.wait_until(ets)
    assert board.is_pulser_free(pulser_idx)

    log = board.read_pulser_log(pulser_idx, 4)
    board.disp_log(log)
    check_log_pulses(log, 1)
    if log[0].pulse == 1:
        off = 0
    else:
        off = 1
    assert log[off].pulse == 1
    assert log[off + 1].start == 1
    diff = log[off].usec - log[off+1].usec
    assert diff == idelay


def stop_pulser(board, pulser_idx, pulser_comp):
    """Check start and stop"""
    assert not board.is_comparator_busy(pulser_comp)
    cfg = struct_wren_mb_comparator()
    cfg.sec = 0
    cfg.nsec = 0
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 1
    cfg.trig.width = 8
    cfg.trig.period = 16
    cfg.trig.npulses = 1
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(pulser_comp, cfg)

    assert board.is_pulser_free(pulser_idx)


def test_repeat(board, pulser_idx, start_idx, num):
    """Check start and stop"""
    # Comparators
    pulser_cmp = (pulser_idx * 4) + 1
    start_cmp = (start_idx * 4) + 3
    assert not board.is_comparator_busy(pulser_cmp)
    assert not board.is_comparator_busy(start_cmp)
    assert board.is_pulser_free(pulser_idx)
    assert board.is_pulser_free(start_idx)
    # Get time
    ts = board.get_wr_time()
    # Program pulser for now + 1sec
    ts.sec += 1
    #
    cfg = struct_wren_mb_comparator()
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_PULSER_0 + (start_idx % 8)
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 1
    cfg.trig.immediat = 0
    cfg.trig.width = 16
    cfg.trig.period = 1000
    cfg.trig.npulses = 2
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(pulser_cmp, cfg)

    board.wait_until(ts)
    assert not board.is_pulser_free(pulser_idx)

    log = board.read_pulser_log(pulser_idx, 4)
    assert log[0].load == 1
    assert log[0].start == 0

    for i in range(num):
        # Generate a pulse
        ts = ts_add_ns(ts, 500_000_000)
        cfg.sec = ts.sec
        cfg.nsec = ts.nsec
        cfg.trig.start = WRENRX_INPUT_NOSTART
        cfg.trig.stop = WRENRX_INPUT_NOSTOP
        cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
        cfg.trig.pulser_idx = start_idx
        cfg.trig.repeat = 0
        cfg.trig.immediat = 0
        cfg.trig.width = 16
        cfg.trig.period = 50
        cfg.trig.npulses = 1
        cfg.trig.idelay = 0
        cfg.trig.load_off_sec = 0
        cfg.trig.load_off_nsec = 0
        board.set_comparator(start_cmp, cfg)

        board.wait_until(ts)
        log = board.read_pulser_log(pulser_idx, 4 + i * 2)
        print("pulse {}".format(i))
        board.disp_log(log)
        for j in range(0, i, 2):
            assert log[j].pulse == 1
            assert log[j].start == 0
            assert log[j + 1].pulse == 1
            assert log[j + 1].start == 1
    stop_pulser(board, pulser_idx, pulser_cmp)
    assert board.is_pulser_free(pulser_idx)


def test_level(board, pulser_idx, stop_idx):
    """Check start and stop"""
    # Comparators
    pulser_cmp = (pulser_idx * 4) + 1
    stop_cmp = (stop_idx * 4) + 2
    assert not board.is_comparator_busy(pulser_cmp)
    assert not board.is_comparator_busy(stop_cmp)
    assert board.is_pulser_free(pulser_idx)
    assert board.is_pulser_free(stop_idx)
    # Get time
    ts = board.get_wr_time()
    # Program pulser for now + 1sec
    ts.sec += 1
    #
    cfg = struct_wren_mb_comparator()
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_PULSER_0 + (stop_idx % 8)
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 0
    cfg.trig.width = 16
    cfg.trig.period = 0
    cfg.trig.npulses = 0
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(pulser_cmp, cfg)

    board.wait_until(ts)
    assert not board.is_pulser_free(pulser_idx)

    log = board.read_pulser_log(pulser_idx, 4)
    board.disp_log(log)
    assert log[0].pulse == 1
    assert log[0].load == 1 or log[1].load == 1

    # Generate a pulse
    ts.sec += 1
    cfg.sec = ts.sec
    cfg.nsec = ts.nsec
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = stop_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 0
    cfg.trig.width = 16
    cfg.trig.period = 50
    cfg.trig.npulses = 1
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(stop_cmp, cfg)

    board.wait_until(ts)
    log = board.read_pulser_log(pulser_idx, 4)
    board.disp_log(log)
    check_log_pulses(log, 1)


def test_abort_pulser(board, pulser_idx, comp_idx):
    """Check the infinite train"""
    assert not board.is_comparator_busy(comp_idx)
    assert board.is_pulser_free(pulser_idx)
    npulses = 4294967295 # 2**32-1
    cfg = struct_wren_mb_comparator()
    cfg.sec = 0
    cfg.nsec = 0
    cfg.trig.start = WRENRX_INPUT_NOSTART
    cfg.trig.stop = WRENRX_INPUT_NOSTOP
    cfg.trig.clock = WRENRX_INPUT_CLOCK_1GHZ
    cfg.trig.pulser_idx = pulser_idx
    cfg.trig.repeat = 0
    cfg.trig.immediat = 1
    cfg.trig.width = 8
    cfg.trig.period = 16
    cfg.trig.npulses = 0 # Infinite
    cfg.trig.idelay = 0
    cfg.trig.load_off_sec = 0
    cfg.trig.load_off_nsec = 0
    board.set_comparator(comp_idx, cfg)
    assert not board.is_comparator_busy(comp_idx)
    assert not board.is_pulser_free(pulser_idx)

    board.abort_pulser(pulser_idx)
    assert board.is_pulser_free(pulser_idx)


def disp_time(ts):
    print("Now is: {} + {}ns".format(time.asctime(time.gmtime(ts.sec)),
                                     ts.nsec))

b = WrenBoard("/dev/wren0")
disp_time(b.get_wr_time())

tests='*'
if len(sys.argv) > 1:
    tests = sys.argv[1:]

if 'start-stop' in tests or tests == '*':
    print("Test start-stop")
    test_start_stop(b, 1, 2, 3)

if 'idelay' in tests or tests == '*':
    print("Test idelay")
    test_idelay(b, 0)

if 'repeat' in tests or tests == '*':
    print("Test repeat")
    test_repeat(b, 5, 2, 3)

if 'infinite' in tests or tests == '*':
    print("Test maximum train")
    test_max_train(b, 0, 7)
    print("Test infinite train")
    test_max_train(b, 4, 16)

if 'level' in tests or tests == '*':
    print("Test level")
    test_level(b, 6, 5)

if 'log' in tests or tests == '*':
    for pulser in range(8):
        comp = pulser * 4
        print("test pulser {} using comparator {}".format(pulser, comp))
        test_log(b, pulser, comp)

if 'abort-pulser' in tests or tests == '*':
    print("Test pulser abort")
    test_abort_pulser(b, 1, 2)

#test_log(b, 7, 28)
#test_log(b, 0, 5)

if tests == '*':
    print("Done and OK")
