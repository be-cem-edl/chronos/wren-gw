#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <elf.h>
#include <time.h>

#include "host_map.h"
#include "wren-mb-defs.h"

#include "wren-ioctl.h"

// #include "pspcie-dma.h"

const char *progname;

struct mb_metadata {
  uint32_t cmd;
  uint32_t len;
  uint32_t addr;
  uint32_t data0;
};

void *open_zynq(unsigned bus, unsigned dev, unsigned fn)
{
	char path[1024];
	void *ptr;
	int fd;

	snprintf (path, sizeof(path),
		  "/sys/bus/pci/devices/0000:%02x:%02x.%u/resource1",
		  bus, dev, fn);

        fd = open(path, O_RDWR | O_SYNC);
	if (fd < 0) {
	  fprintf (stderr, "cannot open %s: %m\n", path);
	  return NULL;
	}

        ptr = mmap(NULL, 1 << 16, PROT_READ | PROT_WRITE,
		   MAP_SHARED, fd, 0);

	if (ptr == MAP_FAILED) {
	  fprintf (stderr, "cannot map %s: %m\n", path);
	  close (fd);
	  return NULL;
	}

	close (fd);

	return ptr;
}

static unsigned read_le16(unsigned char *p)
{
	return (p[1] << 8) | p[0];
}

static unsigned read_le32(unsigned char *p)
{
	return (p[3] << 16) | (p[2] << 16) | (p[1] << 8) | p[0];
}

static int
show_zynq_config(unsigned bus, unsigned dev, unsigned fn)
{
	char path[1024];
	int fd;
	unsigned char buf[64];
	unsigned off;

	snprintf (path, sizeof(path),
		  "/sys/bus/pci/devices/0000:%02x:%02x.%u/config",
		  bus, dev, fn);

        fd = open(path, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "cannot open %s: %m\n", path);
		return -1;
	}

	if (read(fd, buf, sizeof (buf)) != sizeof(buf)) {
		fprintf(stderr, "cannot read config\n");
		close (fd);
		return -1;
	}
	printf ("vendor id: 0x%04x, device id: 0x%04x\n",
		read_le16 (buf + 0), read_le16 (buf + 2));
	printf ("command:   0x%04x, status:    0x%04x\n",
		read_le16 (buf + 4), read_le16 (buf + 6));
	printf ("class:   0x%06x, rev id:    0x%02x\n",
		read_le32(buf + 8) & 0xffffff, buf[8]);
	printf ("bist:        0x%02x, header     0x%02x, "
		"lat: %02x, cache: %02x\n",
		buf[15], buf[14], buf[13], buf[12]);
	printf ("bar0: 0x%08x\n", read_le32 (buf + 0x10));
	printf ("bar1: 0x%08x\n", read_le32 (buf + 0x14));
	printf ("bar2: 0x%08x\n", read_le32 (buf + 0x18));
	printf ("bar3: 0x%08x\n", read_le32 (buf + 0x1c));
	printf ("bar4: 0x%08x\n", read_le32 (buf + 0x20));
	printf ("bar5: 0x%08x\n", read_le32 (buf + 0x24));
	printf ("cis:  0x%08x\n", read_le32 (buf + 0x28));
	printf ("subsys vid: 0x%04x, subsys did: 0x%04x\n",
		read_le16(buf + 0x2c), read_le16(buf + 0x2e));
	printf ("rom:  0x%08x\n", read_le32 (buf + 0x30));
	printf ("cap ptr: 0x%02x\n", buf[0x34]);
	printf ("max lat: 0x%02x, min gnt:   0x%02x, "
		"int pin: %02x, int line: %02x\n",
		buf[0x3f], buf[0x3e], buf[0x3d], buf[0x3c]);
	off = buf[0x34];
	if (off != 0) {
		printf ("capabilities:\n");
		while (1) {
			if (pread(fd, buf, 2, off) != 2) {
				fprintf (stderr, "pread error: %m\n");
				break;
			}
			printf (" Cap at 0x%04x: id: 0x%02x, next: 0x%02x\n",
				off, buf[0], buf[1]);
			off = buf[1];
			if (off == 0)
				break;
		}
	}

	printf ("Extended capabilities:\n");
	off = 0x100;
	while (1) {
		unsigned v;
		if (pread(fd, buf, 4, off) != 4) {
			fprintf (stderr, "pread error: %m\n");
			break;
		}
		v = read_le32 (buf);
		printf (" Cap at 0x%04x: id: 0x%04x, ver: %x, next: 0x%03x\n",
			off, v & 0xffff, (v >> 16) & 0x0f, (v >> 20));
		off = v >> 20;
		if (off == 0)
			break;
	}

	close (fd);

	return 0;
}

static void
show_pspcie_dma(unsigned bus, unsigned dev, unsigned fn)
{
	char path[1024];
	void *ptr;
	int fd;

	snprintf (path, sizeof(path),
		  "/sys/bus/pci/devices/0000:%02x:%02x.%u/resource1",
		  bus, dev, fn);

        fd = open(path, O_RDWR | O_SYNC);
	if (fd < 0) {
	  fprintf (stderr, "cannot open %s: %m\n", path);
	  return;
	}

        ptr = mmap(NULL, 1 << 16, PROT_READ | PROT_WRITE,
		   MAP_SHARED, fd, 0);

	if (ptr == MAP_FAILED) {
	  fprintf (stderr, "cannot map %s: %m\n", path);
	  close (fd);
	  return;
	}

	close (fd);

#if 0
	printf ("scratch0:  %08x\n",
		*(uint32_t*)(ptr + DMA_SCRATCH0_REG_OFFSET));
	printf ("scratch1:  %08x\n",
		*(uint32_t*)(ptr + DMA_SCRATCH0_REG_OFFSET + 4));
	printf ("scratch2:  %08x\n",
		*(uint32_t*)(ptr + DMA_SCRATCH0_REG_OFFSET + 8));
	printf ("scratch3:  %08x\n",
		*(uint32_t*)(ptr + DMA_SCRATCH0_REG_OFFSET + 12));
#endif
	munmap (ptr, 1<<16);

	return;
}

static volatile struct host_map *regs;
static int wren_fd = -1;

static int ps7_wait(void)
{
  while (regs->mb_h2b_host.csr & MAILBOX_OUT_REGS_MAP_CSR_READY)
    usleep(10);

  return 0;
}  

static void pci_write_mb(struct mb_metadata *md, uint32_t *data)
{
  unsigned i;

  for (i = 0; i < md->len; i++)
    regs->mem_h2b_host[i].data = data[i];

  regs->mb_h2b_host.cmd = md->cmd;
  regs->mb_h2b_host.len = md->len;

  regs->mb_h2b_host.csr = MAILBOX_OUT_REGS_MAP_CSR_READY;

  ps7_wait();
}

static void write_mb(struct mb_metadata *md, uint32_t *data)
{
  if (regs)
    pci_write_mb(md, data);
  else {
    uint32_t msg[1028];
    unsigned i;
    int res;

    if (md->len > 1024) {
      fprintf(stderr, "write_mb: message too long (%u)\n", md->len);
      return;
    }

    msg[0] = md->cmd;
    msg[1] = md->len;
    msg[2] = md->addr;
    msg[3] = md->data0;
    for (i = 0; i < md->len; i++)
      msg[4 + i] = data[i];
    while (1) {
      res = write(wren_fd, msg, 16 + md->len * 4);
      if (res > 0)
	break;
      if (errno != EWOULDBLOCK) {
	fprintf (stderr, "write_mb: %m\n");
	return;
      }
    }
  }
}

static int ps7_cmd_write(unsigned *buf, unsigned len, unsigned addr)
{
  struct mb_metadata md;

  len = (len + 3) / 4;

  md.cmd = CMD_WRITE;
  md.len = len;
  md.addr = addr;
  md.data0 = 0;

  write_mb(&md, (uint32_t *)buf);

  return 0;
}

static int ps7_cmd_exec(unsigned addr)
{
  struct mb_metadata md;

  md.cmd = CMD_EXEC;
  md.len = 0;
  md.addr = addr;
  md.data0 = 0;

  write_mb(&md, NULL);

  return 0;
}

#define CHECK_DIRECT_ACCESS() \
  do { \
    if (regs == NULL) {					     \
      printf("command only available using direct acccess\n");	\
      return 0;							\
    }								\
  } while (0)

static int
do_version (char *argv[], int argc)
{
  CHECK_DIRECT_ACCESS();

  printf ("Version: %x\n", regs->version);
  return 0;
}

static int
do_ping (char *argv[], int argc)
{
  uint32_t data = 0xabba8567;
  struct mb_metadata md;

  md.cmd = CMD_PING;
  md.len = 1;
  md.addr = 0;
  md.data0 = 0x12345678;

  write_mb(&md, &data);
  return 0;
}

static int
do_csr (char *argv[], int argc)
{
  CHECK_DIRECT_ACCESS();

  printf ("h2b (host): %08x\n", regs->mb_h2b_host.csr);
  printf ("b2h (host): %08x\n", regs->mb_b2h_host.csr);
  return 0;
}

static int
do_h2b (char *argv[], int argc)
{
  unsigned val;

  CHECK_DIRECT_ACCESS();

  if (argc == 1) {
    printf ("h2b.csr:   %08x\n", regs->mb_h2b_host.csr);
    printf ("h2b.cmd:   %08x\n", regs->mb_h2b_host.cmd);
    printf ("h2b.len:   %08x\n", regs->mb_h2b_host.len);
    return 0;
  }

  if (argc > 2)
    val = strtoul(argv[2], NULL, 0);
  else
    val = 0;

  if (strcmp (argv[1], "csr") == 0) {
    if (argc > 2)
      regs->mb_h2b_host.csr = val;
    else
      printf ("h2b.csr:   %08x\n", regs->mb_h2b_host.csr);
  }
  else if (strcmp (argv[1], "cmd") == 0) {
    if (argc > 2)
      regs->mb_h2b_host.cmd = val;
    else
      printf ("h2b.cmd:   %08x\n", regs->mb_h2b_host.cmd);
  }
  else if (strcmp (argv[1], "len") == 0) {
    if (argc > 2)
      regs->mb_h2b_host.len = val;
    else
      printf ("h2b.len:   %08x\n", regs->mb_h2b_host.len);
  }
  else {
    printf ("unknown mailbox register %s\n", argv[1]);
    return -1;
  }
    
  if (argc >= 2)
    return 2;
  else
    return 1;
}

static int
do_mem_b2h (char *argv[], int argc)
{
  unsigned i;

  CHECK_DIRECT_ACCESS();

  for (i = 0; i < 128; i++) {
    if (i % 4 == 0)
      printf ("%04x:", i);
    printf (" %08x", regs->mem_b2h_host[i].data);
    if (i % 4 == 3)
      printf ("\n");
  }
  return 0;
}

static int
do_mem_h2b (char *argv[], int argc)
{
  unsigned i;

  CHECK_DIRECT_ACCESS();

  for (i = 0; i < 128; i++) {
    if (i % 4 == 0)
      printf ("%04x:", i);
    printf (" %08x", regs->mem_h2b_host[i].data);
    if (i % 4 == 3)
      printf ("\n");
  }
  return 0;
}

static Elf32_Half read_elf_half (const Elf32_Half *v)
{
  const unsigned char *p = (const unsigned char *)v;
  return p[0] | (p[1] << 8);   /* LE  */
}

static Elf32_Word read_elf_word (const Elf32_Word *v)
{
  const unsigned char *p = (const unsigned char *)v;
  return p[0] | (p[1] << 8) | (p[2] << 16) | (p[3] << 24);   /* LE  */
}

static int load_elf (const char *filename)
{
  FILE *f;
  Elf32_Ehdr ehdr;
  unsigned poff;
  Elf32_Phdr phdr;
  unsigned filesz;
  unsigned memsz;
  unsigned entry;
  unsigned phnum;
  unsigned i;

  f = fopen (filename, "r");
  if (f == NULL) {
    fprintf (stderr, "cannot open %s\n", filename);
    goto err;
  }

  if (fread (&ehdr, sizeof (ehdr), 1, f) != 1) {
    fprintf (stderr, "cannot read ELF header of %s\n", filename);
    goto err;
  }
  if (ehdr.e_ident[EI_MAG0] != ELFMAG0
      || ehdr.e_ident[EI_MAG1] != ELFMAG1
      || ehdr.e_ident[EI_MAG2] != ELFMAG2
      || ehdr.e_ident[EI_MAG3] != ELFMAG3) {
    fprintf (stderr, "file %s is not an ELF file\n", filename);
    goto err;
  }

  if (ehdr.e_ident[EI_CLASS] != ELFCLASS32
      || ehdr.e_ident[EI_DATA] != ELFDATA2LSB
      || ehdr.e_ident[EI_VERSION] != EV_CURRENT) {
    fprintf (stderr, "file %s is not expect ELF class\n", filename);
    goto err;
  }

  if (read_elf_half (&ehdr.e_type) != ET_EXEC
      || read_elf_half (&ehdr.e_machine) != EM_ARM
      || read_elf_word (&ehdr.e_version) != EV_CURRENT) {
    fprintf (stderr, "file %s is not an ARM executable\n", filename);
    goto err;
  }

  if (read_elf_half (&ehdr.e_phentsize) != sizeof (Elf32_Phdr)) {
    fprintf (stderr, "file %s has incorrect phentsize\n", filename);
    goto err;
  }

  phnum = read_elf_half (&ehdr.e_phnum);
  entry = read_elf_word (&ehdr.e_entry);
  poff = read_elf_word (&ehdr.e_phoff);

  for (i = 0; i < phnum; i++) {
    unsigned vaddr;
    unsigned buf[4096 / 4];

    if (fseek (f, poff + i * sizeof (Elf32_Phdr), SEEK_SET) != 0
	|| fread (&phdr, sizeof (phdr), 1, f) != 1) {
      fprintf (stderr, "%s: cannot read program header\n", filename);
      goto err;
    }
    if (read_elf_word (&phdr.p_type) != PT_LOAD) {
      fprintf (stderr, "%s: incorrect program header type\n", filename);
      goto err;
    }
    vaddr = read_elf_word (&phdr.p_vaddr);
    memsz = read_elf_word (&phdr.p_memsz);
    memsz = (memsz + 3) & ~3;
    filesz = read_elf_word (&phdr.p_filesz);
    if (fseek (f, read_elf_word (&phdr.p_offset), SEEK_SET) != 0) {
      fprintf (stderr, "%s: cannot read program\n", filename);
      goto err;
    }

    while (memsz > 0) {
      unsigned l = filesz > 4096 ? 4096 : filesz;

      if (filesz > 0) {
	l = filesz > 4096 ? 4096 : filesz;
	if (fread (buf, l, 1, f) != 1) {
	  fprintf (stderr, "%s: cannot read program\n", filename);
	  goto err;
	}
	filesz -= l;
      }
      else {
	l = memsz > 4096 ? 4096 : memsz;
	memset (buf, 0, l);
      }

      if (ps7_cmd_write (buf, l, vaddr) != 0) {
	fprintf (stderr, "%s: cannot write program\n", filename);
	goto err;
      }

      memsz -= l;
      vaddr += l;
    }
  }

  fclose (f);

  if (ps7_cmd_exec (entry) != 0) {
    fprintf (stderr, "%s: cannot exec program\n", filename);
    return -1;
  }

  return 0;

 err:
  fclose (f);
  return -1;
}

static int
do_load (char *argv[], int argc)
{
  if (argc < 1) {
    fprintf (stderr, "%s: missing program name\n", argv[0]);
    return -1;
  }

  if (load_elf (argv[1]) < 0)
    return -1;
  return 1;
}

static int
do_vuart (char *argv[], int argc)
{
#if 0
	while (1) {
		unsigned v = regs->vuart_data;
		if (v & CPU_MAP_VUART_DATA_RDY) {
			putchar (v & 0xff);
			fflush (stdout);
		}
		else
			usleep (10000);
	}
#endif
	return 0;
}

static int
do_link (char *argv[], int argc)
{
  uint32_t v;

  if (ioctl(wren_fd, WREN_IOC_GET_LINK, &v) < 0) {
      fprintf(stderr, "ioctl get_link: %m\n");
      return -1;
  }
  printf("link status: %u\n", v);

  return 0;
}

static int
do_time (char *argv[], int argc)
{
    struct wren_wr_time res;

    if (ioctl(wren_fd, WREN_IOC_GET_TIME, &res) < 0) {
	fprintf(stderr, "ioctl get_time: %m\n");
	return -1;
    }
    else {
	unsigned long long tai = 
	    (((unsigned long long)res.tai_hi) << 32) | res.tai_lo;
	time_t t;
	printf("TAI: %llu + %u ns\n", tai, (unsigned) res.ns);
	t = tai;
	printf("TAI: %s", asctime(gmtime(&t)));
    }

  return 0;
}

static int do_help(char *argv[], int argc);

struct command_t {
  const char *name;
  /* Return the number of args consumed, argv[0] is the command name.  */
  int (*func)(char *argv[], int argc);
  const char *help;
};

static const struct command_t commands[] = {
  {"version", do_version, "print version" },
  {"ping", do_ping, "ping the board" },
  {"csr", do_csr, "print mailbox csr" },
  {"mem-b2h", do_mem_b2h, "print b2h memory (host)" },
  {"mem-h2b", do_mem_h2b, "print h2b memory (board)" },
  {"h2b", do_h2b, "print h2b registers" },
  {"link", do_link, "disp wr link status" },
  {"time", do_time, "disp wr time" },
  {"load", do_load, "load and execute program" },
  {"vuart", do_vuart, "print vuart output" },
  {"help", do_help, "print this help" },
  {NULL, NULL, NULL}
};

static int do_help(char *argv[], int argc)
{
  unsigned i;

  for (i = 0; commands[i].name; i++)
    printf ("%-10s  %s\n", commands[i].name, commands[i].help);

  return 0;
}

static void
usage(void)
{
  printf ("usage: %s -s BUS:DEV.FN | -d /dev/wrenX\n", progname);
}

int
main(int argc, char **argv)
{
  char *slot;
  char *drv;
  int i;
  unsigned verbose;
  unsigned optind;
  unsigned flag_dma;

  progname = argv[0];
  slot = NULL;
  drv = NULL;
  verbose = 0;
  flag_dma = 0;

  /* Simple option decoding. */
  for (i = 1; i < argc; i++) {
    if (argv[i][0] != '-')
      break;
    if (strcmp (argv[i], "-s") == 0 && i < argc + 1) {
      slot = argv[i + 1];
      i++;
    }
    else if (strcmp (argv[i], "-d") == 0 && i < argc + 1) {
      drv = argv[i + 1];
      i++;
    }
    else if (strcmp (argv[i], "-v") == 0)
      verbose++;
    else if (strcmp (argv[i], "--dma") == 0)
      flag_dma++;
    else {
      usage();
      return 2;
    }
  }
  optind = i;
   
  if (slot == NULL && drv == NULL) {
    usage();
    return 2;
  }

  if (slot != NULL)
    {
      unsigned bus, dev, fn;
      if (sscanf(slot, "%x:%x.%u", &bus, &dev, &fn) != 3) {
	usage();
	return 2;
      }
      if (verbose)
	show_zynq_config(bus, dev, fn);

      if (flag_dma)
	show_pspcie_dma(bus, dev, fn);

      regs = open_zynq(bus, dev, fn);
      if (regs == NULL) {
	fprintf (stderr, "cannot map device: %m\n");
	return 2;
      }
    }
  else if (drv != NULL)
    {
      wren_fd = open(drv, O_RDWR);
      if (wren_fd < 0) {
	fprintf (stderr, "cannot open %s: %m\n", drv);
	return 2;
      }
    }

  if (optind == argc)
    do_version(NULL, 0);
  else
    for (i = optind; i < argc; i++) {
      const char *argv0 = argv[i];
      const struct command_t *cmd;
      for (cmd = commands; cmd->name; cmd++)
	if (strcmp (argv0, cmd->name) == 0)
	  break;

      if (cmd->name == NULL) {
	fprintf(stderr, "unknown command %s, try help\n", argv0);
	exit (1);
      }
      else {
	int res = cmd->func (argv + i, argc - i);
	if (res < 0) {
	  fprintf(stderr, "error in command %s\n", argv0);
	  exit (1);
	}
	i += res;
      }
  }

  return 0;
}
