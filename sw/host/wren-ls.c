#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include "host_map.h"
#include "versions.h"

const char *progname;

struct mb_metadata {
  uint32_t cmd;
  uint32_t len;
  uint32_t addr;
  uint32_t data0;
};

void *open_zynq(unsigned bus, unsigned dev, unsigned fn)
{
	char path[1024];
	void *ptr;
	int fd;

	snprintf (path, sizeof(path),
		  "/sys/bus/pci/devices/0000:%02x:%02x.%u/resource1",
		  bus, dev, fn);

        fd = open(path, O_RDWR | O_SYNC);
	if (fd < 0) {
	  fprintf (stderr, "cannot open %s: %m\n", path);
	  return NULL;
	}

        ptr = mmap(NULL, 1 << 16, PROT_READ | PROT_WRITE,
		   MAP_SHARED, fd, 0);

	if (ptr == MAP_FAILED) {
	  fprintf (stderr, "cannot map %s: %m\n", path);
	  close (fd);
	  return NULL;
	}

	close (fd);

	return ptr;
}

static int
compare_file(const char *file_template, const char *slot,
		 const char *content)
{
    char filename[64];
    int fd;
    char buf[32];
    int len;

    snprintf(filename, sizeof(filename), file_template, slot);
    fd = open(filename, O_RDONLY);
    if (fd < 0) {
	fprintf(stderr, "%s: cannot open '%s': %m\n", progname, filename);
	fprintf(stderr, "no board in the slot, or board not set up\n");
	return 2;
    }

    len = read(fd, buf, sizeof(buf));
    if (len <= 0 || len >= sizeof(buf)) {
	fprintf(stderr, "%s: cannot read content of '%s'\n",
		progname, filename);
	close(fd);
	return 3;
    }
    buf[len - 1] = 0;
    if (strcmp(buf, content) != 0) {
	fprintf(stderr, "%s: unexpected id in %s\n", progname, filename);
	fprintf(stderr, "%s: read '%s', expect '%s'\n", progname, buf, content);
	close(fd);
	return 3;
    }
    close(fd);

    return 0;
}

static int
driver_version (const char *template, const char *slot)
{
    char filename[64];
    int fd;
    void *map;
    struct host_map *regs;

    snprintf(filename, sizeof(filename), template, slot);
    fd = open(filename, O_RDONLY);
    if (fd < 0) {
	fprintf(stderr, "%s: cannot open '%s': %m\n", progname, filename);
	return 2;
    }

    map = mmap(NULL, 4096, PROT_READ, MAP_SHARED, fd, 0);
    if (map == MAP_FAILED) {
	fprintf(stderr, "%s: cannot map '%s': %m\n", progname, filename);
	close(fd);
	return 2;
    }
    close(fd);

    regs = (struct host_map *)map;
    if (regs->ident != WREN_HW_IDENT) {
	fprintf(stderr, "%s: incorrect ident: read 0x%08x, expect 0x%08x\n",
		progname, regs->ident, WREN_HW_IDENT);
	return 2;
    }
    printf("%08x\n", regs->fw_version);
    return 0;
}

static int
do_driver_version_vme(const char *slot)
{
    int res;
    
    /* Check manufacturer id */
    res = compare_file("/sys/bus/vme/devices/vme.%s/vendor", slot,
			   "0x00080030");
    if (res != 0)
	return res;

    /* Check device id */
    res = compare_file("/sys/bus/vme/devices/vme.%s/device", slot,
			   "0x000001dc");
    if (res != 0)
	return res;
    
    return driver_version("/sys/bus/vme/devices/vme.%s/resource1", slot);
}

static int
do_driver_version_pcie(const char *slot)
{
    int res;
    
    /* Check manufacturer id */
    res = compare_file("/sys/bus/pci/devices/0000:%s/vendor", slot,
		       "0x10dc");
    if (res != 0)
	return res;

    /* Check device id */
    res = compare_file("/sys/bus/pci/devices/0000:%s/device", slot,
		       "0x0455");
    if (res != 0)
	return res;

    return driver_version ("/sys/bus/pci/devices/0000:%s/resource1", slot);
}

static void
usage(void)
{
    printf("usage: %s CMD\n"
	   "  driver-version vme SLOT\n"
	   "  driver-version pcie SLOT\n", progname);
}

int
main(int argc, char **argv)
{
    progname = argv[0];

    if (argc == 1) {
	printf("%s: no command, try --help\n", progname);
	return 1;
    }

    if (strcmp(argv[1], "--help") == 0
	|| strcmp(argv[1], "-h") == 0) {
	usage();
	return 0;
    }

    if (strcmp(argv[1], "driver-version") == 0) {
	if (argc != 4) {
	    printf("%s: missing BUS and SLOT (try --help)\n", progname);
	    return 1;
	}
	if (strcmp(argv[2], "vme") == 0)
	    return do_driver_version_vme(argv[3]);
	else if (strcmp(argv[2], "pcie") == 0)
	    return do_driver_version_pcie(argv[3]);
	else {
	    printf("%s: unknown bus '%s', expect 'vme' or 'pcie'\n",
		   progname, argv[2]);
	    return 1;
	}
    }

    printf("%s: unknown command '%s', try --help\n", progname, argv[1]);
    return 1;
}
