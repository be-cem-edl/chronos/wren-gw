#ifndef __CHEBY__BOARD_MAP__H__
#define __CHEBY__BOARD_MAP__H__


#include "si5340_ctrl_map.h"
#include "pulser_group_map.h"
#define BOARD_MAP_SIZE 131072 /* 0x20000 = 128KB */

/* Map version */
#define BOARD_MAP_MAP_VERSION 0x0UL

/* Model id */
#define BOARD_MAP_MODEL_ID 0x4UL

/* Firmware version */
#define BOARD_MAP_FW_VERSION 0x8UL

/* WhiteRabbit status */
#define BOARD_MAP_WR_STATE 0xcUL
#define BOARD_MAP_WR_STATE_LINK_UP 0x1UL
#define BOARD_MAP_WR_STATE_TIME_VALID 0x2UL

/* TAI (seconds) lo part */
#define BOARD_MAP_TM_TAI_LO 0x10UL

/* TAI (seconds) hi part */
#define BOARD_MAP_TM_TAI_HI 0x14UL

/* TAI cycles */
#define BOARD_MAP_TM_CYCLES 0x18UL

/* interrupt force */
#define BOARD_MAP_IFR 0x1cUL
#define BOARD_MAP_IFR_MSG 0x8UL
#define BOARD_MAP_IFR_ASYNC 0x10UL

/* Front panel 50ohm termination enable */
#define BOARD_MAP_FP_TERM 0x20UL

/* Front panel input polarity inversion */
#define BOARD_MAP_FP_INV 0x24UL

/* Front panel output enable */
#define BOARD_MAP_FP_OE 0x28UL

/* Invert userio inputs */
#define BOARD_MAP_USERIO_INV 0x2cUL

/* Value of userio inputs */
#define BOARD_MAP_USERIO_IN 0x30UL

/* Control userio (extra inputs and outputs) */
#define BOARD_MAP_USERIO_CFG 0x34UL
#define BOARD_MAP_USERIO_CFG_DIR_MASK 0xfUL
#define BOARD_MAP_USERIO_CFG_DIR_SHIFT 0
#define BOARD_MAP_USERIO_CFG_EN_MASK 0xf0UL
#define BOARD_MAP_USERIO_CFG_EN_SHIFT 4
#define BOARD_MAP_USERIO_CFG_PP_EN 0x100UL

/* Raw pulsers status (WTC) */
#define BOARD_MAP_PULSERS_RAW 0x38UL

/* Pulsers status (WTC) */
#define BOARD_MAP_PULSERS_INT 0x3cUL

/* Mask for pulsers status */
#define BOARD_MAP_PULSERS_MASK 0x40UL

/* Switches, id and geographical address */
#define BOARD_MAP_BOARD_ID 0x44UL
#define BOARD_MAP_BOARD_ID_SWITCHES_MASK 0xffUL
#define BOARD_MAP_BOARD_ID_SWITCHES_SHIFT 0
#define BOARD_MAP_BOARD_ID_FP_ID_MASK 0x700UL
#define BOARD_MAP_BOARD_ID_FP_ID_SHIFT 8
#define BOARD_MAP_BOARD_ID_PP_ID_MASK 0x7000UL
#define BOARD_MAP_BOARD_ID_PP_ID_SHIFT 12
#define BOARD_MAP_BOARD_ID_VME_GA_MASK 0x3f0000UL
#define BOARD_MAP_BOARD_ID_VME_GA_SHIFT 16

/* mux for PXIe DSTAR-C lines */
#define BOARD_MAP_PXIE_DSTARC 0x48UL
#define BOARD_MAP_PXIE_DSTARC_MUX_MASK 0xffUL
#define BOARD_MAP_PXIE_DSTARC_MUX_SHIFT 0

/* mux for PXIe DSTAR-A lines */
#define BOARD_MAP_PXIE_DSTARA 0x4cUL
#define BOARD_MAP_PXIE_DSTARA_MUX_MASK 0x7UL
#define BOARD_MAP_PXIE_DSTARA_MUX_SHIFT 0

/* WR Fabric demux fifo level (to observe) */
#define BOARD_MAP_NIC_FIFO 0x50UL
#define BOARD_MAP_NIC_FIFO_MAX_COUNT_MASK 0xffffUL
#define BOARD_MAP_NIC_FIFO_MAX_COUNT_SHIFT 0
#define BOARD_MAP_NIC_FIFO_FULL 0x10000UL

/* (comment missing) */
#define BOARD_MAP_SI5340 0xd00UL
#define ADDR_MASK_BOARD_MAP_SI5340 0x1ff80UL
#define BOARD_MAP_SI5340_SIZE 128 /* 0x80 */

/* (comment missing) */
#define BOARD_MAP_LEDS 0xe00UL
#define BOARD_MAP_LEDS_SIZE 512 /* 0x200 */

/* Set when leds are controlled by the colors memory */
#define BOARD_MAP_LEDS_FORCE 0xe00UL

/* Led color for an active output */
#define BOARD_MAP_LEDS_RGB_OUT_HI 0xe04UL
#define BOARD_MAP_LEDS_RGB_OUT_HI_PRESET 0xffa500UL

/* Led color for an inactive output */
#define BOARD_MAP_LEDS_RGB_OUT_LO 0xe08UL
#define BOARD_MAP_LEDS_RGB_OUT_LO_PRESET 0x30200UL

/* Led color for an active input */
#define BOARD_MAP_LEDS_RGB_IN_HI 0xe0cUL
#define BOARD_MAP_LEDS_RGB_IN_HI_PRESET 0x4b0082UL

/* Led color for an inactive input */
#define BOARD_MAP_LEDS_RGB_IN_LO 0xe10UL
#define BOARD_MAP_LEDS_RGB_IN_LO_PRESET 0x10002UL

/* Colors of the leds when forced */
#define BOARD_MAP_LEDS_COLORS 0xf00UL
#define BOARD_MAP_LEDS_COLORS_SIZE 4 /* 0x4 */

/* (comment missing) */
#define BOARD_MAP_LEDS_COLORS_RGB 0x0UL

/* (comment missing) */
#define BOARD_MAP_RF 0x1000UL
#define BOARD_MAP_RF_SIZE 512 /* 0x200 */

/* (comment missing) */
#define BOARD_MAP_RF_FTW_HI 0x0UL

/* (comment missing) */
#define BOARD_MAP_RF_FTW_LO 0x4UL

/* (comment missing) */
#define BOARD_MAP_RF_FTW_LOAD 0x8UL
#define BOARD_MAP_RF_FTW_LOAD_EN 0x1UL
#define BOARD_MAP_RF_FTW_LOAD_RESET 0x2UL
#define BOARD_MAP_RF_FTW_LOAD_RFFRAME 0x4UL

/* (comment missing) */
#define BOARD_MAP_RF_BST_CTRL 0xcUL
#define BOARD_MAP_RF_BST_CTRL_EN 0x1UL

/* (comment missing) */
#define BOARD_MAP_RF_H1 0x10UL

/* (comment missing) */
#define BOARD_MAP_RF_GTH_CFG 0x14UL

/* (comment missing) */
#define BOARD_MAP_RF_GTH_STA 0x18UL

/* (comment missing) */
#define BOARD_MAP_RF_RF_FTW 0x1cUL
#define BOARD_MAP_RF_RF_FTW_HI_MASK 0xffffUL
#define BOARD_MAP_RF_RF_FTW_HI_SHIFT 0
#define BOARD_MAP_RF_RF_FTW_RST 0x10000UL

/* (comment missing) */
#define BOARD_MAP_RF_RF_FTW_LO 0x20UL

/* (comment missing) */
#define BOARD_MAP_RF_RF_RX_TAI 0x24UL

/* (comment missing) */
#define BOARD_MAP_RF_RF_RX_CYC 0x28UL

/* (comment missing) */
#define BOARD_MAP_RF_RF_RX_STA 0x2cUL
#define BOARD_MAP_RF_RF_RX_STA_VALID 0x1UL

/* (comment missing) */
#define BOARD_MAP_RF_STREAMER 0x100UL
#define ADDR_MASK_BOARD_MAP_RF_STREAMER 0x700UL
#define BOARD_MAP_RF_STREAMER_SIZE 256 /* 0x100 */

/* Time stamps of rising and falling input edges */
#define BOARD_MAP_INPUTS 0x2000UL
#define BOARD_MAP_INPUTS_SIZE 4096 /* 0x1000 = 4KB */

/* Rise bit summary (one bit for each rise_sr register) */
#define BOARD_MAP_INPUTS_SUMMARY 0x2000UL
#define BOARD_MAP_INPUTS_SUMMARY_RISE_MASK 0x7UL
#define BOARD_MAP_INPUTS_SUMMARY_RISE_SHIFT 0
#define BOARD_MAP_INPUTS_SUMMARY_FALL_MASK 0x38UL
#define BOARD_MAP_INPUTS_SUMMARY_FALL_SHIFT 3

/* (comment missing) */
#define BOARD_MAP_INPUTS_CSR 0x2040UL
#define BOARD_MAP_INPUTS_CSR_SIZE 12 /* 0xc */

/* Enable saving timestamps on edges */
#define BOARD_MAP_INPUTS_CSR_EN 0x0UL

/* Set by HW when a rising edge is detected.
Write 1 to clear the bit (as well as the bit in fall_sr) and
re-enable timestamping.
 */
#define BOARD_MAP_INPUTS_CSR_RISE_SR 0x4UL

/* Set by HW when the falling edge has been detected */
#define BOARD_MAP_INPUTS_CSR_FALL_SR 0x8UL

/* (comment missing) */
#define BOARD_MAP_INPUTS_TS 0x2800UL
#define BOARD_MAP_INPUTS_TS_SIZE 16 /* 0x10 */

/* (comment missing) */
#define BOARD_MAP_INPUTS_TS_RISE_SEC 0x0UL

/* (comment missing) */
#define BOARD_MAP_INPUTS_TS_RISE_NS 0x4UL

/* (comment missing) */
#define BOARD_MAP_INPUTS_TS_FALL_SEC 0x8UL

/* (comment missing) */
#define BOARD_MAP_INPUTS_TS_FALL_NS 0xcUL

/* (comment missing) */
#define BOARD_MAP_PULSER_GROUP 0x8000UL
#define BOARD_MAP_PULSER_GROUP_SIZE 4096 /* 0x1000 = 4KB */

/* submap for pulser group */
#define BOARD_MAP_PULSER_GROUP_EL 0x0UL
#define ADDR_MASK_BOARD_MAP_PULSER_GROUP_EL 0x3000UL
#define BOARD_MAP_PULSER_GROUP_EL_SIZE 4096 /* 0x1000 = 4KB */

/* NIC interface */
#define BOARD_MAP_WRNIC 0x10000UL
#define ADDR_MASK_BOARD_MAP_WRNIC 0x10000UL
#define BOARD_MAP_WRNIC_SIZE 65536 /* 0x10000 = 64KB */

#ifndef __ASSEMBLER__
struct board_map {
  /* [0x0]: REG (ro) Map version */
  uint32_t map_version;

  /* [0x4]: REG (ro) Model id */
  uint32_t model_id;

  /* [0x8]: REG (rw) Firmware version */
  uint32_t fw_version;

  /* [0xc]: REG (ro) WhiteRabbit status */
  uint32_t wr_state;

  /* [0x10]: REG (ro) TAI (seconds) lo part */
  uint32_t tm_tai_lo;

  /* [0x14]: REG (ro) TAI (seconds) hi part */
  uint32_t tm_tai_hi;

  /* [0x18]: REG (ro) TAI cycles */
  uint32_t tm_cycles;

  /* [0x1c]: REG (wo) interrupt force */
  uint32_t ifr;

  /* [0x20]: REG (rw) Front panel 50ohm termination enable */
  uint32_t fp_term;

  /* [0x24]: REG (rw) Front panel input polarity inversion */
  uint32_t fp_inv;

  /* [0x28]: REG (rw) Front panel output enable */
  uint32_t fp_oe;

  /* [0x2c]: REG (rw) Invert userio inputs */
  uint32_t userio_inv;

  /* [0x30]: REG (ro) Value of userio inputs */
  uint32_t userio_in;

  /* [0x34]: REG (rw) Control userio (extra inputs and outputs) */
  uint32_t userio_cfg;

  /* [0x38]: REG (rw) Raw pulsers status (WTC) */
  uint32_t pulsers_raw;

  /* [0x3c]: REG (rw) Pulsers status (WTC) */
  uint32_t pulsers_int;

  /* [0x40]: REG (rw) Mask for pulsers status */
  uint32_t pulsers_mask;

  /* [0x44]: REG (ro) Switches, id and geographical address */
  uint32_t board_id;

  /* [0x48]: REG (rw) mux for PXIe DSTAR-C lines */
  uint32_t pxie_dstarc;

  /* [0x4c]: REG (rw) mux for PXIe DSTAR-A lines */
  uint32_t pxie_dstara;

  /* [0x50]: REG (ro) WR Fabric demux fifo level (to observe) */
  uint32_t nic_fifo;

  /* padding to: 3328 Bytes */
  uint32_t __padding_0[811];

  /* [0xd00]: SUBMAP (comment missing) */
  struct si5340_ctrl_map si5340;

  /* padding to: 3584 Bytes */
  uint32_t __padding_1[32];

  /* [0xe00]: BLOCK (comment missing) */
  struct leds {
    /* [0x0]: REG (rw) Set when leds are controlled by the colors memory */
    uint32_t force;

    /* [0x4]: REG (rw) Led color for an active output */
    uint32_t rgb_out_hi;

    /* [0x8]: REG (rw) Led color for an inactive output */
    uint32_t rgb_out_lo;

    /* [0xc]: REG (rw) Led color for an active input */
    uint32_t rgb_in_hi;

    /* [0x10]: REG (rw) Led color for an inactive input */
    uint32_t rgb_in_lo;

    /* padding to: 256 Bytes */
    uint32_t __padding_0[59];

    /* [0x100]: MEMORY Colors of the leds when forced */
    struct leds_colors {
      /* [0x0]: REG (rw) (comment missing) */
      uint32_t rgb;
    } colors[64];
  } leds;

  /* [0x1000]: REPEAT (comment missing) */
  struct rf {
    /* [0x0]: REG (rw) (comment missing) */
    uint32_t ftw_hi;

    /* [0x4]: REG (rw) (comment missing) */
    uint32_t ftw_lo;

    /* [0x8]: REG (wo) (comment missing) */
    uint32_t ftw_load;

    /* [0xc]: REG (rw) (comment missing) */
    uint32_t bst_ctrl;

    /* [0x10]: REG (rw) (comment missing) */
    uint32_t h1;

    /* [0x14]: REG (rw) (comment missing) */
    uint32_t gth_cfg;

    /* [0x18]: REG (ro) (comment missing) */
    uint32_t gth_sta;

    /* [0x1c]: REG (ro) (comment missing) */
    uint32_t rf_ftw;

    /* [0x20]: REG (ro) (comment missing) */
    uint32_t rf_ftw_lo;

    /* [0x24]: REG (ro) (comment missing) */
    uint32_t rf_rx_tai;

    /* [0x28]: REG (ro) (comment missing) */
    uint32_t rf_rx_cyc;

    /* [0x2c]: REG (rw) (comment missing) */
    uint32_t rf_rx_sta;

    /* padding to: 256 Bytes */
    uint32_t __padding_0[52];

    /* [0x100]: SUBMAP (comment missing) */
    uint32_t streamer[64];
  } rf[3];

  /* padding to: 4096 Bytes */
  uint32_t __padding_2[128];

  /* padding to: 8192 Bytes */
  uint32_t __padding_3[512];

  /* [0x2000]: BLOCK Time stamps of rising and falling input edges */
  struct inputs {
    /* [0x0]: REG (ro) Rise bit summary (one bit for each rise_sr register) */
    uint32_t summary;

    /* padding to: 64 Bytes */
    uint32_t __padding_0[15];

    /* [0x40]: REPEAT (comment missing) */
    struct inputs_csr {
      /* [0x0]: REG (rw) Enable saving timestamps on edges */
      uint32_t en;

      /* [0x4]: REG (rw) Set by HW when a rising edge is detected.
Write 1 to clear the bit (as well as the bit in fall_sr) and
re-enable timestamping.
 */
      uint32_t rise_sr;

      /* [0x8]: REG (ro) Set by HW when the falling edge has been detected */
      uint32_t fall_sr;
    } csr[3];

    /* padding to: 64 Bytes */
    uint32_t __padding_1[7];

    /* padding to: 2048 Bytes */
    uint32_t __padding_2[480];

    /* [0x800]: MEMORY (comment missing) */
    struct inputs_ts {
      /* [0x0]: REG (ro) (comment missing) */
      uint32_t rise_sec;

      /* [0x4]: REG (ro) (comment missing) */
      uint32_t rise_ns;

      /* [0x8]: REG (ro) (comment missing) */
      uint32_t fall_sec;

      /* [0xc]: REG (ro) (comment missing) */
      uint32_t fall_ns;
    } ts[96];

    /* padding to: 2048 Bytes */
    uint32_t __padding_3[128];
  } inputs;

  /* padding to: 32768 Bytes */
  uint32_t __padding_4[5120];

  /* [0x8000]: REPEAT (comment missing) */
  struct pulser_group {
    /* [0x0]: SUBMAP submap for pulser group */
    struct pulser_group_map el;
  } pulser_group[4];

  /* padding to: 65536 Bytes */
  uint32_t __padding_5[4096];

  /* [0x10000]: SUBMAP NIC interface */
  uint32_t wrnic[16384];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__BOARD_MAP__H__ */
