#ifndef __CHEBY__NIC__H__
#define __CHEBY__NIC__H__
#define NIC_SIZE 65536 /* 0x10000 = 64KB */

/* NIC Control Register */
#define NIC_CR 0x0UL
#define NIC_CR_RX_EN 0x1UL
#define NIC_CR_TX_EN 0x2UL
#define NIC_CR_RXTHR_EN 0x4UL
#define NIC_CR_SW_RST 0x80000000UL

/* NIC Status Register */
#define NIC_SR 0x4UL
#define NIC_SR_BNA 0x1UL
#define NIC_SR_REC 0x2UL
#define NIC_SR_TX_DONE 0x4UL
#define NIC_SR_TX_ERROR 0x8UL
#define NIC_SR_CUR_TX_DESC_MASK 0x700UL
#define NIC_SR_CUR_TX_DESC_SHIFT 8
#define NIC_SR_CUR_RX_DESC_MASK 0x70000UL
#define NIC_SR_CUR_RX_DESC_SHIFT 16

/* NIC Current Rx Bandwidth Register */
#define NIC_RXBW 0x8UL

/* NIC Max Rx Bandwidth Register */
#define NIC_MAXRXBW 0xcUL
#define NIC_MAXRXBW_VALUE_MASK 0xffffUL
#define NIC_MAXRXBW_VALUE_SHIFT 0

/* TX descriptors mem */
#define NIC_DTX 0x80UL
#define NIC_DTX_SIZE 4 /* 0x4 */

/* None */
#define NIC_DTX_DATA 0x0UL

/* RX descriptors mem */
#define NIC_DRX 0x100UL
#define NIC_DRX_SIZE 4 /* 0x4 */

/* None */
#define NIC_DRX_DATA 0x0UL

/* None */
#define NIC_EIC 0x20UL
#define NIC_EIC_SIZE 16 /* 0x10 */

/* Interrupt disable register */
#define NIC_EIC_EIC_IDR 0x20UL
#define NIC_EIC_EIC_IDR_RCOMP 0x1UL
#define NIC_EIC_EIC_IDR_TCOMP 0x2UL
#define NIC_EIC_EIC_IDR_TXERR 0x4UL

/* Interrupt enable register */
#define NIC_EIC_EIC_IER 0x24UL
#define NIC_EIC_EIC_IER_RCOMP 0x1UL
#define NIC_EIC_EIC_IER_TCOMP 0x2UL
#define NIC_EIC_EIC_IER_TXERR 0x4UL

/* Interrupt mask register */
#define NIC_EIC_EIC_IMR 0x28UL
#define NIC_EIC_EIC_IMR_RCOMP 0x1UL
#define NIC_EIC_EIC_IMR_TCOMP 0x2UL
#define NIC_EIC_EIC_IMR_TXERR 0x4UL

/* Interrupt status register */
#define NIC_EIC_EIC_ISR 0x2cUL
#define NIC_EIC_EIC_ISR_RCOMP 0x1UL
#define NIC_EIC_EIC_ISR_TCOMP 0x2UL
#define NIC_EIC_EIC_ISR_TXERR 0x4UL

/* buffers */
#define NIC_MEM 0x8000UL
#define NIC_MEM_SIZE 4 /* 0x4 */

/* None */
#define NIC_MEM_DATA 0x0UL

#ifndef __ASSEMBLER__
struct nic {
  /* [0x0]: REG (rw) NIC Control Register */
  uint32_t CR;

  /* [0x4]: REG (rw) NIC Status Register */
  uint32_t SR;

  /* [0x8]: REG (ro) NIC Current Rx Bandwidth Register */
  uint32_t RXBW;

  /* [0xc]: REG (rw) NIC Max Rx Bandwidth Register */
  uint32_t MAXRXBW;

  /* padding to: 8 words */
  uint32_t __padding_0[4];

  /* [0x20]: BLOCK (no description) */
  struct eic {
    /* [0x0]: REG (wo) Interrupt disable register */
    uint32_t eic_idr;

    /* [0x4]: REG (wo) Interrupt enable register */
    uint32_t eic_ier;

    /* [0x8]: REG (ro) Interrupt mask register */
    uint32_t eic_imr;

    /* [0xc]: REG (rw) Interrupt status register */
    uint32_t eic_isr;
  } eic;

  /* padding to: 32 words */
  uint32_t __padding_1[20];

  /* [0x80]: MEMORY TX descriptors mem */
  struct dtx {
    /* [0x0]: REG (rw) (no description) */
    uint32_t data;
  } dtx[32];

  /* [0x100]: MEMORY RX descriptors mem */
  struct drx {
    /* [0x0]: REG (rw) (no description) */
    uint32_t data;
  } drx[32];

  /* padding to: 8192 words */
  uint32_t __padding_2[8096];

  /* [0x8000]: MEMORY buffers */
  struct mem {
    /* [0x0]: REG (rw) (no description) */
    uint32_t data;
  } mem[8192];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__NIC__H__ */
