#ifndef __CHEBY__SI5340_CTRL_MAP__H__
#define __CHEBY__SI5340_CTRL_MAP__H__

#define SI5340_CTRL_MAP_SIZE 128 /* 0x80 */

/* Control register */
#define SI5340_CTRL_MAP_CTRL 0x0UL
#define SI5340_CTRL_MAP_CTRL_SPI_EN 0x1UL
#define SI5340_CTRL_MAP_CTRL_DAC_EN 0x2UL
#define SI5340_CTRL_MAP_CTRL_HELPER_SEL 0x4UL
#define SI5340_CTRL_MAP_CTRL_DAC_UPDATE 0x8UL
#define SI5340_CTRL_MAP_CTRL_EN0 0x10UL
#define SI5340_CTRL_MAP_CTRL_EN1 0x20UL
#define SI5340_CTRL_MAP_CTRL_FORCE0 0x40UL
#define SI5340_CTRL_MAP_CTRL_FORCE1 0x80UL

/* Status register */
#define SI5340_CTRL_MAP_STATUS 0x4UL
#define SI5340_CTRL_MAP_STATUS_UPD0 0x1UL
#define SI5340_CTRL_MAP_STATUS_UPD1 0x2UL

/* (comment missing) */
#define SI5340_CTRL_MAP_PLL 0x40UL
#define SI5340_CTRL_MAP_PLL_SIZE 32 /* 0x20 */

/* dac value (to be set at start) */
#define SI5340_CTRL_MAP_PLL_DAC 0x0UL

/* bias (value = bias + input * mul) */
#define SI5340_CTRL_MAP_PLL_BIAS_H 0x4UL

/* Bias low word */
#define SI5340_CTRL_MAP_PLL_BIAS_L 0x8UL

/* factor */
#define SI5340_CTRL_MAP_PLL_MUL 0xcUL

/* input value from wr core */
#define SI5340_CTRL_MAP_PLL_INP 0x10UL

/* forced value */
#define SI5340_CTRL_MAP_PLL_FORCE 0x14UL

/* current computed value */
#define SI5340_CTRL_MAP_PLL_VALUE_H 0x18UL

/* current computed value */
#define SI5340_CTRL_MAP_PLL_VALUE_L 0x1cUL

#ifndef __ASSEMBLER__
struct si5340_ctrl_map {
  /* [0x0]: REG (rw) Control register */
  uint32_t ctrl;

  /* [0x4]: REG (ro) Status register */
  uint32_t status;

  /* padding to: 64 Bytes */
  uint32_t __padding_0[14];

  /* [0x40]: REPEAT (comment missing) */
  struct pll {
    /* [0x0]: REG (rw) dac value (to be set at start) */
    uint32_t dac;

    /* [0x4]: REG (rw) bias (value = bias + input * mul) */
    uint32_t bias_h;

    /* [0x8]: REG (rw) Bias low word */
    uint32_t bias_l;

    /* [0xc]: REG (rw) factor */
    uint32_t mul;

    /* [0x10]: REG (ro) input value from wr core */
    uint32_t inp;

    /* [0x14]: REG (rw) forced value */
    uint32_t force;

    /* [0x18]: REG (ro) current computed value */
    uint32_t value_h;

    /* [0x1c]: REG (ro) current computed value */
    uint32_t value_l;
  } pll[2];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__SI5340_CTRL_MAP__H__ */
