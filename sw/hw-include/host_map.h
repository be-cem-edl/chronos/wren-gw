#ifndef __CHEBY__HOST_MAP__H__
#define __CHEBY__HOST_MAP__H__

#define HOST_MAP_SIZE 8192 /* 0x2000 = 8KB */

/* Identification (WREN) */
#define HOST_MAP_IDENT 0x0UL

/* Map version */
#define HOST_MAP_MAP_VERSION 0x4UL

/* Model identification (vmeN, pciN, pxeN) */
#define HOST_MAP_MODEL_ID 0x8UL

/* Firmware version */
#define HOST_MAP_FW_VERSION 0xcUL

/* WhiteRabbit status */
#define HOST_MAP_WR_STATE 0x10UL
#define HOST_MAP_WR_STATE_LINK_UP 0x1UL
#define HOST_MAP_WR_STATE_TIME_VALID 0x2UL

/* TAI (seconds) lo part */
#define HOST_MAP_TM_TAI_LO 0x14UL

/* TAI (seconds) hi part */
#define HOST_MAP_TM_TAI_HI 0x18UL

/* TAI cycles */
#define HOST_MAP_TM_CYCLES 0x1cUL

/* timer and interrupt controller */
#define HOST_MAP_INTC 0x20UL
#define HOST_MAP_INTC_SIZE 32 /* 0x20 */

/* TAI cycles + TAI seconds */
#define HOST_MAP_INTC_TM_COMPACT 0x20UL
#define HOST_MAP_INTC_TM_COMPACT_CYCLES_MASK 0xfffffffUL
#define HOST_MAP_INTC_TM_COMPACT_CYCLES_SHIFT 0
#define HOST_MAP_INTC_TM_COMPACT_SEC_MASK 0xf0000000UL
#define HOST_MAP_INTC_TM_COMPACT_SEC_SHIFT 28

/* generate an interrupt */
#define HOST_MAP_INTC_TM_TIMER 0x24UL
#define HOST_MAP_INTC_TM_TIMER_CYCLES_MASK 0xfffffffUL
#define HOST_MAP_INTC_TM_TIMER_CYCLES_SHIFT 0
#define HOST_MAP_INTC_TM_TIMER_SEC_MASK 0xf0000000UL
#define HOST_MAP_INTC_TM_TIMER_SEC_SHIFT 28

/* interrupt status (after mask) */
#define HOST_MAP_INTC_ISR 0x28UL
#define HOST_MAP_INTC_ISR_CLOCK 0x1UL
#define HOST_MAP_INTC_ISR_TIMER 0x2UL
#define HOST_MAP_INTC_ISR_WR_SYNC 0x4UL
#define HOST_MAP_INTC_ISR_MSG 0x8UL
#define HOST_MAP_INTC_ISR_ASYNC 0x10UL

/* raw interrupt status (before mask) */
#define HOST_MAP_INTC_ISR_RAW 0x2cUL
#define HOST_MAP_INTC_ISR_RAW_CLOCK 0x1UL
#define HOST_MAP_INTC_ISR_RAW_TIMER 0x2UL
#define HOST_MAP_INTC_ISR_RAW_WR_SYNC 0x4UL
#define HOST_MAP_INTC_ISR_RAW_MSG 0x8UL
#define HOST_MAP_INTC_ISR_RAW_ASYNC 0x10UL

/* interrupt mask register */
#define HOST_MAP_INTC_IMR 0x30UL
#define HOST_MAP_INTC_IMR_CLOCK 0x1UL
#define HOST_MAP_INTC_IMR_TIMER 0x2UL
#define HOST_MAP_INTC_IMR_WR_SYNC 0x4UL
#define HOST_MAP_INTC_IMR_MSG 0x8UL
#define HOST_MAP_INTC_IMR_ASYNC 0x10UL

/* ack interrupts */
#define HOST_MAP_INTC_IACK 0x34UL
#define HOST_MAP_INTC_IACK_CLOCK 0x1UL
#define HOST_MAP_INTC_IACK_TIMER 0x2UL
#define HOST_MAP_INTC_IACK_WR_SYNC 0x4UL
#define HOST_MAP_INTC_IACK_MSG 0x8UL
#define HOST_MAP_INTC_IACK_ASYNC 0x10UL

/* a ROM containing build information */
#define HOST_MAP_BUILDINFO 0x100UL
#define ADDR_MASK_HOST_MAP_BUILDINFO 0x1f00UL
#define HOST_MAP_BUILDINFO_SIZE 256 /* 0x100 */

/* (comment missing) */
#define HOST_MAP_LOGS 0xc00UL
#define HOST_MAP_LOGS_SIZE 512 /* 0x200 */

/* Address in DDR of the next entry to be written */
#define HOST_MAP_LOGS_ADDR 0xc00UL
#define HOST_MAP_LOGS_ADDR_SIZE 4 /* 0x4 */

/* (comment missing) */
#define HOST_MAP_LOGS_ADDR_VAL 0x0UL

/* (comment missing) */
#define HOST_MAP_WRPC 0x1000UL
#define ADDR_MASK_HOST_MAP_WRPC 0x1000UL
#define HOST_MAP_WRPC_SIZE 4096 /* 0x1000 = 4KB */

#ifndef __ASSEMBLER__
struct host_map {
  /* [0x0]: REG (ro) Identification (WREN) */
  uint32_t ident;

  /* [0x4]: REG (ro) Map version */
  uint32_t map_version;

  /* [0x8]: REG (ro) Model identification (vmeN, pciN, pxeN) */
  uint32_t model_id;

  /* [0xc]: REG (ro) Firmware version */
  uint32_t fw_version;

  /* [0x10]: REG (ro) WhiteRabbit status */
  uint32_t wr_state;

  /* [0x14]: REG (ro) TAI (seconds) lo part */
  uint32_t tm_tai_lo;

  /* [0x18]: REG (ro) TAI (seconds) hi part */
  uint32_t tm_tai_hi;

  /* [0x1c]: REG (ro) TAI cycles */
  uint32_t tm_cycles;

  /* [0x20]: BLOCK timer and interrupt controller */
  struct intc {
    /* [0x0]: REG (ro) TAI cycles + TAI seconds */
    uint32_t tm_compact;

    /* [0x4]: REG (rw) generate an interrupt */
    uint32_t tm_timer;

    /* [0x8]: REG (ro) interrupt status (after mask) */
    uint32_t isr;

    /* [0xc]: REG (ro) raw interrupt status (before mask) */
    uint32_t isr_raw;

    /* [0x10]: REG (rw) interrupt mask register */
    uint32_t imr;

    /* [0x14]: REG (wo) ack interrupts */
    uint32_t iack;

    /* padding to: 64 Bytes */
    uint32_t __padding_0[2];
  } intc;

  /* padding to: 256 Bytes */
  uint32_t __padding_0[48];

  /* [0x100]: SUBMAP a ROM containing build information */
  uint32_t buildinfo[64];

  /* padding to: 3072 Bytes */
  uint32_t __padding_1[640];

  /* [0xc00]: BLOCK (comment missing) */
  struct logs {
    /* [0x0]: MEMORY Address in DDR of the next entry to be written */
    struct logs_addr {
      /* [0x0]: REG (ro) (comment missing) */
      uint32_t val;
    } addr[128];
  } logs;

  /* padding to: 4096 Bytes */
  uint32_t __padding_2[128];

  /* [0x1000]: SUBMAP (comment missing) */
  uint32_t wrpc[1024];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__HOST_MAP__H__ */
