#ifndef __LINUX__WREN_H_
#define __LINUX__WREN_H_

/* To make easy to include in user program, avoid any specific linux
   includes (except linux/ioctl.h) or types. */

/* For _IO helpers */
#include <linux/ioctl.h>

#include "wren-mb-defs.h"
#include "wren/wren-common.h"

struct wren_wr_time {
	uint32_t tai_lo;
	uint32_t tai_hi;
	uint32_t ns;
};

/* For communication with the firmware, using the mailboxes */
struct wren_cmd_msg {
	uint32_t cmd;
	uint32_t len;
	uint32_t data[1024];
};

struct wren_ioctl_get_source {
	/* Input: source index */
	uint32_t source_idx;
	/* Output: protocol */
	struct wren_protocol proto;
};

struct wren_ioctl_config {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	struct wren_mb_cond cond;
	struct wren_mb_pulser_config config;
	char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
};

struct wren_ioctl_imm_config {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	struct wren_mb_pulser_config config;
	struct wren_ts ts;
	char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
};

struct wren_ioctl_del_config {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	uint32_t config_idx;
};

struct wren_ioctl_get_config {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	uint32_t config_idx;
	struct wren_ioctl_config config;
};

struct wren_ioctl_mod_config {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	uint32_t config_idx;
	struct wren_mb_pulser_config config;
};

struct wren_ioctl_force_pulser {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	uint32_t config_idx;
	struct wren_ts ts;
	struct wren_mb_pulser_config config;
};

struct wren_ioctl_event_subscribe {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	uint32_t source_idx;
	uint32_t event_id;
};

struct wren_ioctl_out {
	uint32_t output_idx;
	uint8_t mask;
	uint8_t inv_in;
	uint8_t inv_out;
};

struct wren_ioctl_pin {
	uint32_t pin_idx;
	uint8_t oe;
	uint8_t term;
	uint8_t inv;
};

struct wren_ioctl_stamper_log {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	uint32_t idx;
	uint32_t len;
	void *buf;
	
};

struct wren_ioctl_version_string {
	uint32_t maxlen;
	char *buf;
};

struct wren_ioctl_tx_set_table {
	/* Outputs */
	uint32_t error;
	/* Inputs */
	char name[WRENTX_TABLE_NAME_MAXLEN];
	struct wren_mb_tx_set_table table;
};

/* Log on IRQ from board */
#define WREN_DRV_LOG_ERROR	0
#define WREN_DRV_LOG_IRQ_PACKET	1
#define WREN_DRV_LOG_IRQ	2
#define WREN_DRV_LOG_BUFFER	3
#define WREN_DRV_LOG_ACTION	4
#define WREN_DRV_LOG_IMMEDIAT	5
#define WREN_DRV_LOG_TIMEOUT	6
#define WREN_DRV_LOG_CLIENT	7

/* IOCTL version */
#define WREN_IOCTL_VERSION 0x0000000d

#define WREN_IOC_GET_VERSION _IOR('W', 0, uint32_t)
#define WREN_IOC_VERSION_STRING _IOR('W', 1, struct wren_ioctl_version_string)
#define WREN_IOC_GET_LINK _IOR('W', 2, uint32_t)
#define WREN_IOC_GET_SYNC _IOR('W', 3, uint32_t)
#define WREN_IOC_GET_TIME _IOR('W', 4, struct wren_wr_time)
#define WREN_IOC_WAIT_TIME _IOR('W', 5, struct wren_wr_time)
#define WREN_IOC_MSG _IOWR('W', 6, struct wren_cmd_msg)

/* General convention for ioctls:
   Returns 0 on success, < 0 on error.
   errno:
   EFAULT, EAGAIN: system error,
   EBUSY: operation already performed (subscription)
   ECOMM, EINVAL: wren error in the error field (when it exists...) */
#define WREN_IOC_RX_ADD_SOURCE _IOW('W', 7, struct wren_protocol)
#define WREN_IOC_RX_GET_SOURCE _IOR('W', 8, struct wren_ioctl_get_source)
#define WREN_IOC_RX_DEL_SOURCE _IOW('W', 9, uint32_t)

#define WREN_IOC_RX_ADD_CONFIG _IOW('W', 10, struct wren_ioctl_config)
#define WREN_IOC_RX_GET_CONFIG _IOR('W', 11, struct wren_ioctl_get_config)
#define WREN_IOC_RX_DEL_CONFIG _IOW('W', 12, struct wren_ioctl_del_config)
#define WREN_IOC_RX_MOD_CONFIG _IOW('W', 13, struct wren_ioctl_mod_config)
#define WREN_IOC_RX_IMM_CONFIG _IOW('W', 14, struct wren_ioctl_imm_config)
#define WREN_IOC_RX_SUBSCRIBE_CONFIG _IOW('W', 15, uint32_t)
#define WREN_IOC_RX_UNSUBSCRIBE_CONFIG _IOW('W', 16, uint32_t)
#define WREN_IOC_RX_SUBSCRIBE_EVENT _IOW('W', 17, struct wren_ioctl_event_subscribe)
#define WREN_IOC_RX_UNSUBSCRIBE_EVENT _IOW('W', 18, struct wren_ioctl_event_subscribe)
#define WREN_IOC_RX_SUBSCRIBE_CONTEXT _IOW('W', 19, uint32_t)
#define WREN_IOC_RX_UNSUBSCRIBE_CONTEXT _IOW('W', 20, uint32_t)

#define WREN_IOC_RX_SET_PROMISC _IOW('W', 21, uint32_t)

#define WREN_IOC_RX_SET_OUTPUT _IOW('W', 22, struct wren_ioctl_out)
#define WREN_IOC_RX_GET_OUTPUT _IOR('W', 23, struct wren_ioctl_out)

#define WREN_IOC_RX_SET_PIN _IOW('W', 24, struct wren_ioctl_pin)
#define WREN_IOC_RX_GET_PIN _IOR('W', 25, struct wren_ioctl_pin)

#define WREN_IOC_RX_SET_TIMEOUT _IOW('W', 26, struct wren_wr_time)

#define WREN_IOC_SET_DRV_LOG_FLAGS _IOW('W', 27, uint32_t)
#define WREN_IOC_GET_DRV_LOG_FLAGS _IOR('W', 28, uint32_t)

#define WREN_IOC_GET_PULSER_LOG _IOR('W', 29, struct wren_ioctl_stamper_log)
#define WREN_IOC_GET_PIN_LOG _IOR('W', 30, struct wren_ioctl_stamper_log)
#define WREN_IOC_GET_SW_LOG _IOR('W', 31, struct wren_ioctl_stamper_log)

#define WREN_IOC_GET_PANEL_CONFIG _IOR('W', 32, struct wren_panel_config)

#define WREN_IOC_RX_FORCE_PULSER  _IOW('W', 33, struct wren_ioctl_force_pulser)

#define WREN_IOC_SET_VME_P2A_OUTPUT _IOW('W', 34, unsigned)
#define WREN_IOC_SET_PATCHPANEL  _IOW('W', 35, unsigned)

#define WREN_IOC_GET_HW_LOG_INDEX _IOR('W', 36, uint32_t)

#define WREN_IOC_TX_SET_TABLE  _IOW('W', 37, struct wren_ioctl_tx_set_table)

#endif /* __LINUX__WREN_H_ */
