// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*
 * Driver for WREN_PCIE (PXI express FMC Carrier) board.
 */

#include <linux/device.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/moduleparam.h>
#include <linux/version.h>
#include <linux/sched.h>
#include <linux/poll.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 11, 0)
#include <linux/sched/signal.h>
#endif

#include "wren/wren-packet.h"
#include "wren/wren-hw.h"
#include "wren-common-utils.h"
#include "wren-core.h"
#include "wren-ioctl.h"
#include "host_map.h"
#include "mb_map.h"
#include "versions.h"

#define WREN_MAX_DEVS 32

/* Cheat..  */
#include "wren-common-utils.c"

#define WREN_LOG(wren, flags, fmt, ...) \
	do {								\
		if (wren->log_mask & (1 << flags))			\
			dev_notice(wren->dev_ctl, fmt, ##__VA_ARGS__);	\
	} while (0)

/* Major id for wren devices */
static dev_t wren_devt;

static struct class *wren_class;

static struct dentry *dfs_root;

static DEFINE_IDA(wren_map);

static void wren_send_cached_contexts(struct wren_client *client, unsigned src_idx);

static unsigned wren_get_buffer(struct wren_dev *wren)
{
	while (1) {
		unsigned head = atomic_read(&wren->buf_head);
		unsigned next;

		if (head == WREN_NO_BUFFER) {
			dev_err(wren->dev_ctl, "cannot alloc buffer");
			return WREN_NO_BUFFER;
		}
		next = atomic_read(&wren->bufs[head].len);
		if (atomic_cmpxchg(&wren->buf_head, head, next) == head) {
			/* We got the packet */
			atomic_set(&wren->bufs[head].refcnt, 1);
			return head;
		}
	}
}

static void wren_put_buffer(struct wren_dev *wren, unsigned idx)
{
	if (!atomic_dec_and_test(&wren->bufs[idx].refcnt))
		return;

	WREN_LOG(wren, WREN_DRV_LOG_BUFFER,
		 "wren_put_buffer: release buf %u\n", idx);

	/* Put it on the free list */
	while (1) {
		unsigned head = atomic_read(&wren->buf_head);

		atomic_set(&wren->bufs[idx].len, head);
		if (atomic_cmpxchg(&wren->buf_head, head, idx) == head)
			return;
	}
}

static int wren_client_empty_msg(struct wren_client *client)
{
	unsigned tail = atomic_read(&client->ring_tail);
	unsigned head = atomic_read(&client->ring_head);

	if (head == tail)
		return 1;
	/* The queue is not empty, but maybe the message is not yet ready.
	   Check the message was filled */
	head &= WREN_MAX_CLIENT_MSG - 1;
	if (atomic_read(&client->ring[head].buf_cmd) == 0)
		return 1;
	return 0;
}

static inline int wren_client_wait_timeout(struct wren_client *client)
{
	return client->timeout.flags & TIMEOUT_VALID;
}

static inline int wren_client_timeout_expired(struct wren_client *client)
{
	return client->timeout.flags & TIMEOUT_EXPIRED;
}

static ssize_t wren_usr_read(struct file *filp, char __user *usrbuf, size_t count, loff_t *f_pos)
{
	struct wren_client *client = filp->private_data;
	ssize_t err;
	unsigned head, next;
	unsigned buf_cmd;
	unsigned buf_idx;
	unsigned src_idx;
	uint32_t cmd;
	uint32_t ids;
	unsigned blen, hlen;
	struct wren_client_msg *msg;
	struct wren_buffer *buf;
	struct wren_ts ts;
	union wren_capsule_hdr_un hdr;

	if (!client->wren->wr_synced)
		return -EIO;

	/* Sanity check (must be aligned). */
	if (count < 4 || (count & 3) != 0 || ((uintptr_t)usrbuf & 3) != 0)
		return -EINVAL;

	/* Fast check */
	if (wren_client_timeout_expired(client)) {
		client->timeout.flags = 0;
		return -ETIME;
	}

	spin_lock(&client->wqh.lock);

	/* Check the mailbox is empty. */
	if (filp->f_flags & O_NONBLOCK) {
		if (wren_client_empty_msg(client)
		    && wren_client_wait_timeout(client)) {
			err = -EWOULDBLOCK;
			goto out;
		}
	} else {
		/* TODO: exit on sync loss */
		err = wait_event_interruptible_locked
			(client->wqh, (!wren_client_empty_msg(client)
				       || wren_client_timeout_expired(client)));
		if (err < 0)
			goto out;
	}

	/* Either a timeout... */
	if (wren_client_timeout_expired(client)) {
		client->timeout.flags = 0;
		err = -ETIME;
		goto out;
	}

	/* ... or a message */

	/* Read message from the queue */
	head = atomic_read(&client->ring_head);
	next = (head + 1) & (2 * WREN_MAX_CLIENT_MSG - 1);
	head &= WREN_MAX_CLIENT_MSG - 1;
	msg = &client->ring[head];
	buf_cmd = atomic_read(&msg->buf_cmd);
	cmd = buf_cmd & 0xff;
	buf_idx = buf_cmd >> 8;

	if (buf_idx != WREN_NO_BUFFER) {
		buf = &client->wren->bufs[buf_idx];
		blen = atomic_read(&buf->len);
	}
	else {
		buf = NULL;
		blen = 0;
	}

	ts.sec = 0;
	ts.nsec = 0;
	ids = 0;
	src_idx = 0;

	/* Free slot in the message queue */
	smp_mb__before_atomic();
	atomic_set(&client->ring_head, next);

	spin_unlock(&client->wqh.lock);

	switch (cmd) {
	case CMD_ASYNC_PULSE:
		hlen = 4; /* cmd + config_id + ts */
		ts = msg->ts;
		ids = (msg->config_id << 16) | msg->source_idx;
		break;
	case CMD_ASYNC_CONTEXT:
	case CMD_ASYNC_EVENT:
		hlen = 1; /* cmd */
		src_idx = msg->source_idx;
		break;
	case CMD_ASYNC_PROMISC:
		hlen = 1; /* cmd */
		break;
	case CMD_ASYNC_CONT:
		/* Should not happen */
		dev_err(client->wren->dev_ctl,
			"unexpected async_cont in ring\n");
		err = -EAGAIN;
		goto out;
	default:
		hlen = 0;
		break;
	}

	err = -EFAULT;

	/* First word of header: command. */
	hdr.hdr.typ = cmd;
	hdr.hdr.pad = src_idx;
	hdr.hdr.len = hlen + blen;

	if (put_user(hdr.u32, (u32 __user*)usrbuf))
		goto out1;
	usrbuf += sizeof(uint32_t);

	/* Arguments */
	switch (cmd) {
	case CMD_ASYNC_PULSE:
		if (count >= 8 && put_user(ts.sec, (u32 __user*)usrbuf))
			goto out1;
		if (count >= 12 && put_user(ts.nsec, (u32 __user*)(usrbuf + 4)))
			goto out1;
		if (count >= 16 && put_user(ids, (u32 __user*)(usrbuf + 8)))
			goto out1;
		usrbuf += 12;
		break;
	default:
		break;
	}

	if (count < hlen * 4) {
		/* Buffer was too short for header+args, stop now */
		err = count;
		goto out1;
	}

	count -= hlen * 4;

	/* Packet */
	err = hlen * 4;
	if (buf != NULL) {
		unsigned rlen = blen;

		while (1) {
			unsigned clen;
			unsigned count1;
			clen = rlen > WREN_MAX_BUFFER_LEN ? WREN_MAX_BUFFER_LEN : rlen;
			count1 = clen * 4;
			if (count1 > count)
				count1 = count;
			if (count > 0
			    && copy_to_user(usrbuf, buf->data, count1)) {
				err = -EFAULT;
				break;
			}
			count -= count1;
			err += count1;
			rlen -= clen;
			if (rlen == 0)
				break;
			usrbuf += count1;

			/* Shouldn't happen */
			if (wren_client_empty_msg(client)) {
				dev_err(client->wren->dev_ctl,
					"ring not filled with async_cont\n");
				break;
			}

			wren_put_buffer(client->wren, buf_idx);

			/* Extract next packet */
			head = atomic_read(&client->ring_head);
			next = (head + 1) & (2 * WREN_MAX_CLIENT_MSG - 1);
			head &= WREN_MAX_CLIENT_MSG - 1;
			msg = &client->ring[head];
			buf_cmd = atomic_read(&msg->buf_cmd);
			cmd = buf_cmd & 0xff;
			buf_idx = buf_cmd >> 8;
			if (cmd != CMD_ASYNC_CONT) {
				/* Truncated */
				dev_err(client->wren->dev_ctl,
					"missing async_cont in ring\n");
				err -= rlen * 4;
				buf_idx = WREN_NO_BUFFER;
				break;
			}
			buf = &client->wren->bufs[buf_idx];
			/* Len is not checked */

			smp_mb__before_atomic();
			atomic_set(&client->ring_head, next);
		}
	}

out1:
	if (buf_idx != WREN_NO_BUFFER)
		wren_put_buffer(client->wren, buf_idx);
	return err;

out:
	spin_unlock(&client->wqh.lock);
	return err;
}

static unsigned wren_usr_poll(struct file *filp, poll_table *wait)
{
	struct wren_client *client = filp->private_data;

	poll_wait(filp, &client->wqh, wait);
	if (!client->wren->wr_synced)
		return POLLERR;
	if (!wren_client_empty_msg(client))
		return POLLIN | POLLRDNORM;
	if (wren_client_timeout_expired(client))
		return POLLPRI;
	return 0;
}

static int wren_b2h_busy(struct wren_dev *wren)
{
	struct mb_map *mb = wren->mb;
	uint32_t csr;

	csr = ioread32 (&mb->b2h_csr);
	return csr & MB_CSR_READY;
}

static int wren_async_empty(struct wren_dev *wren)
{
	struct mb_map *mb = wren->mb;
	uint32_t b_off, h_off;

	b_off = ioread32 (&mb->async_board_off);
	h_off = ioread32 (&mb->async_host_off);
	return b_off == h_off;
}

static int wren_read_ctime(struct wren_dev *wren,
			   struct wren_compact_time *res)
{
	struct host_map *regs = wren->regs;

	if (!wren->wr_synced) {
		return -1;
	}

	res->lo = ioread32(&regs->intc.tm_compact);
	res->hi = wren->tai_hi[res->lo >> 31];
	return 0;
}

static int wren_read_time(struct wren_dev *wren,
			  struct wren_wr_time *res)
{
	struct wren_compact_time ctime;

	if (wren_read_ctime(wren, &ctime) < 0) {
		return -1;
	}

	/* Cycles is clk_ref, which is 62.5Mhz so 16ns */
	res->ns = (ctime.lo & 0x0fffffff) << 4;
	res->tai_lo = (ctime.hi << 4) | (ctime.lo >> 28);
	res->tai_hi = ctime.hi >> 28;
	return 0;
}

static ssize_t wren_ctl_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct wren_dev *wren = filp->private_data;
	struct mb_map *mb = wren->mb;
	uint32_t h_off, b_off;
	int32_t  len;
	unsigned i;
	unsigned nwords;
	int err;

	/* Sanity check. */
	if (count < 16 || (count & 3) != 0)
		return -EINVAL;

	spin_lock_irq(&wren->wqh_a.lock);

	/* Check the mailbox is empty. */
	if (filp->f_flags & O_NONBLOCK) {
		if (wren_async_empty(wren)) {
			err = -EWOULDBLOCK;
			goto out;
		}
	} else {
		err = wait_event_interruptible_locked_irq
			(wren->wqh_a, !wren_async_empty(wren));
		if (err < 0)
			goto out;
	}

	/* Read offsets */
	b_off = ioread32 (&mb->async_board_off);
	h_off = ioread32 (&mb->async_host_off);

	/* Compute length in words */
	len = b_off - h_off;
	if (len < 0)
		len += 2048;

	if (len * 4 < count)
		count = len * 4;

	/* Copy data (TODO: DMA ?) */
	nwords = count / 4;
	for (i = 0; i < nwords; i++) {
		uint32_t data;

		data = ioread32(&mb->async_data[h_off]);
		if (put_user(data, (u32 __user*)(buf + i * 4))) {
			err = -EFAULT;
			goto out;
		}
		h_off = (h_off + 1) & 2047;
	}

	/* Update host offet */
	iowrite32 (h_off, &mb->async_host_off);

	err = count;
out:
	spin_unlock_irq(&wren->wqh_a.lock);
	return err;
}

static int wren_mb_snd_wait(struct wren_dev *wren,
			    uint32_t cmd, uint32_t len)
{
	int err;
	struct mb_map *mb = wren->mb;

	/* Write header */
	iowrite32 (cmd, &mb->h2b_cmd);
	iowrite32 (len, &mb->h2b_len);

	/* Mark ready (for the board). */
	iowrite32 (MB_CSR_READY, &mb->h2b_csr);

	/* Wait for answer */
	err = wait_event_interruptible (wren->wqh_r, wren_b2h_busy(wren));
	if (err < 0) {
		/* There might be at most one discard at a time, as the
		   answer is always awaited. */
		wren->wqh_r_discard = 1;
		return err;
	}
	return 0;
}

/* Send and receive a message.
   LEN is the length of the DATA to be sent, in words.
   RSP_MAX is the maximum length of RSP, in words.
   Return the length of the response, or < 0 in case of error.
   The error is a WRENRX_ERR_ error
*/
static int wren_mb_msg(struct wren_dev *wren,
		       uint32_t cmd, void *data, uint32_t len,
		       void *rsp, uint32_t rsp_max)
{
	struct mb_map *mb = wren->mb;
	unsigned i;
	int err;

	/* Get exclusive access to the messages */
	err = mutex_lock_interruptible(&wren->msg_mutex);
	if (err) {
		return WRENRX_ERR_SYSTEM;
	}

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		iowrite32(((uint32_t *)data)[i], &mb->h2b_data[i]);
	}

	/* Send and wait for the answer */
	err = wren_mb_snd_wait(wren, cmd, len);
	if (err < 0) {
		err = WRENRX_ERR_SYSTEM;
		goto out;
	}

	/* Read header */
	cmd = ioread32 (&mb->b2h_cmd);
	len = ioread32 (&mb->b2h_len);

	if (len > rsp_max)
		len = rsp_max;

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		((uint32_t *)rsp)[i] = ioread32(&mb->b2h_data[i]);
	}

	/* Mark as read */
	iowrite32 (0, &mb->b2h_csr);

	if (cmd & CMD_ERROR)
		err = -(cmd & CMD_MASK);
	else
		err = len;
out:
	mutex_unlock(&wren->msg_mutex);
	return err;
}

static int wren_ctl_open(struct inode *inode, struct file *file)
{
	struct wren_dev *wren = container_of(inode->i_cdev,
					     struct wren_dev, cdev_ctl);
	file->private_data = wren;
	kobject_get(&wren->dev_ctl->kobj);
	return 0;
}

static int wren_ctl_release(struct inode *inode, struct file *file)
{
	struct wren_dev *wren = container_of(inode->i_cdev,
					     struct wren_dev, cdev_ctl);
	kobject_put(&wren->dev_ctl->kobj);
	return 0;
}

static int wren_usr_open(struct inode *inode, struct file *file)
{
	struct wren_dev *wren = container_of(inode->i_cdev,
					     struct wren_dev, cdev_usr);
	struct wren_client *client;
	unsigned idx;
	int res;
	unsigned i;

	write_lock(&wren->client_lock);
	idx = find_first_zero_bit(wren->client_used.b, WREN_MAX_CLIENTS);
	if (idx == WREN_MAX_CLIENTS) {
		res = -ENOSPC;
		goto err;
	}

	/* Alloc and init structure */
	client = kmalloc(sizeof(*client), GFP_KERNEL);
	if (client == NULL) {
		res = -ENOMEM;
		goto err;
	}
	client->wren = wren;
	client->idx = idx;
	atomic_set(&client->ring_head, 0);
	atomic_set(&client->ring_tail, 0);
	init_waitqueue_head(&client->wqh);

	for (i = 0; i < WREN_NBR_RX_SOURCES; i++) {
		atomic_set(&client->sources[i].ref, 0);
		bitmap_zero(client->sources[i].evsubs, WREN_NBR_EVENTID);
	}

	INIT_LIST_HEAD(&client->timeout.entry);
	client->timeout.me = current;
	client->timeout.flags = 0;

	wren->clients[idx] = client;

	set_bit(idx, wren->client_used.b);
	file->private_data = client;

	kobject_get(&wren->dev_usr->kobj);

	WREN_LOG(wren, WREN_DRV_LOG_CLIENT, "open client %u\n", idx);

	res = 0;
err:
	write_unlock(&wren->client_lock);
	return res;
}

/* Must be called with data_mutex locked to avoid race condition between
   subscribe and unsubscribe */
static int wren_rx_event_unsubscribe(struct wren_dev *wren,
				     unsigned source_idx,
				     unsigned evid)
{
	if (atomic_dec_return(&wren->sources[source_idx].evsubs[evid]) == 0) {
		/* Need to unsubscribe to the event */
		struct wren_mb_rx_subscribe cmd;
		uint32_t rep;
		int res;

		/* TODO: use a _killable version of
		   wren_mb_msg so that it doesn't fail on
		   the wait */
		cmd.src_idx = source_idx;
		cmd.ev_id = evid;
		res = wren_mb_msg(wren, CMD_RX_UNSUBSCRIBE,
				  &cmd, sizeof(cmd) / 4,
				  &rep, sizeof(rep) / 4);
		if (res != 0)
			WREN_LOG(wren, WREN_DRV_LOG_ERROR,
				 "unsubscribe error: %08x\n", res);
		return res;
	}
	return 0;
}

static int wren_usr_release(struct inode *inode, struct file *file)
{
	struct wren_client *client = file->private_data;
	struct wren_dev *wren = client->wren;
	unsigned idx = client->idx;
	unsigned i;

	WREN_LOG(wren, WREN_DRV_LOG_CLIENT, "release client %u\n", idx);

	/* Unsubscribes to sources.
	   TODO: race condition with subscribe. */
	mutex_lock(&wren->data_mutex);

	for (i = 0; i < WREN_NBR_RX_SOURCES; i++) {
		unsigned ev;

		ev = find_first_bit(client->sources[i].evsubs, WREN_NBR_EVENTID);
		while (ev < WREN_NBR_EVENTID) {
			clear_bit(ev, client->sources[i].evsubs);
			wren_rx_event_unsubscribe(wren, i, ev);
			ev = find_next_bit(client->sources[i].evsubs, WREN_NBR_EVENTID, ev + 1);
		}
	}

	mutex_unlock(&wren->data_mutex);

	/* Clear bits: unsubscribe to all configs and all sources.
	   (clear_bit is atomic) */
	for (i = 0; i < WREN_NBR_ACTIONS; i++)
		clear_bit(idx, wren->client_configs[i].b);
	for (i = 0; i < WREN_NBR_RX_SOURCES; i++)
		clear_bit(idx, wren->client_sources[i].b);

	if (wren_client_wait_timeout(client)) {
		spin_lock_irq(&wren->to_lock);
		list_del(&client->timeout.entry);
		spin_unlock_irq(&wren->to_lock);
	}

	/* Remove promisc mode if set by the client */
	if (atomic_read(&wren->promisc) == idx) {
		uint32_t src = 0;
		wren_mb_msg(wren, CMD_RX_SET_PROMISC,
			    &src, sizeof(src) / 4, NULL, 0);
		atomic_set(&wren->promisc, -1);
	}

	/* Free buffers in ring */
	{
		unsigned tail = atomic_read(&client->ring_tail);
		unsigned head = atomic_read(&client->ring_head);

		while (head != tail) {
			unsigned next;
			struct wren_client_msg *msg;
			unsigned buf_idx;

			next = (head + 1) & (2 * WREN_MAX_CLIENT_MSG - 1);
			head &= WREN_MAX_CLIENT_MSG - 1;
			msg = &client->ring[head];
			buf_idx = atomic_read(&msg->buf_cmd) >> 8;

			head = next;
		}
	}

	/* Remove the client */
	write_lock(&wren->client_lock);
	clear_bit(idx, wren->client_used.b);
	wren->clients[idx] = NULL;
	write_unlock(&wren->client_lock);

	kfree(client);

	kobject_put(&wren->dev_usr->kobj);

	return 0;
}

static int wren_ioctl_msg_usr(struct wren_dev *wren, struct wren_cmd_msg __user *msg)
{
	struct mb_map *mb = wren->mb;
	uint32_t cmd, len, data;
	unsigned i;
	int err;

	/* Read header. */
	if (get_user(cmd, &msg->cmd) || get_user(len, &msg->len))
		return -EFAULT;

	if (len > ARRAY_SIZE(msg->data))
		return -EINVAL;

	/* Get exclusive access to the messages */
	err = mutex_lock_interruptible(&wren->msg_mutex);
	if (err)
		return err;

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		if (get_user(data, &msg->data[i])) {
			/* Failed */
			mutex_unlock(&wren->msg_mutex);
			return -EFAULT;
		}
		iowrite32(data, &mb->h2b_data[i]);
	}

	err = wren_mb_snd_wait(wren, cmd, len);
	if (err < 0)
		goto out;

	/* Read header from board */
	cmd = ioread32 (&mb->b2h_cmd);
	len = ioread32 (&mb->b2h_len);

	/* Write header to the process. */
	if (put_user(cmd, &msg->cmd) || put_user(len, &msg->len)) {
		err = -EFAULT;
		goto out;
	}

	if (len > ARRAY_SIZE(msg->data))
		len = ARRAY_SIZE(msg->data);

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		data = ioread32(&mb->b2h_data[i]);
		if (put_user(data, &msg->data[i])) {
			err = -EFAULT;
			goto out;
		}
	}

	/* Mark as read */
	iowrite32 (0, &mb->b2h_csr);

	err = 0;
out:
	mutex_unlock(&wren->msg_mutex);
	return err;
}

static int wren_ioctl_tx_set_table(struct wren_dev *wren, struct wren_ioctl_tx_set_table __user *utable)
{
	struct wren_ioctl_tx_set_table table;
	unsigned tbl_idx;
	unsigned len;
	int res;
	int err;

	res = -EINVAL;
	
	if (copy_from_user(&table, utable, sizeof(table)))
		return -EFAULT;

	if (table.table.src_idx >= WREN_NBR_TX_SOURCES) {
		err = WRENTX_ERR_BAD_SOURCE_IDX;
		goto out_nolock;
	}
	tbl_idx = table.table.table_idx;
	if (tbl_idx >= WREN_NBR_TX_TABLES) {
		err = WRENTX_ERR_BAD_TABLE_IDX;
		goto out_nolock;
	}

	/* Protect against concurrent accesses to tables and sources */
	mutex_lock(&wren->data_mutex);

	if (wren->tables[tbl_idx].src_idx >= 0) {
		err = WRENTX_ERR_BUSY_TABLE;
		goto out;
	}

	len = (sizeof(table.table) - sizeof(table.table.data)) / 4
		+ (table.table.nbr_insns + table.table.nbr_data);
	err = wren_mb_msg(wren, CMD_TX_SET_TABLE,
			  &table.table, len, NULL, 0);
	if (err != 0)
		goto out;
	memcpy(wren->tables[tbl_idx].name, table.name, WRENTX_TABLE_NAME_MAXLEN);
	wren->tables[tbl_idx].src_idx = table.table.src_idx;
	res = 0;

out:
	mutex_unlock(&wren->data_mutex);
out_nolock:
	if (copy_to_user(&utable->error, &err, 4))
		return -EFAULT;
	return res;
}

static int wren_ioctl_rx_add_source(struct wren_dev *wren, struct wren_protocol __user *uproto)
{
	struct wren_mb_rx_set_source cmd;
	int slot;
	int res;
	unsigned i;
	uint32_t rep;

	if (copy_from_user(&cmd.cfg.proto, uproto, sizeof(cmd.cfg.proto)))
		return -EFAULT;
	if (cmd.cfg.proto.proto == WREN_PROTO_NONE)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Find if the source already exist.  */
	slot = -1;
	for (i = 0; i < WREN_NBR_RX_SOURCES; i++) {
		struct wren_protocol *src = &wren->sources[i].proto;
		if (src->proto != WREN_PROTO_NONE) {
			if (wren_is_same_proto(src, &cmd.cfg.proto)) {
				res = i;
				goto out;
			}
		} else if (slot < 0) {
			slot = i;
		}
	}

	if (slot < 0) {
		res = -ENOSPC;
		goto out;
	}

	/* Fill slot */
	memcpy(&wren->sources[slot].proto, &cmd.cfg.proto, sizeof(cmd.cfg.proto));
	wren->sources[slot].conds = WREN_NO_IDX;

	/* Write proto on board */
	cmd.idx = slot;
	cmd.cfg.dest = 0;
	cmd.cfg.subsample = 0;
	res = wren_mb_msg(wren, CMD_RX_SET_SOURCE,
			  &cmd, sizeof(cmd) / 4, &rep, sizeof(rep) / 4);
	if (res < 0) {
		wren->sources[slot].proto.proto = WREN_PROTO_NONE;
		goto out;
	}
	res = slot;
out:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_get_source(struct wren_dev *wren, struct wren_ioctl_get_source __user *uarg)
{
	struct wren_ioctl_get_source arg;
	int res;

	if (copy_from_user(&arg.source_idx, &uarg->source_idx, sizeof(arg.source_idx)))
		return -EFAULT;
	if (arg.source_idx >= WREN_NBR_RX_SOURCES)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	if (copy_to_user(&uarg->proto, &wren->sources[arg.source_idx].proto, sizeof(arg.proto)))
		res = -EFAULT;
	mutex_unlock(&wren->data_mutex);

	return res;
}

static int wren_ioctl_rx_del_source(struct wren_dev *wren, uint32_t *uidx)
{
	struct wren_dev_source *src;
	uint32_t idx;
	struct wren_mb_rx_set_source cmd;
	unsigned i;
	int res;
	uint32_t rep;

	if (copy_from_user(&idx, uidx, sizeof(idx)))
		return -EFAULT;

	if (idx >= WREN_NBR_RX_SOURCES)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Check the source is not used (no conds, no subscriptions) */
	src = &wren->sources[idx];
	res = -EBUSY;
	if (src->conds != WREN_NO_IDX)
		goto err;
	for (i = 0; i < WREN_NBR_EVENTID; i++)
		if (atomic_read(&src->evsubs[i]) != 0)
			goto err;

	/* Remove the source on the board */
	memset(&cmd, 0, sizeof(cmd));
	cmd.idx = idx;
	cmd.cfg.dest = 0;
	cmd.cfg.subsample = 0;
	cmd.cfg.proto.proto = WREN_PROTO_NONE;
	res = wren_mb_msg(wren, CMD_RX_SET_SOURCE,
			  &cmd, sizeof(cmd) / 4, &rep, sizeof(rep) / 4);

	if (res < 0)
		res = -ECOMM;
	else
		src->proto.proto = WREN_PROTO_NONE;
err:
	mutex_unlock(&wren->data_mutex);
	return res;
}

/* Get a free action id, return < 0 for errors */
static int wren_get_action(struct wren_dev *wren)
{
	int act_idx = atomic_read(&wren->free_actions);

	while (1) {
		int nxt;
		int nidx;

		if (act_idx == WREN_NO_IDX)
			return WRENRX_ERR_NO_ACTION;

		nxt = wren->actions[act_idx].link;

		nidx = atomic_cmpxchg(&wren->free_actions, act_idx, nxt);
		if (nidx == act_idx)
			return act_idx;
		act_idx = nidx;
	}
}

static void wren_put_action(struct wren_dev *wren, int idx)
{
	int old_idx = atomic_read(&wren->free_actions);

	while (1) {
		int nidx;

		wren->actions[idx].link = old_idx;

		nidx = atomic_cmpxchg(&wren->free_actions, old_idx, idx);
		if (nidx == old_idx)
			return;
		old_idx = nidx;
	}
}

static int wren_ioctl_rx_add_config(struct wren_dev *wren, struct wren_ioctl_config __user *ucfg)
{
	struct wren_ioctl_config cfg;
	int res;
	int32_t err;
	uint32_t src_idx;
	uint32_t cond_idx;
	int act_idx;
	struct wren_dev_action *act;

	if (copy_from_user(&cfg, ucfg, sizeof(cfg)))
		return -EFAULT;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	res = -EINVAL;

	/* Check if the source exist.  */
	src_idx = cfg.cond.src_idx;
	if (src_idx >= WREN_NBR_RX_SOURCES) {
		err = WRENRX_ERR_BAD_SOURCE;
		goto out;
	}
	if (wren->sources[src_idx].proto.proto == WREN_PROTO_NONE) {
		err = WRENRX_ERR_NO_SOURCE;
		goto out;
	}

	/* Search for existing condition */
	cond_idx = wren->sources[src_idx].conds;
	while (cond_idx != WREN_NO_IDX) {
		struct wren_dev_cond *dcond = &wren->conds[cond_idx];
		if (wren_is_same_cond (&dcond->cond, &cfg.cond))
			break;
		cond_idx = dcond->link;
	}

	/* Maybe create the condition */
	if (cond_idx == WREN_NO_IDX) {
		struct wren_mb_rx_set_cond cmd;
		struct wren_dev_cond *dcond;
		cond_idx = wren->free_conds;
		if (cond_idx == WREN_NO_IDX) {
			err = WRENRX_ERR_NO_CONDITION;
			goto out;
		}

		/* Program cond on board */
		cmd.cond_idx = cond_idx;
		memcpy(&cmd.cond, &cfg.cond, sizeof(cfg.cond));
		err = wren_mb_msg(wren, CMD_RX_SET_COND, &cmd, sizeof(cmd)/4,
				  NULL, 0);
		if (err < 0)
			goto out;

		dcond = &wren->conds[cond_idx];
		/* Remove from chain */
		wren->free_conds = dcond->link;
		/* Add to source chain */
		dcond->link = wren->sources[src_idx].conds;
		wren->sources[src_idx].conds = cond_idx;
		/* Init */
		dcond->src_idx = src_idx;
		dcond->acts = WREN_NO_IDX;

		memcpy(&dcond->cond, &cfg.cond, sizeof(cfg.cond));
	}

	/* Get an action slot */
	act_idx = wren_get_action(wren);
	if (act_idx < 0) {
		err = act_idx;
		goto out;
	}

	/* Programm on board (if enabled) */
	if (cfg.config.flags & WREN_MB_PULSER_FLAG_ENABLE) {
		struct wren_mb_rx_set_action cmd_act;
		cmd_act.act_idx = act_idx;
		cmd_act.cond_idx = cond_idx;
		memcpy(&cmd_act.conf, &cfg.config, sizeof(cmd_act.conf));
		err = wren_mb_msg(wren, CMD_RX_SET_ACTION,
				  &cmd_act, sizeof(cmd_act)/4, NULL, 0);
		if (err < 0) {
			/* TODO: maybe free condition ? */
			wren_put_action(wren, act_idx);
			goto out;
		}
	}

	/* Extract action slot */
	act = &wren->actions[act_idx];
	act->cond_idx = cond_idx;
	act->link = wren->conds[cond_idx].acts;
	wren->conds[cond_idx].acts = act_idx;

	/* Fill it */
	memcpy(&act->config, &cfg.config, sizeof(act->config));
	memcpy(act->name, cfg.name, sizeof(act->name));

	res = act_idx;
	err = 0;
out:
	mutex_unlock(&wren->data_mutex);
	if (copy_to_user(&ucfg->error, &err, 4))
		return -EFAULT;
	return res;
}

/* Association a comparator to an action so that on comparator irq, we know
   which action triggered */
static void associate_comparator_and_action(struct wren_dev *wren, uint32_t act_idx, uint32_t cmp_idx)
{
	struct wren_dev_comparator *cmp;

	/* Set action to the comparator */
	cmp = &wren->comp_buf[cmp_idx];
	WREN_LOG(wren, WREN_DRV_LOG_ACTION,
		 "action act=%u, cmp=%u, old-buf=%u\n",
		 act_idx, cmp_idx, cmp->buf);
	/* Free the buffer just in case.  Maybe the comparator is free for
	   the board but still in use in the driver. */
	if (cmp->buf != WREN_NO_BUFFER)
		wren_put_buffer(wren, cmp->buf);
	cmp->buf = WREN_NO_BUFFER;
	cmp->config_id = act_idx;
}

static int wren_ioctl_rx_imm_config(struct wren_dev *wren, struct wren_ioctl_imm_config __user *ucfg)
{
	struct wren_ioctl_imm_config cfg;
	int res;
	int32_t err;
	int act_idx;
	struct wren_dev_action *act;
	struct wren_mb_rx_imm_action cmd_act;
	uint32_t cmp_idx;

	if (copy_from_user(&cfg, ucfg, sizeof(cfg)))
		return -EFAULT;
	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	res = -EINVAL;

	/* Get an action slot */
	act_idx = wren_get_action(wren);
	if (act_idx < 0) {
		err = act_idx;
		goto out;
	}
	act = &wren->actions[act_idx];
	act->cond_idx = WREN_INV_IDX;
	act->link = WREN_INV_IDX;

	memcpy(&act->config, &cfg.config, sizeof(act->config));
	memcpy(act->name, cfg.name, sizeof(act->name));

	/* Programm on board */
	if (cfg.config.flags & WREN_MB_PULSER_FLAG_ENABLE) {
	  cmd_act.act_idx = act_idx;
	  memcpy(&cmd_act.cmp.conf, &cfg.config, sizeof(cmd_act.cmp.conf));
	  cmd_act.cmp.ts.sec = cfg.ts.sec;
	  cmd_act.cmp.ts.nsec = cfg.ts.nsec;
	  err = wren_mb_msg(wren, CMD_RX_IMM_ACTION, &cmd_act, sizeof(cmd_act)/4,
			    &cmp_idx, 1);
	  if (err < 0) {
		  wren_put_action(wren, act_idx);
		  goto out;
	  }

	  WREN_LOG(wren, WREN_DRV_LOG_IMMEDIAT,
		   "imm_config: cmp=%u, conf=%u\n", cmp_idx, act_idx);

	  /* Set action to the comparator */
	  associate_comparator_and_action(wren, act_idx, cmp_idx);
	}

	res = act_idx;
	err = 0;
out:
	mutex_unlock(&wren->data_mutex);
	if (copy_to_user(&ucfg->error, &err, 4))
		return -EFAULT;
	return res;
}

static int wren_ioctl_rx_get_config(struct wren_dev *wren, struct wren_ioctl_get_config __user *ucfg)
{
	struct wren_ioctl_get_config cfg;
	int res;
	int32_t err;
	uint32_t act_idx;
	struct wren_dev_action *act;

	if (copy_from_user(&act_idx, &ucfg->config_idx, sizeof(act_idx)))
		return -EFAULT;

	if (act_idx >= WREN_NBR_ACTIONS) {
		res = -EINVAL;
		err = WRENRX_ERR_CFGID_TOO_LARGE;
		goto out_nolock;
	}
	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	res = -EINVAL;

	/* Check if the action exist.  */
	act = &wren->actions[act_idx];
	if (act->cond_idx == WREN_NO_IDX || act->cond_idx == WREN_DEAD_COND) {
		err = WRENRX_ERR_NO_CONFIG;
		goto out;
	}

	/* Copy */
	cfg.config_idx = act_idx;
	memcpy (cfg.config.name, act->name, sizeof(cfg.config.name));
	memcpy (&cfg.config.config, &act->config, sizeof(cfg.config.config));
	if (act->cond_idx == WREN_INV_IDX) {
		cfg.config.cond.src_idx = WREN_NO_SRC_IDX;
		/* FIXME: same as INVALID_ID... */
		cfg.config.cond.evt_id = WREN_NO_IDX;
		cfg.config.cond.len = 0;
	}
	else {
		memcpy (&cfg.config.cond, &wren->conds[act->cond_idx], sizeof(cfg.config.cond));
	}

	/* Done with the mutex */
	mutex_unlock(&wren->data_mutex);

	if (copy_to_user(ucfg, &cfg, sizeof(cfg)))
		return -EFAULT;
	return 0;

out:
	mutex_unlock(&wren->data_mutex);
out_nolock:
	if (copy_to_user(&ucfg->error, &err, 4))
		return -EFAULT;
	return res;
}

static int wren_ioctl_rx_del_config(struct wren_dev *wren, struct wren_ioctl_del_config __user *ucfg)
{
	int res;
	int32_t err;
	uint32_t act_idx, cond_idx;
	struct wren_dev_action *act;
	uint32_t prev_idx;

	if (copy_from_user(&act_idx, &ucfg->config_idx, sizeof(act_idx)))
		return -EFAULT;

	if (act_idx >= WREN_NBR_ACTIONS) {
		res = -EINVAL;
		err = WRENRX_ERR_CFGID_TOO_LARGE;
		goto out_nolock;
	}

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	res = -EINVAL;

	/* Check if the action exist.  */
	act = &wren->actions[act_idx];
	cond_idx = act->cond_idx;
	if (cond_idx == WREN_NO_IDX || cond_idx == WREN_DEAD_COND) {
		err = WRENRX_ERR_NO_CONFIG;
		goto out;
	}

	/* Check the action is not subscribed */
	if (!bitmap_empty(wren->client_configs[act_idx].b, WREN_MAX_CLIENTS)) {
		err = WRENRX_ERR_ACTION_BUSY;
		goto out;
	}

	/* Mark it as dead (before removal on the board). */
	act->cond_idx = WREN_DEAD_COND;

	/* Remove from condition chain (if not an immediate config). */
	if (cond_idx != WREN_INV_IDX) {
		prev_idx = wren->conds[cond_idx].acts;
		if (prev_idx == act_idx)
			wren->conds[cond_idx].acts = act->link;
		else {
			while (1) {
				struct wren_dev_action *prev;
				prev = &wren->actions[prev_idx];
				if (prev->link == act_idx) {
					prev->link = act->link;
					break;
				}
				prev_idx = prev->link;
			}
		}
	}

	/* Remove from board (if enabled).
	   Must be done before deleting the condition. */
	if (act->config.flags & WREN_MB_PULSER_FLAG_ENABLE) {
		err = wren_mb_msg(wren, CMD_RX_DEL_ACTION_NOTIFY,
				  &act_idx, 1, NULL, 0);
		if (err < 0)
			goto out;
	}
	else
		wren_put_action(wren, act_idx);

	/* Free condition (if unused) */
	if (cond_idx != WREN_INV_IDX
	    && wren->conds[cond_idx].acts == WREN_NO_IDX) {
		struct wren_mb_arg1 cmd;

		/* Free it on the board */
		cmd.arg1 = cond_idx;
		err = wren_mb_msg(wren, CMD_RX_DEL_COND, &cmd, sizeof(cmd)/4,
				  NULL, 0);
		if (err < 0)
			dev_err(wren->dev_ctl, "cannot delete cond %u: %d\n",
				cond_idx, err);
		else {
			/* Unlink condition from sources. */
			struct wren_dev_cond *cond = &wren->conds[cond_idx];
			struct wren_dev_source *src = &wren->sources[cond->src_idx];

			if (src->conds == cond_idx)
				src->conds = cond->link;
			else {
				struct wren_dev_cond *pcond =
					&wren->conds[src->conds];
				while (1) {
					if (pcond->link == WREN_NO_IDX) {
						dev_err(wren->dev_ctl,
							"condition %u not found in source %u\n",
							cond_idx, cond->src_idx);
						break;
					}

					if (pcond->link == cond_idx) {
						pcond->link = cond->link;
						break;
					}
					pcond = &wren->conds[pcond->link];
				}
			}
			/* Put it on the free list */
			cond->link = wren->free_conds;
			wren->free_conds = cond_idx;
		}
	}

	res = 0;
	err = 0;
out:
	mutex_unlock(&wren->data_mutex);
out_nolock:
	if (copy_to_user(&ucfg->error, &err, 4))
		return -EFAULT;
	return res;
}

static int wren_ioctl_rx_mod_config(struct wren_dev *wren, struct wren_ioctl_mod_config __user *ucfg)
{
	struct wren_ioctl_mod_config cfg;
	int res;
	int err;
	uint32_t act_idx;
	struct wren_dev_action *act;

	if (copy_from_user(&cfg, ucfg, sizeof(cfg)))
		return -EFAULT;
	act_idx = cfg.config_idx;
	if (act_idx >= WREN_NBR_ACTIONS) {
		err = WRENRX_ERR_CFGID_TOO_LARGE;
		res = -EINVAL;
		goto out_nolock;
	}

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	res = -EINVAL;

	/* Check if the action exist.  */
	act = &wren->actions[act_idx];
	if (act->cond_idx == WREN_NO_IDX || act->cond_idx == WREN_DEAD_COND) {
		err = WRENRX_ERR_ACTION_UNUSED;
		res = -EINVAL;
		goto out;
	}

	/* Cannot change pulser idx (so copy from existing action) */
	cfg.config.pulser_idx = act->config.pulser_idx;

	/* Programm on board */
	if ((act->config.flags & WREN_MB_PULSER_FLAG_ENABLE)
	    && (cfg.config.flags & WREN_MB_PULSER_FLAG_ENABLE)) {
		/* Simply change the action if it was enabled and is still
		   enabled */
		struct wren_mb_rx_mod_action cmd_act;
		uint32_t cmp_idx;

		cmd_act.act_idx = act_idx;
		memcpy(&cmd_act.conf, &cfg.config, sizeof(cmd_act.conf));
		err = wren_mb_msg(wren, CMD_RX_MOD_ACTION,
				  &cmd_act, sizeof(cmd_act)/4,
				  &cmp_idx, sizeof(cmp_idx)/4);
		if (err < 0)
			goto out;
		else if (err == 1) {
			associate_comparator_and_action(wren, act_idx, cmp_idx);
		}
	}
	else if (!(act->config.flags & WREN_MB_PULSER_FLAG_ENABLE)
		 && (cfg.config.flags & WREN_MB_PULSER_FLAG_ENABLE)) {
		/* Add the action if the modification enabled it */
		if (act->cond_idx == WREN_INV_IDX) {
			/* Immediate action */
			struct wren_mb_rx_imm_action cmd_act;
			uint32_t cmp_idx;

			cmd_act.act_idx = act_idx;
			memcpy(&cmd_act.cmp.conf,
			       &cfg.config, sizeof(cmd_act.cmp.conf));
			cmd_act.cmp.ts.sec = 0;
			cmd_act.cmp.ts.nsec = 0;
			cmd_act.cmp.conf.flags |= WREN_MB_PULSER_FLAG_IMMEDIAT;
			err = wren_mb_msg(wren, CMD_RX_IMM_ACTION,
					  &cmd_act, sizeof(cmd_act)/4,
					  &cmp_idx, 1);
			if (err < 0)
				goto out;

			associate_comparator_and_action(wren, act_idx, cmp_idx);
		}
		else {
			/* Normal action */
			struct wren_mb_rx_set_action cmd_act;
			cmd_act.act_idx = act_idx;
			cmd_act.cond_idx = act->cond_idx;
			memcpy(&cmd_act.conf,
			       &cfg.config, sizeof(cmd_act.conf));
			err = wren_mb_msg(wren, CMD_RX_SET_ACTION,
					  &cmd_act, sizeof(cmd_act)/4, NULL, 0);
			if (err < 0)
				goto out;
		}
	}
	else if ((act->config.flags & WREN_MB_PULSER_FLAG_ENABLE)
		 && !(cfg.config.flags & WREN_MB_PULSER_FLAG_ENABLE)) {
		/* Remove the action if the modification disabled it */
		err = wren_mb_msg(wren, CMD_RX_DEL_ACTION, &act_idx, 1,
				  NULL, 0);
		if (err < 0)
			goto out;
	}
	else {
		/* Was disabled and still disabled: nothing to do on the
		   board.  */
	}

	/* Copy */
	memcpy (&act->config, &cfg.config, sizeof(cfg.config));

	res = 0;
	err = 0;

out:
	mutex_unlock(&wren->data_mutex);
out_nolock:
	if (copy_to_user(&ucfg->error, &err, 4))
		return -EFAULT;
	return res;
}

static int wren_ioctl_rx_force_pulser(struct wren_dev *wren, struct wren_ioctl_force_pulser __user *ucfg)
{
	struct wren_ioctl_force_pulser cfg;
	int res;
	int err;
	uint32_t act_idx;
	struct wren_dev_action *act;
	struct wren_mb_rx_force_pulser cmd_act;
	uint32_t cmp_idx;

	if (copy_from_user(&cfg, ucfg, sizeof(cfg)))
		return -EFAULT;
	act_idx = cfg.config_idx;
	if (act_idx >= WREN_NBR_ACTIONS) {
		err = WRENRX_ERR_CFGID_TOO_LARGE;
		res = -EINVAL;
		goto out_nolock;
	}

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	res = -EINVAL;

	/* Check if the action exist.  */
	act = &wren->actions[act_idx];
	if (act->cond_idx == WREN_NO_IDX || act->cond_idx == WREN_DEAD_COND) {
		err = WRENRX_ERR_ACTION_UNUSED;
		goto out;
	}

	/* Cannot change pulser idx (so copy from existing action) */
	cfg.config.pulser_idx = act->config.pulser_idx;

	/* Programm on board */
	cmd_act.act_idx = act_idx;
	cmd_act.ts.sec = 0;
	cmd_act.ts.nsec = 0;
	memcpy(&cmd_act.conf, &cfg.config, sizeof(cmd_act.conf));
	err = wren_mb_msg(wren, CMD_RX_FORCE_PULSER,
			  &cmd_act, sizeof(cmd_act)/4,
			  &cmp_idx, sizeof(cmp_idx)/4);
	if (err < 0) {
		WREN_LOG(wren, WREN_DRV_LOG_ERROR, "force error: %d\n", err);
		goto out;
	}

	/* Set action to the comparator */
	associate_comparator_and_action(wren, act_idx, cmp_idx);

	res = 0;
	err = 0;

out:
	mutex_unlock(&wren->data_mutex);
out_nolock:
	if (copy_to_user(&ucfg->error, &err, 4))
		return -EFAULT;
	return res;
}

static int wren_ioctl_rx_subscribe_config(struct wren_client *client, uint32_t __user *uidx, int en)
{
	struct wren_dev *wren = client->wren;
	int res;
	uint32_t act_idx, cond_idx, src_idx;

	if (copy_from_user(&act_idx, uidx, sizeof(act_idx)))
		return -EFAULT;
	if (act_idx >= WREN_NBR_ACTIONS)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	cond_idx = wren->actions[act_idx].cond_idx;
	if (cond_idx == WREN_NO_IDX || cond_idx == WREN_DEAD_COND) {
		res = -ENOENT;
		goto out;
	}

	if (cond_idx == WREN_INV_IDX) {
		/* Immediate config */
		src_idx = WREN_INV_IDX;
	} else {
		src_idx = wren->conds[cond_idx].cond.src_idx;
	}

	if (en) {
		set_bit(client->idx, wren->client_configs[act_idx].b);

		if (src_idx != WREN_INV_IDX
		    && atomic_inc_return(&client->sources[src_idx].ref) == 1)
			set_bit(client->idx, wren->client_sources[src_idx].b);
	} else {
		clear_bit(client->idx, wren->client_configs[act_idx].b);

		if (src_idx != WREN_INV_IDX
		    && atomic_dec_return(&client->sources[src_idx].ref) == 0)
			clear_bit(client->idx, wren->client_sources[src_idx].b);
	}

	/* TODO: if status has changed, need to inform the board */

	res = 0;
out:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_event_subscribe(struct wren_client *client, struct wren_ioctl_event_subscribe __user *uesub)
{
	struct wren_dev *wren = client->wren;
	struct wren_ioctl_event_subscribe esub;
	int res;
	int32_t err = 0;

	if (copy_from_user(&esub, uesub, sizeof(esub)))
		return -EFAULT;

	if (esub.event_id >= WREN_NBR_EVENTID) {
		err = WRENRX_ERR_EVID_TOO_LARGE;
		res = -EINVAL;
		goto error;
	}
	if (esub.source_idx >= WREN_NBR_RX_SOURCES) {
		err = WRENRX_ERR_SRCID_TOO_LARGE;
		res = -EINVAL;
		goto error;
	}

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	if (test_and_set_bit(esub.event_id, client->sources[esub.source_idx].evsubs)) {
		/* Already subscribed */
		err = WRENRX_ERR_EVID_SUBSCRIBED;
		res = -EINVAL;
		goto out;
	}

	if (atomic_inc_return(&wren->sources[esub.source_idx].evsubs[esub.event_id]) == 1) {
		/* Need to subscribe to the event */
		struct wren_mb_rx_subscribe cmd;
		uint32_t rep;
		cmd.src_idx = esub.source_idx;
		cmd.ev_id = esub.event_id;
		res = wren_mb_msg(wren, CMD_RX_SUBSCRIBE,
				  &cmd, sizeof(cmd) / 4, &rep, sizeof(rep) / 4);
		if (res < 0) {
			atomic_dec(&wren->sources[esub.source_idx].evsubs[esub.event_id]);
			err = res;
			res = -ECOMM;
			goto out;
		}
	}

out:
	mutex_unlock(&wren->data_mutex);
error:
	if (copy_to_user(&uesub->error, &err, 4))
		res = -EFAULT;
	return res;
}

static int wren_ioctl_rx_event_unsubscribe(struct wren_client *client, struct wren_ioctl_event_subscribe __user *uesub)
{
	struct wren_dev *wren = client->wren;
	struct wren_ioctl_event_subscribe esub;
	int res;
	int32_t err;

	if (copy_from_user(&esub, uesub, sizeof(esub)))
		return -EFAULT;
	if (esub.event_id >= WREN_NBR_EVENTID) {
		err = WRENRX_ERR_EVID_TOO_LARGE;
		res = -EINVAL;
		goto error;
	}
	if (esub.source_idx >= WREN_NBR_RX_SOURCES) {
		err = WRENRX_ERR_SRCID_TOO_LARGE;
		res = -EINVAL;
		goto error;
	}

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	if (!test_and_clear_bit(esub.event_id, client->sources[esub.source_idx].evsubs)) {
		/* Already unsubscribed */
		res = -EBUSY;
		goto out;
	}

	res = wren_rx_event_unsubscribe(wren, esub.source_idx, esub.event_id);
	if (res < 0) {
		err = res;
		res = -ECOMM;
	}

out:
	mutex_unlock(&wren->data_mutex);
error:
	if (copy_to_user(&uesub->error, &err, 4))
		res = -EFAULT;
	return res;
}

static int wren_ioctl_rx_subscribe_context(struct wren_client *client, uint32_t __user *usrc, int en)
{
	struct wren_dev *wren = client->wren;
	uint32_t src_idx;

	if (copy_from_user(&src_idx, usrc, sizeof(src_idx)))
		return -EFAULT;
	if (src_idx >= WREN_NBR_RX_SOURCES)
		return -EINVAL;

	if (en) {
		if (atomic_inc_return(&client->sources[src_idx].ref) == 1)
			set_bit(client->idx, wren->client_sources[src_idx].b);
	} else {
		if (atomic_dec_return(&client->sources[src_idx].ref) == 0)
			clear_bit(client->idx, wren->client_sources[src_idx].b);
	}

	if (en) {
		/* Send cached contexts to client */
		wren_send_cached_contexts (client, src_idx);
	}

	/* TODO: add a call to the board */
	return 0;
}

static int wren_ioctl_rx_get_output(struct wren_dev *wren, struct wren_ioctl_out __user *uout)
{
	struct wren_ioctl_out out;
	int res;
	struct wren_mb_out_cfg rep;
	struct wren_mb_arg1 cmd;

	if (copy_from_user(&out.output_idx, &uout->output_idx, sizeof(out.output_idx)))
		return -EFAULT;

	cmd.arg1 = out.output_idx;
	res = wren_mb_msg(wren, CMD_RX_GET_OUT_CFG, &cmd, sizeof(cmd)/4,
			  &rep, sizeof(rep) / 4);
	if (res < 0)
		return -ECOMM;

	out.mask = rep.mask;
	out.inv_in = rep.inv_in;
	out.inv_out = rep.inv_out;

	if (copy_to_user(uout, &out, sizeof(out)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_rx_set_pin(struct wren_dev *wren, struct wren_ioctl_pin __user *upin)
{
	struct wren_ioctl_pin pin;
	int res;
	struct wren_mb_pin_cfg cmd;

	if (copy_from_user(&pin, upin, sizeof(pin)))
		return -EFAULT;

	cmd.pin_idx = pin.pin_idx;
	cmd.oe = pin.oe;
	cmd.term = pin.term;
	cmd.inv = pin.inv;
	res = wren_mb_msg(wren, CMD_RX_SET_PIN_CFG, &cmd, sizeof(cmd)/4,
			  NULL, 0);
	if (res < 0)
		return -ECOMM;

	return 0;
}

static int wren_ioctl_rx_get_pin(struct wren_dev *wren, struct wren_ioctl_pin __user *upin)
{
	struct wren_ioctl_pin pin;
	int res;
	struct wren_mb_pin_cfg rep;
	struct wren_mb_arg1 cmd;

	if (copy_from_user(&pin.pin_idx, &upin->pin_idx, sizeof(pin.pin_idx)))
		return -EFAULT;

	cmd.arg1 = pin.pin_idx;
	res = wren_mb_msg(wren, CMD_RX_GET_PIN_CFG, &cmd, sizeof(cmd)/4,
			  &rep, sizeof(rep) / 4);
	if (res < 0)
		return -ECOMM;

	pin.oe = rep.oe;
	pin.term = rep.term;
	pin.inv = rep.inv;

	if (copy_to_user(upin, &pin, sizeof(pin)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_rx_set_output(struct wren_dev *wren, struct wren_ioctl_out __user *uout)
{
	struct wren_ioctl_out out;
	int res;
	struct wren_mb_out_cfg cmd;

	if (copy_from_user(&out, uout, sizeof(out)))
		return -EFAULT;

	cmd.out_idx = out.output_idx;
	cmd.mask = out.mask;
	cmd.inv_in = out.inv_in;
	cmd.inv_out = out.inv_out;
	res = wren_mb_msg(wren, CMD_RX_SET_OUT_CFG, &cmd, sizeof(cmd)/4,
			  NULL, 0);
	if (res < 0)
		return -ECOMM;

	return 0;
}

static int wren_ioctl_rx_set_promisc(struct wren_client *client, uint32_t __user *usrc)
{
	struct wren_dev *wren = client->wren;
	uint32_t src_mask;

	if (copy_from_user(&src_mask, usrc, sizeof(src_mask)))
		return -EFAULT;

	if (src_mask != 0xffffffff && src_mask != 0)
		return -EINVAL;

	if (src_mask) {
		/* Error if already in promiscuous mode */
		if (atomic_cmpxchg(&wren->promisc, -1, client->idx) != -1)
			return -EBUSY;
	}
	else {
		/* Error if not in promisc mode by this client */
		if (atomic_read(&wren->promisc) != client->idx)
			return -EPERM;
	}

	wren_mb_msg(wren, CMD_RX_SET_PROMISC,
		    &src_mask, sizeof(src_mask) / 4, NULL, 0);

	if (!src_mask)
		atomic_set(&wren->promisc, -1);
	return 0;
}

/* Return true if L > R */
static int wren_ctime_after(const struct wren_compact_time *l,
			    const struct wren_compact_time *r)
{
	if (l->hi > r->hi)
		return 1;
	if (l->hi == r->hi && l->lo > r->lo)
		return 1;
	return 0;
}

static void to_compact_time(struct wren_compact_time *dst,
			    const struct wren_wr_time *src)
{
	dst->lo = ((src->ns >> 4) & 0x0fffffff) | (src->tai_lo << 28);
	dst->hi = (src->tai_lo >> 4) | (src->tai_hi << 28);
}

/* Must be called with to_lock hold; can modify now. */
static void timeout_run(struct wren_dev *wren,
			struct wren_compact_time *now)
{
	struct host_map *regs = wren->regs;

	while (1) {
		struct wren_timeout_entry *it;

		if (list_empty(&wren->to_list)) {
			/* Empty list, can mask timer interrupt */
			if (wren->imr & HOST_MAP_INTC_IMR_TIMER) {
				wren->imr &= ~HOST_MAP_INTC_IMR_TIMER;
				iowrite32(wren->imr, &regs->intc.imr);
			}
			return;
		}

		it = list_first_entry(&wren->to_list,
				      struct wren_timeout_entry, entry);
		WREN_LOG(wren, WREN_DRV_LOG_TIMEOUT,
			 "timeout_run now=%x:%x, ctime=%x:%x\n",
			 now->hi, now->lo, it->ctime.hi, it->ctime.lo);
		if (wren_ctime_after(&it->ctime, now)) {
			/* Reprogram the timer (FIXME: useless if no wakeup) */
			iowrite32(it->ctime.lo, &regs->intc.tm_timer);

			/* Check if time in the past */
			if (wren_read_ctime(wren, now) < 0) {
				/* Should not happen: not sync */
				return;
			}

			/* If not, it's ok */
			if (wren_ctime_after(&it->ctime, now))
				return;

			/* Otherwise, continue */
		}
		list_del(&it->entry);
		it->flags = TIMEOUT_EXPIRED;
		if (it->me)
			wake_up_process(it->me);
	}
}

static void timeout_add_entry(struct wren_dev *wren,
			      struct wren_timeout_entry *my_toe)
{
	struct list_head *prev;
	struct wren_timeout_entry *it;

	BUG_ON(my_toe->flags & TIMEOUT_VALID);

	my_toe->flags = TIMEOUT_VALID;

	prev = &wren->to_list;
	list_for_each_entry(it, &wren->to_list, entry) {
		if (!wren_ctime_after(&my_toe->ctime, &it->ctime)) {
			__list_add(&my_toe->entry, prev, &it->entry);
			return;
		}
		prev = &it->entry;
	}
	list_add_tail(&my_toe->entry, &wren->to_list);
}

/* Must be called with wren->to_lock taken */
static int wren_add_timeout(struct wren_dev *wren, struct wren_timeout_entry *te)
{
	timeout_add_entry(wren, te);

	if (list_first_entry(&wren->to_list,
			     struct wren_timeout_entry, entry) == te) {
		struct host_map *regs = wren->regs;
		struct wren_compact_time now;

		/* First in the list, reprogram the timeout.  */
		iowrite32(te->ctime.lo, &regs->intc.tm_timer);
		if (!(wren->imr & HOST_MAP_INTC_IMR_TIMER)) {
			/* Ack the timer interrupt to get rid of pending
			   interrupt.  */
			iowrite32(HOST_MAP_INTC_ISR_TIMER, &regs->intc.iack);
			/* Unmask.
			   If the timer triggered between the write and the
			   ack, it will be handled as if it was programmed
			   in the past. */
			wren->imr |= HOST_MAP_INTC_IMR_TIMER;
			iowrite32(wren->imr, &regs->intc.imr);
		}

		/* Maybe the timer was programmed in the past... */
		if (wren_read_ctime(wren, &now) < 0) {
			/* TODO: in case of error (not sync) */
			timeout_run(wren, &now);
			return -EIO;
		}

		/* In that case, remove from list and check the list */
		if (!wren_ctime_after(&te->ctime, &now)) {
			list_del(&te->entry);
			te->flags = TIMEOUT_EXPIRED;
			timeout_run(wren, &now);
			return -ETIME;
		}
	}
	return 0;
}

static int wren_wait_time(struct wren_dev *wren, struct wren_compact_time *to)
{
	unsigned long flags;
	struct wren_timeout_entry my_entry;
	int res;

	/* Init entry */
	my_entry.ctime = *to;
	INIT_LIST_HEAD(&my_entry.entry);
	my_entry.me = current;
	my_entry.flags = 0;

	spin_lock_irqsave(&wren->to_lock, flags);

	res = wren_add_timeout(wren, &my_entry);

	if (res == -ETIME) {
		res = 0;
		goto done;
	}
	if (res < 0)
		goto done;

	/* FIXME: should it be an infinite loop ?
	   FIXME: use a normal wait queue ?
	   Who else can awake us ? */
	set_current_state(TASK_INTERRUPTIBLE);
	spin_unlock_irqrestore(&wren->to_lock, flags);
	if (signal_pending(current)) {
		/* Remove link if interrupted! */
		spin_lock_irqsave(&wren->to_lock, flags);
		list_del(&my_entry.entry);
		res = -ERESTARTSYS;
		goto done;
	}
	schedule();

	return 0;

done:
	spin_unlock_irqrestore(&wren->to_lock, flags);
	return res;
}

static int wren_ioctl_get_link(struct wren_dev *wren, u32 __user *uarg)
{
	struct host_map *regs = wren->regs;
	u32 val;

	val = (regs->wr_state & HOST_MAP_WR_STATE_LINK_UP) != 0;

	if (copy_to_user(uarg, &val, sizeof(val)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_get_sync(struct wren_dev *wren, u32 __user *uarg)
{
	u32 val = wren->wr_synced;

	if (copy_to_user(uarg, &val, sizeof(val)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_get_time(struct wren_dev *wren, struct wren_wr_time __user *uarg)
{
	struct wren_wr_time res;

	memset(&res, 0, sizeof(res));
	if (wren_read_time(wren, &res) < 0)
		return -EIO;

	if (copy_to_user(uarg, &res, sizeof(res)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_wait_time(struct wren_dev *wren, struct wren_wr_time __user *uarg)
{
	struct wren_wr_time wr_to;
	struct wren_compact_time cto;

	if (!wren->wr_synced) {
		return -EIO;
	}

	if (copy_from_user(&wr_to, uarg, sizeof(wr_to)))
		return -EFAULT;

	/* Convert to compact time */
	to_compact_time(&cto, &wr_to);

	return wren_wait_time(wren, &cto);
}

static int wren_ioctl_rx_set_timeout(struct wren_client *client, struct wren_wr_time __user *uarg)
{
	struct wren_dev *wren = client->wren;
	struct wren_wr_time wr_to;
	unsigned long flags;
	int res;

	if (!wren->wr_synced) {
		return -EIO;
	}

	if (copy_from_user(&wr_to, uarg, sizeof(wr_to)))
		return -EFAULT;

	spin_lock_irqsave(&wren->to_lock, flags);

	if (wren_client_wait_timeout(client)) {
		/* There is already a timeout, remove it.  */
		/* TODO: maybe force set timer, if this was the first entry */
		list_del(&client->timeout.entry);
		client->timeout.flags = 0;
	}

	/* Convert to compact time */
	to_compact_time(&client->timeout.ctime, &wr_to);

	res = wren_add_timeout(wren, &client->timeout);

	/* The current thread will be woken up */
	client->timeout.me = current;

	spin_unlock_irqrestore(&wren->to_lock, flags);

	return res;
}

static int wren_ioctl_version(struct wren_dev *wren, uint32_t __user *uver)
{
	u32 ver = WREN_IOCTL_VERSION;
	if (copy_to_user((u32 __user *)uver, &ver, sizeof(ver)))
		return -EFAULT;
	return 0;
}

static int append_to_user(const char *str, unsigned len,
			  struct wren_ioctl_version_string *ver)
{
	if (len > ver->maxlen)
		len = ver->maxlen;
	if (copy_to_user(ver->buf, str, len))
		return -1;
	ver->buf += len;
	ver->maxlen -= len;
	return 0;
}

static int wren_ioctl_version_string(struct wren_dev *wren, void __user *uver)
{
	struct host_map *regs = wren->regs;
	struct wren_ioctl_version_string ver;
	struct wren_mb_version rep;
	static const char sect_hw[] = "*hardware:\n";
	static const char sect_gw[] = "*gateware:\n";
	static const char sect_fw[] = "*firmware:\n";
	static const char sect_drv[] = "*driver:\n";
	static const char tag[] = "tag:" VERSION "\n";
	char buf[256];
	int res;
	unsigned i;

	if (copy_from_user(&ver, uver, sizeof(ver)))
		return -EFAULT;

	/* Hardware */
	if (append_to_user(sect_hw, sizeof(sect_hw) - 1, &ver))
		return -EFAULT;
	snprintf(buf, sizeof(buf), "version:0x%08x\n", wren->map_ver);
	if (append_to_user(buf, strlen(buf), &ver))
		return -EFAULT;

	/* Gateware */
	if (append_to_user(sect_gw, sizeof(sect_gw) - 1, &ver))
		return -EFAULT;
	for (i = 0; i < 64; i++) {
		uint32_t w = ioread32(regs->buildinfo + i);
		unsigned j;

		for (j = 0; j < 4; j++) {
			buf[4*i+j] = w;
			w >>= 8;
		}
	}
	buf[255] = 0;
	if (buf[0] == 'b')
		if (append_to_user(buf, strlen(buf), &ver))
			return -EFAULT;

	/* Firmware */
	if (append_to_user(sect_fw, sizeof(sect_fw) - 1, &ver))
		return -EFAULT;
	res = wren_mb_msg(wren, CMD_FW_VERSION, NULL, 0, &rep, sizeof(rep)/4);
	if (res < 0)
		return -ECOMM;
	if (append_to_user(rep.u.c, strlen(rep.u.c), &ver))
		return -EFAULT;

	/* Driver */
	if (append_to_user(sect_drv, sizeof(sect_drv) - 1, &ver))
		return -EFAULT;
	/* Append NUL */
	if (append_to_user(tag, sizeof(tag), &ver))
		return -EFAULT;
	return 0;
}


static int wren_ioctl_get_drv_log_flags(struct wren_dev *wren, uint32_t __user *umask)
{
	if (copy_to_user((u32 __user *)umask, &wren->log_mask, sizeof(wren->log_mask)))
		return -EFAULT;
	return 0;
}

static int wren_ioctl_set_drv_log_flags(struct wren_dev *wren, uint32_t __user *umask)
{
	uint32_t mask;
	if (copy_from_user(&mask, umask, sizeof(mask)))
		return -EFAULT;
	wren->log_mask = mask;
	return 0;
}

static int wren_ioctl_get_log_index(struct wren_dev *wren, uint32_t __user *uidx)
{
	struct host_map *regs = wren->regs;
	uint32_t idx;

	if (copy_from_user(&idx, uidx, sizeof(idx)))
		return -EFAULT;

	if (idx > WREN_NBR_PULSERS + WREN_NBR_PINS)
		return -EINVAL;

	/* Get log index */
	idx = regs->logs.addr[idx].val;

	if (copy_to_user(uidx, &idx, sizeof(idx)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_get_stamper_log(struct wren_dev *wren, struct wren_ioctl_stamper_log __user *ulog, unsigned pulser_or_pin)
{
	struct host_map *regs = wren->regs;
	struct wren_ioctl_stamper_log log;
	struct wren_mb_rx_get_log cmd;
	struct log_entry {
		uint32_t sec;
		uint32_t nsec;
		uint32_t flags;
	};
	struct wren_wr_time now;
	uint32_t rep[128];
	void __user *uptr;
	uint32_t idx;
	uint32_t len;
	uint32_t prev_sec;
	int res;

	if (copy_from_user(&log, ulog, sizeof(log)))
		return -EFAULT;

	/* Read current time */
	memset(&now, 0, sizeof(now));
	if (wren_read_time(wren, &now) < 0) {
		res = WRENRX_ERR_NO_TIME;
		goto failed;
	}

	if (pulser_or_pin == 0 && log.idx > WREN_NBR_PULSERS) {
		res = WRENRX_ERR_PULSER_NUM;
		goto failed;
	}
	if (pulser_or_pin == 1) {
		if (log.idx > WREN_NBR_PINS) {
			res = WRENRX_ERR_PIN_NUM;
			goto failed;
		}
		log.idx += WREN_NBR_PULSERS;
	}

	/* Get log index */
	idx = regs->logs.addr[log.idx].val;

	/* If the index has not yet overflowed, there might be a limited
	   number of entries */
	if ((idx & 1) == 0) {
		/* No overflow */
		unsigned max = (idx & 0x00ffffff) >> 2;
		if (log.len > max)
			log.len = max;
	}

	/* Loop for each index */
	uptr = log.buf;
	prev_sec = now.tai_lo & 0xff;
	for (len = log.len; len != 0; ) {
		struct log_entry e;
		unsigned l = len > 128 ? 128 : len;
		unsigned j;

		/* TODO: use DMA ?  */
		cmd.log_idx = log.idx;
		cmd.nentries = l;
		cmd.entry_idx = idx;
		res = wren_mb_msg(wren, CMD_RX_LOG_READ,
				  &cmd, sizeof(cmd)/4, rep, sizeof(rep)/4);
		if (res < 0)
			goto failed;
		for (j = 0; j < l; j++) {
			/* Reformat */
			unsigned lo_sec = rep[j] >> 24;
			if (lo_sec > prev_sec) {
				/* Handle time wrap-around */
				now.tai_lo -= 0x100;
			}
			prev_sec = lo_sec;
			e.sec = (now.tai_lo & ~0xff) | lo_sec;
			e.nsec = ((rep[j] >> 4) & 0xfffff) * 1000;
			e.flags = rep[j] & 0xff;
			if (copy_to_user(uptr, &e, sizeof(e)))
				return -EFAULT;
			uptr += sizeof(e);
		}
		len -= l;
		/* Indexes are going backward */
		idx -= l * 4;
	}
	return log.len;

failed:
	log.error = res;
	if (copy_to_user(&ulog->error, &log.error, sizeof(log.error)))
		return -EFAULT;
	return -EINVAL;
}

static int wren_ioctl_get_sw_log(struct wren_dev *wren, struct wren_ioctl_stamper_log __user *ulog)
{
	struct wren_ioctl_stamper_log log;
	struct wren_mb_arg_reply rep;
	void __user *uptr;
	uint32_t idx;
	uint32_t max;
	uint32_t len;
	unsigned i;
	unsigned rem;
	int res;

	if (copy_from_user(&log, ulog, sizeof(log)))
		return -EFAULT;

	/* Get log index */
	res = wren_mb_msg(wren, CMD_EVLOG_INDEX,
			  NULL, 0,
			  &rep, sizeof(rep)/4);
	if (res < 0)
		goto failed;

	idx = rep.arg1;
	max = rep.arg2;

	len = log.len > max ? max : log.len;

	if (idx > len)
		idx -= len;
	else
		idx = WREN_EVLOG_MAX + idx - len;

	/* Loop for each index */
	uptr = log.buf;
	rem = 0;
	for (i = 0; i < len; i++) {
		union wren_evlog_entry ent;
		struct wren_mb_arg1 cmd;
		unsigned l;

		/* TODO: use DMA ?  */
		cmd.arg1 = idx;
		res = wren_mb_msg(wren, CMD_EVLOG_READ,
				  &cmd, sizeof(cmd)/4, &ent, sizeof(ent)/4);
		if (res < 0)
			goto failed;
		if (ent.e.type != EVLOG_TYPE_CONT)
			rem = ent.e.len;
		l = rem;
		if (l > WREN_EVLOG_DSIZE)
			l = WREN_EVLOG_DSIZE;
		if (copy_to_user(uptr, &ent, l + WREN_EVLOG_HSIZE))
			return -EFAULT;
		uptr += sizeof(ent);
		rem -= l;

		idx = (idx + 1) & (WREN_EVLOG_MAX - 1);
	}
	return len;

failed:
	log.error = res;
	if (copy_to_user(&ulog->error, &log.error, sizeof(log.error)))
		return -EFAULT;
	return -EINVAL;
}

static int wren_ioctl_get_panel_config(struct wren_dev *wren, struct wren_panel_config *ucfg)
{
	struct wren_panel_config cfg;
	struct wren_mb_hw_config rep;
	struct wren_mb_hw_userio userio;
	int res;

	/* Get hw config */
	res = wren_mb_msg(wren, CMD_HW_GET_CONFIG,
			  NULL, 0, &rep, sizeof(rep)/4);
	if (res < 0)
		return -ECOMM;

	res = wren_mb_msg(wren, CMD_HW_GET_USERIO,
			  NULL, 0, &userio, sizeof(userio)/4);
	if (res < 0)
		return -ECOMM;

	cfg.pp_pin = userio.patchpanel ? 32 : 0;
	cfg.vme_p2a_be = userio.vme_p2a_out_be;

	if (rep.model_ident == 0x766d6530) {
		/* vme0 */
		cfg.fp_pin = 16;
		cfg.fp_fixed_inp = 0;
	}
	else if (rep.model_ident == 0x766d6531) {
		/* vme1 */
		cfg.fp_pin = 14;
		cfg.fp_fixed_inp = 6;
	}
	else if (rep.model_ident == 0x70636931) {
		/* pci1 */
		cfg.fp_pin = 6;
		cfg.fp_fixed_inp = 2;
	}
	else  {
		/* unknown */
		cfg.fp_pin = 0;
		cfg.fp_fixed_inp = 0;
	}
	if (copy_to_user(ucfg, &cfg, sizeof(cfg)))
		return -EFAULT;
	return 0;
}

static int wren_ioctl_set_vme_p2a_output(struct wren_dev *wren, unsigned __user *uarg)
{
	struct wren_mb_arg1 arg;
	int res;

	if (copy_from_user(&arg.arg1, uarg, sizeof(*uarg)))
		return -EFAULT;

	res = wren_mb_msg(wren, CMD_VME_P2A_SET_OUTPUT,
			  &arg, sizeof(arg) / 4, NULL, 0);
	return res;
}

static int wren_ioctl_set_patchpanel_config(struct wren_dev *wren, unsigned __user *uarg)
{
	struct wren_mb_arg1 arg;
	int res;

	if (copy_from_user(&arg.arg1, uarg, sizeof(*uarg)))
		return -EFAULT;

	res = wren_mb_msg(wren, CMD_RX_SET_PATCHPANEL,
			  &arg, sizeof(arg) / 4, NULL, 0);
	return res;
}

static ssize_t wren_ctl_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct wren_dev *wren = filp->private_data;

	switch(cmd) {
	case WREN_IOC_GET_VERSION:
		return wren_ioctl_version(wren, (uint32_t __user*)arg);
	case WREN_IOC_GET_LINK:
		return wren_ioctl_get_link(wren, (u32 __user *)arg);
	case WREN_IOC_GET_SYNC:
		return wren_ioctl_get_sync(wren, (u32 __user *)arg);
	case WREN_IOC_GET_TIME:
		return wren_ioctl_get_time(wren, (void __user*)arg);
	case WREN_IOC_WAIT_TIME:
		return wren_ioctl_wait_time(wren, (void __user*)arg);
	case WREN_IOC_MSG:
		return wren_ioctl_msg_usr(wren, (void __user *)arg);

	case WREN_IOC_RX_ADD_SOURCE:
		return wren_ioctl_rx_add_source(wren, (void __user*)arg);
	case WREN_IOC_RX_GET_SOURCE:
		return wren_ioctl_rx_get_source(wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_SOURCE:
		return wren_ioctl_rx_del_source(wren, (void __user*)arg);

	case WREN_IOC_RX_ADD_CONFIG:
		return wren_ioctl_rx_add_config(wren, (void __user*)arg);
	case WREN_IOC_RX_GET_CONFIG:
		return wren_ioctl_rx_get_config(wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_CONFIG:
		return wren_ioctl_rx_del_config(wren, (void __user*)arg);
	case WREN_IOC_RX_MOD_CONFIG:
		return wren_ioctl_rx_mod_config(wren, (void __user*)arg);
	case WREN_IOC_RX_IMM_CONFIG:
		return wren_ioctl_rx_imm_config(wren, (void __user*)arg);
	case WREN_IOC_RX_FORCE_PULSER:
		return wren_ioctl_rx_force_pulser(wren, (void __user*)arg);
	case WREN_IOC_SET_DRV_LOG_FLAGS:
		return wren_ioctl_set_drv_log_flags(wren, (void __user*)arg);
	case WREN_IOC_GET_DRV_LOG_FLAGS:
		return wren_ioctl_get_drv_log_flags(wren, (void __user*)arg);
	case WREN_IOC_GET_HW_LOG_INDEX:
		return wren_ioctl_get_log_index(wren, (void __user*)arg);
	case WREN_IOC_GET_PULSER_LOG:
		return wren_ioctl_get_stamper_log(wren, (void __user*)arg, 0);
	case WREN_IOC_GET_PIN_LOG:
		return wren_ioctl_get_stamper_log(wren, (void __user*)arg, 1);
	case WREN_IOC_GET_SW_LOG:
		return wren_ioctl_get_sw_log(wren, (void __user*)arg);
	case WREN_IOC_GET_PANEL_CONFIG:
		return wren_ioctl_get_panel_config(wren, (void __user*)arg);
	case WREN_IOC_SET_VME_P2A_OUTPUT:
		return wren_ioctl_set_vme_p2a_output(wren, (void __user*)arg);
	case WREN_IOC_SET_PATCHPANEL:
		return wren_ioctl_set_patchpanel_config(wren, (void __user*)arg);

	case WREN_IOC_TX_SET_TABLE:
		return wren_ioctl_tx_set_table(wren, (void __user*)arg);
	default:
		return -ENOTTY;
	}
}

static ssize_t wren_usr_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct wren_client *client = filp->private_data;

	switch(cmd) {
	case WREN_IOC_GET_VERSION:
		return wren_ioctl_version(client->wren, (uint32_t __user*)arg);
	case WREN_IOC_GET_LINK:
		return wren_ioctl_get_link(client->wren, (u32 __user *)arg);
	case WREN_IOC_GET_SYNC:
		return wren_ioctl_get_sync(client->wren, (u32 __user *)arg);
	case WREN_IOC_VERSION_STRING:
		return wren_ioctl_version_string(client->wren, (void __user *)arg);
	case WREN_IOC_GET_TIME:
		return wren_ioctl_get_time(client->wren, (void __user*)arg);

	case WREN_IOC_RX_ADD_SOURCE:
		return wren_ioctl_rx_add_source(client->wren, (void __user*)arg);
	case WREN_IOC_RX_GET_SOURCE:
		return wren_ioctl_rx_get_source(client->wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_SOURCE:
		return wren_ioctl_rx_del_source(client->wren, (void __user*)arg);

	case WREN_IOC_RX_ADD_CONFIG:
		return wren_ioctl_rx_add_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_GET_CONFIG:
		return wren_ioctl_rx_get_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_CONFIG:
		return wren_ioctl_rx_del_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_MOD_CONFIG:
		return wren_ioctl_rx_mod_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_IMM_CONFIG:
		return wren_ioctl_rx_imm_config(client->wren, (void __user*)arg);

	case WREN_IOC_RX_FORCE_PULSER:
		return wren_ioctl_rx_force_pulser(client->wren, (void __user*)arg);
	case WREN_IOC_RX_SUBSCRIBE_CONFIG:
		return wren_ioctl_rx_subscribe_config(client, (void __user*)arg, 1);
	case WREN_IOC_RX_UNSUBSCRIBE_CONFIG:
		return wren_ioctl_rx_subscribe_config(client, (void __user*)arg, 0);
	case WREN_IOC_RX_SUBSCRIBE_EVENT:
		return wren_ioctl_rx_event_subscribe(client, (void __user*)arg);
	case WREN_IOC_RX_UNSUBSCRIBE_EVENT:
		return wren_ioctl_rx_event_unsubscribe(client, (void __user*)arg);
	case WREN_IOC_RX_SUBSCRIBE_CONTEXT:
		return wren_ioctl_rx_subscribe_context(client, (void __user*)arg, 1);
	case WREN_IOC_RX_UNSUBSCRIBE_CONTEXT:
		return wren_ioctl_rx_subscribe_context(client, (void __user*)arg, 0);

	case WREN_IOC_RX_SET_OUTPUT:
		return wren_ioctl_rx_set_output(client->wren, (void __user*)arg);
	case WREN_IOC_RX_GET_OUTPUT:
		return wren_ioctl_rx_get_output(client->wren, (void __user*)arg);
	case WREN_IOC_RX_SET_PIN:
		return wren_ioctl_rx_set_pin(client->wren, (void __user*)arg);
	case WREN_IOC_RX_GET_PIN:
		return wren_ioctl_rx_get_pin(client->wren, (void __user*)arg);

	case WREN_IOC_RX_SET_PROMISC:
		return wren_ioctl_rx_set_promisc(client, (void __user*)arg);
	case WREN_IOC_RX_SET_TIMEOUT:
		return wren_ioctl_rx_set_timeout(client, (void __user*)arg);
	case WREN_IOC_GET_PULSER_LOG:
		return wren_ioctl_get_stamper_log(client->wren, (void __user*)arg, 0);
	case WREN_IOC_GET_PIN_LOG:
		return wren_ioctl_get_stamper_log(client->wren, (void __user*)arg, 1);
	case WREN_IOC_GET_SW_LOG:
		return wren_ioctl_get_sw_log(client->wren, (void __user*)arg);
	case WREN_IOC_GET_HW_LOG_INDEX:
		return wren_ioctl_get_log_index(client->wren, (void __user*)arg);
	case WREN_IOC_GET_PANEL_CONFIG:
		return wren_ioctl_get_panel_config(client->wren, (void __user*)arg);
	case WREN_IOC_SET_VME_P2A_OUTPUT:
		return wren_ioctl_set_vme_p2a_output(client->wren, (void __user*)arg);
	case WREN_IOC_SET_PATCHPANEL:
		return wren_ioctl_set_patchpanel_config(client->wren, (void __user*)arg);
	default:
		return -ENOTTY;
	}
}

/* Assuming WR is sync (and time valid), read time from the board.
   This function does *NOT* set the valid bit of RES. */
static void wren_read_time_from_tm(struct wren_dev *wren,
				   struct wren_wr_time *res)
{
	struct host_map *regs = wren->regs;

	/* Get a coherent set of values.
	   TODO: do it in HW */
	while (1) {
		res->tai_lo = ioread32(&regs->tm_tai_lo);
		res->tai_hi = ioread32(&regs->tm_tai_hi);
		res->ns = ioread32(&regs->tm_cycles) << 3;

		/* Ack a potentially pending interrupt (even if masked) so
		   that the pending interrupt doesn't corrupt tai_hi */
		iowrite32(HOST_MAP_INTC_ISR_CLOCK, &regs->intc.iack);

		if (res->tai_lo == ioread32(&regs->tm_tai_lo)
		    && res->tai_hi == ioread32(&regs->tm_tai_hi))
			break;
	}

	return;
}

static void wren_sync_time(struct wren_dev *wren)
{
	struct wren_wr_time res;
	uint32_t hi;
	unsigned msb;

	wren_read_time_from_tm(wren, &res);

	/* Save high */
	hi = (res.tai_lo >> 4) | (res.tai_hi) << 28;
	msb = (res.tai_lo >> 3) & 1;
	wren->tai_msb = msb;
	wren->tai_hi[msb] = hi;
	wren->tai_hi[msb ^ 1] = hi + (msb ? 1 : 0);
}

static void wren_init_hw(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;
	struct mb_map *mb = wren->mb;
	uint32_t b_off;
 	uint32_t wr_state;

	/* Clear all pending interrupts. */
	iowrite32(0xffffff, &regs->intc.iack);

	/* Default mask.  */
	wren->imr = HOST_MAP_INTC_IMR_MSG
		| HOST_MAP_INTC_IMR_ASYNC
		| HOST_MAP_INTC_IMR_WR_SYNC;

	/* If already synchronized, read time.  */
	wr_state = ioread32(&regs->wr_state);

	if (wr_state & HOST_MAP_WR_STATE_TIME_VALID) {
		/* Read time */
		wren->wr_synced = 1;
		wren_sync_time(wren);

		/* Allow clock interrupts */
		wren->imr |= HOST_MAP_INTC_IMR_CLOCK;
	}
	else
		wren->wr_synced = 0;

	/* Clear all pending interrupts and enable interrupts */
	iowrite32(0xffffff, &regs->intc.iack);
	iowrite32(wren->imr, &regs->intc.imr);

	/* Empty the async memory */
	b_off = ioread32 (&mb->async_board_off);
	iowrite32 (b_off, &mb->async_host_off);
}

static void wren_init_data(struct wren_dev *wren)
{
	unsigned i, j;

	mutex_init(&wren->msg_mutex);

	wren->log_mask = 0;

	/* Clear all the sources.  */
	for (i = 0; i < WREN_NBR_RX_SOURCES; i++) {
		struct wren_dev_source *s = &wren->sources[i];
		s->proto.proto = WREN_PROTO_NONE;
		s->conds = WREN_NO_IDX;
		bitmap_zero(wren->client_sources[i].b, WREN_MAX_CLIENTS);

		wren->last_evt[i] = WREN_NO_BUFFER;
		for (j = 0; j < WREN_NBR_EVENTID; j++)
			atomic_set(&s->evsubs[j], 0);
		for (j = 0; j < WREN_NBR_CACHED_CONTEXT; j++)
			s->cached_contexts[j] = WREN_NO_BUFFER;
		s->last_cached_context = 0;
		
		spin_lock_init(&s->cached_lock);
	}

	/* All the conds are free. */
	for (i = 0; i < WREN_NBR_CONDS; i++) {
		wren->conds[i].cond.src_idx = WREN_NO_SRC_IDX;
		wren->conds[i].link = i + 1;
	}
	wren->conds[WREN_NBR_CONDS - 1].link = WREN_NO_IDX;
	wren->free_conds = 0;

	/* All the actions are free. */
	for (i = 0; i < WREN_NBR_ACTIONS; i++) {
		wren->actions[i].cond_idx = WREN_NO_IDX;
		wren->actions[i].link = i + 1;
		bitmap_zero(wren->client_configs[i].b, WREN_MAX_CLIENTS);
	}
	wren->actions[WREN_NBR_ACTIONS - 1].link = WREN_NO_IDX;
	atomic_set(&wren->free_actions, 0);

	atomic_set(&wren->promisc, -1);

	/* Clients */
	rwlock_init(&wren->client_lock);
	bitmap_zero(wren->client_used.b, WREN_MAX_CLIENTS);
	memset(wren->clients, 0, sizeof(wren->clients));

	/* Buffers */
	for (i = 0; i < WREN_MAX_BUFFERS - 1; i++)
		atomic_set(&wren->bufs[i].len, i + 1);
	atomic_set(&wren->bufs[i].len, WREN_NO_BUFFER);
	atomic_set(&wren->buf_head, 0);

	/* Comparators */
	for (i = 0; i < WREN_MAX_COMPARATORS; i++)
		wren->comp_buf[i].buf = WREN_NO_BUFFER;

	for (i = 0; i < WREN_NBR_TX_SOURCES; i++) {
		struct wren_dev_table *t = &wren->tables[i];
		t->src_idx = -1;
	}
}

/* A structure for variable number of buffers.
   When a message is read from the board, it is either a small message
   (like a pulse) or a capsule (context or event).  Most of them are small,
   the exception being the promisc message which contains a full ethernet
   frame. */
#define WREN_BUFS_MAX 4
struct wren_bufs {
	uint16_t buf_idx[WREN_BUFS_MAX];
	uint16_t nbr;
};

/* Get an entry in the ring for a client */
static int wren_get_client_msg(struct wren_dev *wren, struct wren_client *client,
			       struct wren_client_msg **msgp, unsigned *nextp)
{
	unsigned res = atomic_read(&client->ring_tail);
	unsigned head = atomic_read(&client->ring_head);

	if (res == (head ^ WREN_MAX_CLIENT_MSG)) {
		dev_err(wren->dev_ctl,
			"message queue of client %u is full\n", client->idx);
		*msgp = NULL;
		return -1;
	}

	*msgp = &client->ring[res & (WREN_MAX_CLIENT_MSG - 1)];
	res = (res + 1) & (WREN_MAX_CLIENT_MSG + WREN_MAX_CLIENT_MSG - 1);
	*nextp = res;
	return 0;
}

/* Send a message to client :param client:
   Set :param firstp: to the message stored in the queue (can be used to adjust
   the message metadata) */
static int wren_put_client_msg(struct wren_client *client, struct wren_bufs *bufs, unsigned cmd, struct wren_client_msg **firstp, unsigned *nextp)
{
	unsigned first;
	unsigned ring;
	unsigned head;
	unsigned i;

	/* Allocate N client ring entries and N buffer, fill */

	first = atomic_read(&client->ring_tail);
	head = atomic_read(&client->ring_head);

	ring = first;
	for (i = 0; i < bufs->nbr; i++) {
		struct wren_client_msg *msg;
		unsigned buf_idx = bufs->buf_idx[i];

		/* Allocate a ring entry */
		if (ring == (head ^ WREN_MAX_CLIENT_MSG)) {
			dev_err(client->wren->dev_ctl,
				"message queue of client %u is full\n", client->idx);
			return -1;
		}

		msg = &client->ring[ring & (WREN_MAX_CLIENT_MSG - 1)];

		atomic_set(&msg->buf_cmd, cmd | (buf_idx << 8));
		if (ring == first) {
			*firstp = msg;
			cmd = CMD_ASYNC_CONT;
		}

		ring = (ring + 1) & (2 * WREN_MAX_CLIENT_MSG - 1);
	}

	*nextp = ring;

	/* Increment reference counter */
	for (i = 0; i < bufs->nbr; i++) {
		struct wren_dev *wren = client->wren;
		unsigned buf_idx = bufs->buf_idx[i];
		atomic_inc(&wren->bufs[buf_idx].refcnt);
	}

	return 0;
}

static void wren_put_bufs(struct wren_dev *wren, struct wren_bufs *bufs)
{
	unsigned i;
	for (i = 0; i < bufs->nbr; i++)
		wren_put_buffer(wren, bufs->buf_idx[i]);
}

static void wren_update_client_msg(struct wren_client *client, unsigned next)
{
	atomic_set(&client->ring_tail, next);
	wake_up(&client->wqh);
}

static void wren_send_cached_contexts(struct wren_client *client, unsigned src_idx)
{
	unsigned i;
	struct wren_dev *wren = client->wren;
	struct wren_dev_source *s = &wren->sources[src_idx];
	uint16_t cached_contexts[WREN_NBR_CACHED_CONTEXT];
	unsigned last_cached_context;

	/* Protect against changes, copy buffer indexes and increment the
	   ref counter */
	spin_lock(&s->cached_lock);
	for (i = 0; i < WREN_NBR_CACHED_CONTEXT; i++) {
		unsigned buf_idx = s->cached_contexts[i];
		if (buf_idx != WREN_NO_BUFFER)
			atomic_inc(&wren->bufs[buf_idx].refcnt);
		cached_contexts[i] = buf_idx;
	}
	last_cached_context = s->last_cached_context;
	spin_unlock(&s->cached_lock);
			

	/* Send cached contexts to client */
	for (i = 0; i < WREN_NBR_CACHED_CONTEXT; i++) {
		unsigned idx = (last_cached_context + i + 1)
			% WREN_NBR_CACHED_CONTEXT;
		unsigned buf_idx;
		struct wren_bufs bufs;
		struct wren_client_msg *msg;
		unsigned next;

		buf_idx = cached_contexts[idx];
		if (buf_idx == WREN_NO_BUFFER)
			continue;

		bufs.buf_idx[0] = buf_idx;
		bufs.nbr = 1;
		if (wren_put_client_msg(client, &bufs, CMD_ASYNC_CONTEXT,
					&msg, &next) == 0) {
			msg->source_idx = src_idx;
			wren_update_client_msg(client, next);
		}
		
		atomic_dec(&wren->bufs[buf_idx].refcnt);
	}
}

static int wren_irq_read_packet(struct wren_dev *wren, union wren_capsule_hdr_un hdr, unsigned h_off, struct wren_bufs *res)
{
	struct mb_map *mb = wren->mb;
#define MEM_ASYNC_WORDS (sizeof(mb->async_data) / 4)
	unsigned len;
	unsigned n;

	len = hdr.hdr.len;
	for (n = 0; len > 0; n++) {
		unsigned start;
		unsigned clen;
		unsigned i;
		struct wren_buffer *buf;
		unsigned buf_idx = wren_get_buffer(wren);

		if (buf_idx == WREN_NO_BUFFER) {
			/* In case of no buffer, discard */
			for (i = 0; i < n; i++)
				wren_put_buffer(wren, res->buf_idx[i]);
			res->nbr = 0;
			return -1;
		}

		res->buf_idx[n] = buf_idx;

		buf = &wren->bufs[buf_idx];
		if (n == 0) {
			/* The first word is the header */
			buf->data[0] = hdr.u32;
			start = 1;
		}
		else
			start = 0;
		clen = WREN_MAX_BUFFER_LEN - start;
		if (clen > len)
			clen = len;
		atomic_set(&buf->len, len);

		for (i = 0; i < clen; i++) {
			h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
			buf->data[start + i] = ioread32(&mb->async_data[h_off]);
		}

		WREN_LOG(wren, WREN_DRV_LOG_IRQ,
			 "read_packet: buf %u, chunk %u (typ: %u)\n",
			 buf_idx, n, hdr.hdr.typ);

		len -= clen;
	}
	res->nbr = n;
	return 0;
}

static void wren_irq_context(struct wren_dev *wren, struct wren_bufs *bufs, unsigned src_idx)
{
	const unsigned long *bm = wren->client_sources[src_idx].b;
	unsigned n;

	/* As we access to client struct, be sure it doesn't get removed */
	read_lock(&wren->client_lock);

	/* Push to all clients who subscribed to the source */
	n = find_first_bit(bm, WREN_MAX_CLIENTS);
	while (n < WREN_MAX_CLIENTS) {
		struct wren_client *client = wren->clients[n];
		struct wren_client_msg *msg;
		unsigned next;

		if (wren_put_client_msg(client, bufs, CMD_ASYNC_CONTEXT,
					&msg, &next) == 0) {
			WREN_LOG(wren, WREN_DRV_LOG_IRQ,
				 "wren_irq_context: send bufs to clt %u\n", n);
			msg->source_idx = src_idx;
			wren_update_client_msg(client, next);
		}

		n = find_next_bit(bm, WREN_MAX_CLIENTS, n + 1);
	}

	read_unlock(&wren->client_lock);

	/* Store in the source */
	{
		struct wren_dev_source *s = &wren->sources[src_idx];
		union {
			struct wren_capsule_ctxt_hdr hdr;
			uint32_t w[4];
		} u;
		unsigned buf_idx = bufs->buf_idx[0];
		struct wren_buffer *buffer = &wren->bufs[buf_idx];
		unsigned cached_idx, prev;

		/* Extract context id */
		u.w[1] = buffer->data[1];
		cached_idx = u.hdr.ctxt_id % WREN_NBR_CACHED_CONTEXT;

		prev = s->cached_contexts[cached_idx];
		
		spin_lock(&s->cached_lock);

		atomic_inc(&wren->bufs[buf_idx].refcnt);
		s->cached_contexts[cached_idx] = buf_idx;
		s->last_cached_context = cached_idx;

		spin_unlock(&s->cached_lock);

		if (prev != WREN_NO_BUFFER)
			wren_put_buffer(wren, prev);
	}

	/* When the buffer was allocated/filled, it's refcount was initialized
	   to 1.  So decrement the refcount as we don't own it anymore. */
	wren_put_bufs(wren, bufs);
}

/* An event has been received and either has been subscribed or will load
   trigger.
   Save it (for irq_config) and push it to all subscribed clients.
*/
static void wren_irq_event(struct wren_dev *wren, struct wren_bufs *bufs, unsigned src_idx)
{
	struct wren_capsule_event_hdr hdr;
	unsigned buf_idx = bufs->buf_idx[0];
	struct wren_buffer *buf;
	unsigned evid;

	/* Update last_evt, so that the event packet will be used for all
	   the triggered configurations */
	unsigned last = wren->last_evt[src_idx];
	if (last != WREN_NO_BUFFER)
		wren_put_buffer(wren, last);
	wren->last_evt[src_idx] = bufs->buf_idx[0];

	/* Subscribed event */
	buf = &wren->bufs[buf_idx];
	memcpy(&hdr, buf->data, sizeof (hdr));
	evid = hdr.ev_id;

	WREN_LOG(wren, WREN_DRV_LOG_IRQ,
		 "wren_irq_event: src=%u, ev=%u, last=%u, buf=%u\n",
		 src_idx, evid, last, buf_idx);

	if (evid < WREN_NBR_EVENTID) {
		const unsigned long *bm = wren->client_used.b;
		unsigned n;

		read_lock(&wren->client_lock);

		/* Push to all clients who subscribed to the source */
		n = find_first_bit(bm, WREN_MAX_CLIENTS);
		while (n < WREN_MAX_CLIENTS) {
			struct wren_client *client = wren->clients[n];
			if (test_bit(evid, client->sources[src_idx].evsubs)) {
				struct wren_client_msg *msg;
				unsigned next;

				if (wren_put_client_msg(client, bufs,
							CMD_ASYNC_EVENT,
							&msg, &next) == 0) {
					WREN_LOG(wren, WREN_DRV_LOG_IRQ,
						 "wren_irq_event: send bufs to clt %u\n", n);
					msg->source_idx = src_idx;
					wren_update_client_msg(client, next);
				}
			}

			n = find_next_bit(bm, WREN_MAX_CLIENTS, n + 1);
		}

		read_unlock(&wren->client_lock);
	}
}

/* Called on a config message which means a configuration has been triggered
   by a received event (a comparator has been loaded) */
static void wren_irq_config(struct wren_dev *wren, unsigned src_idx, unsigned data)
{
	unsigned act_idx = data & 0xffff;
	unsigned cmp_idx = data >> 16;
	struct wren_dev_comparator *cmp = &wren->comp_buf[cmp_idx];
	unsigned last;

	if (cmp_idx >= WREN_MAX_COMPARATORS) {
		dev_err(wren->dev_ctl, "async config: invalid comparator %u\n",
			cmp_idx);
		return;
	}

	last = wren->last_evt[src_idx];
	if (last == WREN_NO_BUFFER) {
		dev_err(wren->dev_ctl, "got a config for source %u without an event", src_idx);
		return;
	}

	WREN_LOG(wren, WREN_DRV_LOG_IRQ,
		 "wren_irq_config src=%u, cmp=%u, conf=%u, last=%u\n",
		 src_idx, cmp_idx, act_idx, last);

	/* Free the previous buffer */
	if (cmp->buf != WREN_NO_BUFFER)
		wren_put_buffer(wren, cmp->buf);

	/* Increment refcnt as we keep this buffer */
	atomic_inc(&wren->bufs[last].refcnt);

	/* TODO: should we also save the comparator idx in the config ?
	   So that we can free the comparator if the config is removed */
	cmp->buf = last;
	cmp->config_id = act_idx;
	cmp->src_idx = src_idx;
}

/* Called by the irq thread when a message has been received for a generated
   pulse */
static void wren_irq_pulse(struct wren_dev *wren, unsigned cmp_idx, struct wren_ts *ts)
{
	struct wren_dev_comparator *cmp;
	unsigned config_id;
	unsigned buf_idx;
	unsigned src_idx;
	const unsigned long *bm;
	unsigned n;

	/* Sanity check */
	if (cmp_idx >= WREN_MAX_COMPARATORS) {
		dev_err(wren->dev_ctl, "async pulse: invalid comparator %u\n",
			cmp_idx);
		return;
	}

	cmp = &wren->comp_buf[cmp_idx];
	config_id = cmp->config_id;
	buf_idx = cmp->buf;
	src_idx = cmp->src_idx;

	WREN_LOG(wren, WREN_DRV_LOG_IRQ,
		 "wren_irq_pulse cmp=%u, conf=%u, buf=%u\n",
		 cmp_idx, config_id, buf_idx);

	/* Remove the buffer from the comparator.  If set, the buffer will
	   be sent to the clients.  */
	cmp->buf = WREN_NO_BUFFER;

	/* Send a pulse message to all clients */

	read_lock(&wren->client_lock);

	bm = wren->client_configs[config_id].b;

	/* Push to all clients who subscribed to the config */
	n = find_first_bit(bm, WREN_MAX_CLIENTS);
	while (n < WREN_MAX_CLIENTS) {
		struct wren_client *client = wren->clients[n];
		struct wren_client_msg *msg;
		unsigned next;

		if (wren_get_client_msg(wren, client, &msg, &next) == 0) {
			/* If there is an associated buffer, add a ref */
			if (buf_idx != WREN_NO_BUFFER)
				atomic_inc(&wren->bufs[buf_idx].refcnt);

			atomic_set(&msg->buf_cmd,
				   CMD_ASYNC_PULSE | (buf_idx << 8));

			msg->config_id = config_id;
			msg->source_idx = src_idx;
			msg->ts = *ts;
			wren_update_client_msg(client, next);
		}

		n = find_next_bit(bm, WREN_MAX_CLIENTS, n + 1);
	}

	read_unlock(&wren->client_lock);

	if (buf_idx != WREN_NO_BUFFER)
		wren_put_buffer(wren, buf_idx);
}

static void wren_irq_promisc(struct wren_dev *wren, struct wren_bufs *bufs)
{
	int cid = atomic_read(&wren->promisc);
	struct wren_client *client;
	struct wren_client_msg *msg;
	unsigned next;

	if (cid == -1) {
		/* No client; could happen temporarly if packets are still
		   in the pipeline but the client has truned off promiscuous
		   mode. */
		wren_put_bufs(wren, bufs);
		return;
	}

	/* Be sure the client is not removed */
	read_lock(&wren->client_lock);

	client = wren->clients[cid];

	if (wren_put_client_msg(client, bufs,
				CMD_ASYNC_PROMISC,
				&msg, &next) == 0)
		wren_update_client_msg(client, next);

	read_unlock(&wren->client_lock);

	wren_put_bufs(wren, bufs);

	return;
}


static unsigned wren_irq_packet(struct wren_dev *wren, unsigned h_off, unsigned len)
{
	union wren_capsule_hdr_un hdr;
	struct mb_map *mb = wren->mb;
	struct wren_bufs bufs;

	/* Read header */
	hdr.u32 = ioread32(&mb->async_data[h_off]);

	WREN_LOG(wren, WREN_DRV_LOG_IRQ_PACKET,
		 "irq packet t=%u l=%u len=%u\n",
		 hdr.hdr.typ, hdr.hdr.len, len);

	if (hdr.hdr.len > len) {
		/* Comm error! */
		dev_err(wren->dev_ctl,
			"async bad len (pkt: %u, mem: %u)\n", hdr.hdr.len, len);

		/* Consume everything */
		return len;
	}

	switch (hdr.hdr.typ) {
	case CMD_ASYNC_CONTEXT:
		/* A context has been received */
		if (hdr.hdr.len < sizeof(struct wren_capsule_ctxt_hdr) / 4
		    || hdr.hdr.len > WREN_MAX_BUFFER_LEN) {
			dev_err(wren->dev_ctl, "async bad context len (%u)\n",
				hdr.hdr.len);
			break;
		}
		if (wren_irq_read_packet(wren, hdr, h_off, &bufs) < 0)
			break;
		wren_irq_context(wren, &bufs, hdr.hdr.pad);
		break;
	case CMD_ASYNC_EVENT:
		/* An event has been received */
		if (hdr.hdr.len < sizeof(struct wren_capsule_event_hdr) / 4
		    || hdr.hdr.len > WREN_MAX_BUFFER_LEN) {
			dev_err(wren->dev_ctl, "async bad event len (%u)\n",
				hdr.hdr.len);
			break;
		}
		if (wren_irq_read_packet(wren, hdr, h_off, &bufs) < 0)
			break;
		wren_irq_event(wren, &bufs, hdr.hdr.pad);
		break;
	case CMD_ASYNC_CONFIG: {
		/* A config has been applied */
		unsigned data;

		if (hdr.hdr.len != 2) {
			dev_err(wren->dev_ctl, "async bad config len (%u)\n",
				hdr.hdr.len);
			break;
		}
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		data = ioread32(&mb->async_data[h_off]);

		wren_irq_config(wren, hdr.hdr.pad, data);
		break;
	}
	case CMD_ASYNC_PULSE: {
		/* A pulse was generated */
		unsigned comp_idx;
		struct wren_ts ts;

		if (hdr.hdr.len != 4) {
			dev_err(wren->dev_ctl, "async bad pulse len (%u)\n",
				hdr.hdr.len);
			break;
		}
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		comp_idx = ioread32(&mb->async_data[h_off]);
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		ts.sec = ioread32(&mb->async_data[h_off]);
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		ts.nsec = ioread32(&mb->async_data[h_off]);

		wren_irq_pulse(wren, comp_idx, &ts);
		break;
	}
	case CMD_ASYNC_PROMISC:
		/* A packet (in promiscuous mode) has been received */
		if (wren_irq_read_packet(wren, hdr, h_off, &bufs) < 0)
			break;
		wren_irq_promisc(wren, &bufs);
		break;
	case CMD_ASYNC_REL_ACT: {
		/* An action has been removed */
		unsigned act_idx;
		struct wren_dev_action *act;

		if (hdr.hdr.len != 2) {
			dev_err(wren->dev_ctl, "async bad rel act len (%u)\n",
				hdr.hdr.len);
			break;
		}
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		act_idx = ioread32(&mb->async_data[h_off]);

		if (act_idx >= WREN_NBR_ACTIONS) {
			dev_err(wren->dev_ctl, "async bad rel act id (%u)\n",
				act_idx);
			break;
		}

		act = &wren->actions[act_idx];
		if (act->cond_idx != WREN_DEAD_COND) {
			dev_err(wren->dev_ctl, "async bad rel act (%u) state(%u)\n",
				act_idx, act->cond_idx);
			break;
		}

		/* Put to free chain */
		act->cond_idx = WREN_NO_IDX;
		wren_put_action(wren, act_idx);
		break;
	}
	default:
		dev_err(wren->dev_ctl,
			"async unknown typ: 0x%02x\n", hdr.hdr.typ);
		/* Not even sure the header length is correct, discard
		   everything */
		return len;
	}

	return hdr.hdr.len;
}

irqreturn_t wren_irq_thread(int irq, void *priv)
{
	struct wren_dev *wren = (struct wren_dev *)priv;
	struct mb_map *mb = wren->mb;
	uint32_t h_off, b_off;
	int32_t  len;

	h_off = ioread32 (&mb->async_host_off);

	while (1) {
		/* Read offsets */
		b_off = ioread32 (&mb->async_board_off);
		if (b_off == h_off)
			break;

		/* Compute length in words */
		len = b_off - h_off;
		if (len < 0)
		  len += MEM_ASYNC_WORDS;

		do {
			unsigned l;

			l = wren_irq_packet(wren, h_off, len);

			h_off = (h_off + l) & (MEM_ASYNC_WORDS - 1);
			len -= l;
		} while (len > 0);
	}
	/* Update host offet */
	iowrite32 (h_off, &mb->async_host_off);
	return IRQ_HANDLED;
}
EXPORT_SYMBOL(wren_irq_thread);

irqreturn_t wren_irq_handler(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;
	struct mb_map *mb = wren->mb;
	irqreturn_t res = IRQ_HANDLED;

	while (1) {
		uint32_t isr = ioread32(&regs->intc.isr);

		if (isr == 0)
			break;

		//dev_notice(wren->dev_ctl, "interrupt isr=0x%x\n", isr);

		/* Ack interrupts, before handling them, so that a new
		   interrupt won't be acknowledged */
		iowrite32(isr, &regs->intc.iack);

		if (isr & HOST_MAP_INTC_ISR_WR_SYNC) {
			uint32_t wr_state;

			wr_state = ioread32(&regs->wr_state);
			if (wr_state & HOST_MAP_WR_STATE_TIME_VALID) {
				dev_notice(wren->dev_ctl, "wr synced");
				wren->imr |= HOST_MAP_INTC_IMR_CLOCK;
				wren->wr_synced = 1;
				wren_sync_time(wren);
			}
			else {
				dev_warn(wren->dev_ctl, "wr un-synced");
				wren->imr &= ~HOST_MAP_INTC_IMR_CLOCK;
				wren->wr_synced = 0;
				/* FIXME: else flush timeout */
			}
			iowrite32(wren->imr, &regs->intc.imr);
		}

		if ((isr & HOST_MAP_INTC_ISR_CLOCK) && wren->wr_synced) {
			uint32_t tm_compact;
			unsigned msb;

			tm_compact = ioread32(&regs->intc.tm_compact);
			msb = tm_compact >> 31;
			if (msb == wren->tai_msb)
				dev_warn(wren->dev_ctl, "irq clock: bad msb");
			wren->tai_hi[msb ^ 1]++;
			wren->tai_msb = msb;
		}

		if (isr & HOST_MAP_INTC_ISR_TIMER) {
			struct wren_compact_time now;

			spin_lock(&wren->to_lock);
			if (wren_read_ctime(wren, &now) == 0)
				timeout_run(wren, &now);
			/* FIXME: else flush */
			spin_unlock(&wren->to_lock);
		}
		if (isr & HOST_MAP_INTC_ISR_MSG) {
			/* TODO: Atomic inc/dec of discard.  */
			if (wren->wqh_r_discard) {
				/* Simply discard the reply */
				iowrite32 (0, &mb->b2h_csr);
				wren->wqh_r_discard = 0;
			}
			else {
				/* Wake up the process waiting for the reply */
				wake_up(&wren->wqh_r);
			}
		}
		if (isr & HOST_MAP_INTC_ISR_ASYNC) {
			wake_up(&wren->wqh_a);
			res = IRQ_WAKE_THREAD;
		}
	}

	return res;
}
EXPORT_SYMBOL(wren_irq_handler);

/*
 * Char device stuff
 */

static const struct file_operations wren_ctl_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.read           = wren_ctl_read,
	.open		= wren_ctl_open,
	.release	= wren_ctl_release,
	.unlocked_ioctl = wren_ctl_ioctl,
};

static const struct file_operations wren_usr_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.read           = wren_usr_read,
	.open		= wren_usr_open,
	.poll           = wren_usr_poll,
	.release	= wren_usr_release,
	.unlocked_ioctl = wren_usr_ioctl,
};

static ssize_t version_show(struct device *dev,
			    struct device_attribute *attr,
			    char *buf)
{

	return sprintf(buf, "%u\n", WREN_IOCTL_VERSION);
}
DEVICE_ATTR(version, S_IRUGO, version_show, NULL);

static struct attribute *wren_attr_versions[] = {
	&dev_attr_version.attr,
	NULL,
};

static const struct attribute_group wren_attr_groups = {
	.attrs = wren_attr_versions,
};


void wren_unregister(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;

	/* Mask interrupts */
	iowrite32(0, &regs->intc.imr);

	/* Clear all pending interrupts. */
	iowrite32(0xffffff, &regs->intc.iack);

	debugfs_remove_recursive(wren->dfs_dir);

	/* Now we can release the ID for re-use */
	device_destroy(wren_class, wren->devid_ctl);
	device_destroy(wren_class, wren->devid_usr);

	cdev_del(&wren->cdev_ctl);
	cdev_del(&wren->cdev_usr);

	ida_simple_remove(&wren_map, wren->index);
}
EXPORT_SYMBOL(wren_unregister);

int wren_register(struct wren_dev *wren, struct device *parent)
{
	struct host_map *regs = wren->regs;
	int err = 0;
	int index;
	int major = MAJOR(wren_devt);
	unsigned hw_ident;

	hw_ident = ioread32(&regs->ident);
	if (hw_ident != WREN_HW_IDENT) {
		dev_err(parent, "invalid hw ident (%08x) - HW issue ?\n",
			hw_ident);
		return -EINVAL;
	}
	wren->map_ver = ioread32(&regs->map_version);
	if (wren->map_ver != WREN_HOST_MAP_VERSION) {
		dev_err(parent, "wrong host map version (%08x, expect %08x)\n",
			wren->map_ver, WREN_HOST_MAP_VERSION);
		return -EINVAL;
	}
	wren->fw_ver = ioread32(&regs->fw_version);
	if (wren->fw_ver != WREN_FW_VERSION) {
		dev_err(parent, "invalid fw version (%08x)\n", wren->fw_ver);
		return -EINVAL;
	}
	dev_notice(parent, "wren map version: %08x, fw version: %08x\n",
		   wren->map_ver, wren->fw_ver);

	/* Allocate index (used as device minor) */
	index = ida_simple_get(&wren_map, 0, WREN_MAX_DEVS, GFP_KERNEL);
	if (index < 0) {
		err = index;
		goto no_slot;
	}

	wren->index = index;
	mutex_init(&wren->msg_mutex);
	init_waitqueue_head(&wren->wqh_r);
	wren->wqh_r_discard = 0;
	init_waitqueue_head(&wren->wqh_a);
	spin_lock_init(&wren->to_lock);
	INIT_LIST_HEAD(&wren->to_list);

	wren_init_data(wren);

	wren_init_hw(wren);

	/* Create character device for ctl. */
	wren->devid_ctl = MKDEV(major, 2 * index);
	cdev_init(&wren->cdev_ctl, &wren_ctl_fops);
	wren->cdev_ctl.owner = wren->owner;
	err = cdev_add(&wren->cdev_ctl, wren->devid_ctl, 1);
	if (err) {
		pr_err("wren: failed to add char device %d:%d\n",
		       MAJOR(wren->devid_ctl), MINOR(wren->devid_ctl));
		goto no_cdev_ctl;
	}

	/* Create a new device, register to sysfs */
	wren->dev_ctl = device_create(wren_class, parent, wren->devid_ctl,
				      wren, "wrenctl%u", wren->index);
	if (IS_ERR(wren->dev_ctl))
		goto no_device_ctl;

	dev_set_drvdata(wren->dev_ctl, wren);

	/* Create character device for usr. */
	wren->devid_usr = MKDEV(major, 2 * index + 1);
	cdev_init(&wren->cdev_usr, &wren_usr_fops);
	wren->cdev_usr.owner = wren->owner;
	err = cdev_add(&wren->cdev_usr, wren->devid_usr, 1);
	if (err) {
		pr_err("wren: failed to add char device %d:%d\n",
		       MAJOR(wren->devid_usr), MINOR(wren->devid_usr));
		goto no_cdev_usr;
	}

	/* Create a new device, register to sysfs */
	wren->dev_usr = device_create(wren_class, parent, wren->devid_usr,
				      wren, "wren%u", wren->index);
	if (IS_ERR(wren->dev_usr))
		goto no_device_usr;

	err = sysfs_create_group(&wren->dev_usr->kobj, &wren_attr_groups);
	if (err)
		goto no_sysfs;
	if (dfs_root) {
		wren->dfs_dir = debugfs_create_dir(dev_name(wren->dev_usr),
						   dfs_root);
		if (IS_ERR_OR_NULL(wren->dfs_dir)) {
			dev_err(wren->dev_ctl, "cannot create debugfs\n");
			wren->dfs_dir = NULL;
		}
	}

#if 0
	err = ptp_populate_sysfs(ptp);
	if (err)
		goto no_sysfs;
#endif

	return 0;

//	sysfs_remove_group(&wren->dev_usr->kobj, &wren_attr_groups);
no_sysfs:
	device_destroy(wren_class, wren->devid_usr);
no_device_usr:
	cdev_del(&wren->cdev_usr);
no_cdev_usr:
	device_destroy(wren_class, wren->devid_ctl);
no_device_ctl:
	cdev_del(&wren->cdev_ctl);
no_cdev_ctl:
	ida_simple_remove(&wren_map, wren->index);
no_slot:
	return err;
}
EXPORT_SYMBOL(wren_register);

int wren_reset(struct wren_dev *wren)
{
	/* Clear board settings */
	return  wren_mb_msg(wren, CMD_RX_RESET, NULL, 0, NULL, 0);
}
EXPORT_SYMBOL(wren_reset);


static int __init wren_module_init(void)
{
	int err;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(6, 2, 0) /* To be adjusted */
	wren_class = class_create("wren");
#else
	wren_class = class_create(THIS_MODULE, "wren");
#endif
	if (IS_ERR(wren_class)) {
		pr_err("wren: failed to allocate class\n");
		return PTR_ERR(wren_class);
	}

	err = alloc_chrdev_region(&wren_devt, 0, WREN_MAX_DEVS, "wren");
	if (err < 0) {
		pr_err("wren: failed to allocate char device region\n");
		goto no_region;
	}

	dfs_root = debugfs_create_dir("wren", NULL);
	if (IS_ERR_OR_NULL(dfs_root)) {
		pr_notice("wren: cannot create dfs\n");
		dfs_root = NULL;
	}

	pr_info("wren-core registered, ioctl version: %08x\n",
		WREN_IOCTL_VERSION);
	return 0;

no_region:
	class_destroy(wren_class);
	return err;
}
module_init(wren_module_init);

static void __exit wren_module_exit(void)
{
	pr_info("destroying wren\n");
	debugfs_remove_recursive(dfs_root);
	unregister_chrdev_region(wren_devt, WREN_MAX_DEVS);
	class_destroy(wren_class);
	ida_destroy(&wren_map);
}
module_exit(wren_module_exit);

MODULE_AUTHOR("Tristan Gingold <tristan.gingold@cern.ch>");
MODULE_LICENSE("GPL v2");
MODULE_VERSION(VERSION);
MODULE_DESCRIPTION("Driver for WREN");
