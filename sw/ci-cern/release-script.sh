#!/bin/bash

install_drv()
{
    archive=$2
    dir=$1

    # Extract software artifacts
    mkdir output/$dir
    tar -xvf $archive -C output/$dir

    # Create destination dir
    mkdir -p ${dist}/$dir
    kver=$(cd output/$dir/usr/lib/modules/; echo *)
    test "$kver" != '*'
    dest="$dist/$dir/$kver/wren"
    destfw="$dest/$fw_ver"
    mkdir -p $destfw

    # Move kernel modules
    mv output/$dir/usr/lib/modules/$kver/wren-gw/*.ko $destfw

    # Copy wrc.elf
    cp wrpc-sw/wrc.elf $destfw

    # Move executable (wren-ls is special)
    mv output/$dir/usr/local/bin/wren-ls $dest
    mkdir -p $destfw/bin
    mv output/$dir/usr/local/bin/* $destfw/bin

    # Copy scripts
    cp sw/ci-cern/scripts/* $dest

    echo "cp -Rv $dir/$kver/wren /acc/dsc/lab/$dir/$kver" >> ${dist}/deploy.sh
}

set -x
set -e

ls -l
ls -l edl_ci_output/software

# Where software is extracted
mkdir output

fw_ver=$(grep WREN_FW_VERSION sw/hw-include/versions.h | sed 's/.*0x//')

dist="dist-${fw_ver}"

# Where the result is put
mkdir $dist

# Prepare the deply script
echo "#!/bin/bash" > ${dist}/deploy.sh

# Artifact produced may have a sanitized tag appended
# Do not duplicate the sanitization logic, use a wildcard
install_drv deb12x64 edl_ci_output/software/wren-gw-software-testing*.tar.xz
install_drv L867 edl_ci_output/software/wren-gw-software-production*.tar.xz

chmod +x ${dist}/deploy.sh
