#!/bin/bash
echo "run hdl-build"
date
uname -a
pwd
env

if [ "$1" = "" ]; then
    echo "missing platform"
    exit 1
fi
plat=$1

. /opt/Xilinx/Vitis/2022.2/settings64.sh
. /opt/python3-venv/bin/activate

set -x
set -e

mkdir -p dist

git submodule update --init

prj=$(echo $plat | sed 's/_/-/')
cd hdl/syn/$plat
hdlmake
#make files.tcl
# Import files (and add verilog included files)
#sed -i s/add_files/import_files/ files.tcl
#sed -i 3i../../../dependencies/urv-core/rtl/urv_defs.v files.tcl
#sed -i 3i../../../dependencies/urv-core/rtl/urv_config.v files.tcl
#sed -i '/^\.\..*mpsoc.tcl/d' files.tcl
# sed -i /mpsoc.bd/d files.tcl
#make project

make synthesize

# create xsa
vivado -mode tcl <<EOF
open_project wren-${prj}.xpr
set_property platform.name "wren" [current_project]
set_property platform.board_id "$plat" [current_project]
write_hw_platform -fixed -hw -minimal -file ../../../sw/vitis/wren_${plat}_top.xsa
EOF

make par || touch par
make bitstream
date

cd ../../..

cd sw/vitis
cp ../../hdl/syn/${plat}/wren-${prj}.runs/impl_1/wren_${plat}_top.bit .
xsct wren.xsct $plat release
make wren-${plat}-boot.bin
cp wren-${plat}-boot.bin ../../dist
cd ../..

date
echo "done"
