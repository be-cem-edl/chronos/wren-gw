#!/usr/bin/env bash

HOSTNAME=$(hostname -s)

case $HOSTNAME in
  cfv-774-celma4)
    # Dev machine, do not load
    ;;
  *)
    sh ./install-drv.sh
    ;;
esac
