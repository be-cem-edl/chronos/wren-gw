#include <stdio.h>
#include <string.h>
#include <sleep.h>
#include <xil_printf.h>
#include <xil_types.h>
#include <xiicps.h>
#include <xiicps_hw.h>
#include <xparameters.h>


#if 1
#define I2C_SI5341_ADDR 0x75
#include "main-Registers.h"
#else
#define I2C_SI5341_ADDR 0x74
#include "helper-Registers.h"
#endif


#define IIC_DEVICE_ID	XPAR_PSU_I2C_0_DEVICE_ID

#define IIC_SCLK_RATE		        100000

void si5341_i2c_read(XIicPs *iic, uint16_t addr, uint8_t *data, size_t size) {
  uint8_t buffer[4];

  // select page and reg address 0
  buffer[0] = 0x01;
  buffer[1] = (0xFF00 & addr) >> 8;
  buffer[2] = (0x00FF & addr);
  XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
  XIicPs_MasterSendPolled(iic, &buffer[2], 1, I2C_SI5341_ADDR);

  XIicPs_MasterRecvPolled(iic, data, size, I2C_SI5341_ADDR);
}

void si5341_i2c_write(XIicPs *InstancePtr, uint16_t addr, uint8_t data) {

	uint8_t buffer[4];

	buffer[0] = 0x01;
	buffer[1] = (0xFF00 & addr) >> 8;
	buffer[2] = (0x00FF & addr);
	buffer[3] = data;

	if (XIicPs_MasterSendPolled(InstancePtr, buffer, 2, I2C_SI5341_ADDR) != XST_SUCCESS)
	  xil_printf("Timeout for addr=%s\r\n", addr);
 	if (XIicPs_MasterSendPolled(InstancePtr, &buffer[2], 2,	I2C_SI5341_ADDR) != XST_SUCCESS)
	  xil_printf("Timeout for addr=%s\r\n", addr);
}

#if 0
void si5341_i2c_write_nvm(XIicPs *iic) {

	uint8_t buffer[4];

	// see p13 of the reference manual Si5341-40-D-RM

	// write 0xC7 to NVM_WRITE register
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xE3;
	buffer[3] = 0xC7;

	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 2,
	I2C_SI5341_ADDR);

	// wait until DEVICE_READY = 0x0F
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xFE;
	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 1,
	I2C_SI5341_ADDR);
	do {
		buffer[0] = 0;
		XIicPs_MasterRecvPolled(iic, buffer, 1, I2C_SI5341_ADDR);
	} while (*buffer != 0xF);

	// Set NVM_READ_BANK 0x00E4[0] = 1
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xE4;
	buffer[4] = 1;
	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 2,
	I2C_SI5341_ADDR);

	// wait until DEVICE_READY = 0x0F
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xFE;
	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 1,
	I2C_SI5341_ADDR);
	do {
		buffer[0] = 0;
		XIicPs_MasterRecvPolled(iic, buffer, 1, I2C_SI5341_ADDR);
	} while (*buffer != 0xF);
}
#endif

static void dump_regs(XIicPs *iic, unsigned addr, unsigned len)
{
  uint8_t regs[0xff];
  unsigned i;

  /* Cannot switch to the next page. */
  if (len > 0x100)
    len = 0x100;
  
  si5341_i2c_read(iic, addr, regs, len);
  for (i = 0; i < len; i++) {
    if ((i & 15) == 0)
      xil_printf("%03x:", addr + i);
    xil_printf(" %02x", regs[i]);
    if ((i & 15) == 15 || i == len - 1)
      xil_printf("\r\n");
  }
}

int main(void)
{
  XIicPs iic;
	XIicPs_Config *iic_config;
	int32_t status;
	unsigned i;

	xil_printf("entering main ...\r\n");

	/* Initialize the IIC0 driver so that it is ready to use */
	iic_config = XIicPs_LookupConfig(IIC_DEVICE_ID);
	if (iic_config == NULL) {
		xil_printf("XIicPs_LookupConfig error\r\n");
		return -1;
	}

	status = XIicPs_CfgInitialize(&iic, iic_config, iic_config->BaseAddress);
	if (status != XST_SUCCESS) {
		xil_printf("XIicPs_CfgInitialize error\r\n");
		return -1;
	}

	/* Set the IIC serial clock rate */
	status = XIicPs_SetSClk(&iic, IIC_SCLK_RATE);
	if (status != XST_SUCCESS) {
		xil_printf("XIicPs_SetSClk error\r\n");
		return -1;
	}

	// send preamble
	xil_printf("Programming device at i2c %02x\r\n", I2C_SI5341_ADDR);
	for (int i = 0; i < 6; i++) {
		si5341_i2c_write(&iic, si5341_revd_registers[i].address,
				si5341_revd_registers[i].value);
	}

	/* Delay 300 msec */
	usleep(300000);

	for (int i = 6; i < SI5341_REVD_REG_CONFIG_NUM_REGS; i++) {
		si5341_i2c_write(&iic, si5341_revd_registers[i].address,
				si5341_revd_registers[i].value);
	}

	/* Wait until bus is idle */
	while (XIicPs_BusIsBusy(&iic) > 0)
		;

	xil_printf("Programming complete\r\n");

#if 0
	/* Soft reset */
	si5341_i2c_write(&iic, 0x1c, 1);

	while (1) {
	  uint8_t val;

	  si5341_i2c_read(&iic, 0x1c, &val, 1);
	  xil_printf("SOFT_RST=%x\r\n", val);
	  if (val == 0)
	    break;
	}
#endif
#if 0
	si5341_i2c_write_nvm(&iic);
	/* Wait until bus is idle */
	while (XIicPs_BusIsBusy(&iic) > 0)
		;

	xil_printf("NVM programming complete\r\n");
#endif

	usleep(200000);

	xil_printf("Dump regs:\r\n");
	xil_printf("adr ");
	for (i = 0; i < 16; i++)
	    xil_printf("  %x", i);
	xil_printf("\r\n");

	dump_regs(&iic, 0x000, 0x100);
	dump_regs(&iic, 0x100, 0x100);
	dump_regs(&iic, 0x200, 0x100);
	dump_regs(&iic, 0x300, 0x100);
	dump_regs(&iic, 0x900, 0x100);
	dump_regs(&iic, 0xa00, 0x100);
	dump_regs(&iic, 0xb00, 0x100);
	dump_regs(&iic, 0x000, 0x20);
	
	xil_printf("exiting main...\r\n");

	return 0;
}
