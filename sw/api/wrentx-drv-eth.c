#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <netinet/ether.h>
#include <net/if.h>
#include <stdlib.h>
#include <fcntl.h>
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wren-simrt.h"
#include "wren-wrtime.h"

#define MAX_SOURCES 4

struct wrentx_handle_eth {
    struct wrentx_handle_op base;

    /* File descriptor to send packets.  */
    int sock;

    /* Fd for wren time (if used) */
    int wren_fd;

    struct wrentx_source sources[MAX_SOURCES];

    unsigned char eth_hdr[12];
    struct sockaddr_ll addr;
};

static int wrentx_eth_set_source (struct wrentx_handle *handle,
				  unsigned source_idx,
				  struct wren_protocol *proto)
{
    struct wrentx_handle_eth *he = (struct wrentx_handle_eth *)handle;
    struct wrentx_source *src;

    if (source_idx >= MAX_SOURCES)
	return WRENTX_ERR_BAD_SOURCE_IDX;

    if (proto->proto != WREN_PROTO_ETHERNET)
	return WRENTX_ERR_BAD_PROTOCOL;

    src = &he->sources[source_idx];
    src->seq_id = 0;
    src->domain_id = proto->u.eth.domain_id;
    src->xmit_id = proto->u.eth.xmit_id;

    src->ethertype = proto->u.eth.ethertype;

    if (proto->u.eth.flags & WREN_PROTO_ETH_FLAGS_MAC)
	memcpy(src->daddr, proto->u.eth.mac, 6);
    else
	memset(src->daddr, 0xff, 6);

    return 0;
}

static int
wrentx_eth_send_frame(struct wrentx_handle *handle,
		      unsigned source_idx,
		      struct wrentx_frame *frame)
{
    struct wrentx_handle_eth *he = (struct wrentx_handle_eth *)handle;
    struct wrentx_source *src;
    unsigned len;
    unsigned char pkt[1536];

    src = wrentx_check_source(he->sources, MAX_SOURCES, source_idx);
    if (src == NULL)
	return WRENTX_ERR_BAD_SOURCE_IDX;

    wrentx_frame_fill_header(handle, src, frame);
    len = frame->pkt.hdr.len * 4;
    if (len + 14 > sizeof(pkt))
	return -1;

    memcpy(pkt+0, he->eth_hdr, 14);
    memcpy(pkt+6, src->daddr, 6);
    pkt[12] = src->ethertype >> 8;
    pkt[13] = src->ethertype >> 0;
    memcpy(pkt+14, &frame->pkt, len);
    len += 14;

    if (sendto(he->sock, pkt, len, 0,
	       (struct sockaddr*)&he->addr, sizeof(he->addr)) < 0)
	return -1;

    return 0;
}

static int wrentx_eth_wren_get_time (struct wrentx_handle *handle,
				     struct wren_ts *time)
{
    struct wrentx_handle_eth *he = (struct wrentx_handle_eth *)handle;
    return wren_wren_get_time(he->wren_fd, time);
}

static int wrentx_eth_wren_wait_until(struct wrentx_handle *handle,
				      const struct wren_ts *time)
{
    struct wrentx_handle_eth *he = (struct wrentx_handle_eth *)handle;
    return wren_wren_wait_until(he->wren_fd, time);
}

static void
wrentx_eth_close(struct wrentx_handle *handle)
{
    struct wrentx_handle_eth *he = (struct wrentx_handle_eth *)handle;

    close(he->sock);
    free(he);
}

static const struct wrentx_operations wrentx_drv_eth_wren_ops = {
    wrentx_eth_close,
    wrentx_eth_set_source,
    wrentx_eth_wren_get_time,
    wrentx_eth_send_frame,
    NULL, /* Inject */
    wrentx_eth_wren_wait_until
};

static int wrentx_eth_rt_get_time (struct wrentx_handle *handle,
				   struct wren_ts *time)
{
  return wren_simrt_get_time(time);
}

static int wrentx_eth_rt_wait_until(struct wrentx_handle *handle,
				    const struct wren_ts *time)
{
  return wren_simrt_wait_until(time);
}

static const struct wrentx_operations wrentx_drv_eth_rt_ops = {
    wrentx_eth_close,
    wrentx_eth_set_source,
    wrentx_eth_rt_get_time,
    wrentx_eth_send_frame,
    NULL, /* Inject */
    wrentx_eth_rt_wait_until
};

static int
wrentx_open_eth_1(struct wrentx_handle_eth *handle,
		  const char *ifname,
		  const unsigned char *eth_dst, unsigned ethtype)
{
  struct ifreq if_idx;
  struct ifreq if_mac;

  for (unsigned i = 0; i < MAX_SOURCES; i++)
      wrentx_init_source(&handle->sources[i]);

  handle->sock = socket(PF_PACKET, SOCK_RAW, IPPROTO_RAW);
  if (handle->sock < 0) {
    perror("raw socket() failed");
    goto error;
  }

  /* Get interface index */
  memset(&if_idx, 0, sizeof(struct ifreq));
  strncpy(if_idx.ifr_name, ifname, sizeof(if_idx.ifr_name));
  if (ioctl(handle->sock, SIOCGIFINDEX, &if_idx) < 0) {
    fprintf(stderr, "%s: cannot get index of interface '%s'\n",
	    progname, ifname);
    goto error;
  }

  /* Get source mac address */
  memset(&if_mac, 0, sizeof(struct ifreq));
  strncpy(if_mac.ifr_name, ifname, sizeof(if_mac.ifr_name));
  if (ioctl(handle->sock, SIOCGIFHWADDR, &if_mac) < 0) {
    fprintf(stderr, "%s: cannot get mac address of interface '%s'\n",
	    progname, ifname);
    goto error;
  }

  memcpy(handle->eth_hdr + 0, eth_dst, 6);
  memcpy(handle->eth_hdr + 6, if_mac.ifr_hwaddr.sa_data, 6);
  handle->eth_hdr[12] = (ethtype >> 8) & 0xff;
  handle->eth_hdr[13] = (ethtype >> 0) & 0xff;

  wrentx_init_handle_base(&handle->base.base);

  handle->addr.sll_ifindex = if_idx.ifr_ifindex;
  handle->addr.sll_halen = ETH_ALEN;
  memcpy(handle->addr.sll_addr, eth_dst, ETH_ALEN);

  return 0;

 error:
  if (handle->sock >= 0)
    close(handle->sock);
  return -1;

}

struct wrentx_drv_eth_init_arg {
    char eth_name[IFNAMSIZ];
    unsigned char eth_dst[6];
};

void *
wrentx_drv_init_eth(int *argc, char *argv[])
{
    const char *ifname = argv[1];
    struct wrentx_drv_eth_init_arg *res;

    res = malloc (sizeof(*res));
    if (res == NULL)
	return NULL;

    memset(res->eth_dst, 0xff, 6);

    /* Copy interface name, stripping characters after '+'
       (to handle eth1+wren0) */
    {
	const char *p;
	int iflen;


	p = strchr(ifname, '+');
	if (p != NULL)
	    iflen = p - ifname;
	else
	    iflen = strlen(ifname);
	if (iflen > IFNAMSIZ - 1)
	    iflen = IFNAMSIZ - 1;

	memcpy(res->eth_name, ifname, iflen);
	res->eth_name[iflen + 1] = 0;
    }

  /* Remove interface name */
  wren_drv_remove_args (argc, argv, 1);

#if 0
  if (*argc > 2 && strcmp(argv[2], "--")) {
    if (inet_aton(argv[2], &dst_addr) == 0) {
      fprintf(stderr, "%s: cannot parse '%s' as ip address\n",
	      progname, argv[2]);
      return NULL;
    }
    wren_drv_remove_args (argc, argv, 1);
  }

  if (*argc > 2 && strcmp(argv[2], "--")) {
      char *e;

      dst_port = strtoul(argv[2], &e, 0);
      if (e == argv[2]) {
	fprintf(stderr, "%s: cannot parse '%s' as a port\n",
		progname, argv[2]);
	return NULL;
      }
      wren_drv_remove_args (argc, argv, 1);
  }
#endif

  if (*argc > 1) {
    if (strcmp(argv[1], "--")) {
      fprintf(stderr, "%s: missing -- after %s\n", progname, ifname);
      return NULL;
    }
    wren_drv_remove_args (argc, argv, 1);
  }

  return res;
}

struct wrentx_handle *
wrentx_drv_open_eth(void *init)
{
    struct wrentx_drv_eth_init_arg *arg = init;
    struct wrentx_handle_eth *handle;

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    handle->base.op = &wrentx_drv_eth_rt_ops;

    if (wrentx_open_eth_1(handle,
			  arg->eth_name, arg->eth_dst, WREN_ETHERTYPE) < 0) {
	free (handle);
	return NULL;
    }
    free (arg);

    return (struct wrentx_handle *)handle;
}

struct wrentx_handle *
wrentx_open_eth_wrenctl(const char *ifname,
			const unsigned char *eth_dst, unsigned ethtype)

{
    const char *devname = "/dev/wrenctl0";
    struct wrentx_handle_eth *handle;

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    handle->base.op = &wrentx_drv_eth_wren_ops;
    handle->wren_fd = open(devname, O_RDWR);
    if (handle->wren_fd < 0) {
	fprintf (stderr, "cannot open %s: %m\n", devname);
      return NULL;
    }

    if (wrentx_open_eth_1(handle, ifname, eth_dst, ethtype) < 0) {
	free (handle);
	return NULL;
    }
    return (struct wrentx_handle *)handle;
}

struct wrentx_handle *
wrentx_drv_open_eth_wrenctl(void *init)
{
    struct wrentx_drv_eth_init_arg *arg = init;
    struct wrentx_handle *handle;

    handle = wrentx_open_eth_wrenctl(arg->eth_name, arg->eth_dst, WREN_ETHERTYPE);
    if (handle == NULL)
	return NULL;
    free (arg);
    return (struct wrentx_handle *)handle;
}

struct wrentx_handle *
wrentx_open_eth_wren_default(void)
{
    static unsigned char bcast[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    return wrentx_open_eth_wrenctl("eth1", bcast, WREN_ETHERTYPE);
}
