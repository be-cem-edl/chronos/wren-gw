#ifndef __WRENTX_SIMVT__H__
#define __WRENTX_SIMVT__H__

#include "wren/wrentx.h"

struct wrentx_simvt {
  /* Current simulated time.  */
  struct wren_ts now;
};

/* Handler initialization */
void wrentx_simvt_init(struct wrentx_simvt *sim);

/* wrentx-drv operations (needs to be wrapped) */
int wrentx_simvt_get_time(struct wrentx_simvt *sim, struct wren_ts *time);
int wrentx_simvt_wait_until(struct wrentx_simvt *sim, const struct wren_ts *t);

#endif /* __WRENTX_SIMVT__H__ */
