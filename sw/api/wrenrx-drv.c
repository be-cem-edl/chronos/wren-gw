#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include "wren-drv.h"
#include "wrenrx-drv.h"
#include "wrenrx-cond.h"

static const struct wren_drv drivers[] = {
    {
	"stdin",
	"stdin\n"
	"  Receive the packets from standard input\n",
	wrenrx_drv_init_stdin,
	(wren_drv_open_t)wrenrx_drv_open_stdin,
    },
    {
	"udp",
	"udp [IP [PORT]]\n"
	"  Receive packets from udp\n",
	wrenrx_drv_init_udp,
	(wren_drv_open_t)wrenrx_drv_open_udp,
    },
    {
	"wren0",
	"wren0\n"
	"  Use wren board lun 0\n",
	wrenrx_drv_wren_init,
	(wren_drv_open_t)wrenrx_drv_wren_open,
    },
    {
	"wren1",
	"wren1\n"
	"  Use wren board lun 1\n",
	wrenrx_drv_wren_init,
	(wren_drv_open_t)wrenrx_drv_wren_open,
    },
    { NULL }
};

struct wren_drv_tuple *
wrenrx_drv_init(int *argc, char *argv[])
{
    return wren_drv_init(drivers, argc, argv);
}

struct wrenrx_handle *
wrenrx_drv_open(struct wren_drv_tuple *init)
{
    struct wrenrx_handle *res;
    res = init->drv->open(init->init);
    free (init);
    return res;
}

int
wrenrx_close(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;
    base->op->close(handle);
    return 0;
}

int
wrenrx_drv_read_packet(struct wrenrx_handle *handle, struct wren_packet *pkt)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;
  return base->op->recv_packet (handle, pkt);
}

int
wrenrx_drv_set_promisc(struct wrenrx_handle *handle, unsigned en)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;
  return base->op->set_promisc (handle, en);
}

int
wrenrx_get_time(struct wrenrx_handle *handle, struct wren_ts *time)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    return base->op->get_time(handle, time);
}

int
wrenrx_get_link(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    return base->op->get_link(handle);
}

int
wrenrx_get_sync(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    return base->op->get_sync(handle);
}

int
wrenrx_get_version(struct wrenrx_handle *handle,
		   char *version,
		   unsigned maxlen)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;
    static const char ver[] = "date:" __DATE__ " " __TIME__ "\n";
    int res;
    int len;

    res = base->op->get_version(handle, version, maxlen);
    if (res < 0)
	return res;
    len = strlen(version);
    snprintf(version + len, maxlen - len, "*software:\n%s", ver);

    version[maxlen-1] = 0;
    return 0;
}

int
wrenrx_get_panel_config(struct wrenrx_handle *handle,
			struct wren_panel_config *config)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    return base->op->get_panel_config(handle, config);
}

int
wrenrx_pulser_define (struct wrenrx_handle *handle,
		      unsigned pulser_num,
		      const struct wrenrx_cond *cond,
		      const struct wrenrx_pulser_configuration *conf,
		      const char *name)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (pulser_num == 0)
	return WRENRX_ERR_PULSER_NUM;

    return base->op->pulser_define (handle, pulser_num, cond, conf, name);
}


int wrenrx_pulser_reconfigure (struct wrenrx_handle *handle,
			       unsigned config_idx,
			       const struct wrenrx_pulser_configuration *conf)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_reconfigure(handle, config_idx, conf);
}

int wrenrx_pulser_force(struct wrenrx_handle *handle,
			unsigned config_idx,
			struct wren_ts *ts,
			struct wrenrx_pulser_configuration *conf)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_force(handle, config_idx, ts, conf);
}

int wrenrx_pulser_remove (struct wrenrx_handle *handle,
			  unsigned config_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_remove(handle, config_idx);
}

int wrenrx_pulser_get_config (struct wrenrx_handle *handle,
			      unsigned config_idx,
			      unsigned *pulse_num,
			      struct wrenrx_pulser_configuration *conf,
			      struct wrenrx_cond *cond,
			      char *name)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;


    return base->op->pulser_get_config(handle, config_idx, pulse_num,
				       conf, cond, name);
}

int wrenrx_pulser_program(struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  const struct wrenrx_pulser_configuration *conf,
			  const struct wren_ts *time,
			  const char *name)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (pulser_num == 0)
	return WRENRX_ERR_PULSER_NUM;

    return base->op->pulser_program(handle, pulser_num, conf, time, name);
}

struct wrenrx_msg *
wrenrx_wait(struct wrenrx_handle *handle)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  if (base->op->wait == NULL) {
    while (1)
      sleep (1);
  }
  return base->op->wait (handle);
}

int
wrenrx_get_fd(struct wrenrx_handle *handle)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  if (base->op->get_fd)
    return base->op->get_fd (handle);
  errno = ENOSYS;
  return -1;
}

int
wrenrx_add_source(struct wrenrx_handle *handle,
		  struct wren_protocol *proto)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->add_source (handle, proto);
}

int
wrenrx_get_source(struct wrenrx_handle *handle,
		  unsigned source_idx,
		  struct wren_protocol *proto)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->get_source (handle, source_idx, proto);
}

int wrenrx_remove_source(struct wrenrx_handle *handle,
			 unsigned source_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->del_source (handle, source_idx);
}

int wrenrx_context_subscribe(struct wrenrx_handle *handle,
			     unsigned source_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->context_subscribe(handle, source_idx);
}

int wrenrx_context_unsubscribe(struct wrenrx_handle *handle,
			       unsigned source_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->context_unsubscribe(handle, source_idx);
}


int wrenrx_event_subscribe (struct wrenrx_handle *handle,
			    unsigned source_idx,
			    uint16_t ev_id,
			    int offset_us)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->event_subscribe(handle, source_idx, ev_id, offset_us);
}

int wrenrx_event_unsubscribe (struct wrenrx_handle *handle, unsigned evsubs_id)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->event_unsubscribe(handle, evsubs_id);
}

int wrenrx_config_subscribe(struct wrenrx_handle *handle,
			    unsigned config_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->config_subscribe(handle, config_idx);
}

int wrenrx_config_unsubscribe(struct wrenrx_handle *handle,
			      unsigned config_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->config_unsubscribe(handle, config_idx);
}

int wrenrx_output_set_config (struct wrenrx_handle *handle,
			      unsigned output_num,
			      const struct wrenrx_output_configuration *cfg)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (output_num == 0)
	return WRENRX_ERR_OUTPUT_NUM;

    return base->op->output_set(handle, output_num, cfg);
}

int wrenrx_output_get_config (struct wrenrx_handle *handle,
			      unsigned output_num,
			      struct wrenrx_output_configuration *cfg)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (output_num == 0)
	return WRENRX_ERR_OUTPUT_NUM;

    return base->op->output_get(handle, output_num, cfg);
}

int wrenrx_pin_set_config(struct wrenrx_handle *handle,
			  unsigned pin_num,
			  const struct wrenrx_pin_configuration *cfg)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (pin_num == 0)
	return WRENRX_ERR_PIN_NUM;

    return base->op->pin_set(handle, pin_num, cfg);
}

int wrenrx_pin_get_config(struct wrenrx_handle *handle,
			  unsigned pin_num,
			  struct wrenrx_pin_configuration *cfg)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (pin_num == 0)
	return WRENRX_ERR_PIN_NUM;

    return base->op->pin_get(handle, pin_num, cfg);
}

int wrenrx_pulser_log_get(struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  struct wrenrx_pulser_log *buf,
			  unsigned len)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (pulser_num == 0)
	return WRENRX_ERR_PULSER_NUM;

    return base->op->pulser_log_get (handle, pulser_num, buf, len);
}

int wrenrx_pin_log_get(struct wrenrx_handle *handle,
		       unsigned pin_num,
		       struct wrenrx_pin_log *buf,
		       unsigned len)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    if (pin_num == 0)
	return WRENRX_ERR_PIN_NUM;

    return base->op->pin_log_get (handle, pin_num, buf, len);
}

void
wrenrx_pulser_configuration_to_mb(struct wren_mb_pulser_config *dst,
				  const struct wrenrx_pulser_configuration *src,
				  unsigned pulser_idx)
{
    memset(dst, 0, sizeof(*dst));

    dst->pulser_idx = pulser_idx;
    dst->inputs =
	  ((src->start & WREN_MB_PULSER_INPUT_MASK)
	   << WREN_MB_PULSER_INPUT_START_SH)
	| ((src->stop & WREN_MB_PULSER_INPUT_MASK)
	   << WREN_MB_PULSER_INPUT_STOP_SH)
	| ((src->clock & WREN_MB_PULSER_INPUT_MASK)
	   << WREN_MB_PULSER_INPUT_CLOCK_SH);

    dst->flags =
	  (src->repeat_mode ? WREN_MB_PULSER_FLAG_REPEAT : 0)
	| (src->output_en ? WREN_MB_PULSER_FLAG_OUT_EN : 0)
	| (src->interrupt_en ? WREN_MB_PULSER_FLAG_INT_EN : 0)
	| (src->config_en ? WREN_MB_PULSER_FLAG_ENABLE : 0);

    dst->width = src->pulse_width;
    dst->period = src->pulse_period;
    dst->npulses = src->npulses;
    dst->idelay = src->initial_delay;

    dst->load_off_sec = src->load_offset_sec;
    dst->load_off_nsec = src->load_offset_nsec;
}

int
wrenrx_cond_to_mb(struct wren_mb_cond *dst,
		  const struct wrenrx_cond *src)
{
    dst->src_idx = src->source_idx;
    dst->evt_id = src->ev_id;

    if (src->nbr_cond == 0)
	dst->len = 0;
    else {
	int len;
	len = wrenrx_cond_expr_to_words (src, src->nbr_cond - 1,
					 dst->ops, WREN_COND_MAXLEN);
	if (len < 0)
	    return WRENRX_ERR_CONDITION_TOO_LONG;
	dst->len = len;
    }

    return 0;
}

struct wrenrx_sw_log
{
    unsigned maxent;
    unsigned nument;
    unsigned nextent;

    unsigned src_idx;
    union wren_evlog_entry *ent;
};

struct wrenrx_sw_log *
wrenrx_sw_log_allocate(unsigned maxent)
{
    union wren_evlog_entry *ent;
    struct wrenrx_sw_log *res;

    ent = malloc(maxent * sizeof(union wren_evlog_entry));
    if (ent == NULL)
	return NULL;


    res = malloc(sizeof(res));
    if (res == NULL) {
	free (ent);
	return NULL;
    }

    res->maxent = maxent;
    res->nument = 0;
    res->ent = ent;

    return res;
}

void
wrenrx_sw_log_free(struct wrenrx_sw_log *buf)
{
    free(buf->ent);
    free(buf);
}

int
wrenrx_sw_log_get(struct wrenrx_handle *handle,
		  struct wrenrx_sw_log *buf)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;
    int res;

    res = base->op->sw_log_get (handle, buf->ent, buf->maxent);

    if (res < 0) {
	buf->nument = 0;
	return res;
    }

    buf->nument = res;
    buf->nextent = 0;
    buf->src_idx = ~0U;
    return 0;
}

unsigned
wrenrx_sw_log_get_length(struct wrenrx_sw_log *buf)
{
    return buf->nument;
}


int
wrenrx_sw_log_get_next_entry(struct wrenrx_sw_log *buf,
			     struct wrenrx_sw_log_entry *ent)
{
    union wren_evlog_entry *e;

    if (buf->nextent >= buf->nument)
	return WRENRX_ERR_EVLOG_TOO_LARGE;

    ent->log = buf;
    ent->idx = buf->nextent;
    buf->nextent++;

    /* Extract some informations for the next entries */
    e = &ent->log->ent[ent->idx];
    switch (e->e.type) {
    case EVLOG_TYPE_RECV:
	buf->src_idx = e->e.d[8];
	break;
    case EVLOG_TYPE_CTXT:
	/* TODO: context to give back parameters to events */
	break;
    default:
	break;
    }

    return 0;
}

unsigned
wrenrx_sw_log_get_type(struct wrenrx_sw_log_entry *ent)
{
    union wren_evlog_entry *e = &ent->log->ent[ent->idx];

    switch (e->e.type) {
    case EVLOG_TYPE_RECV:
	return WRENRX_SW_LOG_TYPE_PACKET;
    case EVLOG_TYPE_EVNT:
	return WRENRX_SW_LOG_TYPE_EVENT;
    case EVLOG_TYPE_CTXT:
	return WRENRX_SW_LOG_TYPE_CONTEXT;
    case EVLOG_TYPE_COND:
	return WRENRX_SW_LOG_TYPE_CONDITION;
    default:
	return WRENRX_SW_LOG_TYPE_UNKNOWN;
    }
}

int wrenrx_sw_log_get_time(struct wrenrx_sw_log_entry *ent,
				 struct wren_ts *time)
{
    union wren_evlog_entry *e = &ent->log->ent[ent->idx];

    if (e->e.type != EVLOG_TYPE_RECV)
	return WRENRX_ERR_EVLOG_BAD_TYPE;

    time->sec = e->w[1];
    time->nsec = e->w[2];

    return 0;
}

unsigned
wrenrx_sw_log_packet_get_source_idx(struct wrenrx_sw_log_entry *ent)
{
    union wren_evlog_entry *e = &ent->log->ent[ent->idx];

    if (e->e.type != EVLOG_TYPE_RECV)
	return ~0U;

    return ent->log->src_idx;
}

static void
wrenrx_sw_log_copy(struct wrenrx_sw_log_entry *ent,
		   void *dest, unsigned woff, unsigned wlen)
{
    struct wrenrx_sw_log *log = ent->log;
    unsigned idx = ent->idx;

    while (wlen > 0) {
	union wren_evlog_entry *e = &log->ent[idx];
	unsigned clen;

	clen = wlen;
	if (clen + woff > sizeof(*e) / 4)
	  clen = (sizeof(*e) / 4) - woff;

	memcpy(dest, &e->w[woff], clen * 4);

	wlen -= clen;
	woff = 1; /* Skip entry header */
	idx++;
	dest += clen * 4;
	if (idx >= log->nument)
	    return;
    }
}

struct wrenrx_msg *
wrenrx_sw_log_get_msg(struct wrenrx_sw_log_entry *ent)
{
    union wren_evlog_entry *e = &ent->log->ent[ent->idx];
    struct wrenrx_msg *res;

    res = malloc (sizeof(struct wrenrx_msg));
    if (res == NULL)
	return NULL;

    switch (e->e.type) {
    case EVLOG_TYPE_EVNT: {
	struct wren_capsule_event_hdr evt;
	const unsigned hdr_wlen = sizeof(evt) / 4;

	memcpy(&evt, &e->w[1], sizeof(evt));

	res->kind = wrenrx_msg_event;
	res->u.evt.source_idx = ent->log->src_idx;
	res->u.evt.evsub_id = ~0U;
	res->u.evt.evt.ev_id = evt.ev_id;
	res->u.evt.evt.ctxt_id = evt.ctxt_id;
	res->u.evt.evt.ts.sec = evt.ts.sec;
	res->u.evt.evt.ts.nsec = evt.ts.nsec;
	res->u.evt.evt.params_len = evt.hdr.len - hdr_wlen;
	memcpy(res->u.evt.evt.params, &e->w[1 + hdr_wlen],
	       res->u.evt.evt.params_len * 4);
	res->u.evt.ctxt.params_len = 0;
	return res;
    }
    case EVLOG_TYPE_CTXT: {
	struct wren_capsule_ctxt_hdr ctxt;
	const unsigned hdr_wlen = sizeof(ctxt) / 4;

	memcpy(&ctxt, &e->w[1], sizeof(ctxt));

	res->kind = wrenrx_msg_context;
	res->u.ctxt.source_idx = ent->log->src_idx;
	res->u.ctxt.ctxt_id = ctxt.ctxt_id;
	res->u.ctxt.ctxt.ts.sec = ctxt.valid_from.sec;
	res->u.ctxt.ctxt.ts.nsec = ctxt.valid_from.nsec;
	res->u.ctxt.ctxt.params_len = ctxt.hdr.len - hdr_wlen;
	wrenrx_sw_log_copy(ent, res->u.ctxt.ctxt.params,
			   1 + hdr_wlen, res->u.ctxt.ctxt.params_len);
	return res;
    }
    default:
	free(res);
	return NULL;
    }
}

void
wrenrx_sw_log_free_msg(struct wrenrx_msg *msg)
{
    free(msg);
}

int
wrenrx_hw_vme_p2a_set_output(struct wrenrx_handle *handle, unsigned en)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    return base->op->hw_vme_p2a_set_output (handle, en);
}

int
wrenrx_hw_patchpanel_set_config(struct wrenrx_handle *handle, unsigned en)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    return base->op->hw_patchpanel_set_config (handle, en);
}
