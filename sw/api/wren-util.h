#include "wren/wren-common.h"
#include "wren/wren-packet.h"

struct wren_packet;
void wren_dump_packet(struct wren_packet *pkt, unsigned len);
void wren_dump_packet_body(const uint32_t *w, unsigned len);
void wren_dump_hex (unsigned char *buf, unsigned wlen);
void wren_dump_packet_hex (struct wren_packet *pkt);

void wren_add_nsec(struct wren_ts *ts, unsigned nsec);
void wren_add_ts(struct wren_ts *ts, const struct wren_ts *v);

int wren_ts_lt(const struct wren_ts *l, const struct wren_ts *r);
int wren_ts_eq(const struct wren_ts *l, const struct wren_ts *r);

void wren_read_packet_ts(struct wren_ts *dest, const struct wren_packet_ts *ts);
void wren_add_packet_ts(struct wren_packet_ts *ts, const struct wren_ts *add);
