#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wrentx-drv-sim.h"
#include "wren-util.h"

void *
wrentx_drv_init_dump(int *argc, char *argv[])
{
  return wrentx_drv_init_stdout(argc, argv);
}

static int
wrentx_sim_send_dump(struct wrentx_handle_sim_fd *hfd,
		     struct wrentx_frame *frame)
{
    unsigned len;
    struct wren_ts now;

    len = frame->pkt.hdr.len * 4;

    wrentx_get_time((struct wrentx_handle *)hfd, &now);
    printf("# at %us + %uns, len=%u\n", now.sec, now.nsec, len);

    wren_dump_packet(&frame->pkt, len);

    return 0;
}

static const struct wrentx_sim_operations wrentx_sim_ops_dump = {
    wrentx_sim_send_dump,
    wrentx_sim_wait_vt
};

struct wrentx_handle *
wrentx_drv_open_dump(void *init)
{
  struct wrentx_handle *handle;

  handle = wrentx_open_sim_fd(&wrentx_sim_ops_dump, 1);
  if (handle == NULL)
    fprintf (stderr, "%s: cannot open dump\n", progname);
  return handle;
}
