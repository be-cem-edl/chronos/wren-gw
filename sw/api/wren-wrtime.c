#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include "wren-wrtime.h"
#include "wren-ioctl.h"

int
wren_wren_get_time(int fd, struct wren_ts *time)
{
  struct wren_wr_time res;

  if (ioctl(fd, WREN_IOC_GET_TIME, &res) < 0) {
    fprintf(stderr, "ioctl get_time: %m\n");
    return -1;
  }
  time->sec = res.tai_lo;
  time->nsec = res.ns;

  return 0;
}

int
wren_wren_wait_until(int fd, const struct wren_ts *t)
{
    struct wren_wr_time res;

    res.tai_hi = 0;
    res.tai_lo = t->sec;
    res.ns = t->nsec;

    if (ioctl(fd, WREN_IOC_WAIT_TIME, &res) < 0) {
	fprintf(stderr, "ioctl wait_time: %m\n");
	return -1;
    }
    return 0;
}
