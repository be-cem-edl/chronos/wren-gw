#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "wrt/wrtet.h"
#include "wrtet-mt.h"

static const char *progname;

int
main(int argc, char *argv[])
{
  struct wrtet_handle *handle;
  struct wrt_ts now;
  int status;
  int c;
  uint32_t dev_id;

  progname = argv[0];

  while ((c = getopt(argc, argv, "D:")) >= 0) {
    switch (c) {
    case 'D':
      dev_id = strtoul(optarg, NULL, 0);
      break;
    case '?':
      fprintf (stderr, "usage: %s [-D dev]\n", progname);
      return 2;
    default:
      abort ();
    }
  }


  handle = wrtet_open_mt(dev_id);
  if (handle == NULL) {
    fprintf (stderr, "%s: cannot open device\n", progname);
    return 1;
  }

  status = wrtet_get_time(handle, &now);
  if (status != 0) {
    fprintf (stderr, "%s: cannot get time\n", progname);
    return 1;
  }
  printf("time: %u+%uns (%x+%xns)\n",
         now.sec, now.nsec, now.sec, now.nsec);

  return 0;
}
