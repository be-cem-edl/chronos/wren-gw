#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include "wrentx-drv-sim.h"
#include "wren-util.h"

#define NBR_SOURCES 4

struct wrentx_handle_sim_fd {
    struct wrentx_handle_op base;
    struct wrentx_simvt sim;
    const struct wrentx_sim_operations *sim_ops;
    int fd;
    struct wrentx_source sources[NBR_SOURCES];
};

static unsigned
wrentx_sim_fd_exec_table(struct wrentx_handle_sim_fd *hfd,
			 unsigned source_idx,
			 unsigned table_idx);

static int wrentx_sim_fd_set_source (struct wrentx_handle *handle,
				     unsigned source_idx,
				     struct wren_protocol *proto)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_source *src;

    if (source_idx >= NBR_SOURCES)
	return WRENTX_ERR_BAD_SOURCE_IDX;

    src = &hfd->sources[source_idx];
    src->seq_id = 0;
    src->domain_id = proto->u.eth.domain_id;
    src->xmit_id = proto->u.eth.xmit_id;

    return 0;
}

int
wrentx_sim_send_fd(struct wrentx_handle_sim_fd *hfd,
		   struct wrentx_frame *frame)
{
    unsigned len;

    len = frame->pkt.hdr.len * 4;

    if (write (hfd->fd, &frame->pkt, len) != len) {
	/* For UDP, ignore unconnected socket errors. */
	if (errno != ECONNREFUSED)
	    return -1;
    }

    return 0;
}

static void
wrentx_start_table (struct wrentx_table_exec *t,
		    const struct wren_ts *exec_ts,
		    const struct wren_ts *due_ts)
{
    t->state = wrentx_table_state_resumed;
    t->pc = 0;
    t->count = 0;
    t->due_ts = *due_ts;
    t->exec_ts = *exec_ts;
}

static int
is_table_triggered(const struct wrentx_table_exec *t,
		   const unsigned char *buf)
{
    const struct wren_capsule_event_hdr *ehdr =
	(const struct wren_capsule_event_hdr *)buf;
    
    switch (t->cond) {
    case wrentx_table_cond_ev:
	return t->start_ev == ehdr->ev_id;
    case wrentx_table_cond_ev_param_s32: {
	unsigned off;
	
	if (t->start_ev != ehdr->ev_id)
	    return 0;

	/* Check param */
	for (off = sizeof(*ehdr); off < ehdr->hdr.len * 4;) {
	    const struct wren_packet_param_hdr *p =
		(struct wren_packet_param_hdr *)(buf + off);

	    if (WREN_PACKET_PARAM_GET_TYP(p->hdr) == t->start_param) {
		const uint32_t *v =
		    (const uint32_t *)(buf + off + sizeof(*p));
		return *v == t->start_val_s32;
	    }
	    off += WREN_PACKET_PARAM_GET_LEN(p->hdr) * 4;
	}
    }
    default:
	return 0;
    }
}

static int
wrentx_sim_fd_trigger_table(struct wrentx_handle_sim_fd *hfd,
			    struct wrentx_source *src,
			    const struct wrentx_frame *frame)
{
    unsigned off;
    unsigned res;

    res = 0;

    for (off = 0; off < frame->len;) {
	const unsigned char *capsule = frame->pkt.u.b + off * 4;
	struct wren_capsule_hdr *hdr = (struct wren_capsule_hdr *)capsule;

	if (hdr->typ == PKT_EVENT) {
	    struct wren_capsule_event_hdr *ehdr =
		(struct wren_capsule_event_hdr *)hdr;
	    unsigned tid;

	    for (tid = 0; tid < NBR_TABLES; tid++) {
		struct wrentx_table_exec *t = &src->tables[tid];
		if (t->state == wrentx_table_state_idle
		    && is_table_triggered (t, capsule)) {
		    struct wren_ts now;
		    struct wren_ts due_ts;

		    hfd->base.op->get_time((struct wrentx_handle *)hfd, &now);
		    wren_read_packet_ts(&due_ts, &ehdr->ts);

		    wrentx_start_table(t, &now, &due_ts);
		    res++;
		}
	    }
	}

	off += hdr->len;
    }

    return res;
}

static void
wrentx_sim_fd_exec_triggered_table(struct wrentx_handle_sim_fd *hfd,
				   unsigned source_idx)
{
    struct wrentx_source *src;
    unsigned trigger = 1;

    src = &hfd->sources[source_idx];

    while (trigger) {
	unsigned table_idx;

	trigger = 0;

	for (table_idx = 0; table_idx < NBR_TABLES; table_idx++) {
	    struct wrentx_table_exec *t = &src->tables[table_idx];
	    if (t->state == wrentx_table_state_resumed) {
		t->state = wrentx_table_state_exec;
		if (wrentx_sim_fd_exec_table (hfd, source_idx, table_idx))
		    trigger = 1;
	    }
	}
    }
}

static int
wrentx_sim_fd_send_frame(struct wrentx_handle *handle,
			 unsigned source_idx,
			 struct wrentx_frame *frame)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_source *src;
    int trigger;
    int res;

    src = wrentx_check_source(hfd->sources, NBR_SOURCES, source_idx);
    if (src == NULL)
	return WRENTX_ERR_BAD_SOURCE_IDX;

    wrentx_frame_fill_header(handle, src, frame);

    trigger = wrentx_sim_fd_trigger_table(hfd, src, frame);

    res = hfd->sim_ops->send(hfd, frame);
    if (res < 0)
      return res;

    if (trigger)
	wrentx_sim_fd_exec_triggered_table(hfd, source_idx);

    return 0;
}

static unsigned
wrentx_sim_fd_send_table_frame(struct wrentx_handle_sim_fd *hfd,
			       unsigned source_idx,
			       const struct wren_ts *due_ts,
			       const uint32_t *data,
			       unsigned data_len)
{
    struct wrentx_handle *h = (struct wrentx_handle *)hfd;
    struct wrentx_frame frame;
    unsigned off;
    struct wrentx_source *src;
    unsigned trigger;

    src = wrentx_check_source(hfd->sources, NBR_SOURCES, source_idx);
    assert(src != NULL);

    wrentx_clear_frame_base(NULL, &frame);

    /* Copy content, update timestamps */
    for (off = 0; off < data_len; ) {
	struct wren_capsule_event_hdr hdr;
	unsigned len;

	memcpy (&hdr, &data[off], sizeof (hdr));
	/* Can only have event capsules. */
	assert(hdr.hdr.typ == PKT_EVENT);
	assert(off + hdr.hdr.len <= data_len);

	/* TODO: update context-id */

	len = hdr.hdr.len;
	wren_add_packet_ts(&hdr.ts, due_ts);

	/* Copy event capsule header */
	memcpy(&frame.pkt.u.b[frame.len * 4], &hdr, sizeof(hdr));
	frame.len += sizeof(hdr) / 4;
	off += sizeof(hdr) / 4;
	len -= sizeof(hdr) / 4;

	/* Copy event capsule parameters */
	memcpy(&frame.pkt.u.b[frame.len * 4], &data[off], len * 4);
	frame.len += len;
	off += len;
    }

    wrentx_frame_fill_header(h, src, &frame);

    trigger = wrentx_sim_fd_trigger_table(hfd, src, &frame);

    hfd->sim_ops->send(hfd, &frame);

    return trigger;
}

/* Return non-0 if a table has been triggered. */
static unsigned
wrentx_sim_fd_exec_table(struct wrentx_handle_sim_fd *hfd,
			 unsigned source_idx,
			 unsigned table_idx)
{
    struct wrentx_table_exec *t;
    int trigger;

    trigger = 0;

    t = &hfd->sources[source_idx].tables[table_idx];

    while (1) {
	if (t->pc >= t->def.nbr_insns) {
	    /* End of table.  */
	    t->count++;
	    t->pc = 0;
	    if (t->loop_count != 0 && t->count == t->loop_count) {
		/* End of execution */
		t->state = wrentx_table_state_idle;
		return trigger;
	    }
	}

	switch (t->def.insns[t->pc].op) {
	case wrentx_table_op_wait: {
	    struct wrentx_table_insn_wait *op = &t->def.insns[t->pc].wait;
	    wren_add_ts(&t->due_ts, &op->delay);
	    wren_add_ts(&t->exec_ts, &op->delay);
	    /* If in the past, will continue at the next wait */
	    return trigger;
	}
	case wrentx_table_op_send: {
	    struct wrentx_table_insn_send *op = &t->def.insns[t->pc].send;
	    trigger += wrentx_sim_fd_send_table_frame
		(hfd, source_idx, &t->due_ts, &t->def.data[op->off], op->len);
	    t->pc++;
	    break;
	}
	}
    }
}

static int
wrentx_sim_fd_get_time (struct wrentx_handle *handle,
			struct wren_ts *time)
{
  struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
  return wrentx_simvt_get_time(&hfd->sim, time);
}

/* Compute the earliest deadline between \p time and wait instructions in
   tables.
   Return 0 if no earlier deadline, 1 if a table has to be executed before
   \p time.
   In any case, \p res is set to the earliest deadline. */
static int
wrentx_sim_fd_first_deadline(struct wrentx_handle_sim_fd *hfd,
			     const struct wren_ts *time,
			     struct wren_ts *res)
{
    unsigned i, j;
    int status;

    *res = *time;
    status = 0;

    for (i = 0; i < NBR_SOURCES; i++)
	for (j = 0; j < NBR_TABLES; j++) {
	    struct wrentx_table_exec *t = &hfd->sources[i].tables[j];
	    if (t->state == wrentx_table_state_exec
		&& t->def.insns[t->pc].op == wrentx_table_op_wait
		&& wren_ts_lt(&t->exec_ts, res)) {

		/* Note: cannot modify table t, as it might not be the earliest
		   to be awaken. */
		*res = t->exec_ts;
		status = 1;
	    }
	}
    return status;
}

static void
wrentx_sim_fd_wakeup_tables(struct wrentx_handle_sim_fd *hfd,
			    const struct wren_ts *now)
{
    unsigned src_idx, tab;

    /* TODO: keep a list of active tables */

    for (src_idx = 0; src_idx < NBR_SOURCES; src_idx++) {
	unsigned trigger;

	trigger = 0;
	for (tab = 0; tab < NBR_TABLES; tab++) {
	    struct wrentx_table_exec *t = &hfd->sources[src_idx].tables[tab];
	    if (t->state == wrentx_table_state_exec
		&& t->def.insns[t->pc].op == wrentx_table_op_wait
		&& wren_ts_eq(&t->exec_ts, now)) {
		/* Skip the wait */
		t->pc++;
		trigger += wrentx_sim_fd_exec_table(hfd, src_idx, tab);
	    }
	}
	if (trigger)
	    wrentx_sim_fd_exec_triggered_table(hfd, src_idx);
    }
}

int
wrentx_sim_wait_vt(struct wrentx_handle_sim_fd *handle,
		   const struct wren_ts *time)
{
    return wrentx_simvt_wait_until(&handle->sim, time);
}

static int
wrentx_sim_fd_wait_until(struct wrentx_handle *handle,
			 const struct wren_ts *time)
{
  struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;

  while (1) {
      int status;
      struct wren_ts ts;
      int res;

      status = wrentx_sim_fd_first_deadline(hfd, time, &ts);

      res = hfd->sim_ops->wait(hfd, &ts);
      if (res != 0)
	  return res;
      if (status == 0)
	  return 0;

      wrentx_sim_fd_wakeup_tables(hfd, &ts);
  }
}

static int
wrentx_sim_fd_load_table(struct wrentx_handle *handle,
			 unsigned source_idx,
			 struct wrentx_table *table,
			 const char *name,
			 unsigned table_idx,
			 unsigned count)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_table_exec *t;
    unsigned namelen;

    t = wrentx_check_table(hfd->sources, NBR_SOURCES, source_idx, table_idx);
    if (t == NULL)
	return WRENTX_ERR_BAD_TABLE_IDX;

    t->state = wrentx_table_state_idle;
    t->loop_count = count;
    t->cond = wrentx_table_cond_none;

    /* TODO:
       check table is not empty,
       check last op is wait if looping. */

    t->def = *table;

    /* Copy (with possible truncation) name */
    namelen = strlen (name);
    if (namelen > WRENTX_TABLE_NAME_MAXLEN)
	namelen = WRENTX_TABLE_NAME_MAXLEN - 1;
    memcpy (t->name, name, namelen);
    t->name[namelen] = 0;

    /* Init */
    t->pc = 0;
    t->count = 0;

    return 0;
}

static int
wrentx_sim_fd_unload_table(struct wrentx_handle *handle,
			   unsigned source_idx,
			   unsigned table_idx)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_table_exec *t;

    t = wrentx_check_table(hfd->sources, NBR_SOURCES, source_idx, table_idx);
    if (t == NULL)
	return WRENTX_ERR_BAD_TABLE_IDX;

    if (t->state == wrentx_table_state_none) {
	return WRENTX_ERR_EMPTY_TABLE;
    }
    t->state = wrentx_table_state_none;
    return 0;
}

static int
wrentx_sim_fd_play_table(struct wrentx_handle *handle,
			 unsigned source_idx,
			 unsigned table_idx,
			 const struct wren_ts *ts)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_table_exec *t;
    struct wren_ts now;
    unsigned trigger;

    t = wrentx_check_table(hfd->sources, NBR_SOURCES, source_idx, table_idx);
    if (t == NULL)
	return WRENTX_ERR_BAD_TABLE_IDX;

    /* Check table status */
    switch (t->state) {
    case wrentx_table_state_none:
	return WRENTX_ERR_EMPTY_TABLE;
    case wrentx_table_state_idle:
	break;
    case wrentx_table_state_exec:
    case wrentx_table_state_resumed:
	return 0;
    }

    hfd->base.op->get_time(handle, &now);
    wrentx_start_table(t, &now, ts);
    t->state = wrentx_table_state_exec;
    trigger = wrentx_sim_fd_exec_table(hfd, source_idx, table_idx);

    if (trigger)
	wrentx_sim_fd_exec_triggered_table(hfd, source_idx);

    return 0;
}

static int
wrentx_sim_fd_play_table_on_event(struct wrentx_handle *handle,
				  unsigned source_idx,
				  unsigned table_idx,
				  wren_event_id ev)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_table_exec *t;

    t = wrentx_check_table(hfd->sources, NBR_SOURCES, source_idx, table_idx);
    if (t == NULL)
	return WRENTX_ERR_BAD_TABLE_IDX;

    /* Check table status */
    if (t->state == wrentx_table_state_none)
	return WRENTX_ERR_EMPTY_TABLE;

    t->cond = wrentx_table_cond_ev;
    t->start_ev = ev;

    return 0;
}

static int
wrentx_sim_fd_play_table_on_cond_event_s32(struct wrentx_handle *handle,
					   unsigned source_idx,
					   unsigned table_idx,
					   wren_event_id ev,
					   wren_param_id param,
					   int32_t val)
{
   struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
   struct wrentx_table_exec *t;

    t = wrentx_check_table(hfd->sources, NBR_SOURCES, source_idx, table_idx);
    if (t == NULL)
	return WRENTX_ERR_BAD_TABLE_IDX;

    /* Check table status */
    if (t->state == wrentx_table_state_none)
	return WRENTX_ERR_EMPTY_TABLE;

    t->cond = wrentx_table_cond_ev_param_s32;
    t->start_ev = ev;
    t->start_param = param;
    t->start_val_s32 = val;

    return 0;
}

static int
wrentx_sim_fd_stop_table(struct wrentx_handle *handle,
			 unsigned source_idx,
			 unsigned table_idx)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_table_exec *t;

    t = wrentx_check_table(hfd->sources, NBR_SOURCES, source_idx, table_idx);
    if (t == NULL)
	return WRENTX_ERR_BAD_TABLE_IDX;

    /* Check table status */
    switch (t->state) {
    case wrentx_table_state_none:
	return WRENTX_ERR_EMPTY_TABLE;
    case wrentx_table_state_idle:
	return 0;
    case wrentx_table_state_exec:
    case wrentx_table_state_resumed:
	break;
    }

    t->state = wrentx_table_state_idle;
    return 0;
}

static int
wrentx_sim_fd_get_table_name(struct wrentx_handle *handle,
			     unsigned source_idx,
			     unsigned table_idx,
			     char *name)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;
    struct wrentx_table_exec *t;

    t = wrentx_check_table(hfd->sources, NBR_SOURCES, source_idx, table_idx);
    if (t == NULL)
	return WRENTX_ERR_BAD_TABLE_IDX;

    /* Check table status */
    if (t->state == wrentx_table_state_none)
	return WRENTX_ERR_EMPTY_TABLE;

    memcpy(name, t->name, WRENTX_TABLE_NAME_MAXLEN);
    return 0;
}

static void
wrentx_sim_fd_close(struct wrentx_handle *handle)
{
    struct wrentx_handle_sim_fd *hfd = (struct wrentx_handle_sim_fd *)handle;

    if (hfd->fd > 2)
	close (hfd->fd);

    free(hfd);
}

static const struct wrentx_operations wrentx_sim_fd_ops = {
    wrentx_sim_fd_close,
    wrentx_sim_fd_set_source,
    wrentx_sim_fd_get_time,
    wrentx_sim_fd_send_frame,
    NULL, /* inject */
    wrentx_sim_fd_wait_until,
    wrentx_sim_fd_load_table,
    wrentx_sim_fd_unload_table,
    wrentx_sim_fd_play_table,
    wrentx_sim_fd_stop_table,
    wrentx_sim_fd_play_table_on_event,
    wrentx_sim_fd_play_table_on_cond_event_s32,
    wrentx_sim_fd_get_table_name,
};

const struct wrentx_sim_operations wrentx_sim_ops_fd_vt = {
    wrentx_sim_send_fd,
    wrentx_sim_wait_vt
};

struct wrentx_handle *
wrentx_open_sim_fd(const struct wrentx_sim_operations *ops, int fd)
{
  struct wrentx_handle_sim_fd *res;

  res = malloc(sizeof (*res));
  if (res == NULL)
    return NULL;

  res->base.op = &wrentx_sim_fd_ops;
  res->sim_ops = ops;
  wrentx_init_handle_base(&res->base.base);
  wrentx_simvt_init(&res->sim);

  for (unsigned i = 0; i < NBR_SOURCES; i++)
      wrentx_init_source(&res->sources[i]);

  res->fd = fd;

  return (struct wrentx_handle *)res;
}
