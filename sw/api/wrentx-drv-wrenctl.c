#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wren-mb.h"
#include "wren-wrtime.h"
#include "wren-ioctl.h"

struct wrentx_handle_wrenctl {
    struct wrentx_handle_op base;

    int wren_fd;
};

static int wrentx_wrenctl_set_source (struct wrentx_handle *handle,
				      unsigned source_idx,
				      struct wren_protocol *proto)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    struct wren_mb_tx_set_source cmd;
    struct wren_mb_metadata wmd;
    int len;

    cmd.source_idx = source_idx;
    cmd.proto = *proto;
    cmd.max_delay_us_p2 = 0;  /* TODO */

    if (proto->proto == WREN_PROTO_ETHERNET) {
	memset (cmd.proto.u.eth.mac, 0xff, 6);
    }
    wmd.cmd = CMD_TX_SET_SOURCE;
    wmd.len = sizeof(cmd) / 4;

    len = wren_mb_msg(he->wren_fd, &wmd, &cmd, NULL, 0);
    if (len == 0 && wmd.cmd == CMD_REPLY)
	return 0;
    if (wmd.cmd & CMD_ERROR)
	return -(wmd.cmd & 0xffff);
    return WRENTX_ERR_SYSTEM;
}

static int
wrentx_wrenctl_send_frame(struct wrentx_handle *handle,
			  unsigned source_idx,
			  struct wrentx_frame *frame)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    uint32_t *data;
    struct wren_mb_metadata wmd;
    int len;
    uint32_t res;

    /* Only the pdu of the frame is sent to the board, but we also need to
       specify the source_idx.  Hidjack the last word of the packet header. */
    data = frame->pkt.u.w;
    data[-1] = source_idx;

    wmd.cmd = CMD_TX_SEND_PACKET;
    wmd.len = frame->len + 1; /* 1 for source_idx */

    len = wren_mb_msg(he->wren_fd, &wmd, data - 1, &res, sizeof(res));
    if (len == 0 && wmd.cmd == CMD_REPLY)
	return 0;
    if (wmd.cmd & CMD_ERROR)
	return -(wmd.cmd & 0xffff);
    return WRENTX_ERR_SYSTEM;
}

static int
wrentx_wrenctl_inject_frame(struct wrentx_handle *handle,
			    unsigned source_idx,
			    struct wrentx_frame *frame)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    struct wren_mb_metadata wmd;
    int len;
    uint32_t res;
    struct cmd_msg {
	uint32_t src_idx;
	struct wren_packet pkt;
    } data;

    /* Build the message */
    data.src_idx = source_idx;
    memcpy(&data.pkt.hdr, &frame->pkt.hdr, sizeof(frame->pkt.hdr));
    memcpy(data.pkt.u.w, frame->pkt.u.w, frame->len * 4);

    wmd.cmd = CMD_RX_INJECT_PACKET;
    wmd.len = 1 + WREN_PACKET_HDR_WORDS + frame->len; /* 1 for source_idx */

    len = wren_mb_msg(he->wren_fd, &wmd, &data, &res, sizeof(res));
    if (len == 0 && wmd.cmd == CMD_REPLY)
	return 0;
    if (wmd.cmd & CMD_ERROR)
	return -(wmd.cmd & 0xffff);
    return WRENTX_ERR_SYSTEM;
}


static int wrentx_wrenctl_wren_get_time (struct wrentx_handle *handle,
					 struct wren_ts *time)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    return wren_wren_get_time(he->wren_fd, time);
}

static int wrentx_wrenctl_wren_wait_until(struct wrentx_handle *handle,
					  const struct wren_ts *time)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    return wren_wren_wait_until(he->wren_fd, time);
}

static int
wrentx_wrenctl_load_table(struct wrentx_handle *handle,
			  unsigned source_idx,
			  struct wrentx_table *table,
			  const char *name,
			  unsigned table_idx,
			  unsigned repeat_count)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    struct wren_ioctl_tx_set_table tbl;
    unsigned namelen;
    int res;

    /* Copy (with possible truncation) name */
    namelen = strlen (name);
    if (namelen >= WRENTX_TABLE_NAME_MAXLEN)
	return WRENTX_ERR_NAME_TOO_LONG;
    memset (tbl.name, 0, WRENTX_TABLE_NAME_MAXLEN);
    memcpy (tbl.name, name, namelen);

    tbl.table.src_idx = source_idx;
    tbl.table.table_idx = table_idx;
    tbl.table.repeat = repeat_count;
    tbl.table.nbr_insns = table->nbr_insns;
    tbl.table.nbr_data = table->nbr_data;

    memcpy(tbl.table.data, table->insns, table->nbr_insns * 4);
    memcpy(tbl.table.data + table->nbr_insns, table->data, table->nbr_data * 4);

    res = ioctl(he->wren_fd, WREN_IOC_TX_SET_TABLE, &tbl);
    if (res < 0) {
	if (errno == EINVAL || errno == ECOMM)
	    res = tbl.error;
	else
	    res = WRENTX_ERR_SYSTEM;
    }

    return res;
}

static void
wrentx_wrenctl_close(struct wrentx_handle *handle)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;

    close(he->wren_fd);
    free(he);
}

static const struct wrentx_operations wrentx_drv_wrenctl_wren_ops = {
    wrentx_wrenctl_close,
    wrentx_wrenctl_set_source,
    wrentx_wrenctl_wren_get_time,
    wrentx_wrenctl_send_frame,
    wrentx_wrenctl_inject_frame,
    wrentx_wrenctl_wren_wait_until,

    wrentx_wrenctl_load_table,
};

struct wrentx_drv_wrenctl_init_arg {
    char devname[32];
};

void *
wrentx_drv_init_wrenctl(int *argc, char *argv[])
{
    const char *ifname = argv[1];
    struct wrentx_drv_wrenctl_init_arg *res;

    res = malloc (sizeof(*res));
    if (res == NULL)
	return NULL;

    snprintf(res->devname, sizeof(res->devname), "/dev/%s", ifname);

    /* Remove interface name */
    wren_drv_remove_args (argc, argv, 1);


    if (*argc > 1) {
	if (strcmp(argv[1], "--")) {
	    fprintf(stderr, "%s: missing -- after %s\n", progname, ifname);
	    return NULL;
	}
	wren_drv_remove_args (argc, argv, 1);
    }

    return res;
}

struct wrentx_handle *
wrentx_open_wrenctl(const char *devname)

{
    struct wrentx_handle_wrenctl *handle;

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    handle->base.op = &wrentx_drv_wrenctl_wren_ops;
    handle->wren_fd = wrenctl_open(devname);
    if (handle->wren_fd < 0) {
	fprintf (stderr, "cannot open %s: %m\n", devname);
	goto error;
    }

    if (wren_check_version(handle->wren_fd) < 0)
	goto error;

    return (struct wrentx_handle *)handle;

  error:
    if (handle->wren_fd >= 0)
      close (handle->wren_fd);
    free(handle);
    return NULL;
}

struct wrentx_handle *
wrentx_drv_open_wrenctl(void *init)
{
    struct wrentx_drv_wrenctl_init_arg *arg = init;
    struct wrentx_handle *handle;

    handle = wrentx_open_wrenctl(arg->devname);
    if (handle == NULL)
	return NULL;
    free (arg);
    return (struct wrentx_handle *)handle;
}
