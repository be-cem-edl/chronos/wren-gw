/* Utility functions that can also be used in the kernel driver */
#include "wren/wren-common.h"
#include "wren-mb-defs.h"

int wren_is_same_proto(const struct wren_protocol *l,
		       const struct wren_protocol *r);

int wren_is_same_cond(const struct wren_mb_cond *l,
		      const struct wren_mb_cond *r);
