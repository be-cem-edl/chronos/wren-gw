#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "wrentx-simvt.h"

void
wrentx_simvt_init(struct wrentx_simvt *sim)
{
  sim->now.sec = 0;
  sim->now.nsec = 0;
}


int
wrentx_simvt_get_time(struct wrentx_simvt *sim, struct wren_ts *time)
{
  *time = sim->now;
  return 0;
}

int
wrentx_simvt_wait_until(struct wrentx_simvt *sim, const struct wren_ts *t)
{
    sim->now = *t;
    return 0;
}
