#include <sys/socket.h>
#include <arpa/inet.h>

struct wren_drv_udp_init_arg {
  struct in_addr dst_addr;
  unsigned dst_port;
};

/* Parse args for udp */
struct wren_drv_udp_init_arg *wren_drv_init_udp(int *argc, char *argv[]);
