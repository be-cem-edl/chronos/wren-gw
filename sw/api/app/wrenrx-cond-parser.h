#ifndef __WRENRX_COND_PARSER__H__
#define __WRENRX_COND_PARSER__H__

#include <stdint.h>
#include "wren/wrenrx.h"

enum tok
  {
   TOK_ERR,
   TOK_EOL,
   TOK_COMMA,
   TOK_EVT_PARAM,
   TOK_CTXT_PARAM,
   TOK_ANY_PARAM,
   TOK_INT,
   TOK_EQ,
   TOK_AND,
   TOK_OR,
   TOK_LPAREN,
   TOK_RPAREN
  };

enum wrenrx_cond_tkind
  {
   T_PARAM,
   T_BINARY
  };

struct wrenrx_cond_tree {
  enum wrenrx_cond_tkind kind;
  union {
    struct {
      enum tok op;
      struct wrenrx_cond_tree *l;
      struct wrenrx_cond_tree *r;
    } binary;
    struct {
      enum tok op;
      enum tok orig;
      uint32_t id;
      uint32_t val;
    } param;
  } u;
};

struct wrenrx_cond_tree_event {
  uint32_t source_idx;
  uint32_t ev_id;
  struct wrenrx_cond_tree *expr;
};

/* Parse a condition.
   Format is:
   COND ::=
       pNUM OP NUM
       COND && COND
       COND || COND
       ( COND )

   Return NULL on error.
*/
struct wrenrx_cond_tree *wrenrx_cond_parse_expr(const char *line);

/* Disp a condition expression */
void wrenrx_cond_disp_tree (struct wrenrx_cond_tree *t);


/* Parse an event condition expression.
   Format is:
       EVID , EVENT-COND [ , DOMAIN ]
*/
struct wrenrx_cond_tree_event *parse_cond(const char *line);

/* Disp a parsed condition.  */
void wrenrx_cond_disp_tree_event(struct wrenrx_cond_tree_event *t);

/* Display a condition expression */
void wrenrx_cond_expr_disp (const struct wrenrx_cond *cond,
			    const struct wrenrx_cond_expr *expr);


#endif /* __WRENRX_COND_PARSER__H__ */
