#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include "wrenrx-cond-parser.h"

struct scan_ctxt
{
  const char *line;
  unsigned pos;
  enum tok tok;
  uint32_t val;
};

static const char *tok_names[] = {
  [TOK_ERR] = "error",
  [TOK_EOL] = "end-of-line",
  [TOK_COMMA] = ",",
  [TOK_EVT_PARAM] = "event-param",
  [TOK_CTXT_PARAM] = "context-param",
  [TOK_ANY_PARAM] = "any-param",
  [TOK_INT] = "integer",
  [TOK_EQ] = "==",
  [TOK_AND] = "&&",
  [TOK_OR] = "||",
  [TOK_LPAREN] = "(",
  [TOK_RPAREN] = ")"
};

static void
error (struct scan_ctxt *sctxt, const char *hdr, const char *msg, va_list args)
{
  unsigned i;

  fprintf (stderr, "%s: ", hdr);
  vfprintf (stderr, msg, args);
  fprintf (stderr, "\n%s\n", sctxt->line);
  for (i = 0; i < sctxt->pos; i++)
    fputc (' ', stderr);
  fputc ('^', stderr);
  fputc ('\n', stderr);
}

static void
scan_error (struct scan_ctxt *sctxt, const char *msg, ...)
{
  va_list args;

  va_start(args, msg);
  error (sctxt, "scan error", msg, args);
  va_end(args);
}

static void
parse_error (struct scan_ctxt *sctxt, const char *msg, ...)
{
  va_list args;

  va_start(args, msg);
  error (sctxt, "parse error", msg, args);
  va_end(args);
}

/* Scan a decimal number at SCTXT.  The current char is a digit. */
static void
scan_num (struct scan_ctxt *sctxt)
{
  unsigned v = 0;
  do {
    v = v * 10 + sctxt->line[sctxt->pos] - '0';
    sctxt->pos++;
  } while (isdigit(sctxt->line[sctxt->pos]));
  sctxt->val = v;
}

static void
scan (struct scan_ctxt *sctxt)
{
  unsigned p = sctxt->pos;
  char c;

  /* Skip spaces.  */
  while (sctxt->line[p] == ' ')
    p++;

  c = sctxt->line[p];
  switch (c) {
  case '=':
    p++;
    if (sctxt->line[p] != '=') {
      sctxt->pos = p;
      scan_error(sctxt, "expect '=='");
      sctxt->tok = TOK_ERR;
      return;
    }
    sctxt->pos = p + 1;
    sctxt->tok = TOK_EQ;
    return;

  case '&':
    p++;
    if (sctxt->line[p] != '&') {
      sctxt->pos = p;
      scan_error(sctxt, "expect '&&'");
      sctxt->tok = TOK_ERR;
      return;
    }
    sctxt->pos = p + 1;
    sctxt->tok = TOK_AND;
    return;

  case '|':
    p++;
    if (sctxt->line[p] != '|') {
      sctxt->pos = p;
      scan_error(sctxt, "expect '||'");
      sctxt->tok = TOK_ERR;
      return;
    }
    sctxt->pos = p + 1;
    sctxt->tok = TOK_OR;
    return;

  case '(':
    sctxt->pos = p + 1;
    sctxt->tok = TOK_LPAREN;
    return;

  case ')':
    sctxt->pos = p + 1;
    sctxt->tok = TOK_RPAREN;
    return;

  case 'e':
  case 'c':
  case 'a':
    /* Parameter number.  */
    p++;
    if (!isdigit(sctxt->line[p])) {
      sctxt->pos = p;
      scan_error(sctxt, "expect digit after '%c'", c);
      sctxt->tok = TOK_ERR;
      return;
    }
    sctxt->pos = p;
    scan_num (sctxt);
    if (c == 'e')
      sctxt->tok = TOK_EVT_PARAM;
    else if (c == 'c')
      sctxt->tok = TOK_CTXT_PARAM;
    else if (c == 'a')
      sctxt->tok = TOK_ANY_PARAM;
    else
      abort();
    return;

  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    sctxt->pos = p;
    scan_num (sctxt);
    sctxt->tok = TOK_INT;
    return;

  case ',':
    sctxt->pos = p + 1;
    sctxt->tok = TOK_COMMA;
    return;

  case 0:
    sctxt->pos = p;
    sctxt->tok = TOK_EOL;
    return;

  default:
    sctxt->pos = p;
    scan_error(sctxt, "bad character '%c'", sctxt->line[p]);
    sctxt->tok = TOK_ERR;
    return;
  }
}

static struct wrenrx_cond_tree *
parse_simple (struct scan_ctxt *sctxt)
{
  struct wrenrx_cond_tree *res;
  enum tok orig = sctxt->tok;

  if (orig != TOK_EVT_PARAM
      && orig != TOK_CTXT_PARAM
      && orig != TOK_ANY_PARAM) {
    parse_error (sctxt, "parameter id expected, got '%s'",
		 tok_names[sctxt->tok]);
    return NULL;
  }

  res = malloc (sizeof (struct wrenrx_cond_tree));
  res->kind = T_PARAM;
  res->u.param.id = sctxt->val;
  res->u.param.orig = orig;

  scan(sctxt);
  switch (sctxt->tok) {
  case TOK_EQ:
    res->u.param.op = TOK_EQ;
    scan(sctxt);
    break;
  default:
    parse_error (sctxt, "comparaison operator expected");
    free (res);
    return NULL;
  }

  if (sctxt->tok != TOK_INT) {
    parse_error (sctxt, "comparaison value expected");
    free (res);
    return NULL;
  }
  res->u.param.val = sctxt->val;
  scan(sctxt);
  return res;
}

static struct wrenrx_cond_tree *parse_condition (struct scan_ctxt *sctxt, int prio);

static struct wrenrx_cond_tree *
build_binary (enum tok op,
	      struct wrenrx_cond_tree *l,
	      struct wrenrx_cond_tree *r)
{
  struct wrenrx_cond_tree *res;

  res = malloc (sizeof (struct wrenrx_cond_tree));
  res->kind = T_BINARY;
  res->u.binary.op = op;
  res->u.binary.l = l;
  res->u.binary.r = r;

  return res;
}

static struct wrenrx_cond_tree *
parse_single (struct scan_ctxt *sctxt)
{
  if (sctxt->tok == TOK_LPAREN) {
    struct wrenrx_cond_tree *res;

    scan(sctxt);
    res = parse_condition(sctxt, 0);
    if (res == NULL)
      return NULL;
    if (sctxt->tok != TOK_RPAREN) {
      parse_error(sctxt, "')' expected");
      return NULL;
    }
    scan (sctxt);
    return res;
  }
  else
    return parse_simple (sctxt);
}

/* Return priority of OP, -1 if not an operator.  */
static int
get_op_prio (enum tok op)
{
  switch (op) {
    case TOK_AND:
      return 1;
    case TOK_OR:
      return 0;
    default:
      return -1;
  }
}

/* Build a tree eating all sub-expressions at priority >= PRIO.  */
static struct wrenrx_cond_tree *
parse_condition (struct scan_ctxt *sctxt, int prio)
{
  struct wrenrx_cond_tree *res;

  res = parse_single (sctxt);
  if (res == NULL)
    return NULL;

  while (1) {
    struct wrenrx_cond_tree *r;
    enum tok op = sctxt->tok;

    if (get_op_prio (op) < prio)
      return res;

    /* Skip operator.  */
    scan (sctxt);

    r = parse_condition (sctxt, prio + 1);
    if (r == NULL)
      return NULL;
    res = build_binary (op, res, r);
  }
}

static struct wrenrx_cond_tree *
parse_condition_or_none (struct scan_ctxt *sctxt)
{
  if (sctxt->tok == TOK_COMMA || sctxt->tok == TOK_EOL)
    return NULL;

  return parse_condition (sctxt, 0);
}

struct wrenrx_cond_tree *
wrenrx_cond_parse_expr(const char *line)
{
  struct scan_ctxt sctxt;

  sctxt.line = line;
  sctxt.pos = 0;
  scan(&sctxt);

  return parse_condition_or_none(&sctxt);
}

struct wrenrx_cond_tree_event *
parse_cond(const char *line)
{
  struct scan_ctxt sctxt;
  uint32_t source_idx;
  uint32_t event;
  struct wrenrx_cond_tree *expr;
  struct wrenrx_cond_tree_event *res;

  sctxt.line = line;
  sctxt.pos = 0;

  source_idx = 0;
  expr = NULL;

  scan(&sctxt);
  if (sctxt.tok != TOK_INT) {
    parse_error (&sctxt, "event id expected");
    return NULL;
  }
  event = sctxt.val;

  /* Skip int. */
  scan(&sctxt);
  if (sctxt.tok != TOK_COMMA) {
    parse_error (&sctxt, "',' expected after event id");
    return NULL;
  }

  /* Skip comma. */
  scan(&sctxt);
  expr = parse_condition_or_none(&sctxt);

  if (sctxt.tok == TOK_COMMA) {
    /* Skip comma. */
    scan(&sctxt);

    if (sctxt.tok != TOK_INT) {
      parse_error (&sctxt, "source-idx expected");
      return NULL;
    }
    source_idx = sctxt.val;

    /* Skip int.  */
    scan(&sctxt);
  }

  if (sctxt.tok != TOK_EOL) {
    parse_error (&sctxt, "garbage after event condition");
    return NULL;
  };

  res = malloc (sizeof (struct wrenrx_cond_tree_event));
  res->source_idx = source_idx;
  res->ev_id = event;
  res->expr = expr;

  return res;
}

static void
disp_op(enum tok op)
{
  switch(op) {
  case TOK_EQ:
    printf ("==");
    break;
  case TOK_AND:
    printf ("&&");
    break;
  case TOK_OR:
    printf ("||");
    break;
  default:
    printf ("?%u?", op);
    break;
  }
}

void
wrenrx_cond_disp_tree (struct wrenrx_cond_tree *t)
{
  switch (t->kind) {
  case T_PARAM:
    printf ("p%u", t->u.param.id);
    disp_op(t->u.param.op);
    printf ("%u", t->u.param.val);
    break;
  case T_BINARY:
    printf ("(");
    wrenrx_cond_disp_tree (t->u.binary.l);
    disp_op(t->u.binary.op);
    wrenrx_cond_disp_tree (t->u.binary.r);
    printf (")");
    break;
  }
}

void
wrenrx_cond_disp_tree_event(struct wrenrx_cond_tree_event *t)
{
  printf ("source-idx: %u, event_id: %u\n", t->source_idx, t->ev_id);
  printf ("\n");
  printf ("  event cond  : ");
  if (t->expr == NULL)
    printf ("-");
  else
    wrenrx_cond_disp_tree (t->expr);
  printf ("\n");
}

static void
disp_cond_expr_cond(const struct wrenrx_cond_expr *expr)
{
    unsigned orig = wrenrx_cond_expr_cond_get_orig(expr);
    char c;

    if (orig == (WRENRX_PARAM_CONTEXT | WRENRX_PARAM_EVENT))
	c = 'a';
    else if (orig == WRENRX_PARAM_CONTEXT)
	c = 'c';
    else if (orig == WRENRX_PARAM_EVENT)
	c = 'e';
    else
	c = '?';
    printf("%c%u", c, wrenrx_cond_expr_cond_get_param(expr));

    switch (wrenrx_cond_expr_cond_get_op(expr)) {
    case WRENRX_EQ:
	printf("==");
	break;
    case WRENRX_NE:
	printf("!=");
	break;
    case WRENRX_SLT:
	printf("<");
	break;
    case WRENRX_SLE:
	printf("<=");
	break;
    case WRENRX_SGT:
	printf(">");
	break;
    case WRENRX_SGE:
	printf(">=");
	break;
    default:
	printf("??");
	break;
    }
    printf("%u", (unsigned)wrenrx_cond_expr_cond_get_value_u32(expr));
}


static void
disp_cond_expr_binary(const struct wrenrx_cond *cond,
		      const struct wrenrx_cond_expr *expr)
{
    enum wrenrx_cond_rel op = wrenrx_cond_expr_bin_get_op(expr);

    if (op == WRENRX_NOT)
	putchar('!');

    putchar('(');
    wrenrx_cond_expr_disp(cond, wrenrx_cond_expr_bin_get_left(cond, expr));
    putchar(')');

    if (op == WRENRX_NOT)
	return;

    switch (wrenrx_cond_expr_bin_get_op(expr)) {
    case WRENRX_AND:
	printf(" && ");
	break;
    case WRENRX_OR:
	printf(" || ");
	break;
    case WRENRX_NOT:
	abort();
    default:
	printf("??");
	break;
    }

    putchar('(');
    wrenrx_cond_expr_disp(cond, wrenrx_cond_expr_bin_get_right(cond, expr));
    putchar(')');
}

void
wrenrx_cond_expr_disp(const struct wrenrx_cond *cond,
		      const struct wrenrx_cond_expr *expr)
{
    switch (wrenrx_cond_expr_get_kind(expr)) {
    case WRENRX_COND:
	disp_cond_expr_cond(expr);
	break;
    case WRENRX_BINARY:
	disp_cond_expr_binary(cond, expr);
	break;
    }
}
