#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include "wren/wren-packet.h"
#include "wren-util.h"
#include "wren-drv.h"
#include "wrenrx-drv.h"
#include "wrenrx-tools.h"

static void
disp_packet_text(const struct wren_packet *pkt, unsigned flag_params)
{
  unsigned off;
  unsigned len;

  if (pkt->hdr.version != PKT_VERSION_v1) {
    printf ("# bad version: %02x\n", pkt->hdr.version);
    return;
  }

  len = pkt->hdr.len - sizeof (struct wren_packet_hdr_v1) / 4;

  for (off = 0; off < len; ) {
    struct wren_capsule_hdr *hdr =
	(struct wren_capsule_hdr *)&pkt->u.b[off * 4];

    switch(hdr->typ) {
    case PKT_CTXT:
      {
        const unsigned l = sizeof (struct wren_capsule_ctxt_hdr) / 4;
        struct wren_capsule_ctxt_hdr *ctxt =
	  (struct wren_capsule_ctxt_hdr *)&pkt->u.b[off * 4];

        printf ("ctxt %u\n", ctxt->ctxt_id);
	if (flag_params)
	    disp_params_text (&pkt->u.w[off + l], hdr->len - l);
        off += hdr->len;
      }
      break;
    case PKT_EVENT:
      {
        const unsigned l = sizeof (struct wren_capsule_event_hdr) / 4;
        struct wren_capsule_event_hdr *ev =
	  (struct wren_capsule_event_hdr *)&pkt->u.b[off * 4];

        printf ("event 0x%03x %u %u\n",
		ev->ev_id, ev->ctxt_id, ev->ts.nsec / 1000);
	if (flag_params)
	    disp_params_text (&pkt->u.w[off + l], hdr->len - l);
        off += hdr->len;
      }
      break;
    default:
      return; // TODO
      break;
    }
  }
  printf ("send\n");
}

int
main(int argc, char *argv[])
{
  void *init;
  struct wrenrx_handle *handle;
  struct wren_packet pkt;
  int flag_hex = 0;
  int flag_text = 0;
  int flag_verbose = 0;
  int c;

  init = wrenrx_drv_init(&argc, argv);
  if (init == NULL)
    return 1;

  while ((c = getopt(argc, argv, "xtv")) > 0)
    switch (c) {
    case 'x':
      flag_hex++;
      break;
    case 't':
      flag_text++;
      break;
    case 'v':
      flag_verbose++;
      break;
    default:
      printf ("usage: %s [-xtv]\n", progname);
      printf (" -x      hex dump\n");
      printf (" -x -x   full hex dump\n");
      printf (" -t      text (without params)\n");
      printf (" -t -t   text (with params)\n");
      return 1;
    }

  handle = wrenrx_drv_open(init);
  if (handle == NULL)
    return 1;

  if (wrenrx_drv_set_promisc(handle, 1) < 0) {
      fprintf(stderr, "cannot set promisc mode\n");
      return 1;
  }

  while (1) {
      struct wrenrx_msg *msg;
      enum wrenrx_msg_kind kind;
      unsigned len, dlen;

      msg = wrenrx_wait(handle);
      if (msg == NULL)
	  return 0;

      kind = wrenrx_get_msg (msg);
      if (kind != wrenrx_msg_packet)
	  continue;

      len = wrenrx_drv_msg_get_packet (msg, &pkt);
      if (len == 0)
	  break;
      if (len < sizeof (struct wren_packet_hdr_v1)) {
	  printf ("## truncated packet (len: %u)\n", len);
	  continue;
      }

      if (flag_hex > 1)
	  wren_dump_hex((unsigned char *)&pkt, len / 4);

      dlen = pkt.hdr.len * 4;
      if (len < dlen) {
	  printf ("## truncated body (len: %uB=0x%04xW, len in hdr: 0x%04xW)\n",
		  len, len / 4, pkt.hdr.len);
	  continue;
      }

      printf ("## Receive (%uB=%uW)", len, len / 4);

      if (flag_verbose) {
	  unsigned ns, us, ms;

	  printf(" on %u", pkt.hdr.snd_ts >> 22);

	  ns = (pkt.hdr.snd_ts & ((1 << 22) - 1)) << 8;
	  ms = ns / 1000000;
	  ns -= ms * 1000000;

	  us = ns / 1000;
	  ns -= us * 1000;

	  printf(" + %ums %uus %uns", ms, us, ns);
      }
      printf (":\n");

      if (flag_hex == 1)
	  wren_dump_packet_hex (&pkt);

      if (flag_text)
	  disp_packet_text (&pkt, flag_text > 1);
      else
	  wren_dump_packet(&pkt, len);
  }

  wrenrx_close(handle);
  return 0;
}
