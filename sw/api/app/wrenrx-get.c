#define CONFIG_READLINE

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <ctype.h>
#include <sys/poll.h>
#ifdef CONFIG_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif
#include "wren/wrenrx.h"
#include "wren/wren-hw.h"
#include "wrenrx-cond-parser.h"
#include "wrenrx-tools.h"
#include "wren-drv.h"
#include "wrenrx-drv.h"

static struct wrenrx_handle *handle;

static int flag_timestamp;

/* If true, disp message from wren.  Can be confusing if the user is
   also typing */
static unsigned flag_msg = 1;

static void
print_parameters (struct wrenrx_msg_param *it)
{
  while (wrenrx_msg_param_is_valid(it)) {
    wren_param_id id;
    unsigned dt;

    id = wrenrx_msg_param_get_id (it);
    dt = wrenrx_msg_param_get_dt (it);
    switch (dt) {
    case PKT_PARAM_S32:
      printf (" %u s32 %d\n", id, wrenrx_msg_param_get_s32(it));
      break;
    case PKT_PARAM_S64:
      printf (" %u s64 %lld\n",
	      id, (long long)wrenrx_msg_param_get_s64(it));
      break;
    default:
      printf (" %u %u ??\n", id, dt);
      break;
    }
    wrenrx_msg_param_next(it);
  }
}

static void
disp_time (unsigned long long sec, unsigned nsec)
{
    struct tm tm;
    time_t t;
    char atime[32];
    unsigned ns, us, ms;

    ns = nsec;
    ms = ns / 1000000;
    ns -= ms * 1000000;

    us = ns / 1000;
    ns -= us * 1000;

    t = sec;
    if (gmtime_r (&t, &tm) == &tm)
	strftime (atime, sizeof(atime), "%D %T", &tm);
    else
	strcpy(atime, "(error)");

    printf("%llu (%s) + %3ums %3uus %3uns",
	   sec, atime, ms, us, ns);
    if (0)
	printf(" [%x:%08x]", (unsigned)sec, nsec);
}

static void
print_event(struct wrenrx_msg *msg)
{
  struct wrenrx_msg_param it;
  struct wren_ts ts;

  wrenrx_msg_get_event_ts(msg, &ts);

  printf ("event src-idx: %u, id: %u, ctxt: %u, evsub: %u, ts: ",
	  wrenrx_msg_get_source_idx(msg),
	  wrenrx_msg_get_event_id(msg),
	  wrenrx_msg_get_context_id(msg),
	  wrenrx_msg_get_event_evsub_id(msg));
  disp_time(ts.sec, ts.nsec);
  printf ("\n");

  wrenrx_msg_get_event_params(msg, &it);
  printf("event parameters:\n");
  print_parameters (&it);
  wrenrx_msg_get_context_params(msg, &it);
  printf("context parameters:\n");
  print_parameters (&it);
}

static void
print_context(struct wrenrx_msg *msg)
{
  struct wrenrx_msg_param it;
  struct wren_ts ts;

  wrenrx_msg_get_context_ts(msg, &ts);

  printf ("> context src-idx: %u, id: %u, ts: %us+%uns (param len: %u)\n",
	  wrenrx_msg_get_source_idx(msg),
	  wrenrx_msg_get_context_id(msg),
	  ts.sec, ts.nsec,
	  wrenrx_msg_get_params_length(msg));

  wrenrx_msg_get_context_params(msg, &it);
  printf("context parameters:\n");
  print_parameters (&it);
}

static void
print_pulse(struct wrenrx_msg *msg)
{
  struct wrenrx_msg_param it;
  struct wren_ts ts;
  unsigned ev_id;

  wrenrx_msg_get_pulse_ts(msg, &ts);
  ev_id = wrenrx_msg_get_event_id(msg);

  printf ("> pulse for config %u, ctxt %u, ev %u, ts: %us+%09uns\n",
	  wrenrx_msg_get_config_id (msg),
	  wrenrx_msg_get_context_id (msg),
	  ev_id, ts.sec, ts.nsec);
  if (ev_id == WREN_EVENT_ID_INVALID)
    return;
  wrenrx_msg_get_event_params(msg, &it);
  printf("event parameters:\n");
  print_parameters (&it);
}

static int
handle_wren(struct wrenrx_handle *handle)
{
    struct wrenrx_msg *msg;
    enum wrenrx_msg_kind kind;
    struct wren_ts now;

    msg = wrenrx_wait(handle);
    if (msg == NULL)
	return 0;

    if (!flag_msg)
	return 1;

    kind = wrenrx_get_msg (msg);
    if (kind == wrenrx_msg_none)
	return 1;

    if (flag_timestamp) {
	if (wrenrx_get_time(handle, &now) == 0) {
	    printf ("At ");
	    disp_time (now.sec, now.nsec);
	    printf("\n");
	}
	else
	    printf ("wren: cannot get time!\n");
    }

    switch (kind) {
    case wrenrx_msg_network_error:
	printf ("> network_error\n");
	break;
    case wrenrx_msg_pulse:
	print_pulse(msg);
	break;
    case wrenrx_msg_event:
	print_event(msg);
	break;
    case wrenrx_msg_context:
	print_context(msg);
	break;
    case wrenrx_msg_none:
	printf ("> none\n");
	break;
    case wrenrx_msg_packet:
	printf ("> packet\n");
	break;
    }
    return 1;
}

static int
do_version (struct wrenrx_handle *handle, char *argv[], int argc)
{
    char buf[512];

    if (wrenrx_get_version(handle, buf, sizeof(buf)) < 0) {
	printf ("error\n");
	return 1;
    }
    printf("%s", buf);
    return 1;
}

static int
do_link (struct wrenrx_handle *handle, char *argv[], int argc)
{
    printf("link: %d\n", wrenrx_get_link(handle));
    return 1;
}

static int
do_sync (struct wrenrx_handle *handle, char *argv[], int argc)
{
    printf("sync: %d\n", wrenrx_get_sync(handle));
    return 1;
}

static int
do_now (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wren_ts now;
    if (wrenrx_get_time(handle, &now) == 0) {
	disp_time (now.sec, now.nsec);
	printf("\n");
    }
    else
	printf("cannot get time\n");

    return 1;
}

static int
do_panel_config (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wren_panel_config cfg;
    int res;

    res = wrenrx_get_panel_config(handle, &cfg);

    if (res < 0) {
	printf("error %d\n", res);
	return 1;
    }

    printf("fp pin: %u\n", cfg.fp_pin);
    printf("fp fixed inputs: %u\n", cfg.fp_fixed_inp);
    printf("pp pin: %u\n", cfg.pp_pin);
    printf("vme p2a: 0x%x\n", cfg.vme_p2a_be);
    return 1;
}

static int
do_disp_one_source(struct wrenrx_handle *handle, unsigned idx)
{
    struct wren_protocol proto;
    int res;

    res = wrenrx_get_source(handle, idx, &proto);
    if (res != 0) {
	if (res != WRENRX_ERR_SRCID_TOO_LARGE)
	    printf ("do_disp_source error: %d\n", res);
	return 0;
    }
    printf ("%u: ", idx);
    disp_protocol (&proto);
    return 1;
}


static int
do_disp_source (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for source idx (%s)\n", argv[1]);
	    return -1;
	}
	do_disp_one_source(handle, idx);
	return 2;
    } else if (argc == 1) {
	for (idx = 0; ; idx++)
	    if (do_disp_one_source(handle, idx) <= 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: disp-source [IDX]\n");
	return -1;
    }
}

static int
do_add_source (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wren_protocol proto;
    int res;

    if (argc != 2) {
	fprintf(stderr, "usage: add-source SRC-ID\n");
	return -1;
    }

    if (parse_protocol(argv + 1, argc - 1, &proto) < 0)
	return -1;

    res = wrenrx_add_source(handle, &proto);
    if (res < 0)
	printf("error: %d\n", res);
    else
	printf("source id: %d\n", res);

    return 1;
}

static int
do_del_source (struct wrenrx_handle *handle, char *argv[], int argc)
{
    uint32_t idx;
    char *e;
    int res;

    if (argc != 2) {
	fprintf(stderr, "usage: del-source SRC-IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_remove_source(handle, idx);
    if (res != 0)
	printf("error: %d\n", res);

    return 2;
}

static int
do_disp_one_config(struct wrenrx_handle *handle, unsigned idx)
{
    struct wrenrx_pulser_configuration conf;
    struct wrenrx_cond *cond;
    const struct wrenrx_cond_expr *expr;
    char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
    unsigned pulser_num;
    int res;

    cond = wrenrx_cond_alloc(handle);
    res = wrenrx_pulser_get_config(handle, idx, &pulser_num, &conf, cond, name);
    if (res != 0) {
	if (res == WRENRX_ERR_CFGID_TOO_LARGE)
	    return -2;
	if (res == WRENRX_ERR_NO_CONFIG)
	    return 0;
	printf ("do_disp_one_config(%u) error: %d\n", idx, res);
	return -1;
    }
    printf ("%u: %s\n", idx, name);
    printf ("  src-idx: %u, event-id: %u\n",
	    wrenrx_cond_get_source_idx (cond),
	    wrenrx_cond_get_event_id (cond));
    expr = wrenrx_cond_get_expr(cond);
    if (expr != NULL) {
	printf ("  cond: ");
	wrenrx_cond_expr_disp(cond, expr);
	printf ("\n");
    }
    wrenrx_cond_free(cond);
    printf ("  pulser:%u en:%u int:%u out:%u repeat:%u",
	    pulser_num, conf.config_en, conf.interrupt_en, conf.output_en,
	    conf.repeat_mode);
    printf (" start:%s stop:%s clock:%s\n",
	    get_input_name(conf.start),
	    get_input_name(conf.stop),
	    get_clock_name(conf.clock));
    printf ("  init-delay:%u pulse-width:%u pulse-period:%u npulses:%u\n",
	    conf.initial_delay, conf.pulse_width, conf.pulse_period,
	    conf.npulses);
    printf ("  load-offset:%dsec+%dnsec\n",
	    (int)conf.load_offset_sec, (int)conf.load_offset_nsec);
    return 1;
}

static int
do_disp_config (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for config idx (%s)\n", argv[1]);
	    return -1;
	}
	do_disp_one_config(handle, idx);
	return 2;
    } else if (argc == 1) {
	for (idx = 0; ; idx++)
	    if (do_disp_one_config(handle, idx) < 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: disp-config [IDX]\n");
	return -1;
    }
}

static void
init_pulser_config (struct wrenrx_pulser_configuration *conf)
{
    conf->config_en = 1;
    conf->interrupt_en = 1;
    conf->output_en = 1;
    conf->repeat_mode = 0;
    conf->start = WRENRX_INPUT_NOSTART;
    conf->stop  = WRENRX_INPUT_NOSTOP;
    conf->clock = WRENRX_INPUT_CLOCK_1GHZ;
    conf->initial_delay = 0;
    conf->pulse_width = 8;
    conf->pulse_period = 125;
    conf->npulses = 1;
    conf->load_offset_sec = 0;
    conf->load_offset_nsec = 0;
}

static int
parse_pulser_clock(const char *opt, const char *arg, unsigned *cfg)
{
    int sig = parse_input_name(arg);
    if (sig < 0) {
	fprintf (stderr, "incorrect value for option %s (%s)\n", opt, arg);
	return -1;
    }
    *cfg = sig;
    return 0;
}

static int
parse_pulser_config(struct wrenrx_pulser_configuration *conf,
		    const char *opt, const char *arg)
{
    long val;
    char *e;

    if (opt[0] != '-') {
	fprintf(stderr, "incorrect option %s\n", opt);
	return -1;
    }

    if (arg == NULL) {
	fprintf(stderr, "missing value for option %s\n", opt);
	return -1;
    }

    if (!strcmp(opt, "-start"))
	return parse_pulser_clock (opt, arg, &conf->start);
    else if (!strcmp(opt, "-stop"))
	return parse_pulser_clock (opt, arg, &conf->stop);
    else if (!strcmp(opt, "-clock"))
	return parse_pulser_clock (opt, arg, &conf->clock);

    val = strtol(arg, &e, 0);
    if (*e == 'M') {
	val *= 1000000;
	e++;
    }
    else if (*e == 'K') {
	val *= 1000;
	e++;
    }
    if (*e != 0) {
	fprintf(stderr, "bad value option %s (%s)\n", opt, arg);
	return -1;
    }

    if (!strcmp(opt, "-width"))
	conf->pulse_width = val;
    else if (!strcmp(opt, "-period"))
	conf->pulse_period = val;
    else if (!strcmp(opt, "-idelay"))
	conf->initial_delay = val;
    else if (!strcmp(opt, "-int"))
	conf->interrupt_en = val;
    else if (!strcmp(opt, "-out"))
	conf->output_en = val;
    else if (!strcmp(opt, "-en"))
	conf->config_en = val;
    else if (!strcmp(opt, "-n"))
	conf->npulses = val;
    else if (!strcmp(opt, "-r"))
	conf->repeat_mode = val;
    else if (!strcmp(opt, "-sec"))
	conf->load_offset_sec = val;
    else if (!strcmp(opt, "-ns"))
	conf->load_offset_nsec = val;
    else {
	fprintf(stderr, "unknown option %s\n", opt);
	return -1;
    }

    return 0;
}

static void
help_pulser_config(const struct wrenrx_pulser_configuration *cfg)
{
    fprintf (stderr,
	     " -start VAL   start input [%s]\n", get_input_name(cfg->start));
    fprintf (stderr,
	     " -stop VAL    stop input [%s]\n", get_input_name(cfg->stop));
    fprintf (stderr,
	     " -clock VAL   clock input [%s]\n", get_input_name(cfg->clock));
    fprintf (stderr,
	     " -width VAL   pulse width [%u]\n", cfg->pulse_width);
    fprintf (stderr,
	     " -period VAL  pulse period [%u] (0: level)\n",
	     cfg->pulse_period);
    fprintf (stderr,
	     " -idelay VAL  initial delay [%u]\n", cfg->initial_delay);
    fprintf (stderr,
	     " -int VAL     interrupt enable [%u]\n", cfg->interrupt_en);
    fprintf (stderr,
	     " -out VAL     output enable [%u]\n", cfg->output_en);
    fprintf (stderr,
	     " -en VAL      config enable [%u]\n", cfg->config_en);
    fprintf (stderr,
	     " -n VAL       number of pulses [%u] (0: infinite)\n",
	     cfg->npulses);
    fprintf (stderr,
	     " -r VAL       repeat mode [%u]\n", cfg->repeat_mode);
    fprintf (stderr,
	     " -sec VAL     second part of load time offset\n");
    fprintf (stderr,
	     " -ns VAL      nsecond part of load time offset (def: 0)\n");
}

static int
do_pulser_define (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_configuration conf;
    unsigned pulser_num;
    unsigned source_idx;
    wren_event_id ev_id;
    char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
    int res;
    char *e;
    struct wrenrx_cond *cond;
    struct wrenrx_cond_expr *expr;
    unsigned narg;

    init_pulser_config (&conf);

    if (argc < 4) {
	fprintf(stderr, "usage: pulser-define PULSER-NUM SRC-IDX EVT-ID "
		"[-name NAME] [CONFIG] [COND]\n");
	help_pulser_config (&conf);
	return -1;
    }

    pulser_num = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser IDX (%s)\n", argv[1]);
	return -1;
    }

    source_idx = strtoul(argv[2], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for SRC-IDX (%s)\n", argv[2]);
	return -1;
    }

    ev_id = strtoul(argv[3], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for EVT-ID (%s)\n", argv[3]);
	return -1;
    }

    narg = 4;

    if (narg + 1 < argc && !strcmp(argv[narg], "-name")) {
	strncpy (name, argv[narg + 1], sizeof(name));
	narg += 2;
    }
    else
	memset(name, 0, sizeof(name));

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    cond = wrenrx_cond_alloc (handle);

    if (narg < argc) {
	struct wrenrx_cond_tree *etree = wrenrx_cond_parse_expr(argv[narg]);
	if (etree == NULL)
	    return -1;

	expr = wrenrx_cond_tree_to_cond_expr(cond, etree);

	narg++;
    }
    else
	expr = NULL;

    res = wrenrx_cond_define(cond, source_idx, ev_id, expr);
    if (res < 0)
	return res;

    res = wrenrx_pulser_define(handle, pulser_num, cond, &conf, name);

    wrenrx_cond_free(cond);

    if (res < 0)
	printf("error: %d\n", res);
    else
	printf("config-idx: %d\n", res);

    return 1;
}

static int
do_pulser_program (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_configuration conf;
    unsigned pulser_num;
    char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
    int res;
    char *e;
    unsigned narg;
    struct wren_ts abstime;
    struct wren_ts *ptime;

    init_pulser_config (&conf);

    conf.interrupt_en = 0;

    if (argc < 2) {
	fprintf(stderr, "usage: pulser-program PULSER-NUM "
		"[-name NAME] [CONFIG]\n");
	help_pulser_config (&conf);
	return -1;
    }

    pulser_num = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser IDX (%s)\n", argv[1]);
	return -1;
    }

    narg = 2;

    if (narg + 1 < argc && !strcmp(argv[narg], "-name")) {
	strncpy (name, argv[narg + 1], sizeof(name));
	narg += 2;
    }
    else
	memset(name, 0, sizeof(name));

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    if (narg < argc) {
	printf("unhandled option '%s' (no argument)\n", argv[narg]);
	return -1;
    }

    if (conf.load_offset_sec != 0 || conf.load_offset_nsec != 0) {
	wrenrx_get_time(handle, &abstime);
	wren_add_nsec(&abstime, conf.load_offset_nsec);
	abstime.sec += conf.load_offset_sec;

	ptime = &abstime;
    }
    else {
	ptime = NULL;
    }
    res = wrenrx_pulser_program(handle, pulser_num, &conf, ptime, name);

    if (res == WRENRX_ERR_SYSTEM)
	printf("system error: %m\n");
    else if (res < 0)
	printf("error: %d\n", res);
    else
	printf("config-idx: %d\n", res);

    return 1;
}

static int
do_pulser_reconfigure (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_configuration conf;
    unsigned config_idx;
    int res;
    char *e;
    unsigned narg;
    char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
    unsigned pulser_num;

    init_pulser_config (&conf);

    /* Default: 1 sec */
    conf.load_offset_sec = 1;

    if (argc < 2) {
	fprintf(stderr, "usage: pulser-reconfigure CONFIG-IDX [CONFIG]\n");
	help_pulser_config (&conf);
	return -1;
    }

    config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    /* Read current config */
    res = wrenrx_pulser_get_config(handle, config_idx,
				   &pulser_num, &conf, NULL, name);
    if (res != 0) {
	printf ("wrenrx_pulser_get_config error: %d\n", res);
	return -1;
    }

    narg = 2;

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    res = wrenrx_pulser_reconfigure(handle, config_idx, &conf);

    if (res < 0)
	printf("error: %d\n", res);

    return 1;
}

static int
do_pulser_force (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_configuration conf;
    unsigned config_idx;
    int res;
    char *e;
    unsigned narg;

    init_pulser_config (&conf);

    /* Default: 1 sec */
    conf.load_offset_sec = 1;

    if (argc < 2) {
	fprintf(stderr, "usage: pulser-force CONFIG-IDX [CONFIG]\n");
	help_pulser_config (&conf);
	return -1;
    }

    config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    narg = 2;

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    res = wrenrx_pulser_force(handle, config_idx, NULL, &conf);

    if (res < 0)
	printf("error: %d\n", res);

    return 1;
}

static int
do_pulser_remove (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned config_idx;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: pulser-remove CONFIG-IDX\n");
	return -1;
    }

    config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_pulser_remove(handle, config_idx);
    if (res < 0)
	printf("error: %d\n", res);
    return 0;
}

static int
do_subscribe_unsubscribe_config (struct wrenrx_handle *handle,
				 unsigned on_off,
				 char *argv[], int argc)
{
    unsigned config_idx;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: subscribe-config CONFIG-IDX\n");
	return -1;
    }

    config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    if (on_off)
	res = wrenrx_config_subscribe(handle, config_idx);
    else
	res = wrenrx_config_unsubscribe(handle, config_idx);
    if (res < 0)
	printf("error: %d\n", res);
    return 0;
}

static int
do_config_subscribe (struct wrenrx_handle *handle,
		     char *argv[], int argc)
{
    return do_subscribe_unsubscribe_config(handle, 1, argv, argc);
}

static int
do_config_unsubscribe (struct wrenrx_handle *handle,
		       char *argv[], int argc)
{
    return do_subscribe_unsubscribe_config(handle, 0, argv, argc);
}

static int
do_context_subscribe (struct wrenrx_handle *handle,
		      char *argv[], int argc)
{
    unsigned source_idx;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: subscribe-context SOURCE-IDX\n");
	return -1;
    }

    source_idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for source IDX (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_context_subscribe(handle, source_idx);
    if (res < 0)
	printf("error: %d\n", res);
    return 0;
}

static int
do_context_unsubscribe (struct wrenrx_handle *handle,
			char *argv[], int argc)
{
    unsigned source_idx;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: unsubscribe-context SOURCE-IDX\n");
	return -1;
    }

    source_idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for source IDX (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_context_unsubscribe(handle, source_idx);
    if (res < 0)
	printf("error: %d\n", res);
    return 0;
}

static int
do_event_subscribe (struct wrenrx_handle *handle,
		      char *argv[], int argc)
{
    unsigned source_idx;
    unsigned event_id;
    int offset;
    int res;
    char *e;

    if (argc != 3 && argc != 4) {
	fprintf(stderr,
		"usage: subscribe-event SOURCE-IDX EVENT-ID [OFFSET]\n");
	return -1;
    }

    source_idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for source IDX (%s)\n", argv[1]);
	return -1;
    }

    event_id = strtoul(argv[2], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for event id (%s)\n", argv[2]);
	return -1;
    }

    if (argc == 4) {
	offset = strtol(argv[3], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for offset (%s)\n", argv[3]);
	    return -1;
	}
    }
    else
	offset = 0;

    res = wrenrx_event_subscribe(handle, source_idx, event_id, offset);
    if (res < 0)
	printf("error: %d\n", res);
    else
	printf ("evsub: %d\n", res);
    return 0;
}

static int
do_event_unsubscribe (struct wrenrx_handle *handle,
		      char *argv[], int argc)
{
    unsigned evsub_idx;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr,
		"usage: unsubscribe-event EVSUB-IDX\n");
	return -1;
    }

    evsub_idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for evsub IDX (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_event_unsubscribe(handle, evsub_idx);
    if (res < 0)
	printf("error: %d (error: %m)\n", res);
    return 0;
}

static int
do_disp_one_output(struct wrenrx_handle *handle, unsigned num)
{
    struct wrenrx_output_configuration out;
    int res;

    res = wrenrx_output_get_config(handle, num, &out);
    if (res != 0) {
	printf ("do_disp_one_output error: %d\n", res);
	return -1;
    }
    printf ("output %2u: "
	    "invert_pulsers: %u, or-mask: 0x%02x, invert_result: %u\n",
	    num, out.invert_pulsers, out.mask, out.invert_result);
    return 0;
}

static int
do_disp_output (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned num;

    if (argc == 2) {
	char *e;
	num = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for output num (%s)\n", argv[1]);
	    return -1;
	}
	do_disp_one_output(handle, num);
	return 2;
    } else if (argc == 1) {
	for (num = 1; ; num++)
	    if (do_disp_one_output(handle, num) < 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: disp-output [NUM]\n");
	return -1;
    }
}

static int
do_set_output (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned num;
    struct wrenrx_output_configuration cfg;
    int res;
    char *e;

    if (argc != 5) {
	fprintf(stderr,
		"usage: set-output NUM INV-PULSER OR-MASK INV-RESULT\n");
	return -1;
    }

    num = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for output num (%s)\n", argv[1]);
	return -1;
    }
    cfg.invert_pulsers = strtoul(argv[2], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for inv-pulser (%s)\n", argv[2]);
	return -1;
    }
    cfg.mask = strtoul(argv[3], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for mask (%s)\n", argv[3]);
	return -1;
    }
    cfg.invert_result = strtoul(argv[4], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for inv-result (%s)\n", argv[4]);
	return -1;
    }
    res = wrenrx_output_set_config(handle, num, &cfg);
    if (res < 0)
	fprintf(stderr, "error: %d\n", res);
    return 5;
}

static int
do_disp_one_pin(struct wrenrx_handle *handle, unsigned num)
{
    struct wrenrx_pin_configuration pin;
    int res;

    res = wrenrx_pin_get_config(handle, num, &pin);
    if (res != 0) {
	printf ("do_disp_one_pin error: %d\n", res);
	return -1;
    }
    printf ("pin %2u: out_en: %u, term_en: %u, inv_inp: %u\n",
	    num, pin.out_en, pin.term_en, pin.inv_inp);
    return 0;
}

static int
do_disp_pin (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned num;

    if (argc == 2) {
	char *e;
	num = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for pin num (%s)\n", argv[1]);
	    return -1;
	}
	do_disp_one_pin(handle, num);
	return 2;
    } else if (argc == 1) {
	for (num = 1; ; num++)
	    if (do_disp_one_pin(handle, num) < 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: disp-pin [NUM]\n");
	return -1;
    }
}

static int
do_set_pin (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned num;
    struct wrenrx_pin_configuration cfg;
    int res;
    char *e;

    if (argc != 5) {
	fprintf(stderr,
		"usage: set-pin NUM OUT-EN TERM-EN INV-INP\n");
	return -1;
    }

    num = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for pin num (%s)\n", argv[1]);
	return -1;
    }
    cfg.out_en = strtoul(argv[2], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for out-en (%s)\n", argv[2]);
	return -1;
    }
    cfg.term_en = strtoul(argv[3], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for term_en (%s)\n", argv[3]);
	return -1;
    }
    cfg.inv_inp = strtoul(argv[4], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for inv-inp (%s)\n", argv[4]);
	return -1;
    }
    res = wrenrx_pin_set_config(handle, num, &cfg);
    if (res < 0)
	fprintf(stderr, "error: %d\n", res);
    return 5;
}

static int
do_pulser_log (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_log *buf;
    unsigned len;
    unsigned pulser_num;
    int res;
    char *e;
    unsigned i;
    struct wren_ts now;

    if (argc < 2 || argc > 3) {
	fprintf(stderr,
		"usage: pulser-num NUM [LEN]\n");
	return -1;
    }

    pulser_num = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for num (%s)\n", argv[1]);
	return -1;
    }
    if (argc > 2) {
	len = strtoul(argv[2], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for len (%s)\n", argv[2]);
	    return -1;
	}
    }
    else
	len = 10;

    buf = (struct wrenrx_pulser_log *)malloc(len * sizeof(*buf));
    if (buf == NULL) {
	fprintf(stderr, "cannot allocate memory\n");
	return -1;
    }

    if (wrenrx_get_time(handle, &now) == 0) {
	printf("Now is ");
	disp_time (now.sec, now.nsec);
	printf("\n");
    }

    res = wrenrx_pulser_log_get(handle, pulser_num, buf, len);
    if (res < 0) {
	fprintf(stderr, "error %d\n", res);
	return -1;
    }

    for (i = 0; i < res; i++) {
	unsigned fl = buf[i].flags;
	printf("%3u ", i);
	if (1)
	    disp_time(buf[i].ts.sec, buf[i].ts.nsec);
	else
	    printf("%02usec + %07u usec",
		   buf[i].ts.sec % 60,
		   buf[i].ts.nsec / 1000);
	printf(": %s %s %s %s\n",
	       fl & WRENRX_PULSER_LOG_FLAG_LOAD  ? "Load"  : "    ",
	       fl & WRENRX_PULSER_LOG_FLAG_START ? "Start" : "     ",
	       fl & WRENRX_PULSER_LOG_FLAG_PULSE ? "Pulse" : "     ",
	       fl & WRENRX_PULSER_LOG_FLAG_IDLE  ? "Idle"  : "    ");
    }

    return argc;
}

static int
do_pin_log (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pin_log *buf;
    unsigned len;
    unsigned pin_num;
    int res;
    char *e;
    unsigned i;
    struct wren_ts now;

    if (argc < 2 || argc > 3) {
	fprintf(stderr,
		"usage: pin-log NUM [LEN]\n");
	return -1;
    }

    pin_num = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for num (%s)\n", argv[1]);
	return -1;
    }
    if (argc > 2) {
	len = strtoul(argv[2], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for len (%s)\n", argv[2]);
	    return -1;
	}
    }
    else
	len = 10;

    buf = (struct wrenrx_pin_log *)malloc(len * sizeof(*buf));
    if (buf == NULL) {
	fprintf(stderr, "cannot allocate memory\n");
	return -1;
    }

    if (wrenrx_get_time(handle, &now) == 0) {
	printf("Now is ");
	disp_time (now.sec, now.nsec);
	printf("\n");
    }

    res = wrenrx_pin_log_get(handle, pin_num, buf, len);
    if (res < 0) {
	fprintf(stderr, "error %d\n", res);
	return -1;
    }

    for (i = 0; i < res; i++) {
	unsigned fl = buf[i].flags;
	printf("%3u ", i);
	if (1)
	    disp_time(buf[i].ts.sec, buf[i].ts.nsec);
	else
	    printf("%02usec + %07u usec",
		   buf[i].ts.sec % 60,
		   buf[i].ts.nsec / 1000);
	printf(": %s %s\n",
	       fl & WRENRX_PIN_LOG_FLAG_PEDGE ? "Raise" : "     ",
	       fl & WRENRX_PIN_LOG_FLAG_NEDGE ? "Fall " : "     ");
    }

    return argc;
}

static int
do_sw_log (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_sw_log *buf;
    unsigned len;
    int res;
    char *e;
    unsigned i;
    unsigned verbose = 0;
    unsigned argn = 1;

    if (argc < 1 || argc > 3) {
	fprintf(stderr,
		"usage: sw-log [-v] [LEN]\n");
	return -1;
    }

    if (argn < argc && strcmp(argv[argn], "-v") == 0) {
	verbose++;
	argn++;
    }
    if (argn < argc) {
	len = strtoul(argv[argn], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for len (%s)\n", argv[1]);
	    return -1;
	}
	argn++;
    }
    else
	len = 20;

    buf = wrenrx_sw_log_allocate(len);
    if (buf == NULL) {
	fprintf(stderr, "cannot allocate memory for %u entries\n", len);
	return -1;
    }

    res = wrenrx_sw_log_get(handle, buf);
    if (res < 0) {
	fprintf(stderr, "wrenrx_sw_log_get: error %d\n", res);
	wrenrx_sw_log_free(buf);
	return -1;
    }
    len = wrenrx_sw_log_get_length(buf);
    for (i = 0; i < len; i++) {
	struct wrenrx_sw_log_entry ent;
	unsigned typ;

	printf("[%6u] ", i);
	res = wrenrx_sw_log_get_next_entry(buf, &ent);
	if (res < 0) {
	    printf("error %d\n", res);
	    continue;
	}
	typ = wrenrx_sw_log_get_type(&ent);
	switch(typ) {
	case WRENRX_SW_LOG_TYPE_UNKNOWN:
	    printf ("unknown\n");
	    break;
	case WRENRX_SW_LOG_TYPE_PACKET: {
	    struct wren_ts ts;
	    printf("packet src: %u, ts: ",
		   wrenrx_sw_log_packet_get_source_idx(&ent));
	    wrenrx_sw_log_get_time(&ent, &ts);
	    disp_time(ts.sec, ts.nsec);
	    printf("\n");
	    break;
	}
	case WRENRX_SW_LOG_TYPE_EVENT: {
	    struct wren_ts ts;
	    struct wrenrx_msg_param params;
	    struct wrenrx_msg *msg = wrenrx_sw_log_get_msg(&ent);
	    printf("event %u, src: %u, ctxt: %u, ts: ",
		   wrenrx_msg_get_event_id(msg),
		   wrenrx_msg_get_source_idx(msg),
		   wrenrx_msg_get_context_id(msg));
	    wrenrx_msg_get_event_ts(msg, &ts);
	    disp_time(ts.sec, ts.nsec);
	    printf("\n");
	    if (verbose) {
		wrenrx_msg_get_event_params(msg, &params);
		print_parameters(&params);
	    }
	    wrenrx_sw_log_free_msg(msg);
	    break;
	}
	case WRENRX_SW_LOG_TYPE_CONTEXT: {
	    struct wren_ts ts;
	    struct wrenrx_msg_param params;
	    struct wrenrx_msg *msg = wrenrx_sw_log_get_msg(&ent);
	    printf("context id: %u, src: %u, ts: ",
		   wrenrx_msg_get_context_id(msg),
		   wrenrx_msg_get_source_idx(msg));
	    wrenrx_msg_get_context_ts(msg, &ts);
	    disp_time(ts.sec, ts.nsec);
	    printf("\n");
	    if (verbose) {
		wrenrx_msg_get_context_params(msg, &params);
		print_parameters(&params);
	    }
	    wrenrx_sw_log_free_msg(msg);
	    break;
	}
	case WRENRX_SW_LOG_TYPE_CONDITION: {
	    printf("condition\n");
	    break;
	}
	default:
	    printf ("unhandled (%u)\n", typ);
	    break;
	}
    }

    wrenrx_sw_log_free(buf);
    return argn;
}


static int
do_vme_p2a (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned val;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: vme-p2a VAL\n");
	return -1;
    }

    val = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for val (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_hw_vme_p2a_set_output(handle, val);
    if (res < 0) {
	fprintf(stderr, "wrenrx_hw_vme_p2a_set_output: error %d\n", res);
	return -1;
    }

    return 2;
}

static int
do_patchpanel (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned val;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: patchpanel 0|1\n");
	return -1;
    }

    val = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for val (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_hw_patchpanel_set_config(handle, val);
    if (res < 0) {
	fprintf(stderr, "wrenrx_hw_patchpanel_set_config: error %d\n", res);
	return -1;
    }

    return 2;
}

static int
do_off (struct wrenrx_handle *handle, char *argv[], int argc)
{
    flag_msg = 0;
    return 1;
}

static int
do_on (struct wrenrx_handle *handle, char *argv[], int argc)
{
    flag_msg = 1;
    return 1;
}

static int
do_timestamp_enable (struct wrenrx_handle *handle, char *argv[], int argc)
{
    flag_timestamp = 1;
    return 1;
}

static int
do_timestamp_disable (struct wrenrx_handle *handle, char *argv[], int argc)
{
    flag_timestamp = 0;
    return 1;
}


static int do_help(struct wrenrx_handle *handle, char *argv[], int argc);

struct command_t {
  const char *name;
  /* Return the number of args consumed, argv[0] is the command name.  */
  int (*func)(struct wrenrx_handle *handle, char *argv[], int argc);
  const char *help;
};

static const struct command_t commands[] = {
  {"help", do_help, "print this help" },
  {"version", do_version, "print versions" },
  {"link", do_link, "print link status" },
  {"sync", do_sync, "print sync status" },
  {"now", do_now, "print current time" },
  {"panel-config", do_panel_config, "print panel config" },
  {"on", do_on, "enable wren messages" },
  {"off", do_off, "disable wren messages" },

  {"disp-source", do_disp_source, "disp one or all source" },
  {"add-source", do_add_source, "add a new rx source" },
  {"del-source", do_del_source, "delete an rx source" },

  {"disp-config", do_disp_config, "print rx config" },
  {"disp-output", do_disp_output, "print output config" },
  {"disp-pin", do_disp_pin, "print pin config" },

  {"pulser-define", do_pulser_define, "define a pulser config" },
  {"pulser-program", do_pulser_program, "program a pulser" },
  {"pulser-remove", do_pulser_remove, "remove a config" },
  {"pulser-reconfigure", do_pulser_reconfigure, "modify a config" },
  {"pulser-force", do_pulser_force, "force a pulser" },
  {"config-subscribe", do_config_subscribe, "subscribe to a config" },
  {"config-unsubscribe", do_config_unsubscribe, "unsubscribe to a config" },
  {"context-subscribe", do_context_subscribe, "subscribe to a context" },
  {"context-unsubscribe", do_context_unsubscribe, "unsubscribe to a context" },
  {"event-subscribe", do_event_subscribe, "subscribe to an event" },
  {"event-unsubscribe", do_event_unsubscribe, "unsubscribe to an event" },

  {"set-output", do_set_output, "configure an output" },
  {"set-pin", do_set_pin, "configure a pin" },

  {"pulser-log", do_pulser_log, "disp logs of a pulser" },
  {"pin-log", do_pin_log, "disp logs of a pin" },
  {"sw-log", do_sw_log, "disp software log" },

  {"vme-p2a", do_vme_p2a, "configure VME P2 raw A" },
  {"patchpanel", do_patchpanel, "enable/disable patch panel" },

  {"+ts", do_timestamp_enable, "enable timestamps" },
  {"-ts", do_timestamp_disable, "enable timestamps" },
  {"+ctxt", do_context_subscribe, "alias for context-subscribe" },
  {"-ctxt", do_context_unsubscribe, "alias for context-unsubscribe" },
  {"+ev", do_event_subscribe, "alias for event-subscribe" },
  {"-ev", do_event_unsubscribe, "alias for event-unsubscribe" },
  {"+cfg", do_config_subscribe, "alias for config-subscribe" },
  {"-cfg", do_config_unsubscribe, "alias for config-unsubscribe" },
  { NULL, NULL, NULL }
};

static int do_help(struct wrenrx_handle *handle, char *argv[], int argc)
{
  unsigned i;

  for (i = 0; commands[i].name; i++)
    printf ("%-10s  %s\n", commands[i].name, commands[i].help);

  return 0;
}

static void
handle_command(char *buf)
{
  int len;
  char *argv[32];
  int argc;
  char *saveptr;
  const struct command_t *cmd;

  for (argc = 0; ; argc++) {
      if (argc == sizeof(argv)/sizeof(*argv)) {
	  printf("Too many arguments\n");
	  return;
      }
      argv[argc] = strtok_r(argc == 0 ? buf : NULL, " \t\n\r", &saveptr);
      if (argv[argc] == NULL)
	  break;
  }

  if (0) {
      printf ("Read (%d, %d): ", len, argc);
      for (int i = 0; i < argc; i++)
	  printf ("%s|", argv[i]);
      printf("\n");
  }

  if (argc == 0)
    return;

  for (cmd = commands; cmd->name; cmd++)
      if (strcmp (argv[0], cmd->name) == 0)
	  break;

  if (cmd->name == NULL) {
      fprintf(stderr, "unknown command %s, try help\n", argv[0]);
      return;
  }
  else {
      int res = cmd->func (handle, argv, argc);
      if (res < 0) {
	  fprintf(stderr, "error in command %s\n", argv[0]);
      }
  }
}


#ifdef CONFIG_READLINE
static unsigned running = 1;
static const char history_filename[] = ".wrenrx-get_history";

static void
cb_linehandler(char *line)
{
    if (line == NULL) {
	/* Also reset terminal settings */
	rl_callback_handler_remove();
	printf("\n");
	running = 0;
	return;
    }

    if (*line)
	add_history(line);

    handle_command(line);

    free(line);
}
#else
static int
handle_stdin(void)
{
  char buf[512];
  int len;
  char *p;

  len = read(0, buf, sizeof(buf) - 1);
  if (len <= 0) {
      printf ("\n");
      return len;
  }
  buf[len] = 0;

  /* Split lines */
  p = buf;
  while (1) {
      char *e;
      e = strchr(p, '\n');
      if (e == NULL) {
	  handle_command(p);
	  break;
      }
      *e = 0;
      handle_command(p);
      p = e + 1;
  }

  return 1;
}
#endif

int
main(int argc, char *argv[])
{
    struct wrenrx_cond *cond;
    unsigned nbr_ev;
    int c;
    struct wrenrx_pulser_configuration pulser_conf;
    struct wren_protocol proto;
    int src;
    struct my_opt { char opt; char *arg; };
    struct my_opt *nargv;
    int nargc;
    void *init;
    unsigned inc_src;
    unsigned flag_i = 0;
    int fd;

    init = wrenrx_drv_init(&argc, argv);
    if (init == NULL)
	return 1;

    nargv = malloc (argc * sizeof (struct my_opt));
    nargc = 0;

    handle = wrenrx_drv_open(init);
    if (handle == NULL)
	return 1;

    pulser_conf.start = WRENRX_INPUT_NOSTART;
    pulser_conf.stop = WRENRX_INPUT_NOSTOP;
    pulser_conf.clock = WRENRX_INPUT_CLOCK_1GHZ;
    pulser_conf.pulse_width = 3;
    pulser_conf.pulse_period = 31;
    pulser_conf.npulses = 10;
    pulser_conf.load_offset_sec = 1;
    pulser_conf.load_offset_nsec = 0;

    nbr_ev = 0;
    inc_src = 0;

    proto.proto = WREN_PROTO_NONE;
    proto.u.eth.flags = 0;
    proto.u.eth.ethertype = WREN_ETHERTYPE;
    proto.u.eth.domain_id = 0;
    proto.u.eth.xmit_id = 0;

    while ((c = getopt(argc, argv, "e:s:S:c:Iit")) > 0)
	switch (c) {
	case 'e':
	case 's':
	case 'c':
	    nargv[nargc].opt = c;
	    nargv[nargc].arg = optarg;
	    nargc++;
	    break;
	case 'S':
	{
	    char *e;
	    int id = strtoul(optarg, &e, 0);
	    if (*e != 0) {
		fprintf(stderr, "cannot parse source id in '%s'\n", optarg);
		return 1;
	    }
	    proto.proto = WREN_PROTO_ETHERNET;
	    proto.u.eth.domain_id = id;
	    proto.u.eth.xmit_id = 0;
	}
	break;
	case 'I':
	{
	    struct wren_protocol proto2;

	    proto2.proto = WREN_PROTO_ETHERNET;
	    proto2.u.eth.flags = 0;
	    proto2.u.eth.ethertype = WREN_ETHERTYPE;
	    proto2.u.eth.domain_id = 64 + inc_src++;
	    proto2.u.eth.xmit_id = 0;

	    src = wrenrx_add_source (handle, &proto2);
	    if (src < 0) {
		fprintf(stderr, "cannot add source: %d\n", src);
		exit(1);
	    }
	}
	break;
	case 'i':
	    flag_i++;
	    break;
	case 't':
	    flag_timestamp++;
	    break;
	default:
	    printf ("usage: %s RECV -- [-i] [-S ID] {-I} {-e EVENT,COND} {-s EVENT}\n",
		    progname);
	    printf ("Options:\n"
		    " -i        interractive (use poll)\n"
		    " -S ID     specify source id\n"
		    " -s EVENT  subscribe to EVENT id\n"
		    " -c CFG    subscribe to CFG id\n"
		    " -e EV,CD  generate a pulse on condition CD for event EV\n"
		    " -I        Increment source index (by adding a source 64+)\n");
	    return 1;
	}

    if (proto.proto != WREN_PROTO_NONE) {
	src = wrenrx_add_source (handle, &proto);
	if (src < 0) {
	    fprintf(stderr, "cannot add source: %d\n", src);
	    exit(1);
	}
    }

    if (!flag_i) {
	if (wrenrx_context_subscribe(handle, src) < 0) {
	    fprintf(stderr, "cannot subscribe context\n");
	    exit(1);
	}
    }

    cond = wrenrx_cond_alloc(handle);

    for (unsigned i = 0; i < nargc; i++) {
	char *arg = nargv[i].arg;

	switch (nargv[i].opt) {
	case 'e': {
	    struct wrenrx_cond_tree_event *event;
	    struct wrenrx_cond_expr *expr;
	    int res;

	    event = parse_cond(arg);
	    if (event == NULL)
		return 1;

	    expr = wrenrx_cond_tree_to_cond_expr(cond, event->expr);

	    /* Override the domain */
	    event->source_idx = src;

	    wrenrx_cond_define (cond, event->source_idx, event->ev_id, expr);
	    res = wrenrx_pulser_define (handle, ++nbr_ev, cond, &pulser_conf, "");
	    if (res < 0) {
		fprintf (stderr, "cannot define a pulser (err=%d)\n", res);
		return 1;
	    }

	    free(event);
	    wrenrx_cond_reinit(handle, cond);
	    break;
	}

	case 's': {
	    char *e;
	    unsigned id;
	    int res;

	    id = strtoul(arg, &e, 0);
	    if (*e != 0) {
		fprintf (stderr, "cannot parse event id '%s'\n", arg);
		return 1;
	    }
	    res = wrenrx_event_subscribe(handle, src, id, 0);
	    if (res < 0) {
		fprintf(stderr, "cannot subscribe to event %u (err=%d)\n", id, res);
		return 1;
	    }
	    break;
	}

	case 'c': {
	    char *e;
	    unsigned id;
	    int res;

	    id = strtoul(arg, &e, 0);
	    if (*e != 0) {
		fprintf (stderr, "cannot parse config id '%s'\n", arg);
		return 1;
	    }
	    res = wrenrx_config_subscribe(handle, id);
	    if (res < 0) {
		fprintf(stderr, "cannot subscribe to config %u (err=%d)\n", id, res);
		return 1;
	    }
	    break;
	}

	default:
	    abort();
	}
    }

    if (flag_i) {
	static const char prompt[] = "cmd> ";
	struct pollfd pfd[2];
	int res;

	fd = wrenrx_get_fd(handle);
	if (fd < 0) {
	    fprintf (stderr, "cannot get file handle: %m\n");
	    return 1;
	}

	pfd[0].fd = fd;
	pfd[0].events = POLLIN | POLLPRI;
	pfd[1].fd = 0;
	pfd[1].events = POLLIN;

#ifdef CONFIG_READLINE
	if (read_history(history_filename) < 0)
	    fprintf(stderr, "cannot read history: %m\n");
	stifle_history(200);
	rl_callback_handler_install(prompt, cb_linehandler);
#else
	printf (prompt);
	fflush(stdout);
#endif
	while (1) {
	    res = poll(pfd, 2, -1);
	    if (res < 0) {
		if (errno == EINTR)
		    continue;
		fprintf(stderr, "poll error: %m\n");
		return 1;
	    }
	    if (res == 0)
		printf ("timeout\n");
	    else if (pfd[0].revents & (POLLIN | POLLPRI)) {
		if (handle_wren(handle) <= 0)
		    break;
	    }
	    else if (pfd[1].revents & POLLIN) {
#ifdef CONFIG_READLINE
		rl_callback_read_char();
		if (!running) {
		    if (write_history(history_filename) < 0)
			fprintf(stderr, "cannot write history: %m\n");

		    break;
		}
#else
		if (handle_stdin() <= 0)
		    break;
		printf ("cmd> ");
		fflush(stdout);
#endif
	    }
	    else if (pfd[1].revents & POLLHUP) {
		break;
	    }
	}
    }
    else {
	while (handle_wren(handle) > 0)
	    ;
    }

    wrenrx_cond_free(cond);
    wrenrx_close(handle);
    return 0;
}
