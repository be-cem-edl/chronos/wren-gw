/* Current file being parsed */
extern const char *filename;
extern unsigned lineno;

/* Scan utilities */
enum param_fmt { FMT_ERR, FMT_S32 };

void skip_spaces (char **b);
int is_eol (char b);
void scan_uint (char **b, uint32_t *v);
void scan_eol (char **b);
int scan_cmp (char **b, const char *tok);
void scan_str (char **b, char *dst, unsigned maxlen);
enum param_fmt scan_fmt(char **b);

void wren_add_usec(struct wren_ts *t, unsigned usec);

int set_ctxt(struct wrentx_handle *handle,
	     struct wrentx_frame *frame,
	     uint16_t cid,
	     struct wren_ts *start_ts);
void add_event(struct wrentx_handle *handle,
	       struct wrentx_frame *frame,
	       uint16_t id,
	       uint16_t cid,
	       struct wren_ts *ts);
void add_param_s32(struct wrentx_frame *frame,
		   uint16_t id,
		   int32_t val);

int parse_ctxt(struct wrentx_handle *handle,
	       struct wrentx_frame *frame,
	       struct wren_ts now,
	       char **b);
int parse_param(struct wrentx_frame *frame, char **b);

int parse_event(struct wrentx_handle *handle,
		struct wrentx_frame *frame,
		struct wren_ts now,
		char **b);
