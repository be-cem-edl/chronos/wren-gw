#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "wren/wrentx.h"
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wrentx-private.h"
#include "wrentx-util.h"

const char *filename;
unsigned lineno;

int
set_ctxt(struct wrentx_handle *handle,
         struct wrentx_frame *frame,
         uint16_t cid,
	 struct wren_ts *start_ts)
{
  int status;

  status = wrentx_set_context (handle, frame, cid, start_ts);
  if (status != 0) {
    fprintf (stderr, "%s: failed to set ctxt\n", progname);
  }
  return status;
}

void
add_param_s32(struct wrentx_frame *frame,
              uint16_t id,
              int32_t val)
{
  int status;

  status = wrentx_add_parameter_s32(frame, id, val);
  if (status != 0) {
    fprintf (stderr, "%s: failed to add parameter\n", progname);
    exit (1);
  }
}

void
add_event(struct wrentx_handle *handle,
          struct wrentx_frame *frame,
          uint16_t id,
          uint16_t cid,
          struct wren_ts *ts)
{
  int status;

  status = wrentx_add_event(handle, frame, id, cid, ts);
  if (status < 0) {
    fprintf (stderr, "%s: failed to add event\n", progname);
    exit (1);
  }
}

void
skip_spaces (char **b)
{
    while (**b == ' ')
	(*b)++;
}

int
is_eol (char b)
{
    return b == '#' || b == '\n' || b == 0;
}

void
scan_uint (char **b, uint32_t *v)
{
  char *e;

  skip_spaces (b);

  *v = strtoul (*b, &e, 0);
  if (e == *b) {
    fprintf(stderr, "%s:%u: missing value\n", filename, lineno);
    return;
  }
  if (*e != ' ' && !is_eol(*e)) {
    fprintf (stderr, "bad value\n");
    return;
  }
  *b = e;
}

void
scan_eol (char **b)
{
  skip_spaces (b);
  if (is_eol (**b))
    return;
  fprintf (stderr, "%s:%u: garbage at end of line\n", filename, lineno);
}

int
scan_cmp (char **b, const char *tok)
{
  char *s = *b;

  while (*tok)
    if (*tok++ != *s++)
      return 0;

  if (*s == ' ' || is_eol (*s)) {
    *b = s;
    return 1;
  }

  return 0;
}

void
scan_str (char **b, char *dst, unsigned maxlen)
{
  char *s;

  skip_spaces (b);

  s = *b;

  while (*s && *s != ' ' && !is_eol (*s)) {
    if (maxlen > 1) {
      *dst++ = *s;
      maxlen--;
    }
    s++;
  }

  *dst = 0;
  *b = s;
}

enum param_fmt
scan_fmt(char **b)
{
  skip_spaces (b);

  if (scan_cmp(b, "s32"))
    return FMT_S32;
  return FMT_ERR;
}

void
wren_add_usec(struct wren_ts *t, unsigned usec)
{
  unsigned sec = usec / 1000000;
  unsigned nsec = (usec - sec * 1000000) * 1000;

  wren_add_nsec(t, nsec);
  t->sec += sec;
}

int
parse_ctxt(struct wrentx_handle *handle,
	   struct wrentx_frame *frame,
	   struct wren_ts now,
	   char **b)
{
    /* ctxt ID TIME(us)
       Start a context capsule.  */
    uint32_t id;
    uint32_t tim;
    struct wren_ts t;

    scan_uint (b, &id);
    scan_uint (b, &tim);
    scan_eol (b);

    t = now;
    wren_add_usec(&t, tim);

    return set_ctxt (handle, frame, id, &t);
}

int
parse_param(struct wrentx_frame *frame, char **b)
{
    /* param ID FMT VAL
       Add a parameter (to current event or context).  */
    uint32_t id;
    uint32_t val;
    enum param_fmt fmt;

    scan_uint (b, &id);
    fmt = scan_fmt(b);
    if (fmt == FMT_ERR)
	fprintf(stderr, "%s:%u: unknown format: %s\n",
		filename, lineno, *b);

    scan_uint (b, &val);
    scan_eol (b);

    switch(fmt) {
    case FMT_S32:
	return wrentx_add_parameter_s32(frame, id, val);
    default:
	abort();
    }
}

int
parse_event(struct wrentx_handle *handle,
	    struct wrentx_frame *frame,
	    struct wren_ts now,
	    char **b)
{
    /* event ID CTXT TIME(us)
       Start an event capsule.  */
    uint32_t id;
    uint32_t ctxt;
    uint32_t tim;
    struct wren_ts ts;

    scan_uint (b, &id);
    scan_uint (b, &ctxt);
    scan_uint (b, &tim);
    scan_eol (b);

    ts = now;
    wren_add_usec(&ts, tim);

    return wrentx_add_event(handle, frame, id, ctxt, &ts);
}
