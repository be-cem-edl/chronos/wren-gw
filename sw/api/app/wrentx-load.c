#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "wren/wrentx.h"
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wrentx-private.h"
#include "wren-util.h"
#include "wrentx-util.h"

static struct wrentx_table *
read_table(struct wrentx_handle *handle)
{
  FILE *f;
  struct wren_ts now;
  struct wrentx_table *table;
  struct wrentx_frame *frame;

  f = fopen(filename, "r");
  if (f == NULL) {
    fprintf (stderr, "%s: cannot open '%s'\n", progname, filename);
    return NULL;
  }

  now.sec = 0;
  now.nsec = 0;

  frame = wrentx_alloc_frame(handle);
  if (frame == NULL) {
    fprintf (stderr, "%s: cannot get frame\n", progname);
    return NULL;
  }

  table = wrentx_alloc_table(handle);

  lineno = 0;
  while (1) {
    char buf[128];
    char *b;

    if (fgets (buf, sizeof buf, f) != buf)
      break;
    lineno++;

    /* Skip spaces.  */
    b = buf;
    skip_spaces (&b);

    /* Skip comments, empty lines.  */
    if (is_eol (*b))
      continue;

    if (scan_cmp (&b, "ctxt")) {
	/* ctxt ID TIME(us)
	   Start a context capsule.  */
	if (parse_ctxt(handle, frame, now, &b) != 0)
	    return NULL;
    }
    else if (scan_cmp (&b, "param")) {
      /* param ID FMT VAL
	 Add a parameter (to current event or context).  */
      int status;

      status = parse_param(frame, &b);
      if (status != 0) {
	fprintf (stderr, "%s:%u: failed to add parameter (%d)\n",
		 filename, lineno, status);
	return NULL;
      }
    }
    else if (scan_cmp (&b, "event")) {
	/* event ID CTXT TIME(us)
	   Start an event capsule.  */
	int status;

	status = parse_event(handle, frame, now, &b);
	if (status < 0) {
	  fprintf (stderr, "%s:%u: failed to add event (%d)\n",
		   filename, lineno, status);
	  return NULL;
	}
    }
    else if (scan_cmp (&b, "table-frame")) {
	/* table-frame
	   Append current frame to the current table */
	int status;

	status = wrentx_table_append_frame(handle, table, frame);
	if (status < 0) {
	    fprintf(stderr, "table append frame error: %d\n", status);
	    return NULL;
	}
	wrentx_clear_frame(handle, frame);
    }
    else if (scan_cmp (&b, "table-wait")) {
      /* table-wait TIM(us)
	 Append a wait */
      uint32_t tim;
      int status;
      struct wren_ts t;

      scan_uint (&b, &tim);
      scan_eol (&b);

      t.sec = tim / 1000000;
      tim -= t.sec * 1000000;
      t.nsec = tim * 1000;

      status = wrentx_table_append_wait(handle, table, &t);
      if (status < 0)
	fprintf(stderr, "table append wait error: %d\n", status);
    }
    else {
      fprintf (stderr, "%s: unknown command: %s", filename, b);
      exit (1);
    }
  }

  fclose(f);
  wrentx_free_frame(frame);

  return table;
}

static void
usage(void)
{
    fprintf (stderr, "usage: %s DRIVER [driver options] -- [-r REPEAT] FILENAME TABLE-NAME SRC-IDX IDX\n",
               progname);
}

static int
parse_unsigned(const char *msg, char *s, unsigned *res)
{
    char *e;
    *res = strtoul (s, &e, 0);
    if (*e != 0) {
	fprintf (stderr, "%s: incorrect value for %s\n", progname, msg);
	return -1;
    }
    return 0;
}

int
main(int argc, char *argv[])
{
  void *init;
  struct wrentx_handle *handle;
  struct wrentx_table *table;
  unsigned repeat;
  unsigned src_idx;
  unsigned table_idx;
  const char *table_name;
  int status;
  int c;

  progname = argv[0];
  repeat = 1;

  init = wrentx_drv_init(&argc, argv);
  if (init == NULL)
    return 1;

  while ((c = getopt(argc, argv, "hr:")) >= 0) {
    switch (c) {
    case 'r':
	if (parse_unsigned("option -r", optarg, &repeat) < 0)
	    return 2;
	break;
    case '?':
    case 'h':
	usage();
	return 2;
    default:
	abort ();
    }
  }

  if (optind + 4 != argc) {
      usage();
      return 2;
  }

  filename = argv[optind];
  table_name = argv[optind + 1];

  if (parse_unsigned("source index", argv[optind + 2], &src_idx) < 0)
      return 2;
  if (parse_unsigned("table index", argv[optind + 3], &table_idx) < 0)
      return 2;

  handle = wrentx_drv_open(init);
  if (handle == NULL)
    return 1;

  table = read_table(handle);

  if (table != NULL) {
      status = wrentx_load_table(handle, src_idx, table,
				 table_name, table_idx, repeat);
      if (status < 0)
	  fprintf(stderr, "table load error: %d\n", status);
  }

  wrentx_close(handle);
  return 0;
}
