#include <sys/ioctl.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include "wren-util.h"
#include "wren-drv.h"
#include "wren-wrtime.h"
#include "wren/wren-packet.h"
#include "wren-mb-defs.h"
#include "wrenrx-core.h"
#include "wrenrx-data.h"
#include "wrenrx-cond.h"
#include "wrenrx-drv.h"
#include "wren-common-utils.h"
#include "wren-mb.h"
#include "wren-ioctl.h"

/* Just in case */
#define wren_rx_sources XXX_sources
#define wren_rx_conds XXX_conds
#define wren_rx_actions XXX_actions

#define NBR_EVSUB 512

#define NBR_EV_BUF (NBR_EVSUB * 2)
#define NBR_EV_REF (NBR_EVSUB * 4)

#define INVALID_SOURCE_IDX 0xffff

/* Invalid index for ev_buf or ev_ref */
#define INVALID_IDX 0xffff

struct wrenrx_handle_wrenctl {
    struct wrenrx_handle_op base;

    FILE *tracef;

    struct wrenrx_msg msg;

    /* Event subscriptions.  Free slots are have source_idx as invalid */
    struct wrenrx_evsub {
	uint16_t source_idx;
	wren_event_id ev_id;
	int offset_us;
    } ev_sub[NBR_EVSUB];

    /* List of waiting to be delivered events */
    struct wrenrx_ev_ref {
	uint16_t evsub;
	uint16_t evbuf;
	uint16_t chain;
	struct wren_ts ts;
    } ev_ref[NBR_EV_REF];

    /* Chain of ev_ref waiting for timeout */
    uint16_t evref_head;

    /* Chain of ev_ref expired */
    uint16_t evref_ready;
    uint16_t evref_tail;

    /* Chain of unused ev_ref */
    uint16_t evref_free;

    /* Event buffers */
    struct wrenrx_ev_buf {
	/* Number of references by ev_ref */
	uint16_t ref;
	uint16_t source_idx;
	struct wrenrx_msg_event evt;
    } ev_buf[NBR_EV_BUF];

    /* Index of chain of free evt_buf, use source_idx as link */
    uint16_t evbuf_free;

    /* The file descriptor to wren driver */
    int wren_fd;
};

static int
wrenrx_drv_wrenctl_read(struct wrenrx_handle *handle,
			struct wren_packet *packet)
{
    return 0;
}

#if 0
int
wrenrx_trigger_interrupt (struct wrenrx_handle *handle,
                        unsigned interrupt_id,
                        struct wrenrx_trigger *trig,
                        int delay_us)
{
  unsigned i;

  for (i = 0; i < NBR_ACTIONS; i++) {
    struct action *act = &handle->act[i];
    if (act->kind != ACT_NONE)
      continue;
    act->kind = ACT_INTER;
    act->trig = *trig;
    act->u.inter.id = interrupt_id;
    act->u.inter.delay_us = delay_us;

    return i;
  }
  return -1;
}
#endif

static int
wrenrx_drv_wren_get_link(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    int res;
    uint32_t val;

    res = ioctl(h->wren_fd, WREN_IOC_GET_LINK, &val);
    if (res < 0)
	return WRENRX_ERR_SYSTEM;
    return val;
}

static int
wrenrx_drv_wren_get_sync(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    int res;
    uint32_t val;

    res = ioctl(h->wren_fd, WREN_IOC_GET_SYNC, &val);
    if (res < 0)
	return WRENRX_ERR_SYSTEM;
    return val;
}

static int
wrenrx_drv_wren_get_version(struct wrenrx_handle *handle,
			    char *version,
			    unsigned maxlen)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_version_string ver;
    int res;

    ver.maxlen = maxlen;
    ver.buf = version;

    res = ioctl(h->wren_fd, WREN_IOC_VERSION_STRING, &ver);
    if (res < 0)
	return WRENRX_ERR_SYSTEM;
    return 0;
}

static int
wrenrx_dev_wren_get_panel_config(struct wrenrx_handle *handle,
				 struct wren_panel_config *config)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_GET_PANEL_CONFIG, config);
    if (res < 0)
	return WRENRX_ERR_SYSTEM;
    return 0;
}

static int
wrenrx_drv_wren_add_source(struct wrenrx_handle *handle,
			   struct wren_protocol *proto)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_ADD_SOURCE, proto);
    if (res < 0) {
	if (errno == EINVAL)
	    res = WRENRX_ERR_BAD_PROTOCOL;
	else if (errno == ENOSPC)
	    res = WRENRX_ERR_NO_SOURCE;
	else
	    res = WRENRX_ERR_SYSTEM;
    }

    if (h->tracef)
	fprintf(h->tracef, "rx_add_source(proto=%u, domain_id=%u) -> %d\n",
		proto->proto, proto->u.eth.domain_id, res);

    return res;
}

static int
wrenrx_drv_wren_del_source(struct wrenrx_handle *handle,
			   unsigned source_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    int res;
    uint32_t idx = source_idx;

    res = ioctl(h->wren_fd, WREN_IOC_RX_DEL_SOURCE, &idx);
    if (res < 0) {
	if (errno == EINVAL)
	    res = WRENRX_ERR_SRCID_TOO_LARGE;
	else
	    res = WRENRX_ERR_SYSTEM;
    }

    if (h->tracef)
	fprintf(h->tracef, "rx_del_source(idx=%u) -> %d\n", source_idx, res);

    return res;
}

static int
wrenrx_drv_wren_get_source(struct wrenrx_handle *handle,
			   unsigned source_idx,
			   struct wren_protocol *proto)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_get_source src;
    int res;

    src.source_idx = source_idx;
    res = ioctl(h->wren_fd, WREN_IOC_RX_GET_SOURCE, &src);
    if (res < 0) {
	if (errno == EINVAL)
	    res = WRENRX_ERR_SRCID_TOO_LARGE;
	else
	    res = WRENRX_ERR_SYSTEM;
    }
    else
	memcpy(proto, &src.proto, sizeof(*proto));

    if (h->tracef)
	fprintf(h->tracef, "rx_get_source(idx=%u) -> %d\n", source_idx, res);

    return res;
}

static int
copy_name(char *dst, const char *name)
{
    unsigned i;

    for (i = 0; i < WRENRX_CONFIGURATION_NAME_MAXLEN; i++)
	if ((*dst++ = *name++) == 0)
	    break;
    if (i == WRENRX_CONFIGURATION_NAME_MAXLEN)
	return WRENRX_ERR_NAME_TOO_LONG;
    for (; i < WRENRX_CONFIGURATION_NAME_MAXLEN; i++)
	*dst++ = 0;
    return 0;
}

static int
wrenrx_drv_wren_pulser_define (struct wrenrx_handle *handle,
			       unsigned pulser_num,
			       const struct wrenrx_cond *cond,
			       const struct wrenrx_pulser_configuration *conf,
			       const char *name)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_config cmd;
    int res;

    wrenrx_pulser_configuration_to_mb(&cmd.config, conf, pulser_num - 1);

    res = copy_name(cmd.name, name);
    if (res < 0)
	return res;

    res = wrenrx_cond_to_mb(&cmd.cond, cond);
    if (res < 0)
	return res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_ADD_CONFIG, &cmd);
    if (res < 0) {
	if (errno == EINVAL || errno == ECOMM)
	    res = cmd.error;
	else
	    res = WRENRX_ERR_SYSTEM;
    }

    if (h->tracef)
	fprintf(h->tracef, "rx_pulser_define(num=%u) -> %d\n",
		pulser_num, res);

    return res;
}

static int
wrenrx_drv_wren_pulser_program (struct wrenrx_handle *handle,
				unsigned pulser_num,
				const struct wrenrx_pulser_configuration *conf,
				const struct wren_ts *time,
				const char *name)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_imm_config cmd;
    int res;

    if (pulser_num == 0)
	return WRENRX_ERR_PULSER_NUM;

    res = copy_name(cmd.name, name);
    if (res < 0)
	return res;

    wrenrx_pulser_configuration_to_mb(&cmd.config, conf, pulser_num - 1);

    if (time)
      cmd.ts = *time;
    else {
      cmd.ts.sec = 0;
      cmd.ts.nsec = 0;
      cmd.config.flags |= WREN_MB_PULSER_FLAG_IMMEDIAT;
    }

    res = ioctl(h->wren_fd, WREN_IOC_RX_IMM_CONFIG, &cmd);
    if (res < 0) {
	if (errno == EINVAL || errno == ECOMM)
	    res = cmd.error;
	else
	    res = WRENRX_ERR_SYSTEM;
    }

    if (h->tracef)
	fprintf(h->tracef, "rx_pulser_program(num=%u) -> %d\n",
		pulser_num, res);

    return res;
}

static int
wrenrx_drv_wren_pulser_reconfigure(struct wrenrx_handle *handle,
				   unsigned config_idx,
				   const struct wrenrx_pulser_configuration *conf)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_mod_config cmd;
    int res;

    cmd.config_idx = config_idx;
    wrenrx_pulser_configuration_to_mb(&cmd.config, conf, 0);

    res = ioctl(h->wren_fd, WREN_IOC_RX_MOD_CONFIG, &cmd);
    if (res < 0) {
	if (errno == EINVAL || errno == ECOMM)
	    res = cmd.error;
	else
	    res = WRENRX_ERR_SYSTEM;
    }

    if (h->tracef)
	fprintf(h->tracef, "rx_pulser_reconfigure(idx=%u) -> %d\n",
		config_idx, res);

    return res;
}

static int
wrenrx_drv_wren_pulser_force(struct wrenrx_handle *handle,
			     unsigned config_idx,
			     struct wren_ts *ts,
			     struct wrenrx_pulser_configuration *conf)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_force_pulser cmd;
    int res;

    if (ts != NULL)
	return WRENRX_ERR_SYSTEM;

    cmd.config_idx = config_idx;
    cmd.ts.sec = 0;
    cmd.ts.nsec = 0;
    wrenrx_pulser_configuration_to_mb(&cmd.config, conf, 0);

    res = ioctl(h->wren_fd, WREN_IOC_RX_FORCE_PULSER, &cmd);
    if (res < 0) {
	if (errno == EINVAL || errno == ECOMM)
	    res = cmd.error;
	else
	    res = WRENRX_ERR_SYSTEM;
    }

    if (h->tracef)
	fprintf(h->tracef, "rx_force_pulser(idx=%u) -> %d\n",
		config_idx, res);

    return res;
}

static int
wrenrx_drv_wren_pulser_remove(struct wrenrx_handle *handle,
			      unsigned config_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_del_config cfg;
    int res;

    cfg.config_idx = config_idx;

    res = ioctl(h->wren_fd, WREN_IOC_RX_DEL_CONFIG, &cfg);
    if (res < 0) {
	if (errno == EINVAL || errno == ECOMM)
	    res = cfg.error;
	else
	    res = WRENRX_ERR_SYSTEM;
    }

    if (h->tracef)
	fprintf(h->tracef, "rx_pulser_remove(idx=%u) -> %d\n",
		config_idx, res);

    return res;
}

static int
wren_rx_drv_read_cond(const struct wren_mb_cond *cond,
		      struct wrenrx_cond *res)
{
    unsigned stack[MAX_COND];
    unsigned sp;
    unsigned idx;

    res->nbr_cond = 0;

    if (cond->len == 0)
	return 0;

    sp = 0;

    for (idx = 0; idx < cond->len;) {
	union wren_rx_cond_word op = cond->ops[idx];

	if (res->nbr_cond >= MAX_COND)
	    return WRENRX_ERR_INVALID_COND;

	struct wrenrx_cond_expr *expr = &res->conds[res->nbr_cond];

	if (op.op.op < WREN_OP_FIRST_BINARY) {
	    expr->kind = WRENRX_COND;
	    expr->u.cond.orig = op.op.param_orig;
	    expr->u.cond.param = op.op.param_id;
	    expr->u.cond.op = op.op.op;
	    expr->u.cond.val = cond->ops[idx+1].vu32;

	    idx += 2;
	}
	else {
	    if (sp < 2)
		return WRENRX_ERR_INVALID_COND;
	    expr->kind = WRENRX_BINARY;
	    switch (op.op.op) {
	    case WREN_OP_AND:
		expr->u.binary.op = WRENRX_AND;
		break;
	    case WREN_OP_OR:
		expr->u.binary.op = WRENRX_OR;
		break;
	    case WREN_OP_NOT:
		expr->u.binary.op = WRENRX_NOT;
		break;
	    default:
		return WRENRX_ERR_INVALID_COND;
	    }
	    expr->u.binary.l = stack[sp - 2];
	    expr->u.binary.r = stack[sp - 1];
	    sp -= 2;
	    idx += 1;
	}

	stack[sp++] = res->nbr_cond;
	res->nbr_cond++;
    }

    if (sp != 1 || stack[0] != res->nbr_cond - 1)
	return WRENRX_ERR_INVALID_COND;

    return 0;
}

static int
wrenrx_drv_wren_pulser_get_config(struct wrenrx_handle *handle,
				  unsigned config_idx,
				  unsigned *pulser_num,
				  struct wrenrx_pulser_configuration *conf,
				  struct wrenrx_cond *cond,
				  char *name)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_get_config cfg;
    int res;

    cfg.config_idx = config_idx;
    res = ioctl(h->wren_fd, WREN_IOC_RX_GET_CONFIG, &cfg);
    if (res < 0) {
	if (errno == EINVAL || errno == ECOMM)
	    return cfg.error;
	return WRENRX_ERR_SYSTEM;
    }

    memcpy(name, cfg.config.name, sizeof(cfg.config.name));
    *pulser_num = cfg.config.config.pulser_idx + 1;
    conf->config_en =
      (cfg.config.config.flags & WREN_MB_PULSER_FLAG_ENABLE) ? 1 : 0;
    conf->interrupt_en =
      (cfg.config.config.flags & WREN_MB_PULSER_FLAG_INT_EN) ? 1 : 0;
    conf->output_en =
      (cfg.config.config.flags & WREN_MB_PULSER_FLAG_OUT_EN) ? 1 : 0;
    conf->repeat_mode =
      (cfg.config.config.flags & WREN_MB_PULSER_FLAG_REPEAT) ? 1 : 0;
    conf->start = WREN_MB_PULSER_GET_START(cfg.config.config.inputs);
    conf->stop =  WREN_MB_PULSER_GET_STOP(cfg.config.config.inputs);
    conf->clock = WREN_MB_PULSER_GET_CLOCK(cfg.config.config.inputs);
    conf->initial_delay = cfg.config.config.idelay;
    conf->pulse_width = cfg.config.config.width;
    conf->pulse_period = cfg.config.config.period;
    conf->npulses = cfg.config.config.npulses;
    conf->load_offset_sec = cfg.config.config.load_off_sec;
    conf->load_offset_nsec = cfg.config.config.load_off_nsec;

    if (cond != NULL) {
	int res;

	cond->source_idx = cfg.config.cond.src_idx;
	cond->ev_id = cfg.config.cond.evt_id;
	res = wren_rx_drv_read_cond(&cfg.config.cond, cond);
	if (res < 0)
	    return res;
    }
    return 0;
}

static int
wrenrx_drv_wren_output_get(struct wrenrx_handle *handle,
			   unsigned output_num,
			   struct wrenrx_output_configuration *cfg)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_out out;
    int res;

    out.output_idx = output_num - 1;
    res = ioctl(h->wren_fd, WREN_IOC_RX_GET_OUTPUT, &out);
    if (res < 0) {
	return WRENRX_ERR_SYSTEM;
    }

    cfg->invert_pulsers = out.inv_in;
    cfg->mask = out.mask;
    cfg->invert_result = out.inv_out;

    return 0;
}

static int
wrenrx_drv_wren_pin_set(struct wrenrx_handle *handle,
			unsigned pin_num,
			const struct wrenrx_pin_configuration *cfg)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_pin pin;
    int res;

    pin.pin_idx = pin_num - 1;
    pin.oe = cfg->out_en;
    pin.term = cfg->term_en;
    pin.inv = cfg->inv_inp;
    res = ioctl(h->wren_fd, WREN_IOC_RX_SET_PIN, &pin);
    if (res < 0) {
	return WRENRX_ERR_SYSTEM;
    }

    return 0;
}

static int
wrenrx_drv_wren_pin_get(struct wrenrx_handle *handle,
			unsigned pin_num,
			struct wrenrx_pin_configuration *cfg)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_pin pin;
    int res;

    pin.pin_idx = pin_num - 1;
    res = ioctl(h->wren_fd, WREN_IOC_RX_GET_PIN, &pin);
    if (res < 0) {
	return WRENRX_ERR_SYSTEM;
    }

    cfg->out_en = pin.oe;
    cfg->term_en = pin.term;
    cfg->inv_inp = pin.inv;

    return 0;
}

static int
wrenrx_drv_wren_output_set(struct wrenrx_handle *handle,
			   unsigned output_num,
			   const struct wrenrx_output_configuration *cfg)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_out out;
    int res;

    out.output_idx = output_num - 1;
    out.inv_in = cfg->invert_pulsers;
    out.mask = cfg->mask;
    out.inv_out = cfg->invert_result;
    res = ioctl(h->wren_fd, WREN_IOC_RX_SET_OUTPUT, &out);
    if (res < 0) {
	return WRENRX_ERR_SYSTEM;
    }

    return 0;
}

static int
wrenrx_drv_wren_pulser_log_get(struct wrenrx_handle *handle,
			       unsigned pulser_num,
			       struct wrenrx_pulser_log *buf,
			       unsigned len)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_stamper_log log;
    int res;

    log.idx = pulser_num - 1;
    log.len = len;
    log.buf = buf;
    res = ioctl(h->wren_fd, WREN_IOC_GET_PULSER_LOG, &log);
    if (res < 0) {
	return WRENRX_ERR_SYSTEM;
    }

    return res;
}

static int
wrenrx_drv_wren_pin_log_get(struct wrenrx_handle *handle,
			    unsigned pin_num,
			    struct wrenrx_pin_log *buf,
			    unsigned len)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_stamper_log log;
    int res;

    log.idx = pin_num - 1;
    log.len = len;
    log.buf = buf;
    res = ioctl(h->wren_fd, WREN_IOC_GET_PIN_LOG, &log);
    if (res < 0) {
        if (errno == EINVAL || errno == ECOMM)
            res = log.error;
        else
            res = WRENRX_ERR_SYSTEM;
    }

    return res;
}

static int
wrenrx_drv_wren_sw_log_get(struct wrenrx_handle *handle,
			   union wren_evlog_entry *ent,
			   unsigned len)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_stamper_log log;
    int res;

    log.idx = 0;
    log.len = len;
    log.buf = ent;
    res = ioctl(h->wren_fd, WREN_IOC_GET_SW_LOG, &log);
    if (res < 0) {
	return WRENRX_ERR_SYSTEM;
    }

    return res;
}

static int
wren_drv_get_fd(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    return h->wren_fd;
}

static void
wren_drv_close(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    close (h->wren_fd);
}

static int
wrenrx_drv_wren_context_subscribe(struct wrenrx_handle *handle,
				  unsigned source_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = source_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_SUBSCRIBE_CONTEXT, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_BAD_SOURCE;
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static int
wrenrx_drv_wren_context_unsubscribe(struct wrenrx_handle *handle,
				  unsigned source_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = source_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_UNSUBSCRIBE_CONTEXT, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_BAD_SOURCE;
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static int
wrenrx_drv_wren_event_subscribe(struct wrenrx_handle *handle,
				unsigned source_idx,
				wren_event_id ev_id,
				int offset_us)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_event_subscribe msg;
    int res;
    unsigned count;
    unsigned slot = INVALID_IDX;

    /* Find an ev_sub slot and look if already subscribed */
    count = 0;
    for (unsigned i = 0; i < NBR_EVSUB; i++) {
	struct wrenrx_evsub *es = &h->ev_sub[i];
	if (es->source_idx == source_idx && es->ev_id == ev_id) {
	    if (slot != INVALID_IDX)
		return slot;
	    count++;
	}
	if (slot == INVALID_IDX && es->source_idx == INVALID_IDX) {
	    es->source_idx = source_idx;
	    es->ev_id = ev_id;
	    es->offset_us = offset_us;
	    slot = i;
	    if (count != 0)
		return slot;
	}
    }

    if (slot == INVALID_IDX)
	return WRENRX_ERR_NO_EVENT_SUBSCRIBE;

    /* Need to subscribe */
    msg.source_idx = source_idx;
    msg.event_id = ev_id;

    res = ioctl(h->wren_fd, WREN_IOC_RX_SUBSCRIBE_EVENT, &msg);
    if (res == 0)
	return slot;
    if (errno == ECOMM || errno == -EINVAL)
	return msg.error;
    else
	return WRENRX_ERR_SYSTEM;
}

static int
wrenrx_drv_wren_event_unsubscribe(struct wrenrx_handle *handle,
				  unsigned evsub_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wrenrx_evsub *es;
    struct wren_ioctl_event_subscribe msg;
    int res;

    if (evsub_idx >= NBR_EVSUB)
	return WRENRX_ERR_EVSUBID_TOO_LARGE;

    es = &h->ev_sub[evsub_idx];

    if (es->source_idx == INVALID_SOURCE_IDX)
	return WRENRX_ERR_EVSUBID;

    msg.source_idx = es->source_idx;
    msg.event_id = es->ev_id;

    /* Free the subscription */
    es->source_idx = INVALID_SOURCE_IDX;

    /* TODO: clean-up ev_ref/ev_buf ? There might be pending messages. */

    for (unsigned i = 0; i < NBR_EVSUB; i++)
	if (h->ev_sub[i].source_idx == msg.source_idx
	    && h->ev_sub[i].ev_id == msg.event_id) {
	    /* Event is still used */
	    return 0;
	}

    res = ioctl(h->wren_fd, WREN_IOC_RX_UNSUBSCRIBE_EVENT, &msg);
    if (res == 0)
	return 0;
    if (errno == ECOMM)
	return msg.error;
    else
	return WRENRX_ERR_SYSTEM;
}

static int wrenrx_drv_wren_get_time (struct wrenrx_handle *handle,
				     struct wren_ts *time)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    return wren_wren_get_time(h->wren_fd, time);
}

static int wrenrx_drv_wren_subscribe_config(struct wrenrx_handle *handle,
					    unsigned config_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = config_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_SUBSCRIBE_CONFIG, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_CFGID_TOO_LARGE;
    case ENOENT:
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static int wrenrx_drv_wren_unsubscribe_config(struct wrenrx_handle *handle,
					      unsigned config_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = config_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_UNSUBSCRIBE_CONFIG, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_CFGID_TOO_LARGE;
    case ENOENT:
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static int wrenrx_drv_wren_set_promisc(struct wrenrx_handle *handle,
				       unsigned en)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx;
    int res;

    idx = en ? ~0UL : 0;

    res = ioctl(h->wren_fd, WREN_IOC_RX_SET_PROMISC, &idx);
    if (res == 0)
	return 0;
    else
	return WRENRX_ERR_SYSTEM;
}

static int
wrenrx_drv_wren_hw_vme_p2a_set_output(struct wrenrx_handle *handle, unsigned en)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    unsigned val = en;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_SET_VME_P2A_OUTPUT, &val);
    if (res == 0)
	return 0;
    else
	return WRENRX_ERR_SYSTEM;
}

static int
wrenrx_drv_wren_hw_patchapanel_set_config(struct wrenrx_handle *handle, unsigned en)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    unsigned val = en;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_SET_PATCHPANEL, &val);
    if (res == 0)
	return 0;
    else
	return WRENRX_ERR_SYSTEM;
}

union wren_usr_union {
    /* The header contains the kind of message (CMD_ASYNC_xxx) and
       the length */
    union wren_capsule_hdr_un hdr;
    uint32_t buf[512];

    struct wren_usr_ctxt {
	union wren_capsule_hdr_un pdu_hdr;
	struct wren_capsule_ctxt_hdr pdu;
    } ctxt;
    struct wren_usr_pulse {
	union wren_capsule_hdr_un pdu_hdr;
	uint32_t config_id;
	struct wren_ts ts;
	struct wren_capsule_event_hdr evt;
    } pulse;
    struct wren_usr_event {
	union wren_capsule_hdr_un pdu_hdr;
	/* TODO: add subscription-id */
	struct wren_capsule_event_hdr pdu;
    } event;
    struct wren_usr_promisc {
	union wren_capsule_hdr_un pdu_hdr;
	uint32_t buf[512 - 1];
    } promisc;
};

static void
wren_drv_read_pulse(struct wrenrx_handle_wrenctl *h, union wren_usr_union *u)
{
    unsigned len = u->hdr.hdr.len;

    /* Fill msg */
    h->msg.kind = wrenrx_msg_pulse;
    h->msg.u.pulse.ts.sec = u->buf[1];
    h->msg.u.pulse.ts.nsec = u->buf[2];
    h->msg.u.pulse.config_id = u->buf[3] >> 16;
    h->msg.u.pulse.source_idx = u->buf[3] & 0xff;
    if (len == 4) {
	h->msg.u.pulse.evt.ev_id = WREN_EVENT_ID_INVALID;
    } else {
	unsigned hlen = sizeof (struct wren_usr_pulse) / 4;
	h->msg.u.pulse.evt.ev_id = u->pulse.evt.ev_id;
	h->msg.u.pulse.evt.ctxt_id = u->pulse.evt.ctxt_id;
	h->msg.u.pulse.evt.ts.sec = u->pulse.evt.ts.sec;
	h->msg.u.pulse.evt.ts.nsec = u->pulse.evt.ts.nsec;
	h->msg.u.pulse.evt.params_len = len - hlen;
	memcpy(h->msg.u.pulse.evt.params, u->buf + hlen, (len - hlen) * 4);
    }
}

static void
wren_drv_read_ctxt(struct wrenrx_handle_wrenctl *h, union wren_usr_union *u)
{
    /* Save context */
    unsigned len = u->hdr.hdr.len;
    unsigned hlen = sizeof(struct wren_usr_ctxt) / 4;

    h->msg.kind = wrenrx_msg_context;
    h->msg.u.ctxt.source_idx = u->hdr.hdr.pad;
    h->msg.u.ctxt.ctxt_id = u->ctxt.pdu.ctxt_id;
    h->msg.u.ctxt.ctxt.ts.sec = u->ctxt.pdu.valid_from.sec;
    h->msg.u.ctxt.ctxt.ts.nsec = u->ctxt.pdu.valid_from.nsec;
    h->msg.u.ctxt.ctxt.params_len = len - hlen;
    memcpy (h->msg.u.ctxt.ctxt.params, u->buf + hlen, (len - hlen) * 4);
}

static void
wren_drv_update_timeout(struct wrenrx_handle_wrenctl *h)
{
    int res;
    struct wren_ts *ts;
    struct wren_wr_time to;

    while (1) {
	/* Set timeout from the head */
	ts = &h->ev_ref[h->evref_head].ts;
	to.tai_lo = ts->sec;
	to.tai_hi = 0;
	to.ns = ts->nsec;

	res = ioctl(h->wren_fd, WREN_IOC_RX_SET_TIMEOUT, &to);
	if (res >= 0)
	    return;

	struct wren_ts now;
	fprintf(stderr,
		"wren_drv_update_timeout: cannot set timeout: %m\n");
	wren_wren_get_time(h->wren_fd, &now);
	fprintf(stderr, " timeout=%xs+%xns, now=%xs+%xns\n",
		(unsigned)ts->sec, (unsigned)ts->nsec,
		(unsigned)now.sec, (unsigned)now.nsec);

	/* Move the expired one to the ready queue */
	while (1) {
	    unsigned slot = h->evref_head;
	    struct wrenrx_ev_ref *ref;

	    if (h->evref_head == INVALID_IDX) {
		/* No more events for the user */
		return;
	    }

	    ref = &h->ev_ref[slot];
	    if (wren_ts_lt(&now, &ref->ts)) {
		/* Event in the future, set the timeout */
		break;
	    }

	    h->evref_head = ref->chain;

	    /* Append it */
	    ref->chain = INVALID_IDX;
	    if (h->evref_tail == INVALID_IDX)
		h->evref_head = slot;
	    else
		h->ev_ref[h->evref_tail].chain = slot;
	    h->evref_tail = slot;
	}
    }
}

static void
wren_drv_read_event(struct wrenrx_handle_wrenctl *h, union wren_usr_union *u)
{
    uint16_t bidx;
    uint16_t next_bidx;
    struct wrenrx_ev_buf *buf;
    unsigned update;

    /* Get an event buffer */
    bidx = h->evbuf_free;
    if (bidx == INVALID_IDX) {
	/* TODO: out of buffer */
	fprintf (stderr, "wren_drv_read_event: out of evbuf!\n");
	return;
    }
    buf = &h->ev_buf[bidx];
    next_bidx = buf->source_idx;

    /* and fill it */
    unsigned len = u->hdr.hdr.len;
    unsigned hlen = sizeof(struct wren_usr_event) / 4;
    assert(buf->ref == 0);
    buf->source_idx = u->hdr.hdr.pad;
    buf->evt.ev_id = u->event.pdu.ev_id;
    buf->evt.ctxt_id = u->event.pdu.ctxt_id;
    buf->evt.ts.sec = u->event.pdu.ts.sec;
    buf->evt.ts.nsec = u->event.pdu.ts.nsec;
    buf->evt.params_len = len - hlen;
    memcpy (buf->evt.params, u->buf + hlen, (len - hlen) * 4);

    update = 0;

    /* For each subscription, check the event.  */
    for (unsigned i = 0; i < NBR_EVSUB; i++) {
	struct wrenrx_evsub *sub = &h->ev_sub[i];

	if (sub->source_idx == buf->source_idx
	    && sub->ev_id == buf->evt.ev_id) {
	    /* Match.  Get a ref.  */
	    unsigned ridx = h->evref_free;
	    struct wrenrx_ev_ref *ref;
	    if (ridx == INVALID_IDX) {
		/* TODO: out of buffer */
		fprintf (stderr, "wren_drv_read_event: out of ev_ref\n");
		break;
	    }

	    /* Fill */
	    ref = &h->ev_ref[ridx];
	    h->evref_free = ref->chain;
	    ref->evsub = i;
	    ref->evbuf = bidx;
	    ref->ts = buf->evt.ts;
	    buf->ref++;

	    /* Adjust time-stamp with offset */
	    int32_t off_sec = sub->offset_us / 1000000;
	    int32_t off_ns = 1000 * (sub->offset_us - off_sec * 1000000);
	    if (off_ns < 0) {
		if (ref->ts.nsec < -off_ns) {
		    ref->ts.nsec = ref->ts.nsec + WREN_NS_PER_SEC + off_ns;
		    ref->ts.sec--;
		}
		else
		    ref->ts.nsec += off_ns;
	    }
	    else {
		ref->ts.nsec += off_ns;
		if (ref->ts.nsec > WREN_NS_PER_SEC) {
		    ref->ts.nsec -= WREN_NS_PER_SEC;
		    ref->ts.sec++;
		}
	    }
	    ref->ts.sec += off_sec;

	    /* Add it to the chain (after prev_slot) */
	    unsigned slot, prev_slot;
	    prev_slot = INVALID_IDX;
	    slot = h->evref_head;
	    while (slot != INVALID_IDX) {
		if (wren_ts_lt(&ref->ts, &h->ev_ref[slot].ts))
		    break;
		prev_slot = slot;
		slot = h->ev_ref[slot].chain;
	    }

	    if (prev_slot == INVALID_IDX) {
		ref->chain = h->evref_head;
		h->evref_head = ridx;
		update = 1;
	    }
	    else {
		ref->chain = h->ev_ref[prev_slot].chain;
		h->ev_ref[prev_slot].chain = ridx;
	    }
	}
    }

    if (buf->ref != 0) {
	/* The buffer is referenced for a subscription */
	/* Remove it from the free chain */
	h->evbuf_free = next_bidx;

	/* And update timeout if needed */
	if (update)
	    wren_drv_update_timeout(h);
    }
    else {
	/* Unexpected! */
	fprintf (stderr, "wren_drv_read_event: unused event!\n");
	buf->source_idx = next_bidx;
    }
}

static void
wren_drv_event_ready(struct wrenrx_handle_wrenctl *h)
{
    unsigned ridx = h->evref_ready;
    struct wrenrx_ev_ref *ref = &h->ev_ref[ridx];
    unsigned bidx = ref->evbuf;
    struct wrenrx_ev_buf *buf = &h->ev_buf[bidx];

    h->evref_ready = ref->chain;
    if (ref->chain == INVALID_IDX)
	h->evref_tail = INVALID_IDX;

    h->msg.kind = wrenrx_msg_event;
    h->msg.u.evt.source_idx = buf->source_idx;
    h->msg.u.evt.evsub_id = ref->evsub;
    h->msg.u.evt.evt.ev_id = buf->evt.ev_id;
    h->msg.u.evt.evt.ctxt_id = buf->evt.ctxt_id;
    h->msg.u.evt.evt.ts = buf->evt.ts;
    h->msg.u.evt.evt.params_len = buf->evt.params_len;
    memcpy (h->msg.u.evt.evt.params, buf->evt.params, buf->evt.params_len * 4);

    /* Free ref */
    ref->chain = h->evref_free;
    h->evref_free = ridx;

    buf->ref--;
    if (buf->ref == 0) {
	/* Free buf */
	buf->source_idx = h->evbuf_free;
	h->evbuf_free = bidx;
    }
}

static void
wren_drv_timeout(struct wrenrx_handle_wrenctl *h)
{
    unsigned ridx = h->evref_head;
    unsigned prev;
    struct wrenrx_ev_ref *ref;
    struct wren_ts ts;

    /* The ready queue must have been flushed before we get a new timeout */
    assert (h->evref_ready == INVALID_IDX);
    assert (h->evref_tail == INVALID_IDX);
    assert (ridx != INVALID_IDX);
    ref = &h->ev_ref[ridx];

    /* Move the first one to the ready chain */
    h->evref_head = ref->chain;
    ref->chain = INVALID_IDX;
    h->evref_ready = ridx;
    h->evref_tail = ridx;

    if (h->evref_head == INVALID_IDX) {
	/* Only one.  Simple: no timeout */
	return;
    }

    ts = ref->ts;
    /* Add a threshold... */
    wren_add_nsec(&ts, 1000);

    /* And make all event refs whose timestamp is before ts ready. */
    prev = ridx;
    while (1) {
	ridx = h->evref_head;
	if (ridx == INVALID_IDX)
	    break;
	ref = &h->ev_ref[ridx];
	if (!wren_ts_lt(&ref->ts, &ts)) {
	    wren_drv_update_timeout(h);
	    return;
	}
	h->ev_ref[prev].chain = ridx;
	h->evref_head = ref->chain;
	ref->chain = INVALID_IDX;
	prev = ridx;
    }

}

static void
wren_drv_read_promisc(struct wrenrx_handle_wrenctl *h, union wren_usr_union *u)
{
    unsigned len = (u->hdr.hdr.len - 1) * 4;

    /* Fill msg */
    h->msg.kind = wrenrx_msg_packet;
    h->msg.u.packet.source_idx = ~0;
    h->msg.u.packet.len = (len - 14) / 4;
    /* Remove pdu header and MAC header */
    memcpy(h->msg.u.packet.buf,
	   ((char *)(u->promisc.buf + 1)) + 14, len - 14);
}

static struct wrenrx_msg *
wren_drv_wait(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;

    while (1) {
	union wren_usr_union u;
	int len;

	if (h->evref_ready != INVALID_IDX) {
	    wren_drv_event_ready(h);
	    return &h->msg;
	}

	len = read(h->wren_fd, u.buf, sizeof(u.buf));

	if (len < 0 && errno == ETIME) {
	    /* Will probably added an entry to the evref_ready queue, so it
	       needs to be checked again */
	    wren_drv_timeout(h);
	    continue;
	}

	if (len < 0 && errno == EIO) {
	    fprintf (stderr, "no sync\n");
	    return NULL;
	}

	if (len < 0) {
	    fprintf (stderr, "wren_drv_wait: error %m\n");
	    continue;
	}

	/* End of file */
	if (len == 0)
	    return NULL;

	switch (u.hdr.hdr.typ) {
	case CMD_ASYNC_PULSE:
	    wren_drv_read_pulse(h, &u);
	    break;
	case CMD_ASYNC_CONTEXT:
	    wren_drv_read_ctxt(h, &u);
	    break;
	case CMD_ASYNC_EVENT:
	    wren_drv_read_event(h, &u);
	    h->msg.kind = wrenrx_msg_none;
	    break;
	case CMD_ASYNC_PROMISC:
	    wren_drv_read_promisc(h, &u);
	    break;
	default:
	    abort();
	}
	return &h->msg;
    }
}

static const struct wrenrx_operations wrenrx_drv_wren_ops = {
    .get_version = wrenrx_drv_wren_get_version,
    .get_link = wrenrx_drv_wren_get_link,
    .get_sync = wrenrx_drv_wren_get_sync,
    .close = wren_drv_close,
    .recv_packet = wrenrx_drv_wrenctl_read,
    .set_promisc = wrenrx_drv_wren_set_promisc,
    .get_time = wrenrx_drv_wren_get_time,
    .get_panel_config = wrenrx_dev_wren_get_panel_config,
    .add_source = wrenrx_drv_wren_add_source,
    .get_source = wrenrx_drv_wren_get_source,
    .del_source = wrenrx_drv_wren_del_source,
    .context_subscribe = wrenrx_drv_wren_context_subscribe,
    .context_unsubscribe = wrenrx_drv_wren_context_unsubscribe,
    .pulser_define = wrenrx_drv_wren_pulser_define,
    .pulser_program = wrenrx_drv_wren_pulser_program,
    .pulser_reconfigure = wrenrx_drv_wren_pulser_reconfigure,
    .pulser_force = wrenrx_drv_wren_pulser_force,
    .pulser_remove = wrenrx_drv_wren_pulser_remove,
    .pulser_get_config = wrenrx_drv_wren_pulser_get_config,
    .event_subscribe = wrenrx_drv_wren_event_subscribe,
    .event_unsubscribe = wrenrx_drv_wren_event_unsubscribe,
    .config_subscribe = wrenrx_drv_wren_subscribe_config,
    .config_unsubscribe = wrenrx_drv_wren_unsubscribe_config,
    .output_get = wrenrx_drv_wren_output_get,
    .output_set = wrenrx_drv_wren_output_set,
    .pin_get = wrenrx_drv_wren_pin_get,
    .pin_set = wrenrx_drv_wren_pin_set,
    .pulser_log_get = wrenrx_drv_wren_pulser_log_get,
    .pin_log_get = wrenrx_drv_wren_pin_log_get,
    .sw_log_get = wrenrx_drv_wren_sw_log_get,
    .hw_vme_p2a_set_output = wrenrx_drv_wren_hw_vme_p2a_set_output,
    .hw_patchpanel_set_config = wrenrx_drv_wren_hw_patchapanel_set_config,
    .get_fd = wren_drv_get_fd,
    .wait = wren_drv_wait,
};

struct wrenrx_drv_wrenctl_init_args {
    unsigned drv_idx;
};

struct wrenrx_handle *
wrenrx_open(const char *filename)
{
    struct wrenrx_handle_wrenctl *handle;
    const char *trace_filename = getenv("WREN_TRACE");

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    if (trace_filename != NULL) {
	if (*trace_filename == 0)
	    handle->tracef = stdout;
	else
	    handle->tracef = fopen(trace_filename, "w");
	if (handle->tracef == NULL) {
	    fprintf(stderr, "%s: cannot open trace file %s: %m\n",
		    progname, trace_filename);
	}
    }
    else
	handle->tracef = NULL;

    handle->base.op = &wrenrx_drv_wren_ops;

    handle->wren_fd = open(filename, O_RDWR);
    if (handle->wren_fd < 0) {
	fprintf (stderr, "cannot open %s: %m\n", filename);
	goto error;
    }

    if (wren_check_version(handle->wren_fd) < 0)
	goto error;

    for (unsigned i = 0; i < NBR_EVSUB; i++)
	handle->ev_sub[i].source_idx = INVALID_SOURCE_IDX;

    handle->evref_head = INVALID_IDX;
    handle->evref_ready = INVALID_IDX;
    handle->evref_tail = INVALID_IDX;
    handle->evref_free = 0;
    for (unsigned i = 0; i < NBR_EVSUB - 1; i++) {
	handle->ev_ref[i].evsub = INVALID_IDX;
	handle->ev_ref[i].chain = i + 1;
    }
    handle->ev_ref[NBR_EVSUB - 1].chain = INVALID_IDX;

    handle->evbuf_free = 0;
    for (unsigned i = 0; i < NBR_EV_BUF - 1; i++) {
	handle->ev_buf[i].ref = 0;
	handle->ev_buf[i].source_idx = i + 1;
    }
    handle->ev_buf[NBR_EV_BUF - 1].source_idx = INVALID_IDX;

    return (struct wrenrx_handle *)handle;

 error:
    if (handle->wren_fd >= 0)
	close (handle->wren_fd);
    free(handle);
    return NULL;
}

static void
wrenrx_get_lun_filename(char name[static 32], unsigned lun)
{
    snprintf(name, 32, "/dev/wren-lun%u", lun);
}

struct wrenrx_handle *
wrenrx_open_by_lun(unsigned lun)
{
    char name[32];

    wrenrx_get_lun_filename(name, lun);

    return wrenrx_open(name);
}

void
wrenrx_enumerate_luns(unsigned *arr, unsigned nbr)
{
    unsigned i;
    char name[32];

    for (i = 0; i < (nbr + 31) / 32; i++)
	arr[i] = 0;

    for (i = 0; i < nbr; i++) {
	wrenrx_get_lun_filename(name, i);
	if (access (name, F_OK) == 0)
	    arr[i >> 5] |= 1 << (i & 31);
    }
}

struct wrenrx_handle *
wrenrx_drv_wren_open(void *init)
{
    struct wrenrx_drv_wrenctl_init_args *res = init;
    char name[32];

    snprintf(name, sizeof(name), "/dev/wren%u", res->drv_idx);

    return wrenrx_open(name);
}

void *
wrenrx_drv_wren_init(int *argc, char *argv[])
{
    struct wrenrx_drv_wrenctl_init_args *res;

    res = malloc (sizeof(*res));
    if (res == NULL)
	return NULL;

    /* Save driver index (wrenX) */
    res->drv_idx = argv[1][4] - '0';

    /* Remove wrenX */
    wren_drv_remove_args (argc, argv, 1);

    if (*argc > 1) {
	if (strcmp(argv[1], "--")) {
	    fprintf(stderr, "%s: missing -- after driver name\n", progname);
	    return NULL;
	}
	wren_drv_remove_args (argc, argv, 1);
    }

    return res;
}
