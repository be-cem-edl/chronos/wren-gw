#include <string.h>
#include <stdlib.h>
#include "wrenrx-drv-evt.h"

void
wrenrx_drv_evt_init(struct wrenrx_handle_events *h)
{
    unsigned i;

    h->nbr_msg = 0;
    h->cur_msg = 0;

    for (i = 0; i < NBR_EVENTS; i++) {
	h->events[i].count = 0;
	h->events[i].src_idx = i + 1;
    }
    h->free_events = 0;
}

int
wrenrx_drv_evt_has_msg(const struct wrenrx_handle_events *h)
{
    return h->cur_msg < h->nbr_msg;
}

struct wrenrx_msg *
wrenrx_drv_evt_get_msg(struct wrenrx_handle_events *h)
{
    struct msg *m = &h->msgs[h->cur_msg];

    switch (m->kind) {
    case wrenrx_msg_event: {
	struct evt *ev = &h->events[m->idx];
	struct ctxt *ctxt = &h->ctxts[ev->src_idx][ev->ctxt_id & (NBR_CTXT-1)];
	struct wrenrx_msg_event *res = &h->smsg.u.evt.evt;

	/* TODO: check matching context */

	h->smsg.kind = wrenrx_msg_event;
	h->smsg.u.evt.source_idx = ev->src_idx;
	res->ev_id = ev->ev_id;
	res->ctxt_id = ev->ctxt_id;
	res->params_len = ev->param_len;
	res->ts = ev->ts;
	memcpy(res->params, ev->params, ev->param_len * 4);

	h->smsg.u.evt.ctxt.params_len = ctxt->len;
	memcpy(h->smsg.u.evt.ctxt.params, ctxt->params, ctxt->len * 4);
	
	/* Free the event */
	ev->count--;
	if (ev->count == 0) {
	    ev->src_idx = h->free_events;
	    h->free_events = m->idx;
	}
	break;
    }
    case wrenrx_msg_context: {
	unsigned src_idx = m->idx / NBR_CTXT;
	struct ctxt *ctxt = &h->ctxts[src_idx][m->idx & (NBR_CTXT-1)];
	struct wrenrx_msg_context *res = &h->smsg.u.ctxt.ctxt;

	h->smsg.kind = wrenrx_msg_context;
	h->smsg.u.ctxt.source_idx = src_idx;
	h->smsg.u.ctxt.ctxt_id = ctxt->ctxt_id;
	res->ts = ctxt->valid_from;
	res->params_len = ctxt->len;
	memcpy(res->params, ctxt->params, ctxt->len * 4);
	break;
    }
    default:
	abort();
    }

    h->cur_msg++;

    if (h->cur_msg == h->nbr_msg) {
	h->cur_msg = 0;
	h->nbr_msg = 0;
    }

    return &h->smsg;
}
