#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "wrenrx-drv-sim.h"
#include "wren-util.h"
#include "wren/wren-packet.h"
#include "wren-mb-defs.h"
#include "wrenrx-core.h"
#include "wrenrx-data.h"
#include "wrenrx-cond.h"

struct wrenrx_handle_sim *sim_handle;

#if 0
int
wrenrx_trigger_interrupt (struct wrenrx_handle *handle,
			  unsigned interrupt_id,
			  struct wrenrx_trigger *trig,
			  int delay_us)
{
  unsigned i;

  for (i = 0; i < NBR_ACTIONS; i++) {
    struct action *act = &handle->act[i];
    if (act->kind != ACT_NONE)
      continue;
    act->kind = ACT_INTER;
    act->trig = *trig;
    act->u.inter.id = interrupt_id;
    act->u.inter.delay_us = delay_us;

    return i;
  }
  return -1;
}
#endif

int
wrenrx_drv_sim_set_promisc(struct wrenrx_handle *handle, unsigned en)
{
    struct wrenrx_handle_sim *h = (struct wrenrx_handle_sim *)handle;

    h->promisc = en;

    return 0;
}

int
wrenrx_sim_pulser_define (struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  const struct wrenrx_cond *cond,
			  const struct wrenrx_pulser_configuration *conf,
			  const char *name)
{
    unsigned cond_idx, act_idx;
    struct wren_rx_cond *wcond;
    struct wren_rx_action *wact;
    struct wren_rx_source *src;
    int res;

    /* Sanity checks */
    if (cond->source_idx >= MAX_RX_SOURCES)
	return WRENRX_ERR_BAD_SOURCE;
    src = &wren_rx_sources[cond->source_idx];
    if (src->proto.proto == WREN_PROTO_NONE)
	return WRENRX_ERR_BAD_SOURCE;

    /* Find a action and a condition slots, fill them */
    wcond = NULL;
    for (cond_idx = 0; cond_idx < MAX_RX_CONDS; cond_idx++)
	if (wren_rx_conds[cond_idx].cond.evt_id == WREN_EVENT_ID_INVALID) {
	    wcond = &wren_rx_conds[cond_idx];
	    break;
	}
    if (wcond == NULL)
	return WRENRX_ERR_NO_CONDITION;

    wact = NULL;
    for (act_idx = 0; act_idx < MAX_RX_ACTIONS; act_idx++)
	if (wren_rx_actions[act_idx].cond_idx == NO_RX_COND_IDX) {
	    wact = &wren_rx_actions[act_idx];
	    break;
	}
    if (wact == NULL)
	return WRENRX_ERR_NO_ACTION;

    /* Fill condition */
    res = wrenrx_cond_to_mb(&wcond->cond, cond);
    if (res < 0)
	return res;
    wcond->act_idx = act_idx;

    /* Fill action */
    wrenrx_pulser_configuration_to_mb(&wact->conf, conf, pulser_num - 1);

    wact->next = NO_RX_ACT_IDX;
    wact->cond_idx = cond_idx;

    wcond->next = src->conds;
    src->conds = cond_idx;

    return act_idx;
}

int
wrenrx_sim_subscribe (struct wrenrx_handle *handle,
		      unsigned source_idx,
		      uint16_t ev_id,
		      int offset_us)
{
    struct wren_rx_source *src;

    if (source_idx >= MAX_RX_SOURCES)
	return WRENRX_ERR_BAD_SOURCE;
    src = &wren_rx_sources[source_idx];
    if (src->proto.proto == WREN_PROTO_NONE)
	return WRENRX_ERR_BAD_SOURCE;

    if (ev_id >= 32 * RX_SUBSCRIBE_MAP_LEN)
	return WRENRX_ERR_EVID_TOO_LARGE;

    if (offset_us != 0) {
	/* TODO */
	return -1;
    }

    src->subscribed_map[ev_id >> 5] |= 1 << (ev_id & 0x1f);

    return 0;
}

int
wrenrx_sim_context_subscribe(struct wrenrx_handle *handle,
			     unsigned source_idx)
{
    return 0;
}

void
wrenrx_drv_sim_init(struct wrenrx_handle_sim *handle)
{
  unsigned i;

  wrenrx_drv_evt_init(&handle->ev);

  handle->nbr_act = 0;
  for (i = 0; i < NBR_ACTIONS; i++)
    handle->act[i].kind = ACT_NONE;

  sim_handle = handle;

  wrenrx_init();
}

struct wrenrx_msg *
wrenrx_sim_wait(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_sim *h = (struct wrenrx_handle_sim *)handle;
    const unsigned hlen = sizeof (struct wren_packet_hdr_v1) / 4;
    union {
	unsigned char buf[1560];
	struct {
	    unsigned char eth[16];
	    struct wren_packet pkt;
	} wren;
    } u;
    static const unsigned char wren_eth_header[14] =
	{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  'W', 'e'};
    struct wren_packet_hdr_v1 *hdr = &u.wren.pkt.hdr;
    struct wren_pkt_buf buf;

    while (1) {
	int len;
	int src;

	if (wrenrx_drv_evt_has_msg(&h->ev)) {
	    return wrenrx_drv_evt_get_msg(&h->ev);
	}

	buf.buf = u.buf;
	buf.frame_off = 2;

	len = wrenrx_drv_read_packet (handle, &u.wren.pkt);

	/* End of file */
	if (len == 0)
	    return NULL;

	if (len < hlen * 4 || len < (hdr->len * 4)) {
	    struct wrenrx_msg *msg = &h->ev.smsg;
	    msg->kind = wrenrx_msg_network_error;
	    return msg;
	}

	if (h->promisc) {
	    struct wrenrx_msg *msg = &h->ev.smsg;

	    msg->kind = wrenrx_msg_packet;
	    msg->u.packet.source_idx = ~0;
	    msg->u.packet.len = len / 4;
	    memcpy(msg->u.packet.buf, &u.wren.pkt, len);
	    return msg;
	}

	/* Add ethernet header */
	memcpy (u.buf + 2, wren_eth_header, sizeof(wren_eth_header));
	buf.len = 14 + hdr->len * 4;

	/* Handle the packet */
	src = wrenrx_filter_packet(&buf);
	if (src >= 0) {
	    wrenrx_handle_packet(&buf, src);
	}
    }
}

void
wrenrx_log (const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    if (0)
	vfprintf(stderr, fmt, args);
    va_end(args);
}

/* Called by wrenrx-core when a received event has to be sent to the user */
void
wrenrx_run_event(unsigned src_idx, struct wren_capsule_event_hdr *evt)
{
    struct msg *msg;
    struct evt *dst;

    if (sim_handle->ev.nbr_msg > NBR_MSGS
	|| sim_handle->ev.free_events == NBR_EVENTS) {
	printf("discard event %u\n", evt->ev_id);
	return;
    }

    /* Fill msg */
    msg = &sim_handle->ev.msgs[sim_handle->ev.nbr_msg++];
    msg->kind = wrenrx_msg_event;
    msg->idx = sim_handle->ev.free_events;
    dst = &sim_handle->ev.events[msg->idx];
    sim_handle->ev.free_events = dst->src_idx;

    /* Fill event */
    dst->count = 1;
    dst->src_idx = src_idx;
    dst->ev_id = evt->ev_id;
    dst->ctxt_id = evt->ctxt_id;
    dst->ts.sec = evt->ts.sec;
    dst->ts.nsec = evt->ts.nsec;
    dst->param_len = evt->hdr.len - sizeof (struct wren_capsule_event_hdr) / 4;
    memcpy (dst->params, evt + 1, dst->param_len * 4);
}

void
wrenrx_run_context(unsigned src_idx, struct wren_rx_context *ctxt)
{
    /* Save context */
    struct msg *msg;
    unsigned ctxt_id = ctxt->ctxt_id;
    struct ctxt *dst;

    if (src_idx >= NBR_SRCS) {
	printf("discard context %u for source %u\n", ctxt_id, src_idx);
	return;
    }

    /* Save the context */
    dst = &sim_handle->ev.ctxts[src_idx][ctxt_id & (NBR_CTXT - 1)];
    dst->ctxt_id = ctxt_id;
    dst->valid_from.sec = ctxt->valid_from.sec;
    dst->valid_from.nsec = ctxt->valid_from.nsec;
    dst->len = ctxt->len;
    memcpy (dst->params, ctxt->params, ctxt->len * 4);

    if (sim_handle->ev.nbr_msg > NBR_MSGS) {
	printf("discard context %u\n", ctxt_id);
	return;
    }

    /* Fill msg */
    msg = &sim_handle->ev.msgs[sim_handle->ev.nbr_msg++];
    msg->kind = wrenrx_msg_context;
    msg->idx = (ctxt_id & (NBR_CTXT - 1)) + src_idx * NBR_CTXT;
}

void
wrenrx_run_action (unsigned src_idx, unsigned act_idx,
		   struct wren_capsule_event_hdr *evt)
{
    printf("> interrupt %u\n", act_idx + 1);
}

void
wrenrx_now(unsigned *sec, unsigned *nsec)
{
    /* Used only for log */
    *sec = 0;
    *nsec = 0;
}

int
wrenrx_sim_add_source(struct wrenrx_handle *handle,
		      struct wren_protocol *proto)
{
    int i;
    struct wren_rx_source *src = NULL;

    /* TODO: try to find existing source */

    for (i = 0; i < MAX_RX_SOURCES; i++)
	if (wren_rx_sources[i].proto.proto == WREN_PROTO_NONE) {
	    src = &wren_rx_sources[i];
	    break;
	}
    if (src == NULL)
	return WRENRX_ERR_NO_SOURCE;

    src->proto = *proto;
    src->next_seqid = PKT_SEQ_SYNC;
    src->dest = RX_DEST_WREN;
    src->subsample = 1;
    src->subsample_cnt = 0;

    return i;
}
