#ifndef __WREN_PACKET__H__
#define __WREN_PACKET__H__

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Note: the protocol is defined using C struct.
   Layout follows the common convention between ARM and x86/64:
   - little endian
   - uint32_t/int32_t are aligned to 4
   - uint16_t are aligned to 2
   - uint8_t are aligned to 1
   - there is no holes in the structures
*/

/* Current version of the protocol */
#define PKT_VERSION_fe 0xfe
#define PKT_VERSION_v1 0x01

/* Sequence id.
   2 LSB are reserved for a possible implementation of a repetition algorithm.
   1 MSB is reserved for special purpose (resync id, invalid id...).
   
   The next sequence id is the current one plus PKT_SEQ_INC, then masked. */
#define PKT_SEQ_INC  0x0004
#define PKT_SEQ_MASK 0x7ffc

/* Special id to resync.  */
#define PKT_SEQ_SYNC 0xfffc

/* Invalid sequence id, must never be transmitted */
#define PKT_SEQ_INVALID 0xfff8

/* Commands.  */
#define PKT_CTXT  0x01
#define PKT_EVENT 0x02

/* Timestamp for packets (2w).  */
struct wren_packet_ts {
  /* Note: the fields are in little endian order (so nsec first), so that it
     is easier to add a value on a stream.  */
  uint32_t nsec;
  uint32_t sec;
};

#if 0
/* Packet header version 0xfe (5w).
   Note: it should be shorter than the minimum ethernet frame length so there
   is not need to check the length. */
struct wren_packet_hdr_fe {
  uint8_t version;
  uint8_t source;
  uint8_t pad0;
  uint8_t pad1;

  /* Length of the frame (in words) - includes header and body.  */
  uint16_t len;

  /* Sequence id.  */
  uint16_t seq_id;

  /* Time before next packet.  */
  uint32_t next_us;

  /* Send timestamp, to measure transmission time.  */
  struct wren_packet_ts snd_ts;
};
#endif

/* Packet header version 1 (3w). */
struct wren_packet_hdr_v1 {
  uint8_t version;
  uint8_t domain;
  uint8_t xmit;
  uint8_t next; /* Time before next packet (2**(next & 0x1f) us) */

  /* Length of the frame (in words) - includes header and body.  */
  uint16_t len;

  /* Sequence id.  */
  uint16_t seq_id;

  /* Send timestamp, to measure transmission time.
     22b for .256us + 2b of tai */
  uint32_t snd_ts;
};

/* Common capsule header (1w). */
struct wren_capsule_hdr {
  uint8_t typ;
  uint8_t pad;
  /* Length of the capsule including the header (in words).  */
  uint16_t len;
};

/* Widely used */
union wren_capsule_hdr_un {
  struct wren_capsule_hdr hdr;
  uint32_t u32;
};

/* Context capsule (cmd=1, 4w + params). */
struct wren_capsule_ctxt_hdr {
  struct wren_capsule_hdr hdr;

  uint16_t ctxt_id;
  uint16_t pad;

  struct wren_packet_ts valid_from;

  /* Followed by parameters */
};

/* Event capsule (cmd=2, 4w + params). */
struct wren_capsule_event_hdr {
  struct wren_capsule_hdr hdr;

  uint16_t ev_id;
  uint16_t ctxt_id;

  /* Action timestamp; usually in the future.  */
  struct wren_packet_ts ts;

  /* Followed by parameters */
};

struct wren_packet_param_hdr {
  /* Parameter header:
     len:       bits 9-0  (number of 32b words, including the header)
     data-type: bits 15-10
     param id:  bits 31-16.  */
  uint32_t hdr;

  /* Followed by LEN - 1 words of data */
};

/* Parameter data-type. */
#define PKT_PARAM_S32 0x1
#define PKT_PARAM_U32 0x2
#define PKT_PARAM_S64 0x3
#define PKT_PARAM_U64 0x4
#define PKT_PARAM_F32 0x5

#define WREN_PACKET_PARAM_GET_LEN(hdr) ((hdr) & 0x3ff)
#define WREN_PACKET_PARAM_GET_DT(hdr)  (((hdr) >> 10) & 0x3f)
#define WREN_PACKET_PARAM_GET_TYP(hdr) (((hdr) >> 16) & 0xffff)

#define WREN_PACKET_MAX_WORDS (1500 / 4)

/* Maximum length of parameters for a context.
   As context are stored in memory, it's better not to waste too much memory */
#define WREN_CONTEXT_MAX_PARAM_WORDS (512 / 4)

#define WREN_PACKET_HDR_WORDS (sizeof (struct wren_packet_hdr_v1) / 4)
#define WREN_PACKET_DATA_MAX_WORDS \
  (WREN_PACKET_MAX_WORDS - WREN_PACKET_HDR_WORDS)

/* A wren_packet contains an header and capsules.
   A capsule can be either an event or a context.
   A capsule usually contains parameters.  */
struct wren_packet {
  struct wren_packet_hdr_v1 hdr;
  union {
    /* Use unsigned char for the body to follow C aliasing rules.
       So it is possible to cast an address to a pointer of an type. */
    unsigned char b[WREN_PACKET_DATA_MAX_WORDS * 4];
    uint32_t w[WREN_PACKET_DATA_MAX_WORDS];
  } u;
};

#ifdef __cplusplus
}
#endif

#endif /* __WREN_PACKET__H__ */
