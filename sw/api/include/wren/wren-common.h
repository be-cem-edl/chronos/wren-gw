/* WhiteRabbit Timing common definitions.  */

/** \mainpage
  WRT - WhiteRabbit Timing - is a system to transmit timing events over
  a WhiteRabbit network.

  It is composed of two types of devices: the event transmitters and the
  timing receivers.
*/

#ifndef __WREN_COMMON__H__
#define __WREN_COMMON__H__

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/** \file wrt/wrt-common.h
    \brief Definitions used by both the event transmitter and the receiver
*/

/** TAI timestamp.
   Because SEC is a 32b unsigned integer, the largest time is about
   year 2090.
   And it allows to easily handle carry if adding/substracting nanoseconds.
*/
struct wren_ts {
  /** \brief Number of seconds since the epoch */
  uint32_t sec;

  /** \brief Number of nanoseconds */
  uint32_t nsec;
};

#define WREN_NS_PER_SEC 1000000000

/** Add nano seconds to a timestamp.

    \todo add substract
    \todo convert between ts and nanosecond

    @param[in] nsec The number of nanoseconds to be added,  must be less
       than 1 sec.
    @param[in,out] ts The timestamp to be modified.
*/
void wren_add_nsec(struct wren_ts *ts, unsigned nsec);

/** \brief Domain identifier

    A domain id represented at the frame level.
 */
typedef uint8_t wren_domain_id;

typedef uint8_t wren_xmit_id;


#define WREN_SOURCE_ID_INVALID 0xff

/** \brief Context identifier

    An event message can be associated to a context, identified by a number.
    A context identifier is valid only for a specific source.
    There is only a limited number of alive context.
*/
typedef uint8_t wren_context_id;

/** \brief Maximum value for a valid context identifier

    A context id must be between 0 and 127
*/
#define WREN_CONTEXT_MAX 0x7f

/** \brief Special context identifier for no context

    An event message with this context id is not associated to any
    context
*/
#define WREN_CONTEXT_NONE 0xff


/** \brief special context identifier for not yet known

    The wrentx will automatically replace this context with the current
    one.
 */
#define WREN_CONTEXT_CURRENT 0xfe

/** \brief Event identifier

    This is a number that identifies the event
 */
typedef uint16_t wren_event_id;

/** \brief Maximum event identifier

    The number of event id is limited (in order to allow use of maps).
 */
#define WREN_EVENT_ID_MAX 0x1ff

/* An event id which is invalid (or never used).
   Could be used to return error
 */
#define WREN_EVENT_ID_INVALID 0xffff

/** \brief Parameter identifier

    This is a number that identifies parameters
*/
typedef uint16_t wren_param_id;

/** \brief Maximum parameter identifier value
 */
#define WREN_PARAM_ID_MAX 0x1ff

/* Maximum number of bytes (including the terminating NUL) for a configuration
   name */
#define WRENRX_CONFIGURATION_NAME_MAXLEN 64

/* Maximum number of bytes (including the terminating NUL) for a table
   name */
#define WRENTX_TABLE_NAME_MAXLEN 64

/* A default ethertype for wren */
#define WREN_ETHERTYPE (('W' << 8) | 'e')

/** \brief Protocol used over WRT
 */
struct wren_protocol {
  /** \brief Protocol selection.
      One of WREN_PROTO_ constant
  */
  uint8_t  proto;

  union {
    struct wren_proto_eth {
      /** \brief VLAN use */
      uint8_t  flags;

      /** \brief MAC address */
      uint8_t  mac[6];

      /** \brief VLAN identifier */
      uint16_t vlan;

      /** \brief Ethertype */
      uint16_t ethertype;

      /** \brief Domain identifier for the messages.  */
      wren_domain_id domain_id;
      wren_xmit_id xmit_id;
    } eth;
  } u;
};

/** \brief Raw ethernet protocol

    Need to specify mac, vlan and ethertype.
 */
#define WREN_PROTO_NONE     0
#define WREN_PROTO_ETHERNET 1

/* If the flag is set, filter on the mac address. */
#define WREN_PROTO_ETH_FLAGS_MAC  (1 << 0)

/* If the flag is set, tx: set vlan, rx: accept vlan. */
#define WREN_PROTO_ETH_FLAGS_VLAN (1 << 1)

/* If the flag is set, tx: N/A, rx: check vlan id. */
#define WREN_PROTO_ETH_FLAGS_VLANID (1 << 2)

/* If the flag is set, tx: N/A, rx: accept no-vlan even if FLAGS_VLAN is set. */
#define WREN_PROTO_ETH_FLAGS_NOVLAN (1 << 3)

/* Front and patch panel configuration. */
struct wren_panel_config {
  /* Total number of connection on the front-panel. */
  uint32_t fp_pin;

  /* Number of fixed inputs.  Those are the first connectors and are
     masking the first pulser group */
  uint32_t fp_fixed_inp;

  /* Number of connectors on the patch panel.  Should be 0 or 32.
     The connectors are output only. */
  uint32_t pp_pin;

  /* Byte enable for vme P2 raw A */
  uint32_t vme_p2a_be;
};

/* Parameter origin */
#define WRENRX_PARAM_CONTEXT (1 << 0)
#define WRENRX_PARAM_EVENT   (1 << 1)

/* Errors */

/* System error (errno is set) */
#define WRENRX_ERR_SYSTEM -2

/* No sources available (cannot be added) */
#define WRENRX_ERR_NO_SOURCE -3

/* No conditions available (cannot be added) */
#define WRENRX_ERR_NO_CONDITION -4

/* No actions available (cannot be added) */
#define WRENRX_ERR_NO_ACTION -5
#define WRENRX_ERR_NO_CONFIG -5

/* No event subscription slot */
#define WRENRX_ERR_NO_EVENT_SUBSCRIBE -6

/* The condition is too complex */
#define WRENRX_ERR_CONDITION_TOO_LONG -7

/* Invalid source id (or source not created) */
#define WRENRX_ERR_BAD_SOURCE -8

/* Incorrect protocol */
#define WRENRX_ERR_BAD_PROTOCOL -9

/* Pulser number is invalid */
#define WRENRX_ERR_PULSER_NUM -10

/* Output number is invalid */
#define WRENRX_ERR_OUTPUT_NUM -11

/* Pin number is invalid */
#define WRENRX_ERR_PIN_NUM -12

/* Evsub id is invalid (not used) */
#define WRENRX_ERR_EVSUBID -13

/* Incorrect source id (beyond the limit) */
#define WRENRX_ERR_SRCID_TOO_LARGE -14

/* Event id is too large */
#define WRENRX_ERR_EVID_TOO_LARGE -15

/* Config id is too large */
#define WRENRX_ERR_CFGID_TOO_LARGE -16

/* Condition id is too large */
#define WRENRX_ERR_CONDID_TOO_LARGE -17

/* Comparator id is too large */
#define WRENRX_ERR_COMPID_TOO_LARGE -18

/* Pulser id is too large */
#define WRENRX_ERR_PULSERID_TOO_LARGE -19

/* Output id is too large */
#define WRENRX_ERR_OUTPUTID_TOO_LARGE -20

/* Pin id is too large */
#define WRENRX_ERR_PINID_TOO_LARGE -21

/* Action id is too large */
#define WRENRX_ERR_ACTIONID_TOO_LARGE -22

/* Log id is too large */
#define WRENRX_ERR_LOGID_TOO_LARGE -23

/* Evsub id is too large */
#define WRENRX_ERR_EVSUBID_TOO_LARGE -24

/* Comparator is busy */
#define WRENRX_ERR_COMPARATOR_BUSY -25

/* Action/config is busy */
#define WRENRX_ERR_ACTION_BUSY -26

/* Condition is busy */
#define WRENRX_ERR_COND_BUSY -27

/* Action is unused */
#define WRENRX_ERR_ACTION_UNUSED -28

/* Bad number of entries */
#define WRENRX_ERR_BAD_NENTRIES -29

/* Condition is too long (too many operations) */
#define WRENRX_ERR_INVALID_COND -30

/* Condition is unused */
#define WRENRX_ERR_COND_UNUSED -31

/* Evlog index is incorrect */
#define WRENRX_ERR_EVLOG_TOO_LARGE -32

/* Evlog type is incorrect */
#define WRENRX_ERR_EVLOG_BAD_TYPE -33

/* Event already subscribed */
#define WRENRX_ERR_EVID_SUBSCRIBED -34

/* Name is too long */
#define WRENRX_ERR_NAME_TOO_LONG -35

/* No time (because no sync) */
#define WRENRX_ERR_NO_TIME -36

/* Internal error: message length is incorrect */
#define WRENRX_ERR_BAD_MSG_LENGTH -99


/* Maximum number of instructions in a table */
#define WRENTX_MAX_INSNS 16

/* Maximum number of data words for frames */
#define WRENTX_MAX_DATA 32


/** \brief Success */
#define WRENTX_SUCCESS 0

/** \brief Link or synchronization has been lost */
#define WRENTX_ERR_NO_SYNC -1

/** \brief Transmitter is controled by the embedded CPU */
#define WRENTX_ERR_BUSY -2

/** \brief Source index is incorrect (bad index or not initialized) */
#define WRENTX_ERR_BAD_SOURCE_IDX -3

/** \brief Protocol is incorrect */
#define WRENTX_ERR_BAD_PROTOCOL -4

/** \brief Frame is full */
#define WRENTX_ERR_FRAME_FULL -5

/** \brief Incorrect construction of a frame (context after event,
    parameters before a context or an event) */
#define WRENTX_ERR_FRAME_CONSTRUCT -6

/** \brief Table is full, no instruction could be added */
#define WRENTX_ERR_TABLE_FULL -7

/** \brief Table index is incorrect (bad index or not initialized) */
#define WRENTX_ERR_BAD_TABLE_IDX -8

/** \brief Table index designates an empty table */
#define WRENTX_ERR_EMPTY_TABLE -9

/* Cannot transmit, queue is full */
#define WRENTX_ERR_QUEUE_FULL -10

#define WRENTX_ERR_SYSTEM -11

/* Context has too many parameters */
#define WRENTX_ERR_CONTEXT_TOO_LONG -12

/* Name is too long */
#define WRENTX_ERR_NAME_TOO_LONG -13

/* Table is busy and cannot be reload */
#define WRENTX_ERR_BUSY_TABLE -14

#ifdef __cplusplus
}
#endif

#endif /* __WREN_COMMON__H__ */
