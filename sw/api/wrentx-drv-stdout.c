#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wrentx-drv-sim.h"

void *
wrentx_drv_init_stdout(int *argc, char *argv[])
{
  /* argv[0] is progname, argv[1] is 'stdout',
     so argv[2] is '--' */
  if (*argc > 2) {
    if (strcmp(argv[2], "--")) {
      fprintf(stderr, "%s: missing -- after %s\n", progname, argv[1]);
      return NULL;
    }
    wren_drv_remove_args (argc, argv, 2);
  }

  /* Non-NULL */
  return (void *)1;
}

struct wrentx_handle *
wrentx_drv_open_stdout(void *init)
{
  struct wrentx_handle *handle;

  handle = wrentx_open_sim_fd(&wrentx_sim_ops_fd_vt, 1);
  if (handle == NULL)
    fprintf (stderr, "%s: cannot open stdout\n", progname);
  return handle;
}
