#include "wren/wren-common.h"
#include "wren-mb-defs.h"
#ifdef __KERNEL__
#include <linux/string.h>
#else
#include <string.h>
#endif

int
wren_is_same_proto(const struct wren_protocol *l, const struct wren_protocol *r)
{
    if (l->proto != r->proto)
	return 0;
    switch (l->proto) {
    case WREN_PROTO_NONE:
	return 1;
    case WREN_PROTO_ETHERNET:
	if (l->u.eth.ethertype != r->u.eth.ethertype)
	    return 0;
	if (l->u.eth.domain_id != r->u.eth.domain_id)
	    return 0;
	if (l->u.eth.xmit_id != r->u.eth.xmit_id)
	    return 0;
	if (l->u.eth.flags != r->u.eth.flags)
	    return 0;
	if ((l->u.eth.flags & WREN_PROTO_ETH_FLAGS_MAC)
	    && memcmp (l->u.eth.mac, r->u.eth.mac, sizeof(l->u.eth.mac)))
	    return 0;
	if ((l->u.eth.flags & WREN_PROTO_ETH_FLAGS_VLAN)
	    && l->u.eth.vlan != r->u.eth.vlan)
	    return 0;
	return 1;
    default:
	return 0;
    }
}

int
wren_is_same_cond (const struct wren_mb_cond *l, const struct wren_mb_cond *r)
{
    unsigned i;

    if (l->evt_id != r->evt_id)
	return 0;
    if (l->src_idx != r->src_idx)
	return 0;
    if (l->len != r->len)
	return 0;
    for (i = 0; i < l->len; i++)
	if (l->ops[i].vu32 != r->ops[i].vu32)
	    return 0;
    return 1;
}

