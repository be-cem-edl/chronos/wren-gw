action = "synthesis"

syn_device = "xczu4cg"
syn_grade = "-1-e"
syn_package = "-sfvc784"
syn_top = "wren_pxie_top"
syn_project = "wren-pxie"
syn_tool = "vivado"

target = "xilinx"

# For WR:
# do not use a predefined board
board = 'none'
# do not use a predefined platform
wrcore_platform=False

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../../dependencies"

files = [
    'buildinfo_pkg.vhd',
    'wren-pxie.xdc', 'gencores_constraints.xdc',
    'ila_0.xci']

modules = {
    'local': ['../../top/pxie'],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
    ],
    'system': ['xilinx', 'vhdl']
}

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  print("failed to run gen_buildinfo.py")
