##################
# Clocks
##################

# Main clock (250Mhz)
create_clock -period 4.000 -name wr_clk_sfp_125m [get_ports wr_clk_sfp_125m_p_i]

# Helper (62.5Mhz)
create_clock -period 16.000 -name fpga_pl_clksys_125m [get_ports fpga_pl_clksys_p_i]

# Unused
create_clock -period 8.000 -name wr_clk_helper_125m -waveform {0.000 4.000} [get_ports wr_clk_helper_125m_p_i]
create_clock -period 8.000 -name wr_clk_main_125m -waveform {0.000 4.000} [get_ports wr_clk_main_125m_p_i]

# Bunch clocks (~40Mhz)
create_clock -period 20.000 -name gth_bclk1 [get_ports gth_bclk1_p_i]
create_clock -period 20.000 -name gth_bclk2 [get_ports gth_bclk2_p_i]

create_generated_clock -name clk_sys_62m5 [get_pins inst_platform/blk_clock.inst_MMCME4_BASE/CLKOUT0]

create_generated_clock -name gth_rxclk [get_pins {inst_platform/b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[1].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST/RXOUTCLK}]

create_generated_clock -name gth_txclk [get_pins {inst_platform/b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[1].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST/TXOUTCLK}]

# All the clocks are asynch
set_clock_groups -asynchronous -group {wr_clk_main_125m fpga_pl_clksys_125m wr_clk_sfp_125m} -group wr_clk_helper_125m -group clk_helper_25m -group gth_rxclk -group gth_txclk -group clk_sys_62m5 -group gth_bclk1 -group gth_bclk2

# FIXME
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets blk_clock.inst_ibuf_helper25m/O]


##################
# I/O constraints
##################



# set_property SLEW FAST [get_ports {trigio_out_o[*]}]


# config options
set_property BITSTREAM.CONFIG.OVERTEMPSHUTDOWN ENABLE [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
# set_property BITSTREAM.GENERAL.JTAG_SYSMON DISABLE [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLNONE [current_design]
