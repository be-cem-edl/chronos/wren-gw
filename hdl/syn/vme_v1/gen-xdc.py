#!/bin/env python3

def bus_split(name):
    for i in range(len(name) - 1, 0, -1):
        if not name[i].isdigit():
            return name[0:i+1], name[i+1:]
    return None, None

# HD
pin_map_33 = {
    "/vmef.as_n": "vme_as_n_b",
    "/vmef.as_oe_n": "vme_as_oe_n_o",
    "/vmef.as_n_dir": "vme_as_dir_o",
    "/vmef.write_n": "vme_write_n_b",
    "/vmef.write_n_dir": "vme_write_dir_o",
    "/vmef.ds0_n_in":  "vme_ds_n_i[0]",
    "/vmef.ds0_n_out": "vme_ds_n_o[0]",
    "/vmef.ds1_n_in":  "vme_ds_n_i[1]",
    "/vmef.ds1_n_out": "vme_ds_n_o[1]",
    "/vmef.ds_n_oe": "vme_ds_oe_o",
    "/vmef.a*": "vme_addr_b[*]",
    "/vmef.a_dir": "vme_addr_dir_o",
    "/vmef.a_oe_n": "vme_addr_oe_n_o",
    "/vmef.am*": "vme_am_b[*]",
    "/vmef.lword_n": "vme_lword_n_b",
    "/vmef.dtack_n_in": "vme_dtack_n_i",
    "/vmef.dtack_n_out": "vme_dtack_n_o",
    "/vmef.dtack_oe": "vme_dtack_oe_o",
    "/vmef.retry_in": "vme_retry_n_i",
    "/vmef.retry_oe": "vme_retry_oe_o",
    "/vmef.retry_out": "vme_retry_n_o",
    "/vmef.berr_in": "vme_berr_n_i",
    "/vmef.berr_out": "vme_berr_n_o",
    "/vmef.berr_oe":   "vme_berr_oe_o",
    "/vmef.iack_n_in": "vme_iack_n_i",
    "/vmef.iack_n_out": "vme_iack_n_o",
    "/vmef.iack_oe_n": "vme_iack_oe_n_o",
    "/vmef.iackout_n_out": "vme_iackout_n_o",
    "/vmef.iackin_n_in": "vme_iackin_n_i",
    "/vmef.sysclk": "vme_sysclk_b",
    "/vmef.irq_in_n*": "vme_irq_n_i[*]",
    "/vmef.irq_out_n*": "vme_irq_n_o[*]",
    "/vmef.sysreset_n": "vme_sysreset_n_b",
    "/vmef.bg3in": "vme_bg3in_n_i",
    "/vmef.bg3out": "vme_bg3out_n_o",
    "/vmef.br3": "vme_br3_n_o",
    "/vmef.bbsy": "vme_bbsy_b",
    "/vmef.bbsy_oe": "vme_bbsy_oe_o",
    "/sfp0_sda": "sfp0_sda_b",
    "/sfp1_sda": "sfp1_sda_b",
    "/sfp0_scl": "sfp0_scl_b",
    "/sfp1_scl": "sfp1_scl_b",

    "/f_p0.rs485_data0": "p2_rs485_d0_b",
    "/f_p0.rs485_data1": "p2_rs485_d1_b",

    "fpga-hd-banks/f_sfp_led_data": "sfp_led_o",
}

# HR
pin_map_18 = {
    "/wr_clk.abscal": "wr_abscal_o",
    "/wr_clk.pps": "wr_pps_o",
    "/wrflash_i2c.scl": "eeprom_scl_b",
    "/wrflash_i2c.sda": "eeprom_sda_b",
    "f_wr_uart_rxd": "uart_rxd_i",
    "f_wr_uart_txd": "uart_txd_o",
    "f_wr_dac_sync": "plldac_sync_n_o",
    "f_wr_dac_din":  "plldac_din_o",
    "f_wr_dac_sclk": "plldac_sclk_o",
    
    "f_ioexp_vme_sclk": "ioexp_vme_sclk_o",
    "f_ioexp_vme_rclk": "ioexp_vme_rclk_o",
    "f_ioexp_vme_d": "ioexp_vme_d_o",
    "f_ioexp_hwbyte2_d": "ioexp_hwbyte2_d_o",
    "f_ioexp_hwbyte2_rclk": "ioexp_hwbyte2_rclk_o",
    "f_ioexp_hwbyte1_d": "ioexp_hwbyte1_d_o",
    "f_ioexp_hwbyte1_rclk": "ioexp_hwbyte1_rclk_o",
    
    "f_ioexp_fpio_rclk": "ioexp_fpio_rclk_o",
    "f_ioexp_fpio_sclk": "ioexp_fpio_sclk_o",
    "f_ioexp_fpio_d": "ioexp_fpio_d_o",

    "f_ioexp_gpio_rclk": "ioexp_gpio_rclk_o",
    "f_ioexp_gpio_sclk": "ioexp_gpio_sclk_o",
    "f_ioexp_gpio_d": "ioexp_gpio_d_o",
    "f_ioexp_gpio_q": "ioexp_gpio_q_i",
    
    "f_ioexp_all_rst_n": "ioexp_all_rst_n_o",
    "f_ioexp_all_oe_n": "ioexp_all_oe_n_o",

    "f_fp_led0_data": "fp_led0_o",
    "f_fp_led1_data": "fp_led1_o",
    "f_pp_led0_data": "pp_led0_o",
    "f_pp_led1_data": "pp_led1_o",

    "/led_sfp0_green": "led_sfp0_green_o",
    "/led_sfp0_yellow": "led_sfp0_yellow_o",
    "/led_sfp1_green": "led_sfp1_green_o",
    "/led_sfp1_yellow": "led_sfp1_yellow_o",

    "/wr_clk.spi_main_cs_n": "spi_main_cs_n_o",
    "/wr_clk.spi_mosi": "spi_mosi_o",
    "/wr_clk.spi_miso": "spi_miso_i",
    "/wr_clk.spi_sck": "spi_sck_o",

    "/userio_a.io*": "userio_b[*]",
    "/f_io*": "fp_io_b[*]",

    "f_perst_n": "perst_n_i",
    
    "/f_p0.ttc_data0": "p2_ttc_d0_o",
    "/f_p0.ttc_data1": "p2_ttc_d1_o",
    "/f_p0.turnclk1": "p0_turnclk_o[1]",
    "/f_p0.turnclk2": "p0_turnclk_o[2]",
    "/f_p0.bunchclk1": "p0_bunchclk_o[1]",
    "/f_p0.bunchclk2": "p0_bunchclk_o[2]",

    "/vmef.d*": "vme_data_b[*]",
    "/vmef.d_oe_n": "vme_data_oe_n_o",
    "/vmef.d_dir": "vme_data_dir_o",
}

pin_map_sfp = {
    "/sfp1.tx_n": "sfp1_txn_o",
    "/sfp1.tx_p": "sfp1_txp_o",
    "/sfp1.rx_n": "sfp1_rxn_i",
    "/sfp1.rx_p": "sfp1_rxp_i",
    "/sfp0.tx_n": "sfp0_txn_o",
    "/sfp0.tx_p": "sfp0_txp_o",
    "/sfp0.rx_n": "sfp0_rxn_i",
    "/sfp0.rx_p": "sfp0_rxp_i",
    "/pcie.refclk_n": "pcie_clk_n_i",
    "/pcie.refclk_p": "pcie_clk_p_i",
    "/wr_clk.sfp_n": "wr_clk_sfp_125m_n_i",
    "/wr_clk.sfp_p": "wr_clk_sfp_125m_p_i",

    "fpga-mgts/sansdac_bclk_b1_n": "bclk_b1_n_i",
    "fpga-mgts/sansdac_bclk_b1_p": "bclk_b1_p_i",
    "fpga-mgts/sansdac_bclk_b2_n": "bclk_b2_n_i",
    "fpga-mgts/sansdac_bclk_b2_p": "bclk_b2_p_i",
}

pin_map_lvds = {
    "/wr_clk.main_n": "wr_clk_main_125m_n_i",
    "/wr_clk.main_p": "wr_clk_main_125m_p_i",
    "/wr_clk.aux_n": "wr_clk_aux_n_i",
    "/wr_clk.aux_p": "wr_clk_aux_p_i",
    "/wr_clk.helper_p": "wr_clk_helper_125m_p_i",
    "/wr_clk.helper_n": "wr_clk_helper_125m_n_i",
    "fpga-pl_clksys_n": "fpga_pl_clksys_n_i",
    "fpga-pl_clksys_p": "fpga_pl_clksys_p_i",
    "gth_bclk_b2_p": "gth_bclk_b2_p_o",
    "gth_bclk_b2_n": "gth_bclk_b2_n_o",
    "gth_bclk_b1_p": "gth_bclk_b1_p_o",
    "gth_bclk_b1_n": "gth_bclk_b1_n_o",
}
pin_ignore = [
    # Power
    'mgt_1v8', "mgt_0v85", "mgt_0v9", "mgt_1v2",
    "vcc_psadc", "psadc_agnd", "vcc_psddr_pll", "vcc_pspll",
    "gnd", "p3v3", "p0v85", "p1v8", "p1v2",
    
    # unused
    "hp9", "hp4", "hp132", "hp86", "hp98", "hp60", "hp51",
    "hp72", "hp50", "hp54", "hp55", "hp76", "hp80", "hp81",
    "hp82", "hp97",

    # PS
    "ps_uart_txd",
    "ps_uart_rxd",
    "ps_por_b",
    "/i2c.scl",
    "/i2c.sda",
    "/wr_clk.rstn",
    "/wr_clk.si5340_irq_n",
    "/clocks/fpga-ps_clkref",
    "fpga-mio/pcb_rev0",
    "fpga-mio/pcb_rev1",
    "fpga-mio/pcb_rev2",
    "/vme_sfp_sda",  # for rtm
    "/vme_sfp_scl",
    "fpga-mio/frtm_reset",

    # LVDS
]

def translate_with_map(name, pmap):
    for k, v in pmap.items():
        if k[-1] == '*':
            name1, idx = bus_split(name)
            if name1 == k[:-1]:
                return v.replace('*', idx)
        elif k == name:
            return v
    return None

def translate(name):
    for pmap, iostd in [(pin_map_33, "LVCMOS33"),
                        (pin_map_18, "LVCMOS18"),
                        (pin_map_sfp, None),
                        (pin_map_lvds, "LVDS")]:
        res = translate_with_map(name, pmap)
        if res is not None:
            if res[-1] == ']':
                res = '{' + res + '}'
            return res, iostd
    return None, None

with open("wren-vme.net") as f:
    for l in f:
        (_, pin, name, _) = l.split()
        name = name.lower()

        for pfx in ["/fpga/fpga-hp-banks/", "/io_drivers_fp/", "/fpga/"]:
            if name.startswith(pfx):
                name = name[len(pfx):]
                break
            
        if (name.startswith("/ps-qspi")
            or name.startswith("fpga-config/")
            or name.startswith("/ddr4-ps")
            or name.startswith("unconnected-")
            or name.startswith("net-")):
            continue
        if name in pin_ignore:
            continue
        nname, iostd = translate(name)
        if nname is None:
            print(f"not handled: {name}")
            # raise AssertionError(name)

        if iostd is not None:
            print("set_property IOSTANDARD {} [get_ports {}]".format(
                iostd, nname))
        print("set_property PACKAGE_PIN {} [get_ports {}]".format(
            pin, nname))
