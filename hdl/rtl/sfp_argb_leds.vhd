--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Project    : General Cores Collection library
--------------------------------------------------------------------------------
--
-- unit name:   gc_argb_led_drv
--
-- description: Driver for argb (or intelligent) led like ws2812b
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sfp_argb_leds is
  generic (
    g_clk_freq : natural
  );
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    sfp_act_i : in std_logic;
    sfp_link_i : in std_logic;

    pps_led_i : in std_logic;
    pps_valid_i : in std_logic;

    --  Output to the first led.
    dout_o  : out std_logic
  );
end sfp_argb_leds;

architecture arch of sfp_argb_leds is
  signal cnt               : natural range 0 to 6;
  signal waiting, ack      : std_logic;
  signal r, g, b           : std_logic_vector(7 downto 0);
  signal valid, ready, res : std_logic;

  subtype t_counter is natural range 0 to g_clk_freq / 15;
  signal pps_counter : t_counter;
begin
  process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        cnt <= 0;
        waiting <= '1';
        valid <= '0';
        ack <= '0';
        pps_counter <= 0;
      else
        --  Extend pps_led pulse
        if pps_led_i = '1' then
          pps_counter <= t_counter'high;
        elsif pps_counter /= 0 then
          pps_counter <= pps_counter - 1;
        end if;
      
        if waiting = '1' then
          if res = '1' then
            waiting <= '0';
            cnt <= 0;
          end if;
        elsif ack <= '0' and ready = '1' then
          r <= x"00";
          g <= x"00";
          b <= x"00";
          case cnt is
            when 0 =>
              --  Not used
              null;
            when 1 =>
              --  link
              if sfp_link_i = '1' then
                g <= x"40";
              end if;
            when 2 =>
              -- active
              if sfp_act_i = '1' then
                g <= x"40";
              end if;
            when 3 =>
              -- pps
              if pps_counter /= 0 then
                b <= x"40";
              elsif pps_valid_i = '1' then
                g <= x"40";
              else
                r <= x"40";
              end if;
            when 4 =>
              r <= x"40";
            when 5 =>
              g <= x"40";
            when 6 =>
              b <= x"40";
            when others =>
              null;
          end case;
          valid <= '1';
          ack <= '1';
        elsif ack = '1' then
          valid <= '0';
          ack <= '0';
          if cnt = 6 then
            waiting <= '1';
          else
            cnt <= cnt + 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  inst_drv: entity work.gc_argb_led_drv
    generic map (
      g_clk_freq => g_clk_freq
      )
    port map (
      clk_i => clk_i,
      rst_n_i => rst_n_i,
      g_i => g,
      r_i => r,
      b_i => b,
      valid_i => valid,
      dout_o => dout_o,
      ready_o => ready,
      res_o => res
      );
end arch;