library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.wrcore_pkg.all;
use work.endpoint_pkg.all;
use work.wishbone_pkg.all;
use work.wr_fabric_pkg.all;

entity wren_platform is
  generic (
    g_simulation : integer;
    g_WRPC_INITF    : string;
    g_hwbld_date    : std_logic_vector(31 downto 0);
    g_with_external_clock_input : boolean
  );
  port (
    --  GTHe4 refclk 250Mhz
    wr_clk_sfp_p_i: in std_logic;
    wr_clk_sfp_n_i: in std_logic;

    --  Also available on uFL connector 62.5Mhz
    wr_clk_main_p_i: in std_logic;
    wr_clk_main_n_i: in std_logic;

    --  Used for helper pll
    fpga_pl_clksys_p_i: in std_logic;
    fpga_pl_clksys_n_i: in std_logic;

    --  Not used
    wr_clk_helper_p_i: in std_logic;
    wr_clk_helper_n_i: in std_logic;

    --  From PL, for bootstrap
    clk_pl_i : in std_logic;
    rst_pl_n_i : in std_logic;

    rst_pl_done_o : out std_logic;
    rst_pl_cont_i : in std_logic;

    --  Generated clocks
    clk_sys_62m5_o: out std_logic;
    clk_sys_125m_o: out std_logic;
    clk_sys_250m_o: out std_logic;
    clk_sys_500m_o: out std_logic;

    clk_sys2_125m_o : out std_logic;

    --  Reset for the generated clocks
    rst_sys_n_o : buffer std_logic;

    --  WR sfp
    sfp_txp_o : out std_logic;
    sfp_txn_o : out std_logic;
    sfp_rxp_i : in std_logic;
    sfp_rxn_i : in std_logic;

    --  Loopbacks from GTH for SansDAC
    gth_bclk1_p_i : in std_logic;
    gth_bclk1_n_i : in std_logic;
    gth_bclk2_p_i : in std_logic;
    gth_bclk2_n_i : in std_logic;

    --  SFP eeprom
    sfp_sda_b  : inout std_logic;
    sfp_scl_b  : inout std_logic;

    led_link_o : out std_logic;
    led_act_o : out std_logic;

    sfp_los_i : in std_logic;
    sfp_tx_disable_o : out std_logic;
    sfp_det_i : in std_logic;

    wr_abscal_o : out std_logic;

    --  Timing port
    tm_link_up_o    : out std_logic;
    tm_time_valid_o : out std_logic;
    tm_tai_o        : out std_logic_vector(39 downto 0);
    tm_cycles_o     : out std_logic_vector(27 downto 0);

    --  SPI interface to PLL
    spi_main_cs_n_o   : out std_logic;
    spi_mosi_o        : out std_logic;
    spi_miso_i        : in  std_logic;
    spi_sck_o         : out std_logic;

    -- EEPROM I2C interface for storing configuration and accessing unique ID
    eeprom_sda_b  : inout std_logic;
    eeprom_scl_b  : inout std_logic;

    -- UART
    uart_rxd_i    : in  std_logic;
    uart_txd_o    : out std_logic;

    pps_p_o : out std_logic;
    pps_valid_o : out std_logic;
    pps_led_o : out std_logic;

    wb_si5340_i  : in  t_wishbone_slave_in;
    wb_si5340_o  : out t_wishbone_slave_out;

    wr_master_i  : in  t_wishbone_slave_in;
    wr_master_o  : out t_wishbone_slave_out;

    --  WR fabric.
    eth_tx_i  : in  t_wrf_sink_in;
    eth_tx_o  : out t_wrf_sink_out;
    eth_rx_i  : in  t_wrf_source_in;
    eth_rx_o  : out t_wrf_source_out;

    soc_spi_helper_cs_n_i : in std_logic;
    soc_spi_main_cs_n_i   : in std_logic;
    soc_spi_mosi_i        : in std_logic;
    soc_spi_miso_o        : out std_logic;
    soc_spi_sck_i         : in std_logic;

    clk_bunch_o : out std_logic_vector(2 downto 1);

    --  refclk for transceivers
    gth_clk_250m_o: out std_logic
  );
end wren_platform;

architecture arch of wren_platform is
    component ila_0 is
        port (
          clk    : in STD_LOGIC;
          probe0 : in STD_LOGIC_VECTOR(63 downto 0)
        );
      end component;

  signal clk_sys_62m5: std_logic;
  signal clk_sys_125m: std_logic;
  signal gth_clk_250m_gt : std_logic;

  signal clk_gth_250m : std_logic;

  signal rst_sys_62m5_n : std_logic;

  signal clk_dmtd: std_logic;
  signal clk_ext_locked : std_logic;
  signal clk_ext_rst: std_logic;
  signal clk_ext_62m5 : std_logic;
  signal clk_ext_10m : std_logic;

  signal clk_mon : std_logic;
  signal rst_pl : std_logic;
  signal rst_phy : std_logic;

  signal clk_sys2_125m, clk_sys2_125m_ce : std_logic;

  signal clk_main_62m5_int : std_logic;
  signal clk_gth_ce, clk_gth_clr, clk_gth_62m5, clk_gth_125m : std_logic;
  signal clk_free_62m5 : std_logic;

  signal gth_reset_done_a, gth_reset_done, gth_reset : std_logic;

  signal gt_powergood : std_logic;
  signal helper_sel : std_logic;
begin
  rst_pl <= not rst_pl_n_i;

  --  Clocking.
  blk_clock: block
    signal fpga_pl_clksys_int : std_logic;
    signal wr_clk_helper_int : std_logic;
    signal clk_gth_250m_int: std_logic;
    signal clk_fb, clk_fb_int : std_logic;
    signal clk_sys_500m_int : std_logic;
    signal clk_locked : std_logic;
    signal rstlogic_arst : std_logic;
    signal bclk1_int, bclk2_int : std_logic;

    signal clk_ext_fb_int, clk_ext_fb : std_logic;
    signal clk_ext_62m5_int, clk_ext_10m_int : std_logic;

    attribute keep: string;
    attribute keep of inst_ibufgds_pllmain: label is "true";
  begin
    inst_ibufds_gt : IBUFDS_GTE4
      generic map (
        REFCLK_EN_TX_PATH  => '0',
        REFCLK_HROW_CK_SEL => "00",
        REFCLK_ICNTL_RX    => "00")
      port map (
        O     => gth_clk_250m_gt,
        ODIV2 => clk_gth_250m_int,
        CEB   => '0',
        I     => wr_clk_sfp_p_i,
        IB    => wr_clk_sfp_n_i);

    gth_clk_250m_o <= gth_clk_250m_gt;

    --  For the free running clock to reset gthe4
    inst_buf_gt_62m5 : BUFG_GT
      port map (
        O => clk_free_62m5,
        CE => '1',
        CEMASK => '0',
        CLR => '0',
        CLRMASK => '0',
        DIV => "011",
        I => clk_gth_250m_int
      );

    --  For dmtd helper (62.5m)
    inst_bufds: IBUFDS
      generic map (
        DIFF_TERM => True)
      port map (
        O => fpga_pl_clksys_int,
        I => fpga_pl_clksys_p_i,
        IB => fpga_pl_clksys_n_i
        );

    --  clk_main; real freq is 62.5m
    --  Available on uFL.  Used for pseudo grand master
    inst_ibufgds_pllmain : IBUFDS
      generic map (
        DIFF_TERM => True,
        DQS_BIAS  => "FALSE")
      port map (
        O  => clk_main_62m5_int,
        I  => wr_clk_main_p_i,
        IB => wr_clk_main_n_i);

    --  Not used
    inst_ibufgds_dmtd : IBUFDS
      generic map (
        DIFF_TERM => True,
        DQS_BIAS  => "FALSE")
      port map (
        O  => wr_clk_helper_int,
        I  => wr_clk_helper_p_i,
        IB => wr_clk_helper_n_i);

    --  Bunch clocks from gth SansDAC
    inst_ibufgds_bclk1 : IBUFDS
    generic map (
      DIFF_TERM => True,
      DQS_BIAS  => "FALSE")
    port map (
      O  => bclk1_int,
      I  => gth_bclk1_p_i,
      IB => gth_bclk1_n_i);

    inst_bufg_bclk1 : BUFG
      port map (
        I => bclk1_int,
        O => clk_bunch_o(1)          -- 1-bit output: Buffer
        );

    inst_ibufgds_bclk2 : IBUFDS
      generic map (
        DIFF_TERM => True,
        DQS_BIAS  => "FALSE")
      port map (
        O  => bclk2_int,
        I  => gth_bclk2_p_i,
        IB => gth_bclk2_n_i);

    inst_bufg_bclk2 : BUFG
      port map (
        I => bclk2_int,
        O => clk_bunch_o(2)          -- 1-bit output: Buffer
        );

    g_dmtd_nomux: if true generate
      inst_bufg_clk_dmtd: BUFGCE_DIV
        generic map (
          BUFGCE_DIVIDE => 1)
        port map (
          O   => clk_dmtd,
          CE  => '1',
          CLR => '0',
          I   => fpga_pl_clksys_int);
    end generate;


    --  PLL Fvco should be between 800Mhz and 1600Mhz
    --  (cf DS925 p 67)
    --  For an input of 250Mhz -> *4  [previously 125Mhz -> *8]
    inst_MMCME4_BASE : MMCME4_BASE
      generic map (
        BANDWIDTH => "OPTIMIZED",-- Jitter programming
        CLKFBOUT_MULT_F => 4.0,  -- Multiply value for all CLKOUT
        CLKFBOUT_PHASE => 0.0,  -- Phase offset in degrees of CLKFB
        CLKIN1_PERIOD => 4.0,   -- Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
        CLKOUT0_DIVIDE_F => 2.0, -- 500Mhz
        CLKOUT0_DUTY_CYCLE => 0.5,
        CLKOUT0_PHASE => 0.0,
        CLKOUT1_DIVIDE => open,
        CLKOUT1_DUTY_CYCLE => 0.5,
        CLKOUT1_PHASE => 0.0,
        CLKOUT2_DIVIDE => open,
        CLKOUT2_DUTY_CYCLE => 0.5,
        CLKOUT2_PHASE => 0.0,
        CLKOUT3_DIVIDE => open,
        CLKOUT3_DUTY_CYCLE => 0.5,
        CLKOUT3_PHASE => 0.0,
        DIVCLK_DIVIDE => 1,
        IS_CLKFBIN_INVERTED => '0',
        IS_CLKIN1_INVERTED => '0',
        IS_PWRDWN_INVERTED => '0',
        IS_RST_INVERTED => '0',
        REF_JITTER1 => 0.0,
        STARTUP_WAIT => "FALSE"
        )
      port map (
        CLKFBOUT => clk_fb_int,
        CLKFBOUTB => open,
        CLKOUT0 => clk_sys_500m_int,
        CLKOUT0B => open,
        CLKOUT1 => open,
        CLKOUT1B => open,
        CLKOUT2 => open,
        CLKOUT2B => open,
        CLKOUT3 => open,
        CLKOUT3B => open,
        CLKOUT4 => open,
        CLKOUT5 => open,
        CLKOUT6 => open,
        LOCKED => clk_locked,
        CLKFBIN => clk_fb,
        CLKIN1 => clk_gth_250m,
        PWRDWN => '0',
        RST => rst_pl
        );

    inst_bufg_fb: BUFG
      port map (
        I => clk_fb_int,
        O => clk_fb
      );

    clk_sys_62m5_o <= clk_gth_62m5;
    clk_sys_62m5 <= clk_gth_62m5;

    clk_sys_125m <= clk_gth_125m;
    clk_sys_125m_o <= clk_gth_125m;

    --  Used for DDR write
    clk_sys_250m_o <= clk_gth_250m;

    inst_bufg_clk500m : BUFG
      port map (
        I => clk_sys_500m_int,     -- 1-bit input: Buffer
        O => clk_sys_500m_o       -- 1-bit output: Buffer
        );

    inst_BUFGCE_DIV : BUFGCE_DIV
      generic map (
        BUFGCE_DIVIDE => 4,
        IS_CE_INVERTED => '0', -- Optional inversion for CE
        IS_CLR_INVERTED => '0', -- Optional inversion for CLR
        IS_I_INVERTED => '0'
        )
      port map (
        O => clk_sys2_125m,
        CE => clk_sys2_125m_ce,
        CLR => '0',
        I => clk_sys_500m_int
        );


    clk_sys2_125m_o <= clk_sys2_125m;

    gen_ext_clk: if g_with_external_clock_input generate
      inst_MMCME4_BASE_ext : MMCME4_BASE
        generic map (
          BANDWIDTH => "OPTIMIZED",-- Jitter programming
          CLKFBOUT_MULT_F => 16.0,  -- Multiply value for all CLKOUT
          CLKFBOUT_PHASE => 0.0,  -- Phase offset in degrees of CLKFB
          CLKIN1_PERIOD => 16.0,   -- Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
          CLKOUT0_DIVIDE_F => 16.0, -- 62.5Mhz
          CLKOUT0_DUTY_CYCLE => 0.5,
          CLKOUT0_PHASE => 0.0,
          CLKOUT1_DIVIDE => 100,     --  10m
          CLKOUT1_DUTY_CYCLE => 0.5,
          CLKOUT1_PHASE => 0.0,
          CLKOUT2_DIVIDE => open,
          CLKOUT2_DUTY_CYCLE => open,
          CLKOUT2_PHASE => open,
          CLKOUT3_DIVIDE => open,
          CLKOUT3_DUTY_CYCLE => open,
          CLKOUT3_PHASE => open,
          DIVCLK_DIVIDE => 1,
          IS_CLKFBIN_INVERTED => '0',
          IS_CLKIN1_INVERTED => '0',
          IS_PWRDWN_INVERTED => '0',
          IS_RST_INVERTED => '0',
          REF_JITTER1 => 0.0,
          STARTUP_WAIT => "FALSE"
          )
        port map (
          CLKFBOUT => clk_ext_fb_int,
          CLKFBOUTB => open,
          CLKOUT0 => clk_ext_62m5_int,
          CLKOUT0B => open,
          CLKOUT1 => clk_ext_10m_int,
          CLKOUT1B => open,
          CLKOUT2 => open,
          CLKOUT2B => open,
          CLKOUT3 => open,
          CLKOUT3B => open,
          CLKOUT4 => open,
          CLKOUT5 => open,
          CLKOUT6 => open,
          LOCKED => clk_ext_locked,
          CLKFBIN => clk_ext_fb,
          CLKIN1 => clk_main_62m5_int,
          PWRDWN => '0',
          RST => clk_ext_rst
          );

      inst_bufg_ext_fb: BUFG
        port map (
          I => clk_ext_fb_int,
          O => clk_ext_fb
          );
      inst_bufg_ext_62m5 : BUFG
        port map (
          I => clk_ext_62m5_int,     -- 1-bit input: Buffer
          O => clk_ext_62m5      -- 1-bit output: Buffer
          );

      inst_bufg_ext_10m : BUFG
        port map (
          I => clk_ext_10m_int,     -- 1-bit input: Buffer
          O => clk_ext_10m      -- 1-bit output: Buffer
          );
    end generate;

    rstlogic_arst <= rst_pl or (not clk_locked);

    inst_rstlogic_reset : entity work.gc_reset_multi_aasd
      generic map (
        g_CLOCKS  => 1,   -- 62.5MHz, 125MHz
        g_RST_LEN => 16)  -- 16 clock cycles
      port map (
        arst_i  => rstlogic_arst,
        clks_i (0) => clk_sys_62m5,
        rst_n_o (0) => rst_sys_62m5_n
        );
    rst_sys_n_o <= rst_sys_62m5_n;
  end block;

  b_align: block
    signal samp_main_62m5, samp_gth_62m5, samp_gth_125m, samp_sys2_125m : std_logic_vector(7 downto 0);
    signal prof_main_62m5, prof_gth_62m5, prof_gth_125m, prof_sys2_125m : std_logic_vector(7 downto 0);
    signal samp_cnt                                                     : unsigned(1 downto 0);
    signal samp_slide : std_logic;

    signal pre_rst_pl_n, pre_rst_gth_n : std_logic;
    subtype t_gth_rst_cnt is natural range 0 to 250_000 / 20;
    signal gth_rst_cnt : t_gth_rst_cnt;

    type t_gth_state is (S_GTH_PGOOD, S_GTH_RESET, S_GTH_DONE);
    signal gth_state : t_gth_state;

    type t_state is (S_SLIDE, S_SLIDING, S_GTH, S_GTH_SLIDE, S_BUFCE, S_BUFCE_SLIDE);
    signal is_slide, is_sliding, is_gth, is_gth_slide, is_bufce, is_bufce_slide : std_logic;
    signal cnt : natural range 0 to 31;
    signal cnt_slv : std_logic_vector(4 downto 0);
    signal state: t_state;
  begin
    --  Sample clocks (using falling edge).
    process(clk_gth_250m)
    begin
      if falling_edge(clk_gth_250m) then
        if samp_cnt = 0 then
          prof_main_62m5 <= samp_main_62m5;
          prof_sys2_125m <= samp_sys2_125m;
          prof_gth_62m5 <= samp_gth_62m5;
          prof_gth_125m <= samp_gth_125m;
        end if;

        samp_main_62m5 <= samp_main_62m5(6 downto 0) & clk_main_62m5_int;
        samp_sys2_125m <= samp_sys2_125m(6 downto 0) & clk_sys2_125m;
        samp_gth_62m5 <= samp_gth_62m5(6 downto 0) & clk_gth_62m5;
        samp_gth_125m <= samp_gth_125m(6 downto 0) & clk_gth_125m;
        if samp_slide = '0' then
          samp_cnt <= samp_cnt + 1;
        end if;
      end if;
    end process;

    inst_sync_gth_done: entity work.gc_sync
    port map (
      rst_n_a_i => rst_pl_n_i,
      clk_i => clk_pl_i,
      d_i => gth_reset_done_a,
      q_o => gth_reset_done
    );

    --  gth-reset
    process(clk_pl_i)
    begin
      if rising_edge(clk_pl_i) then
        if rst_pl_n_i = '0' or gt_powergood = '0' then
          pre_rst_pl_n <= '0';
          gth_rst_cnt <= t_gth_rst_cnt'high;
          gth_reset <= '0';
          gth_state <= S_GTH_PGOOD;
        else
          case gth_state is
            when S_GTH_PGOOD =>
              --  Wait until GTHe4 powergood is ok + 250us
              if gth_rst_cnt = 0 then
                pre_rst_pl_n <= '1';

                gth_reset <= '1';
                gth_rst_cnt <= 4;
                gth_state <= S_GTH_RESET;
              else
                gth_rst_cnt <= gth_rst_cnt - 1;
              end if;
            when S_GTH_RESET =>
              if gth_rst_cnt = 0 then
                gth_reset <= '0';
                if gth_reset_done = '1' then
                  gth_state <= S_GTH_RESET;
                end if;                
              else
                gth_rst_cnt <= gth_rst_cnt - 1;
              end if;
            when S_GTH_DONE =>
          end case;
        end if;
      end if;
    end process;

    inst_sync_pre_rst: entity work.gc_sync
    port map (
      rst_n_a_i => rst_pl_n_i,
      clk_i => clk_gth_250m,
      d_i => pre_rst_pl_n,
      q_o => pre_rst_gth_n
    );

    --  Align clocks.
    process (clk_gth_250m, pre_rst_gth_n)
    begin
      if pre_rst_gth_n = '0' then
        state <= S_SLIDE;
        clk_gth_ce <= '0';
        clk_gth_clr <= '1';
        rst_pl_done_o <= '0';
        cnt <= 0;
      elsif rising_edge(clk_gth_250m) then
        samp_slide <= '0';
        clk_gth_ce <= '1';
        clk_gth_clr <= '0';
        clk_sys2_125m_ce <= '1';

        is_slide <= '0';
        is_sliding <= '0';
        is_gth <= '0';
        is_gth_slide <= '0';
        is_bufce <= '0';
        is_bufce_slide <= '0';

        case state is
          when S_SLIDE =>
            is_slide <= '1';
            --  Ensure the profile of the main clock is the usual one.
            --  That will be simpler to compare the profiles.
            if prof_main_62m5(3 downto 0) = "1100" then
              if cnt = 31 then
                cnt <= 0;
                state <= S_GTH;
              else
                cnt <= cnt + 1;
              end if;
            else
              samp_slide <= '1';
              state <= S_SLIDING;
              cnt <= 0;
            end if;
          when S_SLIDING =>
            is_sliding <= '1';
            if cnt = 8 then
              state <= S_SLIDE;
              cnt <= 0;
            else
              cnt <= cnt + 1;
            end if;
          when S_GTH =>
            is_gth <= '1';
            if prof_gth_62m5(3 downto 0) = "1100" then
              if cnt = 31 then
                cnt <= 0;
                state <= S_BUFCE;
              else
                cnt <= cnt + 1;
              end if;
            else
              clk_gth_ce <= '0';
              cnt <= 0;
              state <= S_GTH_SLIDE;
            end if;
          when S_GTH_SLIDE =>
            is_gth_slide <= '1';
            if cnt = 8 then
              cnt <= 0;
              state <= S_GTH;
            else
              cnt <= cnt + 1;
            end if;
          when S_BUFCE =>
            is_bufce <= '1';
            if prof_sys2_125m(3 downto 0) = "1010" then
              if cnt = 31 then
                --  Done!
                rst_pl_done_o <= '1';
                null;
              else
                cnt <= cnt + 1;
              end if;
            else
              clk_sys2_125m_ce <= '0';
              cnt <= 0;
              state <= S_BUFCE_SLIDE;
            end if;
          when S_BUFCE_SLIDE =>
            is_bufce_slide <= '1';
            if cnt = 8 then
              cnt <= 0;
              state <= S_BUFCE;
            else
              cnt <= cnt + 1;
            end if;
        end case;
      end if;
    end process;

    inst_ila: ila_0
      port map (
        clk => clk_gth_250m,
        probe0(7 downto 0) => prof_main_62m5,
        probe0(15 downto 8) => prof_sys2_125m,
        probe0(23 downto 16) => prof_gth_62m5,
        probe0(31 downto 24) => prof_gth_125m,
        probe0(32) => is_slide,
        probe0(33) => is_sliding,
        probe0(34) => is_gth,
        probe0(35) => is_gth_slide,
        probe0(36) => '0',
        probe0(37) => is_bufce,
        probe0(38) => is_bufce_slide,
        probe0(39) => gth_reset_done,
        probe0(40) => gth_reset,
        probe0(41) => pre_rst_gth_n,
        probe0(42) => rst_pl_cont_i,
        probe0(43) => rst_phy,
        probe0(58 downto 44) => (others => '0'),
        probe0(63 downto 59) => cnt_slv
        );
    cnt_slv <= std_logic_vector(to_unsigned(cnt, 5));
  end block;

  b_wr: block
    constant g_diag_id             : integer := 0;
    constant g_diag_ver            : integer := 0;
    constant g_diag_ro_size        : integer := 0;
    constant g_diag_rw_size        : integer := 0;
    constant c_streamers_diag_id  : integer := 1;  -- id reserved for streamers
    constant c_streamers_diag_ver : integer := 2;  -- version that will be probably increased
--    constant c_diag_id            : integer := f_pick_diag_val(g_fabric_iface,  c_streamers_diag_id,         g_diag_id);
--    constant c_diag_ver           : integer := f_pick_diag_val(g_fabric_iface,  c_streamers_diag_ver,        g_diag_id);
    constant c_diag_ro_size       : integer := 0; --f_pick_diag_size(g_fabric_iface, c_WR_STREAMERS_ARR_SIZE_OUT, g_diag_ro_size);
    constant c_diag_rw_size       : integer := 0; --f_pick_diag_size(g_fabric_iface, c_WR_STREAMERS_ARR_SIZE_IN,  g_diag_rw_size);

    signal phy16_out :t_phy_16bits_from_wrc;
    signal phy16_in :t_phy_16bits_to_wrc;

    signal sfp_scl_out, sfp_scl_in : std_logic;
    signal sfp_sda_out, sfp_sda_in : std_logic;

    signal eeprom_scl_out, eeprom_scl_in : std_logic;
    signal eeprom_sda_out, eeprom_sda_in : std_logic;

    signal wb_aux_out : t_wishbone_master_out;
    signal wb_aux_in  : t_wishbone_master_in;

    --  DAC control (from WR to si5340_ctrl)
    signal dac_mpll_val, dac_hpll_val : std_logic_vector(15 downto 0);
    signal dac_mpll_wr, dac_hpll_wr : std_logic;

    signal rst_sys_62m5 : std_logic;

    -- WR SNMP
    signal aux_diag_in  : t_generic_word_array(c_diag_ro_size-1 downto 0);
    signal aux_diag_out : t_generic_word_array(c_diag_rw_size-1 downto 0);

    signal tx_out_clk : std_logic;

    signal clk_pps : std_logic;
    signal cnt_pps : natural range 0 to 62_499_999;
  begin
    rst_sys_62m5 <= not rst_sys_62m5_n;

    cmp_gth: entity work.wr_gthe4_phy_family7_xilinx_ip_pxie
      generic map (
        g_simulation         => g_simulation)
      port map (
        clk_gth_i      => gth_clk_250m_gt,
        clk_freerun_i  => clk_free_62m5,
        gth_reset_i    => gth_reset,
        gth_reset_done_o => gth_reset_done_a,
        tx_out_clk_o   => tx_out_clk,
        tx_usr_clk_i   => clk_gth_62m5,
        tx_data_i      => phy16_out.tx_data,
        tx_k_i         => phy16_out.tx_k,
        tx_disparity_o => phy16_in.tx_disparity,
        tx_enc_err_o   => phy16_in.tx_enc_err,
        rx_rbclk_o     => phy16_in.rx_clk,
        rx_data_o      => phy16_in.rx_data,
        rx_k_o         => phy16_in.rx_k,
        rx_enc_err_o   => phy16_in.rx_enc_err,
        rx_bitslide_o  => phy16_in.rx_bitslide,
        rst_i          => rst_phy,
        pad_txn_o      => sfp_txn_o,
        pad_txp_o      => sfp_txp_o,
        pad_rxn_i      => sfp_rxn_i,
        pad_rxp_i      => sfp_rxp_i,
        rdy_o          => phy16_in.rdy,

        gt_powergood_o => gt_powergood,
        clk_mon_o => clk_mon);

    rst_phy <= phy16_out.rst;

  inst_buf_gt_62m5 : BUFG_GT
    port map (
      O => clk_gth_62m5,
      CE => clk_gth_ce,
      CEMASK => '0',
      CLR => clk_gth_clr,
      CLRMASK => '0',
      DIV => "011",
      I => tx_out_clk
      );

  inst_buf_gt_125m : BUFG_GT
    port map (
      O => clk_gth_125m,
      CE => clk_gth_ce,
      CEMASK => '0',
      CLR => clk_gth_clr,
      CLRMASK => '0',
      DIV => "001",
      I => tx_out_clk
      );

      inst_buf_gt_250 : BUFG_GT
      port map (
        O => clk_gth_250m,
        CE => '1',
        CEMASK => '1',
        CLR => '0',
        CLRMASK => '1',
        DIV => "000",
        I => tx_out_clk);
  
    phy16_in.sfp_tx_fault <= '0'; --  not connected
    phy16_in.sfp_los      <= sfp_los_i;
    sfp_tx_disable_o     <= phy16_out.sfp_tx_disable;

    phy16_in.ref_clk <= clk_gth_62m5;

    inst_WR_CORE : entity work.xwr_core
    generic map(
        g_simulation                     => g_simulation,
        g_with_external_clock_input      => g_with_external_clock_input,
        --
        g_board_name                     => "VMEN",
        --g_ram_address_space_size_kb    => 256,
        g_phys_uart                      => true,  --  Is false OK ?
        g_virtual_uart                   => true,
        g_vuart_fifo_size                => 1024,
        g_aux_clks                       => 0,
--        g_softpll_reverse_dmtds          => true,
        g_ep_rxbuf_size                  => 1024,
        g_tx_runt_padding                => true,
        g_records_for_phy                => true,
        g_pcs_16bit                      => true,
        g_dpram_initf                    => g_wrpc_initf,
        g_dpram_size                     => (131072+65536)/4,
        g_interface_mode                 => PIPELINED,
        g_address_granularity            => BYTE,
        g_aux_sdb                        => c_wrc_periph3_sdb,
        g_softpll_enable_debugger        => true,
--        g_softpll_use_sampled_ref_clocks => true,
        g_diag_id                        => 0,
        g_diag_ver                       => 0,
        g_diag_ro_size                   => 0,
        g_diag_rw_size                   => 0,
        g_hwbld_date                     => g_hwbld_date)
    port map(
        clk_sys_i              => clk_sys_62m5,
        clk_dmtd_i             => clk_dmtd,
        clk_ref_i              => clk_sys_62m5,
        --  No external 10Mhz
        clk_ext_i              => clk_ext_10m,
        clk_ext_mul_i          => clk_ext_62m5,
        clk_ext_stopped_i      => open,
        clk_ext_mul_locked_i   => clk_ext_locked,
        clk_ext_rst_o          => clk_ext_rst,
        clk_aux_i              => open,
        clk_dmtd_over_i        => open,
        pps_ext_i              => clk_pps,
        rst_n_i                => rst_sys_62m5_n,

        dac_hpll_load_p1_o     => dac_hpll_wr,
        dac_hpll_data_o        => dac_hpll_val,
        dac_dpll_load_p1_o     => dac_mpll_wr,
        dac_dpll_data_o        => dac_mpll_val,

        phy16_i                => phy16_in,
        phy16_o                => phy16_out,
        phy8_i                 => open,
        phy8_o                 => open,
        phy_mdio_master_o      => open,
        phy_mdio_master_i      => open,
        phy_ref_clk_i          => open,
        phy_tx_disparity_i     => open,
        phy_tx_data_o          => open,
        phy_tx_k_o             => open,
        phy_tx_enc_err_i       => open,
        phy_rx_data_i          => open,
        phy_rx_rbclk_i         => open,
        phy_rx_rbclk_sampled_i => open,
        phy_rx_k_i             => open,
        phy_rx_enc_err_i       => open,
        phy_rx_bitslide_i      => open,
        phy_rst_o              => open,
        phy_rdy_i              => open,
        phy_loopen_o           => open,
        phy_loopen_vec_o       => open,
        phy_tx_prbs_sel_o      => open,
        phy_sfp_tx_fault_i     => open,
        phy_sfp_los_i          => open,
        phy_sfp_tx_disable_o   => open,

        led_act_o              => led_act_o,
        led_link_o             => led_link_o,
        scl_o                  => eeprom_scl_out,
        scl_i                  => eeprom_scl_in,
        sda_o                  => eeprom_sda_out,
        sda_i                  => eeprom_sda_in,
        sfp_scl_o              => sfp_scl_out,
        sfp_scl_i              => sfp_scl_in,
        sfp_sda_o              => sfp_sda_out,
        sfp_sda_i              => sfp_sda_in,
        sfp_det_i              => sfp_det_i,
        spi_sclk_o             => open,
        spi_ncs_o              => open,
        spi_mosi_o             => open,
        spi_miso_i             => open,
        uart_rxd_i             => uart_rxd_i,
        uart_txd_o             => uart_txd_o,
        owr_pwren_o            => open,
        owr_en_o               => open,
        owr_i                  => open,

        btn1_i                 => open,
        btn2_i                 => open,
        abscal_txts_o          => open,

        slave_i                => wr_master_i,
        slave_o                => wr_master_o,
        aux_master_o           => wb_aux_out,
        aux_master_i           => wb_aux_in,

        wrf_src_i => eth_rx_i,
        wrf_src_o => eth_rx_o,
        wrf_snk_i => eth_tx_i,
        wrf_snk_o => eth_tx_o,

        timestamps_o           => open,
        timestamps_ack_i       => '1',
        fc_tx_pause_req_i      => '0',
        fc_tx_pause_delay_i    => x"0000",
        fc_tx_pause_ready_o    => open,

        tm_link_up_o => tm_link_up_o,
        tm_time_valid_o => tm_time_valid_o,
        tm_tai_o => tm_tai_o,
        tm_cycles_o => tm_cycles_o,

        tm_dac_value_o         => open,
        tm_dac_wr_o            => open,
        tm_clk_aux_lock_en_i   => (others => '1'),
        tm_clk_aux_locked_o    => open,

        abscal_rxts_o          => wr_abscal_o,

        pps_p_o                => pps_p_o,
        pps_valid_o            => pps_valid_o,
        pps_led_o              => pps_led_o,
        pps_csync_o            => open,
        rst_aux_n_o            => open,
        aux_diag_i             => aux_diag_in,
        aux_diag_o             => aux_diag_out,

        link_ok_o              => open);

        sfp_scl_b <= '0' when sfp_scl_out = '0' else 'Z';
        sfp_sda_b <= '0' when sfp_sda_out = '0' else 'Z';
        sfp_scl_in <= sfp_scl_b;
        sfp_sda_in <= sfp_sda_b;

        eeprom_scl_b <= '0' when eeprom_scl_out = '0' else 'Z';
        eeprom_sda_b <= '0' when eeprom_sda_out = '0' else 'Z';
        eeprom_scl_in <= eeprom_scl_b;
        eeprom_sda_in <= eeprom_sda_b;

    inst_si5340: entity work.si5340_ctrl
        generic map (
          g_spi_div => 32,
          g_dac_bits => 16
        )
        port map (
          clk_i => clk_sys_62m5,
          pl_reset_n_i => rst_pl_n_i,
          rst_n_i => rst_sys_62m5_n,
          wb_i => wb_si5340_i,
          wb_o => wb_si5340_o,
          helper_sel_o => helper_sel,
          dac_h_o => open,
          dac_h_wr_o => open,
          dac_d_o => open,
          dac_d_wr_o => open,
          dac_h_i => dac_hpll_val,
          dac_h_p_i => dac_hpll_wr,
          dac_d_i => dac_mpll_val,
          dac_d_p_i => dac_mpll_wr,
          bypass_spi_mosi_i => soc_spi_mosi_i,
          bypass_spi_miso_o => soc_spi_miso_o,
          bypass_spi_clk_i => soc_spi_sck_i,
          bypass_spi_h_cs_n_i => soc_spi_helper_cs_n_i,
          bypass_spi_d_cs_n_i => soc_spi_main_cs_n_i,
          spi_mosi_o => spi_mosi_o,
          spi_miso_i => spi_miso_i,
          spi_clk_o => spi_sck_o,
          spi_h_cs_n_o => open,
          spi_d_cs_n_o => spi_main_cs_n_o
        );

    --  Generate dummy pps/10M to test master
    process(clk_ext_62m5)
    begin
      if rising_edge(clk_ext_62m5) then
        -- pps
        if cnt_pps = 62_499_999 then
          clk_pps <= '0';
          cnt_pps <= 0;
        else
          if cnt_pps = 31_249_999 then
            clk_pps <= '1';
          end if;
          cnt_pps <= cnt_pps + 1;
        end if;
      end if;
    end process;
  end block;
end arch;
