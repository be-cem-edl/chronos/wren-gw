# SPDX-FileCopyrightText: 2020 CERN (home.cern)
#
# SPDX-License-Identifier: CC-BY-SA-4.0 OR CERN-OHL-W-2.0+ OR GPL-2.0-or-later

memory-map:
  name: si5340_ctrl_map
  bus: wb-32-be
  description: Automatic control of si534x frequency
  x-hdl:
    busgroup: True
  children:
  - reg:
     name: ctrl
     comment: Control register
     access: rw
     width: 32
     children:
     - field:
         name: spi_en
         comment: Enable SPI control (and disable the core)
         description: |
           When 1, the si534x is controlled by this core.
           When 0, the si534x is controlled by the FPGA SPI.
         range: 0
     - field:
         name: dac_en
         comment: Enable DAC control (and disable the core)
         description: |
           When 1, the DAC is controlled by this core.
           When 0, the DAC is controlled by the wr-core.
         range: 1
     - field:
         name: helper_sel
         comment: "Select helper clock (0: wr_clk_helper, 1: dco)"
         description: |
           Control the dmtd clock source, either the helper pll or the
           main pll.
         range: 2
     - field:
         name: dac_update
         comment: Program the DAC
         description: |
           Trigger a DAC update, possible only if dac_en is set.
         range: 3
         x-hdl:
           type: autoclear
     - field:
         name: en0
         comment: Enable pll 0 update on input
         description: |
           If set, a write from the wr-core is converted into an SPI command
           to modify the DCO.
         range: 4
     - field:
         name: en1
         comment: Enable pll 1 update on input
         range: 5
     - field:
         name: force0
         comment: Update pll 0 using the force value
         range: 6
         x-hdl:
           type: autoclear
     - field:
         name: force1
         comment: Update pll 1 using the force value
         range: 7
         x-hdl:
           type: autoclear
  - reg:
     name: status
     comment: Status register
     access: ro
     width: 32
     children:
     - field:
         name: upd0
         comment: Update in progress for pll 0
         range: 0
     - field:
         name: upd1
         comment: Update in progress for pll 1
         range: 1
  - repeat:
     count: 2
     name: pll
     children:
     - reg:
        name: dac
        comment: dac value (to be set at start)
        access: rw
        width: 32
     - reg:
        name: bias_h
        comment: "bias (value = bias + input * mul)"
        access: rw
        width: 32
     - reg:
        name: bias_l
        comment: Bias low word
        access: rw
        width: 32
     - reg:
        name: mul
        comment: factor
        access: rw
        width: 32
     - reg:
        name: inp
        comment: input value from wr core
        access: ro
        width: 32
     - reg:
        name: force
        comment: forced value
        access: rw
        width: 32
     - reg:
        name: value_h
        comment: current computed value
        access: ro
        width: 32
     - reg:
        name: value_l
        comment: current computed value
        access: ro
        width: 32
