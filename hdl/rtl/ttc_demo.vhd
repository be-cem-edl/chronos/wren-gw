library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ttc_demo is
  port (
    clk_80m_i   : in std_logic;
    rst_80m_n_i : in std_logic;

    bst_out_ddr_o : out std_logic_vector(1 downto 0)
  );
end ttc_demo;

architecture arch of ttc_demo is
  signal ttc_data, ttc_addr : std_logic_vector(7 downto 0);
  signal frev, load : std_logic;
  signal count : unsigned(10 downto 0);
begin
  inst_frametx: entity work.ttc_frametx
    port map (
      clk_i => clk_80m_i,
      rst_n_i => rst_80m_n_i,
      out_o => bst_out_ddr_o,
      data_i => ttc_data,
      subaddr_i => ttc_addr,
      frev_i => frev,
      load_imm_i => load,
      rdy_i => '0',
      ack_o => open
      );

  process (clk_80m_i)
  begin
    if rising_edge(clk_80m_i) then
      load <= '0';
      frev <= '0';
      if rst_80m_n_i = '0' then
        count <= to_unsigned(860 * 2, count'length);
        ttc_data <= x"ff";
        ttc_addr <= x"00";
      else
        if count = 923 * 2 then
          load <= '1';
          frev <= '1';
          count <= (others => '0');
          ttc_addr <= std_logic_vector(unsigned(ttc_addr) + 1);
          ttc_data <= not ttc_addr;
        else
          count <= count + 1;
        end if;
      end if;
    end if;
  end process;
end arch;
