library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pulser_pkg.all;

--  Clock/frequence generator for subpulses.
entity freqgen is
  generic (
    --  Period of the clock, can be odd.
    g_PERIOD : natural
  );
  port (
    clk_125m_i   : in  std_logic;

    --  Set when the next cycle will be pps.
    pps_i : in std_logic;

    clk_o : out t_subpulse;
    pulse_o : out t_subpulse
  );
end freqgen;

architecture arch of freqgen is
  constant C_LO_PERIOD : natural := g_PERIOD / 2;
  constant C_HI_PERIOD : natural := g_PERIOD - C_LO_PERIOD;
  signal clk_out : t_subpulse := ('0', "000");
  signal cnt : unsigned (31 downto 0);
begin
  process (clk_125m_i)
  begin
    if rising_edge(clk_125m_i) then
      if pps_i = '1' then
        cnt <= to_unsigned(C_HI_PERIOD - 8, 32);
        clk_out <= ('1', "000");
        pulse_o <= ('1', "000");
      else
        if cnt < 8 then
          clk_out <= (not clk_out.v, std_logic_vector (cnt (2 downto 0)));
          pulse_o <= (not clk_out.v, std_logic_vector (cnt (2 downto 0)));
          if clk_out.v = '1' then
            cnt <= to_unsigned(C_LO_PERIOD, 32) - (8 - cnt);
          else
            cnt <= to_unsigned(C_HI_PERIOD, 32) - (8 - cnt);
          end if;
        else
          clk_out <= (clk_out.v, "000");
          pulse_o <= ('0', "000");
          cnt <= cnt - 8;
        end if;
      end if;
    end if;
  end process;

  clk_o <= clk_out;
end arch;
