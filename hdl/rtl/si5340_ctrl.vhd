--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Project    : WREN
--------------------------------------------------------------------------------
--
-- unit name:   si5340_ctrl
--
-- description: si5340 frequency update from wr-core
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.wishbone_pkg.all;

entity si5340_ctrl is
  generic (
    g_spi_div : natural := 32;
    g_dac_bits : natural := 16
  );
  port (
    clk_i : in std_logic;
    pl_reset_n_i : in std_logic;
    rst_n_i : in std_logic;

    --  Registers
    wb_i              : in    t_wishbone_slave_in;
    wb_o              : out   t_wishbone_slave_out;

    helper_sel_o  : out std_logic;

    --  DAC outputs
    dac_h_o       : out std_logic_vector (g_dac_bits - 1 downto 0);
    dac_h_wr_o    : out std_logic;
    dac_d_o       : out std_logic_vector (g_dac_bits - 1 downto 0);
    dac_d_wr_o    : out std_logic;

    --  DAC inputs from wr-core
    dac_h_i : in std_logic_vector(g_dac_bits - 1 downto 0);
    dac_h_p_i : in std_logic;
    dac_d_i : in std_logic_vector(g_dac_bits - 1 downto 0);
    dac_d_p_i : in std_logic;

    --  SPI input (from emio)
    bypass_spi_mosi_i : in  std_logic;
    bypass_spi_miso_o : out std_logic;
    bypass_spi_clk_i  : in  std_logic;
    bypass_spi_h_cs_n_i : in std_logic;
    bypass_spi_d_cs_n_i : in std_logic;

    --  SPI master (to si5340)
    spi_mosi_o   : out std_logic;
    spi_miso_i   : in  std_logic;
    spi_clk_o    : out std_logic;
    spi_h_cs_n_o : out std_logic;
    spi_d_cs_n_o : out  std_logic
  );
end si5340_ctrl;

architecture arch of si5340_ctrl is
  signal bypass : std_logic;

  signal spi_mosi, spi_miso, spi_clk, spi_h_cs_n, spi_d_cs_n : std_logic;

  signal ctrl_spi_en        : std_logic;
  signal ctrl_dac_en        : std_logic;
  signal ctrl_dac_update    : std_logic;

  type t_pll_regs is record
    inp_p : std_logic;
    inp_val : std_logic_vector(g_dac_bits - 1 downto 0);
    dac   : std_logic_vector(31 downto 0);
    force_val : std_logic_vector(31 downto 0);
    inp   : std_logic_vector(31 downto 0);
    bias  : std_logic_vector(63 downto 0);
    mul   : std_logic_vector(31 downto 0);
    value : std_logic_vector(63 downto 0);

    update : std_logic;
    done : std_logic;
    en : std_logic;
    force : std_logic;
  end record;

  --  Note: pll 0 is helper, pll 1 is main.
  type t_pll_regs_array is array(natural range <>) of t_pll_regs;
  signal pll_regs : t_pll_regs_array(0 to 1);

  type t_state is (S_IDLE, S_PAGE, S_NUM, S_UPDATE, S_UPDATE2);
  signal state : t_state;

  --  Temporary value where multiplication is done.
  signal value : std_logic_vector(47 downto 0);
  --  Shift register (set from mul) for multiplication
  signal shreg1 : std_logic_vector(31 downto 0);
  signal shreg2 : std_logic_vector(g_dac_bits - 1 downto 0);
  signal mul_cnt : natural range 0 to g_dac_bits;

  signal idx : natural range 0 to 1;
  signal spi_reg_num, spi_reg_upd : std_logic_vector(7 downto 0);
  signal spi_start, spi_done : std_logic;
  signal spi_len : natural range 0 to 64;
  signal spi_dat : std_logic_vector(63 downto 0);

  signal spi_clk_cnt : natural range 0 to g_spi_div - 1;
  signal spi_run, spi_wait : std_logic;

  signal helper_sel : std_logic;
begin
  --  SPI bypass.
  --  SPI must be controlled by EMIO while the CPU is programming the PLL.
  bypass <= not pl_reset_n_i or not ctrl_spi_en;

  spi_mosi_o <= bypass_spi_mosi_i when bypass = '1' else spi_mosi;
  spi_clk_o <= bypass_spi_clk_i when bypass = '1' else spi_clk;
  spi_h_cs_n_o <= bypass_spi_h_cs_n_i when bypass = '1' else spi_h_cs_n;
  spi_d_cs_n_o <= bypass_spi_d_cs_n_i when bypass = '1' else spi_d_cs_n;
  bypass_spi_miso_o <= spi_miso_i;
  spi_miso <= spi_miso_i;

  process(clk_i)
  begin
    if rising_edge (clk_i) then
      helper_sel_o <= helper_sel;
    end if;
  end process;

  inst_map: entity work.si5340_ctrl_map
    port map (
      rst_n_i => rst_n_i,
      clk_i => clk_i,
      wb_i => wb_i,
      wb_o => wb_o,
      ctrl_spi_en_o => ctrl_spi_en,
      ctrl_dac_en_o => ctrl_dac_en,
      ctrl_helper_sel_o => helper_sel,
      ctrl_dac_update_o => ctrl_dac_update,
      ctrl_en0_o => pll_regs(0).en,
      ctrl_en1_o => pll_regs(1).en,
      ctrl_force0_o => pll_regs(0).force,
      ctrl_force1_o => pll_regs(1).force,
      status_upd0_i => pll_regs(0).update,
      status_upd1_i => pll_regs(1).update,
      pll_0_dac_o => pll_regs(0).dac,
      pll_0_bias_h_o => pll_regs(0).bias(63 downto 32),
      pll_0_bias_l_o => pll_regs(0).bias(31 downto 0),
      pll_0_mul_o => pll_regs(0).mul,
      pll_0_inp_i => pll_regs(0).inp,
      pll_0_force_o => pll_regs(0).force_val,
      pll_0_value_h_i => pll_regs(0).value(63 downto 32),
      pll_0_value_l_i => pll_regs(0).value(31 downto 0),
      pll_1_dac_o => pll_regs(1).dac,
      pll_1_bias_h_o => pll_regs(1).bias(63 downto 32),
      pll_1_bias_l_o => pll_regs(1).bias(31 downto 0),
      pll_1_mul_o => pll_regs(1).mul,
      pll_1_inp_i => pll_regs(1).inp,
      pll_1_force_o => pll_regs(1).force_val,
      pll_1_value_h_i => pll_regs(1).value(63 downto 32),
      pll_1_value_l_i => pll_regs(1).value(31 downto 0)
    );

  process(clk_i)
  begin
    if rising_edge(clk_i) then
      dac_d_wr_o <= '0';
      dac_h_wr_o <= '0';
      if ctrl_dac_en = '1' then
        if ctrl_dac_update = '1' then
          dac_h_o <= pll_regs(0).dac(g_dac_bits - 1 downto 0);
          dac_d_o <= pll_regs(1).dac(g_dac_bits - 1 downto 0);
          dac_d_wr_o <= '1';
          dac_h_wr_o <= '1';
        end if;
      else
        --  DAC controlled directly by WR
        dac_h_o <= dac_h_i;
        dac_h_wr_o <= dac_h_p_i;
        dac_d_o <= dac_d_i;
        dac_d_wr_o <= dac_d_p_i;
      end if;
    end if;
  end process;

  --  pll 0 is helper
  pll_regs(0).inp_p <= dac_h_p_i;
  pll_regs(0).inp_val <= dac_h_i;

  --  pll 1 is main
  pll_regs(1).inp_p <= dac_d_p_i;
  pll_regs(1).inp_val <= dac_d_i;

  gen_pll: for i in 0 to 1 generate
    pll_regs(i).value (63 downto 48) <= x"0000";
    pll_regs(i).inp(31 downto g_dac_bits) <= (others => '0');

    process(clk_i)
    begin
      if rising_edge(clk_i) then
        if rst_n_i = '0' then
          pll_regs(i).update <= '0';
        else
          if pll_regs(i).done = '1' then
            --  End of TX
            pll_regs(i).update <= '0';
            pll_regs(i).value (47 downto 0) <= value;
          end if;

          if pll_regs(i).inp_p = '1' then
            --  Write from the wr-core
            pll_regs(i).inp (g_dac_bits - 1 downto 0) <= pll_regs(i).inp_val;
            pll_regs(i).update <= pll_regs(i).en;
          end if;
          if pll_regs(i).force = '1' then
            --  Write from software (force)
            pll_regs(i).inp (g_dac_bits - 1 downto 0) <= pll_regs(i).force_val (g_dac_bits - 1 downto 0);
            pll_regs(i).update <= '1';
          end if;
        end if;
      end if;
    end process;
  end generate;

  --  Numerator register: N0_NUM(0x302) or N2_NUM(0x318)
  with idx select
    spi_reg_num <= x"02" when 1, x"18" when 0;
  --  Update register: N0_UPDATE(0x30c) or N2_UPDATE(0x322)
  with idx select
    spi_reg_upd <= x"0c" when 1, x"22" when 0;

  spi_h_cs_n <= '1';

  process(clk_i)
  begin
    if rising_edge (clk_i) then
      pll_regs(0).done <= '0';
      pll_regs(1).done <= '0';
      spi_done <= '0';

      if rst_n_i = '0' then
        state <= S_IDLE;
        spi_d_cs_n <= '1';
        spi_wait <= '0';
        spi_run <= '0';
        spi_start <= '0';
      else
        --  Main FSM, set the SPI transactions.
        case state is
          when S_IDLE =>
            if pll_regs(0).update = '1' then
              idx <= 0;
              value <= pll_regs(0).bias(47 downto 0);
              shreg1 <= pll_regs(0).mul;
              shreg2 <= pll_regs(0).inp (g_dac_bits - 1 downto 0);

              state <= S_PAGE;
              spi_start <= '1';
            elsif pll_regs(1).update = '1' then
              idx <= 1;
              value <= pll_regs(1).bias(47 downto 0);
              shreg1 <= pll_regs(1).mul;
              shreg2 <= pll_regs(1).inp (g_dac_bits - 1 downto 0);

              state <= S_PAGE;
              spi_start <= '1';
            end if;

            mul_cnt <= g_dac_bits;

            --  Select page 3.
            spi_dat (63 downto 40) <= x"e0_01_03";
            spi_len <= 24;
          when S_PAGE =>
            --  Multiplication
            --  At least 2 * 24 clock tick are available to do the multiplication, which is
            --  enough.
            if mul_cnt /= 0 then
              if shreg2(g_dac_bits - 1) = '1' then
                --  Substract to have a positive slope for the frequency.
                value <= std_logic_vector(unsigned(value) - unsigned(shreg1));
              end if;
              shreg2 <= shreg2(g_dac_bits - 2 downto 0) & '0';
              shreg1 <= '0' & shreg1(31 downto 1);

              mul_cnt <= mul_cnt - 1;
            end if;

            if spi_done = '1' then
              --  Set Mn_NUM
              spi_dat (63 downto 56) <= x"e0";
              spi_dat(55 downto 48) <= spi_reg_num;
              spi_dat(47 downto 0) <= value(7 downto 0) & value(15 downto 8) & value(23 downto 16)
                 & value (31 downto 24) & value(39 downto 32) & value(47 downto 40);
              spi_len <= 64;
              spi_start <= '1';

              state <= S_NUM;
            end if;
          when S_NUM =>
            if spi_done = '1' then
              --  Set Mn_UPDATE
              spi_dat(63 downto 56) <= x"e0";
              spi_dat(55 downto 48) <= spi_reg_upd;
              spi_dat(47 downto 40) <= x"01";
              spi_len <= 24;
              spi_start <= '1';

              state <= S_UPDATE;
            end if;
          when S_UPDATE =>
            --  Set done and wait for 1 cycle
            if spi_done = '1' then
              if idx = 0 then
                pll_regs(0).done <= '1';
              else
                pll_regs(1).done <= '1';
              end if;
              state <= S_UPDATE2;
            end if;
          when S_UPDATE2 =>
            state <= S_IDLE;
          end case;
      end if;

      if bypass = '1' then
        spi_clk_cnt <= g_spi_div - 1;
        spi_clk <= bypass_spi_clk_i;
      else
        if spi_clk_cnt = 0 then
          spi_clk_cnt <= g_spi_div - 1;
          spi_clk <= not spi_clk;
        else
          spi_clk_cnt <= spi_clk_cnt - 1;
        end if;

        if spi_clk_cnt = 0 and spi_clk = '1' then
          --  Falling edge
          --  Set CS, mosi
          if spi_wait = '1' then
            spi_start <= '0';
            if spi_len = 0 then
              spi_done <= '1';
              spi_wait <= '0';
            else
              spi_len <= spi_len - 1;
            end if;
          elsif spi_run = '1' or spi_start = '1' then
            if spi_len = 0 then
              spi_run <= '0';
              spi_wait <= '1';
              spi_d_cs_n <= '1';
            else
              spi_run <= '1';
              spi_d_cs_n <= '0';
              spi_mosi <= spi_dat(63);
              spi_dat <= spi_dat(62 downto 0) & '0';
              spi_len <= spi_len - 1;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;
end arch;
