--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   gmtwr_v0
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nco_x7 is
  port (
    --  Main (and nco) clock
    nco_clk_i : std_logic;
    rst_n_i : std_logic;

    --  Load a new FTW for the next clock edge.
    ftw_load_i : std_logic;
    ftw_i : std_logic_vector(47 downto 0);

    --  *Only* on a load: initialize the accumulator with an
    --  initial value.
    nco_rst_i : std_logic;
    nco_init_i : std_logic_vector(47 downto 0);

    msb_0_o : out std_logic;
    msb_p1_o : out std_logic;
    msb_p2_o : out std_logic;
    msb_p4_o : out std_logic;
    msb_m1_o : out std_logic;
    msb_m2_o : out std_logic;
    msb_m4_o : out std_logic
  );
end nco_x7;

architecture arch of nco_x7 is
  signal acc, ftw : unsigned(47 downto 0);
  signal n_acc, n_acc_p1, n_acc_p2, n_acc_p4,
    n_acc_m1, n_acc_m2, n_acc_m4 : unsigned(47 downto 0);
begin
  n_acc <= acc;
  n_acc_p1 <= acc + (ftw srl 5);
  n_acc_p2 <= acc + (ftw srl 4);
  n_acc_p4 <= acc + (ftw srl 3);
  n_acc_m1 <= acc - (ftw srl 5);
  n_acc_m2 <= acc - (ftw srl 4);
  n_acc_m4 <= acc - (ftw srl 3);

  process (nco_clk_i)
  begin
    if rising_edge(nco_clk_i) then
      msb_0_o <= n_acc (47);
      msb_p1_o <= n_acc_p1 (47);
      msb_p2_o <= n_acc_p2 (47);
      msb_p4_o <= n_acc_p4 (47);
      msb_m1_o <= n_acc_m1 (47);
      msb_m2_o <= n_acc_m2 (47);
      msb_m4_o <= n_acc_m4 (47);
      acc <= acc + ftw;
      if ftw_load_i = '1' then
        ftw <= unsigned (ftw_i);
        if nco_rst_i = '1' then
          acc <= unsigned (nco_init_i);
        end if;
      end if;
    end if;
  end process;
end arch;