library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pulser_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity pulser_input is
  port (
    clk_500m_i : in std_logic;
    clk_125m_i : in std_logic;

    pad_i : in std_logic;

    pulse_o : out t_subpulse
  );
end pulser_input;

architecture arch of pulser_input is
  signal q : std_logic_vector(7 downto 0);
begin
  inst_ISERDESE3 : ISERDESE3
    generic map (
      DATA_WIDTH => 8,
      FIFO_ENABLE => "FALSE",
      FIFO_SYNC_MODE => "FALSE",
      IS_CLK_B_INVERTED => '1',
      IS_CLK_INVERTED => '0',
      IS_RST_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE_PLUS")
    port map (
      FIFO_EMPTY => open, -- 1-bit output: FIFO empty flag
      INTERNAL_DIVCLK => open, -- 1-bit output: Internally divided down clock used when FIFO is disabled (do not connect)
      Q => Q,
      CLK => clk_500m_i,  -- input: High-speed clock
      CLKDIV => clk_125m_i, -- input: Divided Clock
      CLK_B => clk_500m_i, -- input: Inversion of High-speed clock CLK
      D => pad_i, -- input: Serial Data Input
      FIFO_RD_CLK => '0',
      FIFO_RD_EN => '0',
      RST => '0');
 
  process (clk_125m_i)
  begin
    if rising_edge (clk_125m_i) then
      --  Note: first bit on q(0)
      pulse_o.v <= q(7);
      if q(3) = q(7) then
        --  Assume the edge position <= 3
        if q(1) = q(7) then
          --  Assume the edge position <= 1
          if q(0) = q(7) then
            pulse_o.dly <= "000";
          else
            pulse_o.dly <= "001";
          end if;
        else
          --  Edge pos > 1
          if q(2) = q(7) then
            pulse_o.dly <= "010";
          else
            pulse_o.dly <= "011";
          end if;
        end if;
      else
        --  The edge position is > 3
        if q(5) = q(7) then
          if q(4) = q(7) then
            pulse_o.dly <= "100";
          else
            pulse_o.dly <= "101";
          end if;
        else
          if q(6) = q(7) then
            pulse_o.dly <= "110";
          else
            pulse_o.dly <= "111";
          end if;
        end if;
      end if;
    end if;
  end process;
end arch;
