--  Notes:
--  frametx mark encoding
--  channel A is Bunch clock
--  channel B is data frames
--  Channel A is sent before channel B

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ttc_frametx is
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;
    clk_en_i : in std_logic;

    --  Output (DDR).  First (1) then (0)
    out_o : out std_logic_vector(1 downto 0);

    --  Frame content
    subaddr_i : in std_logic_vector(7 downto 0);
    data_i : in std_logic_vector(7 downto 0);

    --  Frev pulse
    frev_i : in std_logic;

    --  Immediate load of data
    load_imm_i : in std_logic;

    --  Data are ready and must be loaded when previous data have been sent. 
    rdy_i : in std_logic;
    --  Data have been loaded
    ack_o : out std_logic
  );
end ttc_frametx;

architecture arch of ttc_frametx is
  signal chan_a, chan_b : std_logic_vector(43 downto 0);
  signal hamming : std_logic_vector(6 downto 0);
  signal hamaux : std_logic_vector(31 downto 0);
begin
  --  Channel A is 'just' a pulse for frev.
  chan_a <= frev_i & (42 downto 0 => '0');

  --  CF CTG-BST Master User?s and Maintainer?s Manual
  --  https://edms.cern.ch/file/485334/2/CTG-BSTv2_00.pdf
  hamaux <= b"00_0000_0000_0000" & "11" & subaddr_i & data_i;
  hamming(0) <= hamAux(0) xor hamAux(1) xor hamAux(2) xor hamAux(3) xor
    hamAux(4) xor hamAux(5);
  hamming(1) <= hamAux(6) xor hamAux(7) xor hamAux(8) xor hamAux(9) xor
    hamAux(10) xor hamAux(11) xor hamAux(12) xor hamAux(13) xor
    hamAux(14) xor hamAux(15) xor hamAux(16) xor hamAux(17) xor
    hamAux(18) xor hamAux(19) xor hamAux(20);
  hamming(2) <= hamAux(6) xor hamAux(7) xor hamAux(8) xor hamAux(9) xor
    hamAux(10) xor hamAux(11) xor hamAux(12) xor hamAux(13) xor
    hamAux(21) xor hamAux(22) xor hamAux(23) xor hamAux(24) xor
    hamAux(25) xor hamAux(26) xor hamAux(27);
  hamming(3) <= hamAux(0) xor hamAux(1) xor hamAux(2) xor hamAux(6) xor
    hamAux(7) xor hamAux(8) xor hamAux(9) xor hamAux(14) xor
    hamAux(15) xor hamAux(16) xor hamAux(17) xor hamAux(21) xor
    hamAux(22) xor hamAux(23) xor hamAux(24) xor hamAux(28) xor
    hamAux(29) xor hamAux(30);
  hamming(4) <= hamAux(0) xor hamAux(3) xor hamAux(4) xor hamAux(6) xor
    hamAux(7) xor hamAux(10) xor hamAux(11) xor hamAux(14) xor
    hamAux(15) xor hamAux(18) xor hamAux(19) xor hamAux(21) xor
    hamAux(22) xor hamAux(25) xor hamAux(26) xor hamAux(28) xor
    hamAux(29) xor hamAux(31);
  hamming(5) <= hamAux(1) xor hamAux(3) xor hamAux(5) xor hamAux(6) xor
    hamAux(8) xor hamAux(10) xor hamAux(12) xor hamAux(14) xor
    hamAux(16) xor hamAux(18) xor hamAux(20) xor hamAux(21) xor
    hamAux(23) xor hamAux(25) xor hamAux(27) xor hamAux(28) xor
    hamAux(30) xor hamAux(31);
  hamming(6) <= hamAux(2) xor hamAux(4) xor hamAux(5) xor hamAux(7) xor
    hamAux(8) xor hamAux(10) xor hamAux(13) xor hamAux(14) xor
    hamAux(17) xor hamAux(19) xor hamAux(20) xor hamAux(21) xor
    hamAux(24) xor hamAux(26) xor hamAux(27) xor hamAux(29) xor
    hamAux(30) xor hamAux(31);

  --  Channel B contains data + hamming code.
  chan_b <= "01" & hamaux & hamming & '1' & "11";

  inst_biphase: entity work.ttc_biphase
    generic map (
      g_chan_len => chan_a'length
    )
    port map (
      clk_i => clk_i,
      rst_n_i => rst_n_i,
      clk_en_i => clk_en_i,
      out_o => out_o,
      chan_a_i => chan_a,
      chan_b_i => chan_b,
      load_imm_i => load_imm_i,
      rdy_i => rdy_i,
      ack_o => ack_o
    );
end arch;