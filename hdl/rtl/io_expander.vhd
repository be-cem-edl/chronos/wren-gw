--------------------------------------------------------------------------------
-- CERN BE-CO-HT
--------------------------------------------------------------------------------
--
-- unit name:   output_expander
--
-- description: Mini-core for a chain of sn74lv595
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity io_expander is
  generic (
    --  Number of SN74LV595 in the output chain 
    g_NBR_CHIPS_OUT : positive := 1;
    --  Number of SN74LV166 in the input chain
    g_NBR_CHIPS_IN  : natural := 1;
    --  SCLK is clk_i / (2 * (g_CLKDIV + 1))
    g_CLKDIV    : natural := 1
  );
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    --  Signals to be serialized to the sn74lv595.
    --  Bit 0 is output A of the first chip (so the last serialized).
    outputs_i : in  std_logic_vector(8 * g_NBR_CHIPS_OUT - 1 downto 0);
    inputs_o  : out std_logic_vector(8 * g_NBR_CHIPS_IN - 1 downto 0);

    --  True iff inputs_o are valid (cleared at reset)
    inp_valid_o : out std_logic;

    --  Ports to sn74lv595
    exp_oe_n_o : out std_logic;
    exp_rclk_o : out std_logic;
    exp_sclk_o : out std_logic;
    exp_d_o    : out std_logic;
    exp_q_i    : in  std_logic
  );
end;

architecture arch of io_expander is
  constant max_len : natural :=
    8 * g_NBR_CHIPS_IN * boolean'pos(g_NBR_CHIPS_IN > g_NBR_CHIPS_OUT)
    + 8 * g_NBR_CHIPS_OUT * boolean'pos (g_NBR_CHIPS_OUT > g_NBR_CHIPS_IN);
  type t_state is (S_LATCH_INP, S_LATCH_OUT, S_CLOCK, S_PULSE, S_PULSE_OE);
  signal state : t_state;
  signal clk_counter : unsigned(31 downto 0);
  signal clk_tick : std_logic;
  signal out_regs : std_logic_vector(8 * g_NBR_CHIPS_OUT - 1 downto 0);
  signal in_regs  : std_logic_vector(8 * g_NBR_CHIPS_IN - 1 downto 0);
  signal shift_counter : unsigned(31 downto 0);
  signal is_first : std_logic;
begin
  --  Not tested in the other direction.
  assert g_NBR_CHIPS_IN > g_NBR_CHIPS_OUT severity failure;

  process (clk_i)
  begin
    if rising_edge(clk_i) then
      clk_tick <= '0';
      if rst_n_i = '0' then
        clk_counter <= (others => '0');
      else
        if clk_counter = g_CLKDIV then
          clk_tick <= '1';
          clk_counter <= (others => '0');
        else
          clk_counter <= clk_counter + 1;
        end if;
      end if;
    end if;
  end process;

  process (clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        state <= S_LATCH_INP;
        exp_oe_n_o <= '1';
        exp_sclk_o <= '0';
        exp_d_o <= '0';
        exp_rclk_o <= '0';
        inp_valid_o <= '0';
        is_first <= '1';
      elsif clk_tick = '1' then
        case state is
          when S_LATCH_INP =>
            out_regs <= outputs_i;
            shift_counter <= (others => '0');
            state <= S_LATCH_OUT;
          when S_LATCH_OUT =>
            exp_d_o <= out_regs (out_regs'left);
            if shift_counter >= (max_len - out_regs'left) - 1 then
              out_regs <= out_regs (out_regs'left - 1 downto 0) & '0';
            end if;
            if shift_counter <= in_regs'left then
              in_regs <= in_regs(in_regs'left - 1 downto 0) & exp_q_i;
            end if;
            exp_sclk_o <= '0';
            state <= S_CLOCK;
          when S_CLOCK =>
            exp_sclk_o <= '1';
            if shift_counter = max_len - 2 then 
              --  Will be a load for sn74lv166
              exp_rclk_o <= '0';
            end if;
            if shift_counter = max_len - 1 then
              state <= S_PULSE;
            else
              state <= S_LATCH_OUT;
            end if;
            shift_counter <= shift_counter + 1;
          when S_PULSE =>
            --  Clock the output flip-flop.
            exp_sclk_o <= '0';
            exp_rclk_o <= '1';
            if is_first = '1' then
              is_first <= '0';
            else
              inputs_o <= in_regs;
              inp_valid_o <= '1';
            end if;
            state <= S_PULSE_OE;
          when S_PULSE_OE =>
            --  Validate outputs
            exp_oe_n_o <= '0';
            state <= S_LATCH_INP;
        end case;
      end if;
    end if;
  end process;

end arch;