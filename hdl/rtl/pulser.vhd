library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pulser_pkg.all;

entity pulser is
  port (
    rst_n_i : std_logic;
    clk_i : std_logic; -- 125m

    --  Like reset.
    abort_i : std_logic;

    --  Configuration
    config_i : in t_pulser_config;
    --  Pulse to load the configuration (from the comparators).
    load_i : in  std_logic;
    --  Pulse to enable the configuration at the end of the comparator scan.
    --  The pulse is sent to every pulser at the same time.
    load_en_i : in std_logic;

    --  Start/stop inputs
    inputs_i : in t_subpulse_array(31 downto 0);

    --  The output (waveform)
    out_o : out t_subpulse;

    --  Interrupt enable
    int_en_o : out std_logic;

    --  Output enable
    out_en_o : out std_logic;

    --  True if the pulser is loaded.
    loaded_o : out std_logic;
    --  True if the pulser is running (generate pulses).
    running_o  : out std_logic;
    --  True if the pulser is waiting for the start pulse
    start_o : out std_logic;
    --  True if the current configuration is completed (either normally or aborted)
    done_o : out std_logic;

    --  Current state (for debug)
    state_o : out std_logic_vector(3 downto 0)
  );
end pulser;

architecture arch of pulser is
  type t_state is (
      --  Pulser is idle, not configured.
      S_IDLE,
      --  Pulser is configured, waiting for the comparator clock tick
      S_CONFIG,
      --  Wait for a number of cycles.
      S_WAIT,
      --  Wait for start
      S_START,
      --  Initial delay
      S_DELAY,
      S_HI,
      S_LO);

  type t_regs is record
    state : t_state;
    cfg : t_pulser_config;
    --  Initial delay after the start.
    idelay : unsigned(31 downto 0);
    --  Number of pulses (in the current waveform)
    npulses : unsigned (31 downto 0);
    --  Number of clock cycles in the high state
    hcount : unsigned(31 downto 0);
    --  Number of clock cycles in the current pulse
    --  When this counter reaches 0, this is the end of the pulse
    pcount : unsigned(31 downto 0);
    --  Extra delay (in ns)
    delay : unsigned (4 downto 0);
    --  No clock sync (1Ghz)
    noclock : boolean;
    --  Never ending pulses
    infinite : boolean;
    --  Level (or one infinite pulse)
    level : boolean;
    --  Pulser started
    start_p : std_logic;
    --  Pulser has been stopped.  Need to wait until the end of the width.
    stopped : boolean;
  end record;

  signal regs, nregs : t_regs;
  signal start_d, stop, clk_d : std_logic;
  signal start, clk, n_out : t_subpulse;
  signal n_done : std_logic;
  signal s_eoc, s_eow : boolean;
begin
  process (clk_i) is
  begin
    if rising_edge (clk_i) then
      if rst_n_i = '0' or abort_i = '1' then
        regs <= (state => S_IDLE,
                 cfg => (start => (others => '0'),
                         stop => (others => '0'),
                         clk_en => (others => '0'),
                         int_en => '0',
                         out_en => '0',
                         repeat => '0',
                         time_ns => (others => '0'),
                         idelay => (others => '0'),
                         high => (others => '0'),
                         period => (others => '0'),
                         npulses => (others => '0')),
                 idelay => (others => '0'),
                 npulses => (others => '0'),
                 hcount => (others => '0'),
                 pcount => (others => '0'),
                 delay => (others => '0'),
                 noclock => false,
                 infinite => false,
                 level => false,
                 start_p => '0',
                 stopped => false);
        out_o <= ('0', "000");
        state_o <= (others => '0');
        done_o <= '0';
      else
        regs <= nregs;
        out_o <= n_out;
        done_o <= n_done;

        case regs.state is
          when S_IDLE =>
            state_o <= "0000";
          when S_CONFIG =>
            state_o <= "0001";
          when S_WAIT =>
            state_o <= "0010";
          when S_START =>
            state_o <= "0011";
          when S_DELAY =>
            state_o <= "0100";
          when S_HI =>
            state_o <= "0101";
          when S_LO =>
            state_o <= "0110";
        end case;
      end if;
    end if;
  end process;

  process (clk_i) is
  begin
    if rising_edge (clk_i) then
      start_d <= start.v;
      start <= inputs_i (to_integer(unsigned(regs.cfg.start)));
      stop <= inputs_i (to_integer(unsigned(regs.cfg.stop))).v;
      clk_d <= clk.v;
      --  Note: clocks are delayed by 1 cycle.
      clk <= inputs_i (to_integer(unsigned(regs.cfg.clk_en)));
    end if;
  end process;

  loaded_o <= '0' when regs.state = S_IDLE or regs.state = S_CONFIG else '1';
  running_o <= '1' when regs.state = S_HI or regs.state = S_LO else '0';
  start_o <= regs.start_p;
  int_en_o <= regs.cfg.int_en;
  out_en_o <= regs.cfg.out_en;

  process (regs, config_i, load_en_i, load_i, start, start_d, stop, clk, clk_d)
  is
    --  End of period (1 pulse), end of waveforn (N pulses)
    variable eop, eow : boolean;

    variable n_count : unsigned(31 downto 0);
    variable n_delay : unsigned(4 downto 0);
  begin
    nregs <= regs;
    n_out <= ('0', "000");
    n_done <= '0';

    nregs.start_p <= '0';

    --  Pulse period
    eop := false;
    eow := false;
    case regs.state is
      when S_HI | S_LO =>
        --  Pulse period
        if regs.noclock then
          n_count := regs.pcount - 8;
          if n_count < 8 then
            --  End of cycle
            eop := True;
          else
            nregs.pcount <= n_count;
          end if;
          if regs.npulses = 1 and not regs.infinite then
            --  Done.
            eow := True;
          elsif eop then
            --  Next pulse
            nregs.npulses <= unsigned(regs.npulses) - 1;
            nregs.pcount <= unsigned (regs.cfg.period) + n_count;
          end if;
        else
          if clk_d = '0' and clk.v = '1' then
            if regs.pcount = 1 then
              --  End of period.
              eop := True;
            else
              nregs.pcount <= regs.pcount - 1;
            end if;
            if regs.npulses = 1 and not regs.infinite then
              --  Done.
              eow := True;
            elsif eop then
              nregs.npulses <= regs.npulses - 1;
              nregs.pcount <= unsigned (regs.cfg.period);
            end if;
          end if;
        end if;
      when others =>
        null;
    end case;

    --  For debug.
    s_eow <= eow;
    s_eoc <= eop;

    case regs.state is
      when S_IDLE =>
        --  Fully disabled, wait for a configuration pulse
        --  (this is handled at the end of this process)
        null;
      when S_CONFIG =>
        --  Configured, wait for load pulse.
        nregs.hcount <= unsigned (regs.cfg.high);
        nregs.pcount <= unsigned (regs.cfg.period);
        nregs.npulses <= unsigned (regs.cfg.npulses);
        nregs.noclock <= regs.cfg.clk_en = c_NO_CLOCK;
        nregs.level <= unsigned(regs.cfg.period) = 0;
        nregs.infinite <= unsigned (regs.cfg.npulses) = 0;
        --  Move the ns subpart to delay.
        nregs.delay <= "00" & unsigned (regs.cfg.time_ns(2 downto 0));
        if load_en_i = '1' then
          if unsigned(regs.cfg.time_ns (9 downto 3)) = 0 then
            nregs.state <= S_START;
          else
            nregs.state <= S_WAIT;
          end if;
        end if;

      when S_WAIT =>
        --  Fine grain wait (well 8ns granularity)
        if unsigned(regs.cfg.time_ns(9 downto 3)) = 1 then
            --  Wait for start
          nregs.state <= S_START;
        else
          nregs.cfg.time_ns <= std_logic_vector(unsigned(regs.cfg.time_ns) - 8);
        end if;

      when S_START =>
        --  Wait for start.
        nregs.idelay <= unsigned (regs.cfg.idelay);
        nregs.npulses <= unsigned (regs.cfg.npulses);
        nregs.hcount <= unsigned (regs.cfg.high);
        nregs.stopped <= false;
        if regs.cfg.start = c_NO_START or (start_d = '0' and start.v = '1')
        then
          nregs.start_p <= '1';

          --  Start arrived!
          if regs.noclock then
            if regs.cfg.start = c_NO_START then
              --  Propagate the wait delay
              n_delay := regs.delay;
            else
              --  Realign on the start.
              --  Note: it should be the max between start.dly and
              --    the remaining delay.
              n_delay := "00" & unsigned (start.dly);
            end if;
            --  Add the initial delay
            n_delay := n_delay + ("00" & unsigned (regs.cfg.idelay(2 downto 0)));
            nregs.delay <= n_delay;

            nregs.pcount <= unsigned (regs.cfg.period) + resize(n_delay, 32);
            if unsigned (regs.cfg.idelay(31 downto 3)) = 0 then
              nregs.state <= S_HI;
            else
              nregs.state <= S_DELAY;
            end if;
          else
            -- Clock
            nregs.pcount <= unsigned (regs.cfg.period);
            if regs.cfg.idelay = (31 downto 0 => '0') then
              --  Start immediately.
              nregs.state <= S_HI;
            else
              nregs.state <= S_DELAY;
            end if;
          end if;
        end if;

      when S_DELAY =>
        if regs.noclock then
          if regs.delay(3) = '1' then
            nregs.pcount <= regs.pcount - 8;
            if unsigned (regs.idelay(31 downto 3)) = 0 then
              nregs.state <= S_HI;
            else
              --  Substract 8ns
              nregs.delay(3) <= '0';
            end if;
          elsif unsigned (regs.idelay(31 downto 3)) = 1 then
            --  Between 8 and 15ns of delay, postponed to S_HI.
            nregs.state <= S_HI;
          else
            nregs.idelay <= regs.idelay - 8;
          end if;
        else
          if clk_d = '0' and clk.v = '1' then
            --  For clocks, 0 means immediately, 1 means at the first clock.
            if unsigned (regs.idelay) = 1 then
              nregs.state <= S_HI;
              nregs.delay <= "00" & unsigned (clk.dly);
            else
              nregs.idelay <= regs.idelay - 1;
            end if;
          end if;
        end if;
        if stop = '1' then
          if regs.cfg.repeat = '1' then
            nregs.state <= S_START;
          else
            nregs.state <= S_IDLE;
            n_done <= '1';
          end if;
        end if;

      when S_HI =>
        --  Output.
        if regs.delay(3) = '0' then
          n_out <= ('1', std_logic_vector(regs.delay(2 downto 0)));
          n_count := unsigned(regs.hcount) - 8 + regs.delay(2 downto 0);
          nregs.delay <= (others => '0');
        else
          nregs.delay(3) <= '0';
          n_count := unsigned(regs.hcount);
        end if;
        if n_count < 8 and not regs.level then
          nregs.hcount <= unsigned (regs.cfg.high);
          nregs.delay <= n_count(4 downto 0);
          nregs.state <= S_LO;
        else
          nregs.hcount <= n_count;
        end if;

        if stop = '1' then
          nregs.stopped <= true;
          if regs.level then
            --  Stop immediately for levels.
            nregs.delay <= (others => '0');
            nregs.state <= S_LO;
          end if;
        end if;

        if eop and not eow then
          --  If end of period (but not the last), start a new period.
          nregs.hcount <= unsigned (regs.cfg.high);
        end if;

      when S_LO =>
        n_out <= ('0', std_logic_vector(regs.delay(2 downto 0)));
        nregs.delay <= (others => '0');

        --  Pulse period
        if eow or stop = '1' or regs.stopped then
          if regs.cfg.repeat = '1' then
            nregs.state <= S_START;
          else
            nregs.state <= S_IDLE;
            n_done <= '1';
          end if;
        elsif eop then
          if regs.noclock then
            nregs.delay <= "00" & regs.pcount(2 downto 0);
          else
            nregs.delay <= "00" & unsigned(clk.dly);
          end if;
          nregs.state <= S_HI;
        end if;
    end case;

    --  Load will always abort the current config.
    if load_i = '1' then
      nregs.cfg <= config_i;
      nregs.state <= S_CONFIG;
      if regs.state /= S_IDLE then
        n_done <= '1';
      end if;
    end if;
  end process;
end arch;
