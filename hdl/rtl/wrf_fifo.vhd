-------------------------------------------------------------------------------
-- Title      : WR fabric fifo
-------------------------------------------------------------------------------
-- Company    : CERN BE-CO-HT
-- Standard   : VHDL
--
-- FIFO on a WR fabric stream so that input is never stalled
-- Could be used to send a stream to several sinks.
-- We don't care about ack, err, rty.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2023 CERN / BE-CO-HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;

library work;
use work.genram_pkg.all;
use work.wr_fabric_pkg.all;

entity wrf_fifo is
  generic (
    g_depth : integer := 64);
  port (
    clk_sys_i : in std_logic;
    rst_n_i   : in std_logic;

    --  Incoming stream
    snk_i : in  t_wrf_sink_in;
    snk_o : out t_wrf_sink_out;

    --  Outgoing stream
    src_o : out t_wrf_source_out;
    src_i : in  t_wrf_source_in;

    -- Stats
    max_count_o : out std_logic_vector(f_log2_size(g_depth)-1 downto 0);
    fifo_full_o : out std_logic;
    clear_stat_i : in std_logic
   );
end wrf_fifo;

architecture rtl of wrf_fifo is
  constant c_fifo_width : integer := 16 + 2 + 3;

  signal fifo_write   : std_logic;
  signal fifo_read    : std_logic;
  signal fifo_in_ser  : std_logic_vector(c_fifo_width-1 downto 0);
  signal fifo_out_ser : std_logic_vector(c_fifo_width-1 downto 0);
  signal fifo_full    : std_logic;
  signal fifo_empty   : std_logic;

  signal cyc_d0 : std_logic;
  
  --  Set if a frame is being sent on src.
  signal frame_out, n_frame_out : std_logic;

  signal sof_in, eof_in : std_logic;
  signal sof_out, eof_out : std_logic;

  signal fifo_full_out : std_logic;
  signal count, max_count_out : std_logic_vector(max_count_o'range);
begin  -- rtl

  p_delay_cyc : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        cyc_d0 <= '0';
      else
        if snk_i.cyc = '0' then
          cyc_d0 <= '0';
        elsif snk_i.cyc = '1' and snk_i.stb = '1' then
          cyc_d0 <= '1';
        end if;
      end if;
    end if;
  end process;

  snk_o.err <= '0';
  snk_o.rty <= '0';
  snk_o.stall <= '0';

  p_gen_ack : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        snk_o.ack <= '0';
      else
        snk_o.ack <= snk_i.cyc and snk_i.stb;
      end if;
    end if;
  end process;

  sof_in <= not cyc_d0 and snk_i.cyc and snk_i.stb;
  eof_in <= cyc_d0 and not snk_i.cyc;

  fifo_write  <= (snk_i.stb and snk_i.cyc) or eof_in;
  fifo_in_ser <= snk_i.sel(0) & sof_in & eof_in & snk_i.adr & snk_i.dat;

  U_fifo : generic_sync_fifo
    generic map (
      g_data_width => c_fifo_width,
      g_size       => g_depth,
      g_with_count => false)
    port map (
      rst_n_i => rst_n_i,
      clk_i   => clk_sys_i,
      we_i    => fifo_write,
      d_i     => fifo_in_ser,
      rd_i    => fifo_read,
      q_o     => fifo_out_ser,
      empty_o => fifo_empty,
      full_o  => fifo_full,
      almost_empty_o => open,
      almost_full_o => open,
      count_o => count
      );

  n_frame_out <= sof_out or (frame_out and not eof_out);

  p_gen_valid_flag : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        frame_out <= '0';
      else
        frame_out <= n_frame_out;
      end if;
    end if;
  end process;

  fifo_read     <= not fifo_empty and not src_i.stall;

  src_o.dat   <= fifo_out_ser(15 downto 0);
  src_o.adr   <= fifo_out_ser(17 downto 16);
  eof_out <= fifo_out_ser (18);
  sof_out <= fifo_out_ser (19);
  src_o.sel <= '0' & fifo_out_ser(20);
  src_o.cyc   <= n_frame_out;
  src_o.stb   <= n_frame_out and (not fifo_empty);
  src_o.we    <= '1';

  p_stat : process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' or clear_stat_i = '1' then
        max_count_out <= (others => '0');
        fifo_full_out <= '0';
      else
        fifo_full_out <= fifo_full_out or fifo_full;
        if unsigned(count) > unsigned(max_count_out) then
          max_count_out <= count;
        end if;
      end if;
    end if;
  end process;

  max_count_o <= max_count_out;
  fifo_full_o <= fifo_full_out;
end rtl;
