--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   wren_vme_top
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.vme64x_pkg.all;
use work.axi4_pkg.all;
use work.wr_xilinx_pkg.all;
use work.pulser_pkg.all;
use work.wr_fabric_pkg.all;
use work.wrcore_pkg.all;
use work.endpoint_pkg.all;
use work.streamers_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity wren_vme_v1_top is
  generic (
    -- g_WRPC_INITF    : string  :=  "../../../../../dependencies/files/wrc-pxie.bram";
    g_WRPC_INITF    : string  :=  "";
    g_hwbld_date    : std_logic_vector(31 downto 0) := x"0000_0000";
    g_WITH_VME_MASTER : boolean := False;
    g_pcb_version : string := "vme v1.0";
    --  If set, generate 10Mhz + PPS from clk_main input clock (the one which has a uFL).
    --  This could be used to enable grand master mode (and abscal mode).  The Si5941 needs
    --  a special initialization so that the clk_main is not locked on clksys.
    g_with_external_clock_input : boolean := false;
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the
    -- testbench. Its purpose is to reduce some internal counters/timeouts
    -- to speed up simulations.
    g_SIMULATION     : integer := 0);
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------
    ps_por_i               : in std_logic;

    fpga_pl_clksys_p_i     : in  std_logic;
    fpga_pl_clksys_n_i     : in  std_logic;

    wr_clk_helper_125m_p_i : in  std_logic;
    wr_clk_helper_125m_n_i : in  std_logic;
    wr_clk_main_125m_p_i   : in  std_logic;
    wr_clk_main_125m_n_i   : in  std_logic;
    wr_clk_sfp_125m_p_i    : in  std_logic;
    wr_clk_sfp_125m_n_i    : in  std_logic;

    --  Unused
    wr_clk_aux_p_i         : in  std_logic;
    wr_clk_aux_n_i         : in  std_logic;

    -- clk_helper_25m_i       : in  std_logic;  --  The oscillator before the pll

    --  VME
    vme_as_n_b      : inout std_logic;
    vme_as_oe_n_o   : out   std_logic;
    vme_as_dir_o    : out   std_logic;
    vme_write_n_b   : inout std_logic;
    vme_ds_n_i      : in    std_logic_vector(1 downto 0);
    vme_ds_n_o      : out   std_logic_vector(1 downto 0);
    vme_ds_oe_o     : out   std_logic;
    vme_data_b      : inout std_logic_vector(31 downto 0);
    vme_data_dir_o  : out   std_logic;
    vme_data_oe_n_o : out   std_logic;
    vme_addr_b      : inout std_logic_vector(31 downto 1);
    vme_lword_n_b   : inout std_logic;
    vme_am_b        : inout std_logic_vector(5 downto 0);
    vme_addr_dir_o  : out   std_logic;
    vme_addr_oe_n_o : out   std_logic;
    vme_dtack_n_i   : in    std_logic;
    vme_dtack_n_o   : out   std_logic;
    vme_dtack_oe_o  : out   std_logic;
    vme_retry_n_i   : in    std_logic;
    vme_retry_n_o   : out   std_logic;
    vme_retry_oe_o  : out   std_logic;
    vme_berr_n_i    : in    std_logic;
    vme_berr_oe_o   : out   std_logic;
    vme_iack_n_i    : in    std_logic;
    vme_iack_oe_o   : out   std_logic;
    vme_iack_n_o    : out   std_logic;
    vme_iackin_n_i  : in    std_logic;
    vme_iackout_n_o : out   std_logic;
    vme_irq_o     : out   std_logic_vector(7 downto 1);
    vme_irq_n_i     : in    std_logic_vector(7 downto 1);
    vme_bg3in_n_i   : in    std_logic;
    vme_bg3out_n_o  : out   std_logic;
    vme_br3_o       : out   std_logic;
    vme_bbsy_oe_o   : out   std_logic;
    vme_bbsy_n_i    : inout std_logic;
    vme_sysclk_b    : inout std_logic;
    vme_sysreset_n_b : inout std_logic;

    --  IO expanders
    --  vme_interface (p19)
    ioexp_vme_sclk_o : out std_logic;
    ioexp_vme_rclk_o : out std_logic;
    ioexp_vme_d_o    : out std_logic;
    ioexp_hwbyte1_d_o : out std_logic;
    ioexp_hwbyte2_d_o : out std_logic;
    ioexp_hwbyte1_rclk_o : out std_logic;
    ioexp_hwbyte2_rclk_o : out std_logic;

    ioexp_fpio_rclk_o : out std_logic;
    ioexp_fpio_sclk_o : out std_logic;
    ioexp_fpio_d_o    : out std_logic;

    ioexp_gpio_rclk_o : out std_logic;
    ioexp_gpio_sclk_o : out std_logic;
    ioexp_gpio_d_o    : out std_logic;
    ioexp_gpio_q_i    : in  std_logic;

    ioexp_all_rst_n_o : out std_logic;
    ioexp_all_oe_n_o : out std_logic;

    fp_led0_o : out std_logic;
    fp_led1_o : out std_logic;
    pp_led0_o : out std_logic;
    pp_led1_o : out std_logic;
    sfp_led_o : out std_logic;

    led_sfp0_green_o : out std_logic;
    led_sfp0_yellow_o : out std_logic;
    led_sfp1_green_o : out std_logic;
    led_sfp1_yellow_o : out std_logic;

    wr_abscal_o : out std_logic;
    wr_pps_o    : out std_logic;

    --  SPI interface to PLL
    spi_main_cs_n_o   : out std_logic;
    spi_mosi_o        : out std_logic;
    spi_miso_i        : in  std_logic;
    spi_sck_o         : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/Os for transceiver
    ---------------------------------------------------------------------------
    sfp0_txp_o         : out std_logic;
    sfp0_txn_o         : out std_logic;
    sfp0_rxp_i         : in  std_logic;
    sfp0_rxn_i         : in  std_logic;
    sfp0_sda_b         : inout std_logic;
    sfp0_scl_b         : inout std_logic;

    --  sfp1
    sfp1_txp_o  : out   std_logic;
    sfp1_txn_o  : out   std_logic;
    sfp1_rxp_i  : in    std_logic;
    sfp1_rxn_i  : in    std_logic;

    sfp1_sda_b  : inout std_logic;
    sfp1_scl_b  : inout std_logic;

    userio_b    : inout std_logic_vector(31 downto 0);
    fp_io_b     : inout std_logic_vector(31 downto 0);

    p0_turnclk_o : out std_logic_vector(2 downto 1);
    p0_bunchclk_o : out std_logic_vector(2 downto 1);
    p2_ttc_d0_o   : out std_logic;
    p2_ttc_d1_o   : out std_logic;
    p2_rs485_d0_b : inout std_logic;
    p2_rs485_d1_b : inout std_logic;

    ---------------------------------------------------------------------------
    -- EEPROM I2C interface for storing configuration and accessing unique ID
    ---------------------------------------------------------------------------
    eeprom_sda_b  : inout std_logic;
    eeprom_scl_b  : inout std_logic;
    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------
    uart_rxd_i    : in  std_logic;
    uart_txd_o    : out std_logic;

    gth0_txp_o  : out std_logic;
    gth0_txn_o  : out std_logic;
    gth0_rxp_i  : in  std_logic;
    gth0_rxn_i  : in  std_logic;

    gth1_txp_o  : out std_logic;
    gth1_txn_o  : out std_logic;
    gth1_rxp_i  : in  std_logic;
    gth1_rxn_i  : in  std_logic;

    gth_bclk1_p_i : in std_logic;
    gth_bclk1_n_i : in std_logic;
    gth_bclk2_p_i : in std_logic;
    gth_bclk2_n_i : in std_logic
);
end entity wren_vme_v1_top;

architecture arch of wren_vme_v1_top is
  -- clock and reset
  signal clk_sys_62m5     : std_logic;
  signal clk_sys_125m     : std_logic;
  signal clk_sys_250m : std_logic;
  signal clk_sys_500m     : std_logic;
  signal rst_sys_n   : std_logic;

  signal gth_clk_250m : std_logic;

  signal clk_bunch : std_logic_vector(2 downto 1);

  alias clk : std_logic is clk_sys_62m5;
  alias rst_n : std_logic is rst_sys_n;

  attribute keep : string;

  --  WR fabric.
  signal eth_tx_out : t_wrf_source_out;
  signal eth_tx_in  : t_wrf_source_in;
  signal eth_rx_out : t_wrf_sink_out;
  signal eth_rx_in  : t_wrf_sink_in;

  --  mpsoc to pci map
  signal m_axi_out: t_axi4_lite_master_out_32;
  signal m_axi_in : t_axi4_lite_master_in_32;

  signal vme_wb_out : t_wishbone_master_out;
  signal vme_wb_in  : t_wishbone_master_in;
  signal vme_dati_swp, vme_dato_swp : std_logic_vector(31 downto 0);

  signal vme_irq_level  : std_logic_vector(2 downto 0);
  signal vme_irq_vector : std_logic_vector(7 downto 0);

  signal host_wb_out : t_wishbone_master_out;
  signal host_wb_in  : t_wishbone_master_in;

  signal wr_master_out : t_wishbone_master_out;
  signal wr_master_in  : t_wishbone_master_in;

  signal wb_si5340_out : t_wishbone_master_out;
  signal wb_si5340_in  : t_wishbone_master_in;

  -- WRPC TM interface and aux clocks
  signal tm_link_up         : std_logic;
  signal tm_tai             : std_logic_vector(39 downto 0);
  signal tm_cycles          : std_logic_vector(27 downto 0);
  signal tm_time_valid      : std_logic;

  signal s_axi_addr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal s_axi_awvalid : STD_LOGIC;
  signal s_axi_awready :  STD_LOGIC;
  signal s_axi_wdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_wvalid : STD_LOGIC;
  signal s_axi_wready :  STD_LOGIC;
  signal s_axi_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_bvalid :  STD_LOGIC;
  signal s_axi_bready : STD_LOGIC;
  signal s_axi_arvalid : std_logic;
  signal s_axi_arready : std_logic;
  signal s_axi_rready : std_logic;
  signal s_axi_rvalid : std_logic;
  signal s_axi_rrsp : std_logic_vector(1 downto 0);
  signal s_axi_rdata : STD_LOGIC_VECTOR ( 31 downto 0 );

  signal S_AXI_LOG_awid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_awaddr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal S_AXI_LOG_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_LOG_awvalid : STD_LOGIC;
  signal S_AXI_LOG_awready : STD_LOGIC;
  signal S_AXI_LOG_wdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal S_AXI_LOG_wstrb : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal S_AXI_LOG_wlast : STD_LOGIC;
  signal S_AXI_LOG_wvalid : STD_LOGIC;
  signal S_AXI_LOG_wready : STD_LOGIC;
  signal S_AXI_LOG_bid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_bresp : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_LOG_bvalid : STD_LOGIC;
  signal S_AXI_LOG_bready : STD_LOGIC;

  signal pps_p, pps_valid : std_logic;

  signal irq : std_logic;

  signal clk_pl, rst_pl_n : std_logic;
  
  signal gpio_in, gpio_out : std_logic_vector(94 downto 0);

  signal lemo_oe, lemo_term : std_logic_vector(31 downto 0);

  signal pp_out, pp_pin_oe : std_logic_vector(31 downto 0);

  signal pps_led : std_logic;

  --  io-expander VME
  signal ioexp_vme_oe_n, ioexp_vme_rclk : std_logic;
  signal ioexp_vme_load, ioexp_vme_shift : std_logic;
  signal rs485_data_re_n, rs485_data_de, rs485_dir, rs485_dir_reg : std_logic_vector(1 downto 0);
  signal rs485_data_in, rs485_data_out, rs485_data_reg : std_logic_vector(1 downto 0);
  signal rs485_wr : std_logic;
  signal vme_sysclk_dir : std_logic;
  signal vme_sysreset_dir : std_logic;
  signal p0_hwbyte_h2, p0_hwbyte_l2 : std_logic_vector(7 downto 0);
  signal p0_hwbyte_h1, p0_hwbyte_l1 : std_logic_vector(7 downto 0);
  signal userio_in : std_logic_vector(31 downto 0);
  signal userio_dir, userio_en, userio_oe_n : std_logic_vector(3 downto 0);
  signal userio_dir_reg : std_logic_vector(3 downto 0);
  signal p0_hwbyte_h2_oe, p0_hwbyte_l2_oe : std_logic;
  signal p0_hwbyte_h1_oe, p0_hwbyte_l1_oe : std_logic;
  signal p0_hwbyte_h2_oe_n, p0_hwbyte_l2_oe_n : std_logic;
  signal p0_hwbyte_h1_oe_n, p0_hwbyte_l1_oe_n : std_logic;

  signal vme_dtack_oe_n, vme_retry_oe_n : std_logic;
  signal vme_irq_n     :  std_logic_vector(7 downto 1);

  -- io-expander FP
  signal ioexp_fpio_oe_n : std_logic;
  signal ioexp_fpio_load, ioexp_fpio_shift : std_logic;
  signal fp_term_en : std_logic_vector(31 downto 0);
  signal fp_en_n      : std_logic_vector(31 downto 0);

  --  io-expander gpio
  signal ioexp_gpio_oe_n : std_logic;
  signal sfp0_los, sfp1_los : std_logic;  -- rssi
  signal switch0_n, switch1_n : std_logic;
  signal vme_noga_n, vme_noga_usr : std_logic_vector(4 downto 0);
  signal vme_usega_n : std_logic;
  signal vme_ga_int : std_logic_vector(4 downto 0);
  signal vme_gap_int : std_logic;
  signal vme_ga : std_logic_vector(5 downto 0);
  signal fp_id : std_logic_vector(2 downto 0);
  signal pp_id : std_logic_vector(2 downto 0);
  signal sfp0_tx_disable, sfp1_tx_disable : std_logic;
  signal sfp0_det, sfp1_det : std_logic;
  signal pp_en, pp_oe_n : std_logic;

  signal vme_rst_n, gpio_inp_valid : std_logic;

  signal p0_h1 : std_logic_vector(15 downto 0);
  type t_p0_clk is record
    bunch_cnt : unsigned(15 downto 0);
  end record;
  type t_p0_clk_arr is array (natural range <>) of t_p0_clk;
  signal p0_clk : t_p0_clk_arr(2 downto 1);

  --  spi
  signal soc_spi_helper_cs_n : std_logic;
  signal soc_spi_main_cs_n   : std_logic;
  signal soc_spi_mosi        : std_logic;
  signal soc_spi_miso        : std_logic;
  signal soc_spi_sck         : std_logic;

  signal clk_sys2_125m : std_logic;

  --  To VME master
  signal M_AXI_VME_awaddr : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal M_AXI_VME_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal M_AXI_VME_awsize : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal M_AXI_VME_awburst : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal M_AXI_VME_awlock : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_awcache : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal M_AXI_VME_awprot : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal M_AXI_VME_awqos : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal M_AXI_VME_awvalid : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_awready : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_wdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal M_AXI_VME_wstrb : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal M_AXI_VME_wlast : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_wvalid : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_wready : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_bresp : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal M_AXI_VME_bid : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal M_AXI_VME_bvalid : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_bready : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_araddr : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal M_AXI_VME_arlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal M_AXI_VME_arsize : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal M_AXI_VME_arburst : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal M_AXI_VME_arlock : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_arcache : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal M_AXI_VME_arprot : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal M_AXI_VME_arqos : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal M_AXI_VME_arvalid : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_arready : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_rdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal M_AXI_VME_rresp : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal M_AXI_VME_rlast : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_rvalid : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_rready : STD_LOGIC_VECTOR ( 0 to 0 );
  signal M_AXI_VME_rid : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal M_AXI_VME_araddr_masked, M_AXI_VME_awaddr_masked : STD_LOGIC_VECTOR ( 31 downto 0 );

  --  VME master core uses 256MB
  constant M_AXI_VME_addr_mask : std_logic_vector(31 downto 0) := x"0fff_ffff";

  --  From 'view instantiation template'
  component mpsoc is
    port (
      M_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
      GPIO_tri_i : in STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_o : out STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_t : out STD_LOGIC_VECTOR ( 94 downto 0 );
      S_AXI_aruser : in STD_LOGIC;
      S_AXI_awuser : in STD_LOGIC;
      S_AXI_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_awlock : in STD_LOGIC;
      S_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awvalid : in STD_LOGIC;
      S_AXI_awready : out STD_LOGIC;
      S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_wlast : in STD_LOGIC;
      S_AXI_wvalid : in STD_LOGIC;
      S_AXI_wready : out STD_LOGIC;
      S_AXI_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_bvalid : out STD_LOGIC;
      S_AXI_bready : in STD_LOGIC;
      S_AXI_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_arlock : in STD_LOGIC;
      S_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arvalid : in STD_LOGIC;
      S_AXI_arready : out STD_LOGIC;
      S_AXI_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_rlast : out STD_LOGIC;
      S_AXI_rvalid : out STD_LOGIC;
      S_AXI_rready : in STD_LOGIC;
      S_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_aruser : in STD_LOGIC;
      S_AXI_LOG_awuser : in STD_LOGIC;
      S_AXI_LOG_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_awlock : in STD_LOGIC;
      S_AXI_LOG_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awvalid : in STD_LOGIC;
      S_AXI_LOG_awready : out STD_LOGIC;
      S_AXI_LOG_wdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_wstrb : in STD_LOGIC_VECTOR ( 15 downto 0 );
      S_AXI_LOG_wlast : in STD_LOGIC;
      S_AXI_LOG_wvalid : in STD_LOGIC;
      S_AXI_LOG_wready : out STD_LOGIC;
      S_AXI_LOG_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_bvalid : out STD_LOGIC;
      S_AXI_LOG_bready : in STD_LOGIC;
      S_AXI_LOG_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_arlock : in STD_LOGIC;
      S_AXI_LOG_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arvalid : in STD_LOGIC;
      S_AXI_LOG_arready : out STD_LOGIC;
      S_AXI_LOG_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_rdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_rlast : out STD_LOGIC;
      S_AXI_LOG_rvalid : out STD_LOGIC;
      S_AXI_LOG_rready : in STD_LOGIC;
      S_AXI_LOG_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      clk_i : in STD_LOGIC;
      aresetn_i : in STD_LOGIC;
      pl_resetn_o : out STD_LOGIC;
      pl_ps_irq0_i : in STD_LOGIC_VECTOR ( 0 to 0 );
      pl_clk_o : out STD_LOGIC;
      saxi_clk : in STD_LOGIC;
      spi0_sclk_i : in STD_LOGIC;
      spi0_sclk_o : out STD_LOGIC;
      spi0_sclk_t : out STD_LOGIC;
      spi0_m_i : in STD_LOGIC;
      spi0_m_o : out STD_LOGIC;
      spi0_mo_t : out STD_LOGIC;
      spi0_s_i : in STD_LOGIC;
      spi0_s_o : out STD_LOGIC;
      spi0_so_t : out STD_LOGIC;
      spi0_ss_i_n : in STD_LOGIC;
      spi0_ss_o_n : out STD_LOGIC;
      spi0_ss_n_t : out STD_LOGIC;
      spi0_ss1_o_n : out STD_LOGIC;
      saxi_log_clk : in STD_LOGIC;
      axi_vme_clk : in STD_LOGIC;
      M_AXI_VME_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_VME_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
      M_AXI_VME_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_VME_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_VME_awlock : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_VME_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_VME_awregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_VME_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_VME_awuser : out STD_LOGIC_VECTOR ( 15 downto 0 );
      M_AXI_VME_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_VME_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_VME_wlast : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_VME_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_VME_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
      M_AXI_VME_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_VME_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_VME_arlock : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_VME_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_VME_arregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_VME_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_VME_aruser : out STD_LOGIC_VECTOR ( 15 downto 0 );
      M_AXI_VME_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_VME_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_VME_rlast : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_VME_rready : out STD_LOGIC_VECTOR ( 0 to 0 )
    );
    end component mpsoc;
begin
  inst_mpsoc: mpsoc
    port map (
      --  Master bus (from PS) to give CPU access to the wren memmap.
      M_AXI_araddr(31 downto 0)  => m_axi_out.araddr,
      M_AXI_arprot  => open,
      M_AXI_arready(0) => m_axi_in.arready,
      M_AXI_arvalid(0) => m_axi_out.arvalid,
      M_AXI_awaddr(31 downto 0) => m_axi_out.awaddr,
      M_AXI_awprot  => open,
      M_AXI_awready(0) => m_axi_in.awready,
      M_AXI_awvalid(0) => m_axi_out.awvalid,
      M_AXI_bready(0)  => m_axi_out.bready,
      M_AXI_bresp   => m_axi_in.bresp,
      M_AXI_bvalid(0)  => m_axi_in.bvalid,
      M_AXI_rdata   => m_axi_in.rdata,
      M_AXI_rready(0)  => m_axi_out.rready,
      M_AXI_rresp   => m_axi_in.rresp,
      M_AXI_rvalid(0) => m_axi_in.rvalid,
      M_AXI_wdata   => m_axi_out.wdata,
      M_AXI_wready(0)  => m_axi_in.wready,
      M_AXI_wstrb   => m_axi_out.wstrb,
      M_AXI_wvalid(0)  => m_axi_out.wvalid,

      --  Windowed access to the PS from the host.
      S_AXI_awuser => '0',
      S_AXI_awid => (others => '0'),
      S_AXI_awaddr => s_axi_addr,
      S_AXI_awlen => x"00", --  No burst, 1 beat
      S_AXI_awsize => "010", -- 4B
      S_AXI_awburst => "01", -- INCR
      S_AXI_awlock => '0',   -- Normal
      S_AXI_awcache => "0001",
      S_AXI_awprot => "001",
      S_AXI_awvalid => s_axi_awvalid,
      S_AXI_awready => s_axi_awready,
      S_AXI_awqos => (others => '0'),
      S_AXI_wdata => s_axi_wdata,
      S_AXI_wstrb => "1111",
      S_AXI_wlast => '1',
      S_AXI_wvalid => s_axi_wvalid,
      S_AXI_wready => s_axi_wready,
      S_AXI_bid => open,
      S_AXI_bresp => s_axi_bresp,
      S_AXI_bvalid => s_axi_bvalid,
      S_AXI_bready => s_axi_bready,
      S_AXI_aruser => '0',
      S_AXI_arid => (others => '0'),
      S_AXI_araddr => s_axi_addr,
      S_AXI_arlen => x"00",  -- No burst, 1 beat
      S_AXI_arsize => "010", -- 4B
      S_AXI_arburst => "01", -- INCR
      S_AXI_arlock => '0',  --  Normal
      S_AXI_arcache => "0001",
      S_AXI_arprot => "001",
      S_AXI_arvalid => s_axi_arvalid,
      S_AXI_arready => s_axi_arready,
      S_AXI_rid => open,
      S_AXI_rdata => s_axi_rdata,
      S_AXI_rresp => s_axi_rrsp,
      S_AXI_rlast => open,
      S_AXI_rvalid => s_axi_rvalid,
      S_AXI_rready => s_axi_rready,
      S_AXI_arqos => (others => '0'),

      saxi_clk => clk,

      saxi_log_clk => clk_sys_250m,

      --  Direct access to DDR for logging.
      S_AXI_LOG_awid => S_AXI_LOG_awid,
      S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
      S_AXI_LOG_awlen => S_AXI_LOG_awlen,
      S_AXI_LOG_awsize => b"100",  -- 100: 16 bytes, 011: 8 bytes
      S_AXI_LOG_awburst => b"01",  -- 00: FIXED, (01: INCR)
      S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
      S_AXI_LOG_awready => S_AXI_LOG_awready,
      S_AXI_LOG_wdata => S_AXI_LOG_wdata,
      S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
      S_AXI_LOG_wlast => S_AXI_LOG_wlast,
      S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
      S_AXI_LOG_wready => S_AXI_LOG_wready,
      S_AXI_LOG_bid => S_AXI_LOG_bid,
      S_AXI_LOG_bresp => S_AXI_LOG_bresp,
      S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
      S_AXI_LOG_bready => S_AXI_LOG_bready,
      s_AXI_LOG_awuser => '0',
      s_AXI_LOG_awqos => (others => '0'),
      s_AXI_LOG_awcache => "0001",  -- Set AWCACHE[1] to upsizing ?
      s_AXI_LOG_awlock => '0',
      s_AXI_LOG_awprot => "001",
      s_AXI_LOG_araddr => (others => 'X'),
      s_AXI_LOG_aruser => 'X',
      s_AXI_LOG_arid => (others => 'X'),
      s_AXI_LOG_arlen => (others => 'X'),
      s_AXI_LOG_arsize => (others => 'X'),
      s_AXI_LOG_arburst => (others => 'X'),
      s_AXI_LOG_arcache => (others => 'X'),
      s_AXI_LOG_arlock => 'X',
      s_AXI_LOG_arprot => (others => 'X'),
      s_AXI_LOG_arvalid => '0',
      s_AXI_LOG_arready => open,
      s_AXI_LOG_arqos => (others => 'X'),
      s_AXI_LOG_rready => '0',
      s_AXI_LOG_rid => open,
      s_AXI_LOG_rvalid => open,
      s_AXI_LOG_rlast => open,
      s_AXI_LOG_rresp => open,
      s_AXI_LOG_rdata => open,

      --  To VME master
      M_AXI_VME_awaddr => M_AXI_VME_awaddr,
      M_AXI_VME_awprot => M_AXI_VME_awprot,
      M_AXI_VME_awvalid => M_AXI_VME_awvalid,
      M_AXI_VME_awready => M_AXI_VME_awready,
      M_AXI_VME_awlen => M_AXI_VME_awlen,
      M_AXI_VME_awsize => M_AXI_VME_awsize,
      M_AXI_VME_awburst => M_AXI_VME_awburst,
      M_AXI_VME_awlock => M_AXI_VME_awlock,
      M_AXI_VME_awcache => M_AXI_VME_awcache,
      M_AXI_VME_awregion => open,
      M_AXI_VME_awqos => open,
      M_AXI_VME_awuser => open,
      M_AXI_VME_wdata => M_AXI_VME_wdata,
      M_AXI_VME_wstrb => M_AXI_VME_wstrb,
      M_AXI_VME_wvalid => M_AXI_VME_wvalid,
      M_AXI_VME_wready => M_AXI_VME_wready,
      M_AXI_VME_wlast => M_AXI_VME_wlast,
      M_AXI_VME_bresp => M_AXI_VME_bresp,
      M_AXI_VME_bvalid => M_AXI_VME_bvalid,
      M_AXI_VME_bready => M_AXI_VME_bready,
      M_AXI_VME_araddr => M_AXI_VME_araddr,
      M_AXI_VME_arprot => M_AXI_VME_arprot,
      M_AXI_VME_arvalid => M_AXI_VME_arvalid,
      M_AXI_VME_arready => M_AXI_VME_arready,
      M_AXI_VME_arlen => M_AXI_VME_arlen,
      M_AXI_VME_arsize => M_AXI_VME_arsize,
      M_AXI_VME_arburst => M_AXI_VME_arburst,
      M_AXI_VME_arlock => M_AXI_VME_arlock,
      M_AXI_VME_arcache => M_AXI_VME_arcache,
      M_AXI_VME_arregion => open,
      M_AXI_VME_arqos => open,
      M_AXI_VME_aruser => open,
      M_AXI_VME_rdata => M_AXI_VME_rdata,
      M_AXI_VME_rresp => M_AXI_VME_rresp,
      M_AXI_VME_rvalid => M_AXI_VME_rvalid,
      M_AXI_VME_rready => M_AXI_VME_rready,
      M_AXI_VME_rlast => M_AXI_VME_rlast,
      axi_vme_clk => clk_sys_62m5,

      pl_ps_irq0_i  => "0",
      GPIO_tri_i => gpio_in,
      GPIO_tri_o => gpio_out,
      GPIO_tri_t => open,

      spi0_m_i => soc_spi_miso,
      spi0_m_o => soc_spi_mosi,
      spi0_mo_t => open,
      spi0_s_i => '1', -- spi_miso_i,
      spi0_s_o => open,
      spi0_so_t => open,
      spi0_sclk_i => '0',
      spi0_sclk_o => soc_spi_sck,
      spi0_sclk_t => open,
      spi0_ss_i_n => '1',
      spi0_ss_n_t => open,
      spi0_ss_o_n => soc_spi_main_cs_n,
      spi0_ss1_o_n => soc_spi_helper_cs_n,

      aresetn_i     => rst_n,
      clk_i         => clk,
      pl_clk_o      => clk_pl,
      pl_resetn_o   => rst_pl_n);

  b_vme: block
    --  DIR: 1: out, 0: in
    signal vme_write_n_out : std_logic;
    signal vme_data_in     : std_logic_vector(31 downto 0);
    signal vme_data_out    : std_logic_vector(31 downto 0);
    signal vme_data_dir    : std_logic;
    signal vme_data_oe_n   : std_logic;
    signal vme_addr_in     : std_logic_vector(31 downto 1);
    signal vme_addr_out    : std_logic_vector(31 downto 1);
    signal vme_lword_n_out : std_logic;
    signal vme_am_out : std_logic_vector(5 downto 0);
    signal vme_addr_dir    : std_logic;
    signal vme_addr_oe_n   : std_logic;
    signal vme_retry_n_out : std_logic;
    signal vme_retry_oe    : std_logic;
    signal vme_berr_n_out  : std_logic;
    signal vme_iack_n_out   : std_logic;
    signal vme_bg2_0_out, vme_br2_0_out   : std_logic_vector(2 downto 0);
    signal vme_br3_n : std_logic;
    signal vme_ds_oe_n : std_logic;
    signal vme_bbsy_n_out : std_logic;
    signal vme_as_oe_n, vme_as_n_out : std_logic;

    signal vme_nogap : std_logic;
--    signal vme_ga : std_logic_vector(5 downto 0);

    signal base_addr_adr : std_logic_vector(4 downto 2);
    signal base_addr_idx : natural range 0 to 7;
    signal base_addr_rack, base_addr_wack : std_logic;
    signal base_addr_dati, base_addr_dato : std_logic_vector(31 downto 0);
    signal base_addr_rd, base_addr_wr : std_logic;

    type t_window_bar is array(7 downto 0) of std_logic_vector(31 downto 0);
    signal windows_bar: t_window_bar;

    signal windows_wr, windows_rd : std_logic;
    signal windows_rack, windows_wack : std_logic;
    signal windows_dati, windows_dato : std_logic_vector(31 downto 0);
    signal windows_adr : std_logic_vector(14 downto 2);
    signal windows_idx : natural range 0 to 7;

    type t_windows_state is (S_IDLE, S_READ, S_WRITE);
    signal windows_state : t_windows_state;
  begin
    -- IC10 p26
    vme_as_dir_o <= not vme_as_oe_n;
    vme_as_oe_n_o <= not vme_rst_n;
    vme_as_n_b <= vme_as_n_out when vme_as_oe_n = '0' else 'Z';

    --  IC10 p26
    vme_am_b <= vme_am_out when vme_as_oe_n = '0' else (others => 'Z');
    vme_write_n_b <= vme_write_n_out when vme_as_oe_n = '0' else 'Z';

    --  dtack_n_i is not used.

    --  IC10 p26
    vme_retry_oe_o <= vme_retry_oe;
    vme_retry_n_o <= vme_retry_n_out;

    --  IC10 p 26
    vme_berr_oe_o <= not vme_berr_n_out;

    vme_ds_oe_o <= not vme_ds_oe_n;

    vme_addr_oe_n_o <= vme_addr_oe_n;
    vme_addr_dir_o <= vme_addr_dir;
    vme_addr_in <= vme_addr_b;
    vme_addr_b <= vme_addr_out when vme_addr_oe_n = '0' and vme_addr_dir = '1' else (others => 'Z');
    vme_lword_n_b <= vme_lword_n_out when vme_addr_oe_n = '0' and vme_addr_dir = '1' else 'Z';

    vme_iack_oe_o <= not vme_iack_n_out;
    vme_iack_n_o <= '0';

    vme_data_in <= vme_data_b;
    vme_data_b <= vme_data_out when vme_data_oe_n = '0' and vme_data_dir = '1' else (others => 'Z');
    vme_data_oe_n_o <= vme_data_oe_n;
    vme_data_dir_o <= vme_data_dir;

    vme_bbsy_oe_o <= not vme_bbsy_n_out;

    vme_noga_usr <= vme_noga_n;
    vme_nogap <= not (vme_noga_usr(0) xor vme_noga_usr(1) xor vme_noga_usr(2)
      xor vme_noga_usr(3) xor vme_noga_usr(4));
    --  Use vme_ga_n_i if not unconnected (there are pull-up).
    --  Despite the name, the vme64x core want negated logic.
    vme_ga <= vme_gap_int & vme_ga_int when vme_ga_int /= (4 downto 0 => '1')
      else vme_nogap & vme_noga_usr;

    M_AXI_VME_araddr_masked <= M_AXI_VME_araddr(31 downto 0) and M_AXI_VME_addr_mask;
    M_AXI_VME_awaddr_masked <= M_AXI_VME_awaddr(31 downto 0) and M_AXI_VME_addr_mask;

    inst_vme: entity work.xvme64x_core_v2
      generic map (
        g_clock_period => 16,
        g_decode_am => False,
        g_enable_cr_csr => True,
        g_user_csr_ext => True,
        g_vme32 => True,
        g_vme_2e => False,
        g_async_dtack => False,
        g_wb_granularity => BYTE,
        g_wb_mode => CLASSIC,
        g_manufacturer_id => c_CERN_ID,
        g_board_id => x"0000_01dc",
        g_revision_id => x"0000_0000",
        g_program_id => x"00",
        g_ascii_ptr => open,
        g_beg_user_cr => open,
        g_end_user_cr => open,
        g_beg_cram => open,
        g_end_cram => open,
        g_beg_user_csr => open,
        g_end_user_csr => open,
        g_beg_sn => open,
        g_end_sn => open,
        g_decoder => (0 => (adem  => x"ffff0000",
                            amcap => x"00000000_0000ff00",
                            dawpr => x"84"),
                      1 => (adem  => x"ffff0000",
                            amcap => x"ff000000_00000000",
                            dawpr => x"84"),
                      others => c_vme64x_decoder_disabled)
        )
      port map (
        clk_i => clk,
        startup_rst_n_i => vme_rst_n,
        prevent_sysrst_i => '0',
        rst_n_i => vme_rst_n,
        rst_n_o => open, -- rst_n_o,
        vme_as_n_i      => vme_as_n_b,
        vme_as_n_o      => vme_as_n_out,
        vme_as_oe_n_o   => vme_as_oe_n,
        vme_sysrst_n_i  => '1',
        vme_sysrst_n_o  => open,
        vme_sysfail_n_i => '1',
        vme_sysfail_n_o => open,
        vme_acfail_n_i  => '1',
        vme_write_n_i   => vme_write_n_b,
        vme_write_n_o   => vme_write_n_out,
        vme_am_i        => vme_am_b,
        vme_am_o        => vme_am_out,
        vme_am_dir_o    => open,
        vme_am_oe_n_o   => open,
        vme_ds_n_i      => vme_ds_n_i,
        vme_ds_n_o      => vme_ds_n_o,
        vme_ds_oe_n_o   => vme_ds_oe_n,
        vme_ga_n_i      => vme_ga(4 downto 0),
        vme_gap_n_i     => vme_ga(5),
        vme_data_i      => vme_data_in,
        vme_addr_i(31 downto 1)      => vme_addr_in,
        vme_addr_i(0) => vme_lword_n_b,
        vme_iack_n_i    => vme_iack_n_i,
        vme_iack_n_o    => vme_iack_n_out,
        vme_iackin_n_i  => vme_iackin_n_i,
        vme_iackout_n_o => vme_iackout_n_o,
        vme_dtack_n_o   => vme_dtack_n_o,
        vme_dtack_n_i   => vme_dtack_n_i,
        vme_dtack_oe_n_o  => vme_dtack_oe_n,
        vme_data_o      => vme_data_out,
        vme_data_dir_o  => vme_data_dir,
        vme_data_oe_n_o => vme_data_oe_n,
        vme_addr_o(31 downto 1)      => vme_addr_out,
        vme_addr_o(0) => vme_lword_n_out,
        vme_addr_dir_o  => vme_addr_dir,
        vme_addr_oe_n_o => vme_addr_oe_n,
        vme_retry_n_o   => vme_retry_n_out,
        vme_retry_n_i   => vme_retry_n_i,
        vme_retry_oe_n_o => vme_retry_oe_n,
        vme_berr_n_o    => vme_berr_n_out,
        vme_berr_n_i    => vme_berr_n_i,
        vme_irq_n_o     => vme_irq_n,
        vme_irq_n_i     => vme_irq_n_i,
        vme_bbsy_n_i    => vme_bbsy_n_i,
        vme_bbsy_n_o    => vme_bbsy_n_out,
        vme_bclr_n_i    => '1',
        vme_bclr_n_o    => open,
        vme_br_n_i      => "1111",
        vme_br_n_o(3)   => vme_br3_n,
        vme_br_n_o(2 downto 0) => vme_br2_0_out,
        vme_bg_n_i(2 downto 0) => "111",
        vme_bg_n_i(3) => vme_bg3in_n_i,
        vme_bg_n_o(3) => vme_bg3out_n_o,
        vme_bg_n_o(2 downto 0) => vme_bg2_0_out,

        vme_irq_o => open,
        berr_irq_o => open,

        wb_i => vme_wb_in,
        wb_o => vme_wb_out,

        s_axi_i.ARVALID => M_AXI_VME_arvalid(0),
        s_axi_i.ARADDR => M_AXI_VME_araddr_masked,
        s_axi_i.ARID => (others => '0'),
        s_axi_i.ARBURST => M_AXI_VME_arburst,
        s_axi_i.ARSIZE => M_AXI_VME_arsize,
        s_axi_i.ARLEN => M_AXI_VME_arlen,
        s_axi_i.ARLOCK => M_AXI_VME_arlock(0),
        s_axi_i.ARPROT => M_AXI_VME_arprot,
        s_axi_i.ARQOS => M_AXI_VME_arqos,
        s_axi_i.ARCACHE => M_AXI_VME_arcache,
        s_axi_i.AWVALID => M_AXI_VME_awvalid(0),
        s_axi_i.AWADDR => M_AXI_VME_awaddr_masked,
        s_axi_i.AWID => (others => '0'),
        s_axi_i.AWBURST => M_AXI_VME_awburst,
        s_axi_i.AWSIZE => M_AXI_VME_awsize,
        s_axi_i.AWLEN => M_AXI_VME_awlen,
        s_axi_i.AWLOCK => M_AXI_VME_awlock(0),
        s_axi_i.AWPROT => M_AXI_VME_awprot,
        s_axi_i.AWQOS => M_AXI_VME_awqos,
        s_axi_i.AWCACHE => M_AXI_VME_awcache,
        s_axi_i.WVALID => M_AXI_VME_wvalid(0),
        s_axi_i.WDATA => M_AXI_VME_wdata,
        s_axi_i.WSTRB => M_AXI_VME_wstrb,
        s_axi_i.WID => (others => '0'),
        s_axi_i.BREADY => M_AXI_VME_bready(0),
        s_axi_i.RREADY => M_AXI_VME_rready(0),
        s_axi_i.WLAST => 'X',
        s_axi_o.ARREADY => M_AXI_VME_arready(0),
        s_axi_o.AWREADY => M_AXI_VME_awready(0),
        s_axi_o.WREADY => M_AXI_VME_wready(0),
        s_axi_o.RVALID => M_AXI_VME_rvalid(0),
        s_axi_o.RRESP => M_AXI_VME_rresp,
        s_axi_o.RDATA => M_AXI_VME_rdata,
        s_axi_o.RID => M_AXI_VME_rid,
        s_axi_o.RLAST => M_AXI_VME_rlast(0),
        s_axi_o.BVALID => M_AXI_VME_bvalid(0),
        s_axi_o.BRESP => M_AXI_VME_bresp,
        s_axi_o.BID => M_AXI_VME_bid,

        int_i => irq,
        irq_ack_o => open,
        irq_level_i  => vme_irq_level,
        irq_vector_i => vme_irq_vector,
        user_csr_addr_o => open,
        user_csr_data_i => open,
        user_csr_data_o => open,
        user_csr_we_o => open,
        user_cr_addr_o => open,
        user_cr_data_i => open
        );

    vme_dtack_oe_o <= not vme_dtack_oe_n;
    vme_retry_oe_o <= not vme_retry_oe_n;

    vme_irq_o <= not vme_irq_n;

    vme_br3_o <= not vme_br3_n;

    -- vme_am_oe_n_o <= vme_am_oe_n;
    -- vme_am_dir_o <= vme_am_dir;

    --  Stay little endian (even on VME).
    vme_dato_swp <= vme_wb_out.dat(7 downto 0) & vme_wb_out.dat(15 downto 8) & vme_wb_out.dat(23 downto 16) & vme_wb_out.dat(31 downto 24);
    vme_wb_in.dat <= vme_dati_swp(7 downto 0) & vme_dati_swp(15 downto 8) & vme_dati_swp(23 downto 16) & vme_dati_swp(31 downto 24);

    inst_vme_map: entity work.vme_map
      port map (
        rst_n_i => rst_n,
        clk_i => clk,

        wb_i.cyc => vme_wb_out.cyc,
        wb_i.stb => vme_wb_out.stb,
        wb_i.adr => vme_wb_out.adr,
        wb_i.sel => vme_wb_out.sel,
        wb_i.we => vme_wb_out.we,
        wb_i.dat => vme_dato_swp,
        wb_o.ack => vme_wb_in.ack,
        wb_o.err => vme_wb_in.err,
        wb_o.rty => vme_wb_in.rty,
        wb_o.stall => vme_wb_in.stall,
        wb_o.dat => vme_dati_swp,

        host_i => host_wb_in,
        host_o => host_wb_out,
        vme_irq_level_o => vme_irq_level,
        vme_irq_vector_o => vme_irq_vector,
        vme_ga_ga_i (4 downto 0) => vme_ga_int,
        vme_ga_ga_i (5) => vme_gap_int,
        vme_ga_noga_i (4 downto 0) => vme_noga_n,
        vme_ga_noga_i (5) => '0',
        vme_ga_vme_ga_i => vme_ga,
        vme_ga_cst_i => x"5a",

        vme_p2_rs485_dat0_i => rs485_data_in(0),
        vme_p2_rs485_dat1_i => rs485_data_in(1),
        vme_p2_rs485_dat0_o => rs485_data_out(0),
        vme_p2_rs485_dat1_o => rs485_data_out(1),
        vme_p2_rs485_wr_o => rs485_wr,
        vme_p2_rs485_dir0_o => rs485_dir(0),
        vme_p2_rs485_dir1_o => rs485_dir(1),

        vme_p2_ttc_dat_o(0) => p2_ttc_d0_o,
        vme_p2_ttc_dat_o(1) => p2_ttc_d1_o,

        vme_hwbytes_l1_o => p0_hwbyte_l1,
        vme_hwbytes_h1_o => p0_hwbyte_h1,
        vme_hwbytes_l2_o => p0_hwbyte_l2,
        vme_hwbytes_h2_o => p0_hwbyte_h2,
        vme_hwbytes_oe_l1_o => p0_hwbyte_l1_oe,
        vme_hwbytes_oe_h1_o => p0_hwbyte_h1_oe,
        vme_hwbytes_oe_l2_o => p0_hwbyte_l2_oe,
        vme_hwbytes_oe_h2_o => p0_hwbyte_h2_oe,

        vme_p0_clk_ttl_oe_o => open,
        vme_p0_h1_o => p0_h1,

        vme_base_addr_adr_o => base_addr_adr,
        vme_base_addr_rd_o  => base_addr_rd,
        vme_base_addr_wr_o  => base_addr_wr,
        vme_base_addr_rack_i => base_addr_rack,
        vme_base_addr_wack_i => base_addr_wack,
        vme_base_addr_dati_o => base_addr_dati,
        vme_base_addr_dato_i => base_addr_dato,

        windows_adr_o => windows_adr,
        windows_wr_o => windows_wr,
        windows_rd_o => windows_rd,
        windows_dato_i => windows_dato,
        windows_dati_o => windows_dati,
        windows_rack_i => windows_rack,
        windows_wack_i => windows_wack
      );

    --  Windows bar
    base_addr_idx <= to_integer(unsigned(base_addr_adr));
    p_wbar: process (clk)
    begin
      if rising_edge (clk) then
        base_addr_rack <= '0';
        base_addr_wack <= '0';

        if base_addr_rd = '1' then
          base_addr_dato <= windows_bar(base_addr_idx);
          base_addr_rack <= '1';
        end if;
        if base_addr_wr = '1' then
          windows_bar(base_addr_idx) <= base_addr_dati;
          base_addr_wack <= '1';
        end if;
      end if;
    end process;

    --  Windows access
    windows_idx <= to_integer(unsigned(windows_adr (14 downto 12)));
    p_windows: process (clk)
    begin
      if rising_edge(clk) then
        windows_rack <= '0';
        windows_wack <= '0';

        if rst_n = '0' then
          windows_state <= S_IDLE;
          s_axi_awvalid <= '0';
          s_axi_wvalid <= '0';
          s_axi_bready <= '0';
          s_axi_arvalid <= '0';
          s_axi_rready <= '0';
        else
          case windows_state is
            when S_IDLE =>
              s_axi_addr (48 downto 32) <= (others => '0');
              s_axi_addr (31 downto 0) <= windows_bar(windows_idx);
              s_axi_addr (11 downto 2) <= windows_adr(11 downto 2);
              s_axi_addr (1 downto 0) <= "00";

              if windows_rd = '1' then
                s_axi_arvalid <= '1';
                s_axi_rready <= '1';
                windows_state <= S_READ;
              end if;
              if windows_wr = '1' then
                s_axi_awvalid <= '1';
                s_axi_wvalid <= '1';
                s_axi_wdata <= windows_dati;
                s_axi_bready <= '1';
                windows_state <= S_WRITE;
              end if;
            when S_READ =>
              if s_axi_arready = '1' then
                s_axi_arvalid <= '0';
              end if;
              if s_axi_rvalid = '1' then
                s_axi_rready <= '0';
--                if s_axi_rrsp = "00" then
                  -- Okday
                  windows_dato <= s_axi_rdata;
--                else
--                  windows_dato <= (others => '1');
--                end if;
              end if;
              if (s_axi_arvalid = '0' or s_axi_arready = '1')
                and (s_axi_rvalid = '1' or s_axi_rready = '0')
              then
                  windows_rack <= '1';
                  windows_state <= S_IDLE;
              end if;
            when S_WRITE =>
              if s_axi_awready = '1' then
                s_axi_awvalid <= '0';
              end if;
              if s_axi_wready = '1' then
                s_axi_wvalid <= '0';
              end if;
              if s_axi_bvalid = '1' then
                s_axi_bready <= '0';
              end if;
              if (s_axi_bvalid = '1' or s_axi_bready = '0')
                and (s_axi_wready = '1' or s_axi_wvalid = '0')
                and (s_axi_awready = '1' or s_axi_awvalid = '0')
              then
                windows_wack <= '1';
                windows_state <= S_IDLE;
              end if;
          end case;
        end if;
      end if;
    end process;
  end block;

  --  v1: p30, IC60, 61, 62.
  --  v0: p19 IC 48, 1, 66
  inst_ioexp_sh_vme: entity work.output_shifter
    generic map (
      g_WIDTH => 24
      )
    port map (
      clk_i => clk,
      load_i => ioexp_vme_load,
      shift_i => ioexp_vme_shift,
      data_i (0) => rs485_data_re_n(1),
      data_i (1) => rs485_data_de(1),
      data_i (2) => rs485_data_re_n(0),
      data_i (3) => rs485_data_de(0),
      data_i (4) => '0',
      data_i (5) => '0',
      data_i (6) => vme_sysclk_dir,
      data_i (7) => vme_sysreset_dir,
      data_i (8) => userio_dir(0),  -- NB: must be 1 on WREN 0.0 (inversion)
      data_i (9) => userio_dir(1),  -- NB: must be 0 on WREN 0.0
      data_i (10) => userio_dir(2), -- NB: must be 3 on WREN 0.0
      data_i (11) => userio_dir(3), -- NB: must be 2 on WREN 0.0
      data_i (15 downto 12) => "0000",
      data_i (16) => userio_oe_n(0),    -- NB: must be 1 on WREN 0.0
      data_i (17) => userio_oe_n(1),    -- NB: must be 0 on WREN 0.0
      data_i (18) => userio_oe_n(2),    -- NB: must be 3 on WREN 0.0
      data_i (19) => userio_oe_n(3),    -- NB: must be 2 on WREN 0.0
      data_i (20) => p0_hwbyte_l1_oe_n,
      data_i (21) => p0_hwbyte_h1_oe_n,
      data_i (22) => p0_hwbyte_l2_oe_n,
      data_i (23) => p0_hwbyte_h2_oe_n,
      msb_o => ioexp_vme_d_o
      );

  -- p19 IC 63, 65
  inst_ioexp_sh_hwbyte1: entity work.output_shifter
    generic map (
      g_WIDTH => 24
      )
    port map (
      clk_i => clk,
      load_i => ioexp_vme_load,
      shift_i => ioexp_vme_shift,
      data_i(7 downto 0) => p0_hwbyte_h1,
      data_i(15 downto 8) => p0_hwbyte_l1,
      data_i(23 downto 16) => x"00",
      msb_o => ioexp_hwbyte1_d_o
      );

  -- p19 IC 49, 64
  inst_ioexp_sh_hwbyte2: entity work.output_shifter
    generic map (
      g_WIDTH => 24
      )
    port map (
      clk_i => clk,
      load_i => ioexp_vme_load,
      shift_i => ioexp_vme_shift,
      data_i(7 downto 0) => p0_hwbyte_h2,
      data_i(15 downto 8) => p0_hwbyte_l2,
      data_i(23 downto 16) => x"00",
      msb_o => ioexp_hwbyte2_d_o
      );

  inst_ioexp_vme: entity work.output_expander
    generic map (
      g_nbr_chips => 3,
      g_clkdiv => 7
      )
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      load_o => ioexp_vme_load,
      shift_o => ioexp_vme_shift,
      exp_oe_n_o => ioexp_vme_oe_n,
      exp_rclk_o => ioexp_vme_rclk,
      exp_sclk_o => ioexp_vme_sclk_o
      );

  --  IOexpanders outputs are updated on rclk.
  ioexp_hwbyte1_rclk_o <= ioexp_vme_rclk;
  ioexp_hwbyte2_rclk_o <= ioexp_vme_rclk;
  ioexp_vme_rclk_o <= ioexp_vme_rclk;

  ioexp_all_rst_n_o <= '1';
  ioexp_all_oe_n_o <= ioexp_vme_oe_n or ioexp_fpio_oe_n or ioexp_gpio_oe_n;

  --  VME P2 raw A
  gen_userio: for i in 0 to 3 generate
    subtype t_rng is natural range i*8+7 downto i*8;
  begin
    process (clk)
    begin
      if rising_edge(clk) then
        if rst_n = '0' then
          --  At reset disable output, configure as input
          userio_oe_n(i) <= '1';
          userio_dir_reg(i) <= '0';
        else
          userio_oe_n(i) <= not userio_en(i);

          if userio_dir(i) = '0' then
            --  input
            userio_dir_reg(i) <= '0';
          elsif userio_dir(i) = '1' and ioexp_vme_rclk = '1' then
            --  Output, delay until the buffer is updated.
            userio_dir_reg(i) <= '1';
          end if;
        end if;
      end if;

    end process;

    --  OE for fpga iobuf
    gen_pp_oe: for j in 0 to 7 generate
      pp_pin_oe(8*i + j) <= userio_dir_reg (i);
    end generate;

    userio_in(t_rng) <= userio_b(t_rng);

  end generate;

  userio_b <= pp_out;

  gen_rs485: for i in 0 to 1 generate
    process (clk)
    begin
      if rising_edge(clk) then
        if rst_n = '0' then
          rs485_dir_reg(i) <= '0';
          rs485_data_reg(i) <= '0';
        else
          if rs485_dir(i) = '0' then
            -- input
            rs485_dir_reg(i) <= '0';
          elsif rs485_dir(i) = '1' and ioexp_vme_rclk = '1' then
            rs485_dir_reg(i) <= '1';
          end if;

          if rs485_wr = '1' then
            rs485_data_reg(i) <= rs485_data_out(i);
          end if;
        end if;
      end if;
    end process;
  end generate;

  rs485_data_in(0) <= p2_rs485_d0_b;
  rs485_data_in(1) <= p2_rs485_d1_b;
  p2_rs485_d0_b <= rs485_data_reg(0) when rs485_dir_reg(0) = '1' else 'Z';
  p2_rs485_d1_b <= rs485_data_reg(1) when rs485_dir_reg(1) = '1' else 'Z';
  rs485_data_de <= rs485_dir_reg;  -- Output
  rs485_data_re_n <= rs485_dir_reg;  --  Input

  --  TODO
  vme_sysclk_dir <= '0';
  vme_sysreset_dir <= '0';

  p0_hwbyte_h2_oe_n <= not p0_hwbyte_h2_oe;
  p0_hwbyte_l2_oe_n <= not p0_hwbyte_l2_oe;
  p0_hwbyte_h1_oe_n <= not p0_hwbyte_h1_oe;
  p0_hwbyte_l1_oe_n <= not p0_hwbyte_l1_oe;

  -- v1: p33, IC67, 76.
  inst_ioexp_fp_sh: entity work.output_shifter
    generic map (
      g_WIDTH => 64
      )
    port map (
      clk_i => clk,
      load_i => ioexp_fpio_load,
      shift_i => ioexp_fpio_shift,
      data_i (1 downto 0)   => fp_term_en(1 downto 0),
      data_i (3 downto 2)   => fp_en_n(1 downto 0),
      data_i (5 downto 4)   => fp_term_en(3 downto 2),
      data_i (7 downto 6)   => fp_en_n(3 downto 2),
      data_i (9 downto 8)   => fp_term_en(5 downto 4),
      data_i (11 downto 10) => fp_en_n(5 downto 4),
      data_i (13 downto 12) => fp_term_en(7 downto 6),
      data_i (15 downto 14) => fp_en_n(7 downto 6),
      data_i (17 downto 16) => fp_term_en(9 downto 8),
      data_i (19 downto 18) => fp_en_n(9 downto 8),
      data_i (21 downto 20) => fp_term_en(11 downto 10),
      data_i (23 downto 22) => fp_en_n(11 downto 10),
      data_i (25 downto 24) => fp_term_en(13 downto 12),
      data_i (27 downto 26) => fp_en_n(13 downto 12),
      data_i (29 downto 28) => fp_term_en(15 downto 14),
      data_i (31 downto 30) => fp_en_n(15 downto 14),
      data_i (33 downto 32) => fp_term_en(17 downto 16),
      data_i (35 downto 34) => fp_en_n(17 downto 16),
      data_i (37 downto 36) => fp_term_en(19 downto 18),
      data_i (39 downto 38) => fp_en_n(19 downto 18),
      data_i (41 downto 40) => fp_term_en(21 downto 20),
      data_i (43 downto 42) => fp_en_n(21 downto 20),
      data_i (45 downto 44) => fp_term_en(23 downto 22),
      data_i (47 downto 46) => fp_en_n(23 downto 22),
      data_i (49 downto 48) => fp_term_en(25 downto 24),
      data_i (51 downto 50) => fp_en_n(25 downto 24),
      data_i (53 downto 52) => fp_term_en(27 downto 26),
      data_i (55 downto 54) => fp_en_n(27 downto 26),
      data_i (57 downto 56) => fp_term_en(29 downto 28),
      data_i (59 downto 58) => fp_en_n(29 downto 28),
      data_i (61 downto 60) => fp_term_en(31 downto 30),
      data_i (63 downto 62) => fp_en_n(31 downto 30),
      msb_o => ioexp_fpio_d_o
      );

  fp_en_n <= not lemo_oe;
  fp_term_en <= lemo_term;

  pp_oe_n <= not pp_en;

  inst_ioexp_fp: entity work.output_expander
    generic map (
      g_nbr_chips => 8,
      g_clkdiv => 7
      )
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      shift_o => ioexp_fpio_shift,
      load_o => ioexp_fpio_load,
      exp_oe_n_o => ioexp_fpio_oe_n,
      exp_rclk_o => ioexp_fpio_rclk_o,
      exp_sclk_o => ioexp_fpio_sclk_o
      );

  -- v1: p1, p53
  -- p1, p51 slow_gpio
  inst_ioexp_gpio: entity work.io_expander
    generic map (
      g_nbr_chips_out => 1,
      g_nbr_chips_in => 3,
      g_clkdiv => 7
      )
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      outputs_i (0) => sfp0_tx_disable,
      outputs_i (1) => '0',
      outputs_i (2) => sfp1_tx_disable,
      outputs_i (3) => '0',
      outputs_i (4) => pp_oe_n,
      outputs_i (5) => '0',
      outputs_i (6) => '0',
      outputs_i (7) => '0',
      inputs_o (7) => sfp0_los,
      inputs_o (6) => sfp1_los,
      inputs_o (5) => switch0_n,
      inputs_o (4) => switch1_n,
      inputs_o (3) => vme_noga_n(0),
      inputs_o (2) => vme_noga_n(1),
      inputs_o (1) => vme_noga_n(2),
      inputs_o (0) => vme_noga_n(3),
      inputs_o (16) => vme_noga_n(4),
      inputs_o (17) => vme_usega_n,
      inputs_o (18) => vme_ga_int(0),
      inputs_o (19) => vme_ga_int(1),
      inputs_o (20) => vme_ga_int(2),
      inputs_o (21) => vme_ga_int(3),
      inputs_o (22) => vme_ga_int(4),
      inputs_o (23) => vme_gap_int,
      inputs_o (8) => fp_id(0),
      inputs_o (9) => fp_id(1),
      inputs_o (10) => fp_id(2),
      inputs_o (11) => pp_id(0),
      inputs_o (12) => pp_id(1),
      inputs_o (13) => pp_id(2),
      inputs_o (14) => sfp0_det,
      inputs_o (15) => sfp1_det,

      inp_valid_o => gpio_inp_valid,
      exp_oe_n_o => ioexp_gpio_oe_n,
      exp_rclk_o => ioexp_gpio_rclk_o,
      exp_sclk_o => ioexp_gpio_sclk_o,
      exp_d_o => ioexp_gpio_d_o,
      exp_q_i => ioexp_gpio_q_i
      );

  --  p0 clocks
  gen_p0_clk: for i in 1 to 2 generate
    alias bclk is clk_bunch(i);
    alias cnt is p0_clk(i).bunch_cnt;
  begin
    p0_bunchclk_o(i) <= bclk;

    p_turnclk1: process(bclk)
    begin
      if rising_edge(bclk) then
        p0_turnclk_o(i) <= '0';
        if rst_n = '0' then
          cnt <= (others => '0');
        else
          if cnt = unsigned(p0_h1) then
            p0_turnclk_o(i) <= '1';
            cnt <= (others => '0');
          else
            cnt <= cnt + 1;
          end if;
        end if;
      end if;
    end process;
  end generate;

  --  The vme core reads vme_ga once just after reset, so we must wait
  --  for the io expander.
  vme_rst_n <= rst_n and gpio_inp_valid;

  inst_platform: entity work.wren_platform
    generic map (
      g_simulation => g_simulation,
      g_wrpc_initf => g_wrpc_initf,
      g_hwbld_date => g_hwbld_date,
      g_with_external_clock_input => g_with_external_clock_input
    )
    port map (
      wr_clk_sfp_p_i => wr_clk_sfp_125m_p_i,
      wr_clk_sfp_n_i => wr_clk_sfp_125m_n_i,
      fpga_pl_clksys_p_i => fpga_pl_clksys_p_i,
      fpga_pl_clksys_n_i => fpga_pl_clksys_n_i,
      wr_clk_main_p_i => wr_clk_main_125m_p_i,
      wr_clk_main_n_i => wr_clk_main_125m_n_i,
      wr_clk_helper_p_i => wr_clk_helper_125m_p_i,
      wr_clk_helper_n_i => wr_clk_helper_125m_n_i,
      clk_pl_i => clk_pl,
      rst_pl_n_i => rst_pl_n,
      rst_pl_done_o => gpio_in(0),
      rst_pl_cont_i => gpio_out(1),
      clk_sys_62m5_o => clk_sys_62m5,
      clk_sys_125m_o => clk_sys_125m,
      clk_sys_250m_o => clk_sys_250m,
      clk_sys_500m_o => clk_sys_500m,
      rst_sys_n_o => rst_sys_n,
      clk_sys2_125m_o => clk_sys2_125m,
      sfp_txp_o => sfp1_txp_o,
      sfp_txn_o => sfp1_txn_o,
      sfp_rxp_i => sfp1_rxp_i,
      sfp_rxn_i => sfp1_rxn_i,
      gth_bclk1_p_i => gth_bclk1_p_i,
      gth_bclk1_n_i => gth_bclk1_n_i,
      gth_bclk2_p_i => gth_bclk2_p_i,
      gth_bclk2_n_i => gth_bclk2_n_i,
      sfp_sda_b => sfp1_sda_b,
      sfp_scl_b => sfp1_scl_b,
      led_link_o => led_sfp1_green_o,
      led_act_o => led_sfp1_yellow_o,
      sfp_los_i => sfp1_los,
      sfp_tx_disable_o => sfp1_tx_disable,
      sfp_det_i => sfp1_det,
      wr_abscal_o => wr_abscal_o,
      tm_link_up_o => tm_link_up,
      tm_time_valid_o => tm_time_valid,
      tm_tai_o => tm_tai,
      tm_cycles_o => tm_cycles,
      spi_main_cs_n_o => spi_main_cs_n_o,
      spi_mosi_o => spi_mosi_o,
      spi_miso_i => spi_miso_i,
      spi_sck_o => spi_sck_o,
      eeprom_sda_b => eeprom_sda_b,
      eeprom_scl_b => eeprom_scl_b,
      uart_rxd_i => uart_rxd_i,
      uart_txd_o => uart_txd_o,
      pps_p_o => pps_p,
      pps_valid_o => pps_valid,
      pps_led_o => pps_led,
      wb_si5340_i => wb_si5340_out,
      wb_si5340_o => wb_si5340_in,
      wr_master_i => wr_master_out,
      wr_master_o => wr_master_in,
      eth_tx_i => eth_tx_out,
      eth_tx_o => eth_tx_in,
      eth_rx_i => eth_rx_out,
      eth_rx_o => eth_rx_in,
      soc_spi_helper_cs_n_i => soc_spi_helper_cs_n,
      soc_spi_main_cs_n_i => soc_spi_main_cs_n,
      soc_spi_mosi_i => soc_spi_mosi,
      soc_spi_miso_o => soc_spi_miso,
      soc_spi_sck_i => soc_spi_sck,
      clk_bunch_o => clk_bunch,
      gth_clk_250m_o => gth_clk_250m
    );

  inst_wren: entity work.wren_core
    generic map (
      g_with_serdes => true,
      g_with_patch_panel => true,
      g_nbr_outputs => 26,
      g_pcb_version => g_pcb_version,
      g_model_ident => x"76_6d_65_31" -- vme1
      )
    port map (
      clk_sys_62m5_i => clk_sys_62m5,
      clk_sys_125m_i => clk_sys_125m,
      clk_sys_250m_i => clk_sys_250m,
      clk_sys_500m_i => clk_sys_500m,
      rst_sys_62m5_n_i => rst_sys_n,
      board_axi_o => m_axi_in,
      board_axi_i => m_axi_out,
      host_wb_o => host_wb_in,
      host_wb_i => host_wb_out,
      tm_link_up_i => tm_link_up,
      tm_tai_i => tm_tai,
      tm_cycles_i => tm_cycles,
      tm_time_valid_i => tm_time_valid,
      wrpc_master_o => wr_master_out,
      wrpc_master_i => wr_master_in,
      irq_o => irq,
      eth_tx_o => eth_tx_out,
      eth_tx_i => eth_tx_in,
      eth_rx_o => eth_rx_out,
      eth_rx_i => eth_rx_in,

      S_AXI_LOG_awid => S_AXI_LOG_awid,
      S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
      S_AXI_LOG_awlen => S_AXI_LOG_awlen,
      S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
      S_AXI_LOG_awready => S_AXI_LOG_awready,
      S_AXI_LOG_wdata => S_AXI_LOG_wdata,
      S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
      S_AXI_LOG_wlast => S_AXI_LOG_wlast,
      S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
      S_AXI_LOG_wready => S_AXI_LOG_wready,
      S_AXI_LOG_bid => S_AXI_LOG_bid,
      S_AXI_LOG_bresp => S_AXI_LOG_bresp,
      S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
      S_AXI_LOG_bready => S_AXI_LOG_bready,

      wb_si5340_i => wb_si5340_in,
      wb_si5340_o => wb_si5340_out,

      fp_led0_o => fp_led0_o,
      fp_led1_o => fp_led1_o,
      pp_led0_o => pp_led0_o,
      pp_led1_o => pp_led1_o,

      switches_i (4 downto 0)=> vme_noga_n,
      switches_i(5) => vme_usega_n,
      switches_i(6) => switch0_n,
      switches_i(7) => switch1_n,
      fp_id_i => fp_id,
      pp_id_i => pp_id,
      vme_ga_i (4 downto 0)=> vme_ga_int,
      vme_ga_i (5)=> vme_gap_int,

      pad_oe_o => lemo_oe(31 downto 6),
      pad_term_o => lemo_term,
      pad_i => fp_io_b,
      pad_o => fp_io_b(31 downto 6),
      pps_p_i => pps_p,
      pps_valid_i => pps_valid,
      pp_o => pp_out,
      pp_oe_i => pp_pin_oe,
      pp_en_o => pp_en,
      vme_userio_i => userio_in,
      vme_userio_dir_o => userio_dir,
      vme_userio_en_o  => userio_en,

      pxie_dstara_mux_o => open,
      pxie_dstarc_mux_o => open,

      rf1_rxn_i => gth0_rxn_i,
      rf1_rxp_i => gth0_rxp_i,
      rf1_txn_o => gth0_txn_o,
      rf1_txp_o => gth0_txp_o,

      rf2_rxn_i => gth1_rxn_i,
      rf2_rxp_i => gth1_rxp_i,
      rf2_txn_o => gth1_txn_o,
      rf2_txp_o => gth1_txp_o,

      rf3_rxn_i => sfp0_rxn_i,
      rf3_rxp_i => sfp0_rxp_i,
      rf3_txn_o => sfp0_txn_o,
      rf3_txp_o => sfp0_txp_o,

      gth_clk_250m_i => gth_clk_250m
      );

  --  Fixed inputs
  lemo_oe(5 downto 0) <= (others => '0');


  wr_pps_o <= pps_p;

  led_sfp0_green_o <= '0';
  led_sfp0_yellow_o <= '0';

  b_leds: block
    constant clk_freq : natural := 62_500_000;
  begin
    inst_sfp: entity work.sfp_argb_leds
      generic map (
        g_clk_freq => clk_freq
      )
      port map (
        clk_i => clk,
        rst_n_i => rst_n,
        sfp_act_i => '1',
        sfp_link_i => '1',
        pps_led_i => pps_led,
        pps_valid_i => pps_valid,
        dout_o => sfp_led_o
      );
  end block;
end architecture arch;
