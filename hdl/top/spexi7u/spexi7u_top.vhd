--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   gmtwr_v0
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.axi4_pkg.all;
use work.wr_xilinx_pkg.all;
use work.wr_board_pkg.all;
use work.wr_fabric_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity spexi7u_top is
  generic (
    g_WRPC_INITF    : string  :=  "../../../../../dependencies/files/wrc-pxie.bram";
    g_map_version   : std_logic_vector(31 downto 0) := x"3510_f003";
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the
    -- testbench. Its purpose is to reduce some internal counters/timeouts
    -- to speed up simulations.
    g_SIMULATION     : integer := 0);
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------
    ps_por_i               : in std_logic;

    wr_clk_helper_125m_p_i : in  std_logic;
    wr_clk_helper_125m_n_i : in  std_logic;
    wr_clk_main_125m_p_i   : in  std_logic;
    wr_clk_main_125m_n_i   : in  std_logic;
    wr_clk_sfp_125m_p_i    : in  std_logic;
    wr_clk_sfp_125m_n_i    : in  std_logic;

    ---------------------------------------------------------------------------
    -- SPI interface to DACs
    ---------------------------------------------------------------------------
    plldac_sclk_o   : out std_logic;
    plldac_din_o    : out std_logic;
    pll25dac_cs_n_o : out std_logic;
    pll20dac_cs_n_o : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/Os for transceiver
    ---------------------------------------------------------------------------
    sfp_txp_o         : out std_logic;
    sfp_txn_o         : out std_logic;
    sfp_rxp_i         : in  std_logic;
    sfp_rxn_i         : in  std_logic;
    sfp_det_i         : in  std_logic;
    sfp_sda_b         : inout std_logic;
    sfp_scl_b         : inout std_logic;
    sfp_tx_disable_o  : out std_logic;
    sfp_los_i         : in  std_logic;

    ---------------------------------------------------------------------------
    -- EEPROM I2C interface for storing configuration and accessing unique ID
    ---------------------------------------------------------------------------
    eeprom_sda_b  : inout std_logic;
    eeprom_scl_b  : inout std_logic;
    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------
    uart_rxd_i    : in  std_logic;
    uart_txd_o    : out std_logic;

    ---------------------------------------------------------------------------
    -- LEDs
    ---------------------------------------------------------------------------
    led_o    : out std_logic_vector(5 downto 0);
    pps_p_o    : out std_logic;

    i2c_scl_b : inout std_logic;
    i2c_sda_b : inout std_logic);
end entity spexi7u_top;

architecture arch of spexi7u_top is
  -- clock and reset
  signal clk_sys_62m5     : std_logic;
  signal rst_sys_62m5_n   : std_logic;
  signal clk_ref_62m5     : std_logic;
  signal rst_ref_62m5_n   : std_logic;

  alias clk : std_logic is clk_ref_62m5;
  alias rst_n : std_logic is rst_ref_62m5_n;

  signal sfp_scl_out, sfp_scl_in : std_logic;
  signal sfp_sda_out, sfp_sda_in : std_logic;
  signal eeprom_scl_out, eeprom_scl_in : std_logic;
  signal eeprom_sda_out, eeprom_sda_in : std_logic;

  attribute keep                   : string;

  --  MT fabric.
  signal eth_tx_out : t_wrf_source_out;
  signal eth_tx_in  : t_wrf_source_in;
  signal eth_rx_out : t_wrf_sink_out;
  signal eth_rx_in  : t_wrf_sink_in;

  signal wr_master_out : t_wishbone_master_out;
  signal wr_master_in  : t_wishbone_master_in;

  -- WRPC TM interface and aux clocks
  signal tm_link_up         : std_logic;
  signal tm_tai             : std_logic_vector(39 downto 0);
  signal tm_cycles          : std_logic_vector(27 downto 0);
  signal tm_time_valid      : std_logic;

  -- MT TM interface
--  signal tm : t_mt_timing_if;

  --  mpsoc to pci map
  signal m_axi_out: t_axi4_lite_master_out_32;
  signal m_axi_in : t_axi4_lite_master_in_32;

  signal board_axi_out: t_axi4_lite_master_out_32;
  signal board_axi_in : t_axi4_lite_master_in_32;
  
  signal host_wb_out : t_wishbone_master_out;
  signal host_wb_in  : t_wishbone_master_in;

  signal M_AXI_araddr  : STD_LOGIC_VECTOR (39 downto 32);
  signal M_AXI_awaddr  : STD_LOGIC_VECTOR (39 downto 32);

  signal S_AXI_awuser : STD_LOGIC;
  signal S_AXI_awid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_awaddr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal S_AXI_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_awsize : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_awburst : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_awlock : STD_LOGIC;
  signal S_AXI_awcache : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_awprot : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_awvalid : STD_LOGIC;
  signal S_AXI_awready :  STD_LOGIC;
  signal S_AXI_wdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_wstrb : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_wlast : STD_LOGIC;
  signal S_AXI_wvalid : STD_LOGIC;
  signal S_AXI_wready :  STD_LOGIC;
  signal S_AXI_bid :  STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_bvalid :  STD_LOGIC;
  signal S_AXI_bready : STD_LOGIC;

  signal irq : std_logic;

  signal iic0_sda_in, iic0_sda_out, iic0_sda_tri : std_logic;
  signal iic0_scl_in, iic0_scl_out, iic0_scl_tri : std_logic;

  signal led_act, led_link : std_logic;

  signal pps, pps_valid : std_logic;

  component mpsoc is
    port (
      M_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_awvalid : out STD_LOGIC;
      M_AXI_awready : in STD_LOGIC;
      M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_wvalid : out STD_LOGIC;
      M_AXI_wready : in STD_LOGIC;
      M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_bvalid : in STD_LOGIC;
      M_AXI_bready : out STD_LOGIC;
      M_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_arvalid : out STD_LOGIC;
      M_AXI_arready : in STD_LOGIC;
      M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_rvalid : in STD_LOGIC;
      M_AXI_rready : out STD_LOGIC;
      GPIO_tri_i : in STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_o : out STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_t : out STD_LOGIC_VECTOR ( 94 downto 0 );
      IIC_0_scl_i : in STD_LOGIC;
      IIC_0_scl_o : out STD_LOGIC;
      IIC_0_scl_t : out STD_LOGIC;
      IIC_0_sda_i : in STD_LOGIC;
      IIC_0_sda_o : out STD_LOGIC;
      IIC_0_sda_t : out STD_LOGIC;
      S_AXI_aruser : in STD_LOGIC;
      S_AXI_awuser : in STD_LOGIC;
      S_AXI_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_awlock : in STD_LOGIC;
      S_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awvalid : in STD_LOGIC;
      S_AXI_awready : out STD_LOGIC;
      S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_wlast : in STD_LOGIC;
      S_AXI_wvalid : in STD_LOGIC;
      S_AXI_wready : out STD_LOGIC;
      S_AXI_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_bvalid : out STD_LOGIC;
      S_AXI_bready : in STD_LOGIC;
      S_AXI_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_arlock : in STD_LOGIC;
      S_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arvalid : in STD_LOGIC;
      S_AXI_arready : out STD_LOGIC;
      S_AXI_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_rlast : out STD_LOGIC;
      S_AXI_rvalid : out STD_LOGIC;
      S_AXI_rready : in STD_LOGIC;
      S_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      clk_i : in STD_LOGIC;
      aresetn_i : in STD_LOGIC;
      pl_resetn_o : out STD_LOGIC;
      pl_ps_irq0_i : in STD_LOGIC_VECTOR ( 0 to 0 );
      pl_clk_o : out STD_LOGIC;
      saxi_clk : in STD_LOGIC
    );
    end component mpsoc;
begin
  inst_mpsoc: mpsoc
    port map (
      M_AXI_araddr(31 downto 0)  => m_axi_out.araddr,
      M_AXI_araddr(39 downto 32) => M_AXI_araddr(39 downto 32),
      M_AXI_arprot  => open,
      M_AXI_arready => m_axi_in.arready,
      M_AXI_arvalid => m_axi_out.arvalid,
      M_AXI_awaddr(31 downto 0) => m_axi_out.awaddr,
      M_AXI_awaddr(39 downto 32) => M_AXI_awaddr(39 downto 32),
      M_AXI_awprot  => open,
      M_AXI_awready => m_axi_in.awready,
      M_AXI_awvalid => m_axi_out.awvalid,
      M_AXI_bready  => m_axi_out.bready,
      M_AXI_bresp   => m_axi_in.bresp,
      M_AXI_bvalid  => m_axi_in.bvalid,
      M_AXI_rdata   => m_axi_in.rdata,
      M_AXI_rready  => m_axi_out.rready,
      M_AXI_rresp   => m_axi_in.rresp,
      M_AXI_rvalid  => m_axi_in.rvalid,
      M_AXI_wdata   => m_axi_out.wdata,
      M_AXI_wready  => m_axi_in.wready,
      M_AXI_wstrb   => m_axi_out.wstrb,
      M_AXI_wvalid  => m_axi_out.wvalid,

      S_AXI_awuser => s_axi_awuser,
      S_AXI_awid => s_axi_awid,
      S_AXI_awaddr => s_axi_awaddr,
      S_AXI_awlen => s_axi_awlen,
      S_AXI_awsize => s_axi_awsize,
      S_AXI_awburst => s_axi_awburst,
      S_AXI_awlock => s_axi_awlock,
      S_AXI_awcache => s_axi_awcache,
      S_AXI_awprot => s_axi_awprot,
      S_AXI_awvalid => s_axi_awvalid,
      S_AXI_awready => s_axi_awready,
      S_AXI_awqos => (others => '0'),
      S_AXI_wdata => s_axi_wdata,
      S_AXI_wstrb => s_axi_wstrb,
      S_AXI_wlast => s_axi_wlast,
      S_AXI_wvalid => s_axi_wvalid,
      S_AXI_wready => s_axi_wready,
      S_AXI_bid => s_axi_bid,
      S_AXI_bresp => s_axi_bresp,
      S_AXI_bvalid => s_axi_bvalid,
      S_AXI_bready => s_axi_bready,
      S_AXI_aruser => 'X',
      S_AXI_arid => (others => 'X'),
      S_AXI_araddr => (others => 'X'),
      S_AXI_arlen => (others => 'X'),
      S_AXI_arsize => (others => 'X'),
      S_AXI_arburst => (others => 'X'),
      S_AXI_arlock => 'X',
      S_AXI_arcache => (others => 'X'),
      S_AXI_arprot => (others => 'X'),
      S_AXI_arvalid => 'X',
      S_AXI_arready => open,
      S_AXI_rid => open,
      S_AXI_rdata => open,
      S_AXI_rresp => open,
      S_AXI_rlast => open,
      S_AXI_rvalid => open,
      S_AXI_rready => 'X',
      S_AXI_arqos => (others => 'X'),

      saxi_clk => clk,

      pl_ps_irq0_i  => "0",
      GPIO_tri_i => (others => '0'),
      GPIO_tri_o => open,
      GPIO_tri_t => open,
      IIC_0_scl_i => iic0_scl_in,
      IIC_0_scl_o => iic0_scl_out,
      IIC_0_scl_t => iic0_scl_tri,
      IIC_0_sda_i => iic0_sda_in,
      IIC_0_sda_o => iic0_sda_out,
      IIC_0_sda_t => iic0_sda_tri,
      aresetn_i     => rst_n,
      clk_i         => clk,
      pl_clk_o      => open,
      pl_resetn_o   => open);

  -- I2C
  i2c_sda_b <= 'Z' when iic0_sda_tri = '1' else iic0_sda_out;
  iic0_sda_in <= i2c_sda_b;

  i2c_scl_b <= 'Z' when iic0_scl_tri = '1' else iic0_scl_out;
  iic0_scl_in <= i2c_scl_b;

  inst_int_gen: entity work.mpsoc_int_gen
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      irq_i => irq,
      s_axi_awaddr => s_axi_awaddr,
      s_axi_awburst => s_axi_awburst,
      s_axi_awcache => s_axi_awcache,
      s_axi_awid => s_axi_awid,
      s_axi_awlen => s_axi_awlen,
      s_axi_awlock => s_axi_awlock,
      s_axi_awprot => s_axi_awprot,
      s_axi_awready => s_axi_awready,
      s_axi_awsize => s_axi_awsize,
      s_axi_awuser => s_axi_awuser,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_wdata => s_axi_wdata,
      s_axi_wlast => s_axi_wlast,
      s_axi_wready => s_axi_wready,
      s_axi_wstrb => s_axi_wstrb,
      s_axi_wvalid => s_axi_wvalid,
      s_axi_bid => s_axi_bid,
      s_axi_bready => s_axi_bready,
      s_axi_bresp => s_axi_bresp,
      s_axi_bvalid => s_axi_bvalid
    );
  
  inst_pci_map: entity work.pci_map
    port map (
      aclk => clk,
      areset_n => rst_n,
      awvalid => m_axi_out.awvalid,
      awready => m_axi_in.awready,
      awaddr => m_axi_out.awaddr(20 downto 2),
      awprot => "000",
      wvalid => m_axi_out.wvalid,
      wready => m_axi_in.wready,
      wdata => m_axi_out.wdata,
      wstrb => m_axi_out.wstrb,
      bvalid => m_axi_in.bvalid,
      bready => m_axi_out.bready,
      bresp => m_axi_in.bresp,
      arvalid => m_axi_out.arvalid,
      arready => m_axi_in.arready,
      araddr => m_axi_out.araddr(20 downto 2),
      arprot => "000",
      rvalid => m_axi_in.rvalid,
      rready => m_axi_out.rready,
      rdata => m_axi_in.rdata,
      rresp => m_axi_in.rresp,

      host_i => host_wb_in,
      host_o => host_wb_out,

      board_awvalid_o => board_axi_out.awvalid,
      board_awready_i => board_axi_in.awready,
      board_awaddr_o => board_axi_out.awaddr (16 downto 2),
      board_awprot_o => open,
      board_wvalid_o => board_axi_out.wvalid,
      board_wready_i => board_axi_in.wready,
      board_wdata_o => board_axi_out.wdata,
      board_wstrb_o => board_axi_out.wstrb,
      board_bvalid_i => board_axi_in.bvalid,
      board_bready_o => board_axi_out.bready,
      board_bresp_i => board_axi_in.bresp,
      board_arvalid_o => board_axi_out.arvalid,
      board_arready_i => board_axi_in.arready,
      board_araddr_o => board_axi_out.araddr (16 downto 2),
      board_arprot_o => open,
      board_rvalid_i => board_axi_in.rvalid,
      board_rready_o => board_axi_out.rready,
      board_rdata_i => board_axi_in.rdata,
      board_rresp_i => board_axi_in.rresp
    );

  inst_wren: entity work.wren_core
      generic map (
        g_map_version => g_map_version,
        g_host_ident => open
      )
      port map (
        clk_sys_62m5_i => clk_sys_62m5,
        rst_sys_62m5_n_i => rst_sys_62m5_n,
        board_axi_o => board_axi_in,
        board_axi_i => board_axi_out,
        host_wb_o => host_wb_in,
        host_wb_i => host_wb_out,
        tm_link_up_i => tm_link_up,
        tm_tai_i => tm_tai,
        tm_cycles_i => tm_cycles,
        tm_time_valid_i => tm_time_valid,
        wr_master_o => wr_master_out,
        wr_master_i => wr_master_in,
        irq_o => irq,
        eth_tx_o => eth_tx_out,
        eth_tx_i => eth_tx_in,
        eth_rx_o => eth_rx_out,
        eth_rx_i => eth_rx_in,
        led_o => led_o,
        pps_p_i => pps,
        pps_valid_i => pps_valid
      );
  
  cmp_xwrc_board_pxie_fmc : entity work.xwrc_board_pxie_fmc
    generic map (
      g_simulation   => g_SIMULATION,
      g_fabric_iface => plain,
      g_aux_pll_cfg => (0 => (enabled => False, bufg_en => True, divide => 4),
                        others => c_AUXPLL_CFG_DEFAULT),
      g_dpram_initf  => g_WRPC_INITF)
    port map (
      areset_n_i             => '1', -- do not use PS_POR for now
      wr_clk_helper_125m_p_i => wr_clk_helper_125m_p_i,
      wr_clk_helper_125m_n_i => wr_clk_helper_125m_n_i, 
      wr_clk_main_125m_p_i   => wr_clk_main_125m_p_i, 
      wr_clk_main_125m_n_i   => wr_clk_main_125m_n_i, 
      wr_clk_sfp_125m_p_i    => wr_clk_sfp_125m_p_i, 
      wr_clk_sfp_125m_n_i    => wr_clk_sfp_125m_n_i, 
      clk_sys_62m5_o         => clk_sys_62m5,
      clk_ref_125m_o         => clk_ref_62m5,
      rst_sys_62m5_n_o       => rst_sys_62m5_n,
      rst_ref_125m_n_o       => rst_ref_62m5_n,

      plldac_sclk_o   => plldac_sclk_o,
      plldac_din_o    => plldac_din_o,
      pll25dac_cs_n_o => pll25dac_cs_n_o,
      pll20dac_cs_n_o => pll20dac_cs_n_o,

      clk_pll_aux_o => open,

      sfp_txp_o       => sfp_txp_o,
      sfp_txn_o       => sfp_txn_o,
      sfp_rxp_i       => sfp_rxp_i,
      sfp_rxn_i       => sfp_rxn_i,
      sfp_det_i       => sfp_det_i,
      sfp_sda_i       => sfp_sda_in,
      sfp_sda_o       => sfp_sda_out,
      sfp_scl_i       => sfp_scl_in,
      sfp_scl_o       => sfp_scl_out,
      sfp_tx_disable_o => sfp_tx_disable_o,
      sfp_los_i        => sfp_los_i,
  
      wrf_src_i => eth_rx_out,
      wrf_src_o => eth_rx_in,
      wrf_snk_i => eth_tx_out,
      wrf_snk_o => eth_tx_in,

      eeprom_sda_i => eeprom_sda_in, 
      eeprom_sda_o => eeprom_sda_out, 
      eeprom_scl_i => eeprom_scl_in, 
      eeprom_scl_o => eeprom_scl_out, 
      uart_rxd_i   => uart_rxd_i, 
      uart_txd_o   => uart_txd_o, 

      tm_link_up_o => tm_link_up,
      tm_time_valid_o => tm_time_valid,
      tm_tai_o => tm_tai,
      tm_cycles_o => tm_cycles,
      
      wb_slave_i => wr_master_out,
      wb_slave_o => wr_master_in,

      link_ok_o => open,
      pps_led_o => open,

      led_act_o  => led_act,
      led_link_o => led_link,
      pps_p_o    => pps,
      pps_valid_o => pps_valid);

  pps_p_o <= pps;

  sfp_scl_b <= '0' when sfp_scl_out = '0' else 'Z';
  sfp_sda_b <= '0' when sfp_sda_out = '0' else 'Z';
  sfp_scl_in <= sfp_scl_b;
  sfp_sda_in <= sfp_sda_b;

  eeprom_scl_b <= '0' when eeprom_scl_out = '0' else 'Z';
  eeprom_sda_b <= '0' when eeprom_sda_out = '0' else 'Z';
  eeprom_scl_in <= eeprom_scl_b;
  eeprom_sda_in <= eeprom_sda_b;
end architecture arch;
