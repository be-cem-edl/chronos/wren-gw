library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity tb_bst is
end tb_bst;

architecture arch of tb_bst is
  signal clk_80m, rst_n : std_logic := '0';
  signal out_ddr : std_logic_vector(1 downto 0);
  signal output, bst_out : std_logic;
begin
  --  Clocking
  p_clk: process
  begin
    clk_80m <= not clk_80m;
    wait for 12_500 ps;
  end process;

  dut: entity work.ttc_demo
    port map (
      clk_80m_i => clk_80m,
      rst_80m_n_i => rst_n,
      bst_out_ddr_o => out_ddr
    );

  process
  begin
    rst_n <= '0';
    wait until rising_edge(clk_80m);
    rst_n <= '1';
    wait;
  end process;

  process
  begin
    wait until rising_edge(clk_80m);
    output <= out_ddr (0);
    wait until falling_edge(clk_80m);
    output <= out_ddr (1);
  end process;

  inst_ODDR : ODDRE1
  generic map (
    IS_C_INVERTED => '0', -- Optional inversion for C
    SIM_DEVICE => "ULTRASCALE_PLUS",
    SRVAL => '0'
    )
  port map (
    Q => bst_out,
    C => clk_80m,
    D1 => out_ddr(1),
    D2 => out_ddr(0),
    SR => '0'
    );

end arch;
