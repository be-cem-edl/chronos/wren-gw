# HDLMake 'develop' branch required.
#
# Due to bugs in release v3.0 of hdlmake it is necessary to use the "develop"
# branch of hdlmake, commit db4e1ab.

sim_tool   = "ghdl"
sim_top    = "tb_timing"
action     = "simulation"
syn_device = "xc6slx150t"

# For wr-cores
target     = "xilinx"
board  = "svec"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../ip_cores"


files = [
    "tb_timing.vhd",
]

modules = {
    "local" :  [
        "../../rtl",
    ],
    "git" : [
        "git://ohwr.org/projects/general-cores.git",
    ],
    "system": ['xilinx']
}
