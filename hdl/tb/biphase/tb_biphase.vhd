library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_biphase is
end tb_biphase;

architecture arch of tb_biphase is
  signal clk_80m, rst_n : std_logic := '0';
  signal out_ddr : std_logic_vector(1 downto 0);
  signal output : std_logic;
  signal ack, rdy, imm : std_logic;
  signal chan_a, chan_b : std_logic_vector(31 downto 0);
begin
  --  Clocking
  p_clk: process
  begin
    clk_80m <= not clk_80m;
    wait for 12_500 ps;
  end process;

  dut: entity work.ttc_biphase
    port map (
      clk_i => clk_80m,
      rst_n_i => rst_n,
      out_o => out_ddr,
      chan_a_i => chan_a,
      chan_b_i => chan_b,
      load_imm_i => imm,
      rdy_i => rdy,
      ack_o => ack
    );

  process
  begin
    rdy <= '0';
    imm <= '0';
    rst_n <= '0';
    wait until rising_edge(clk_80m);
    rst_n <= '1';
    chan_a <= x"8000_0000";
    chan_b <= x"ffff_ffff";
    imm <= '1';
    wait until rising_edge(clk_80m);
    imm <= '0';
    wait;
  end process;

  process
  begin
    wait until rising_edge(clk_80m);
    output <= out_ddr (0);
    wait until falling_edge(clk_80m);
    output <= out_ddr (1);
  end process;
end arch;
