# HDLMake 'develop' branch required.
#
# Due to bugs in release v3.0 of hdlmake it is necessary to use the "develop"
# branch of hdlmake, commit db4e1ab.

sim_tool   = "ghdl"
sim_top    = "tb_frame"
action     = "simulation"
syn_device = "xc6slx150t"

# For wr-cores
target     = "xilinx"
board  = "svec"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../ip_cores"


ttc_rx = "../../../dependencies/BST_FPGA/bst_decoder/ttc_rx/"
files = [
    "tb_frame.vhd",
    ttc_rx + "BiPhaseMark_Decoder.vhd",
    ttc_rx + "TTCrx_AB_Demux.vhd",
    ttc_rx + "TTCrx_Frame_Decoder.vhd",
    ttc_rx + "TTCrx_Hamming_Decoder.vhd",
]

modules = {
    "local" :  [
        "../../rtl",
    ],
    "git" : [
        "git://ohwr.org/projects/general-cores.git",
    ],
    "system": ['xilinx']
}
