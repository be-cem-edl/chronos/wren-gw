--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Project    : WREN
--------------------------------------------------------------------------------
--
-- unit name:   si5340_ctrl
--
-- description: si5340 frequency update from wr-core
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.wishbone_pkg.all;
use work.si5340_ctrl_map_consts.all;

entity tb_si5340_ctrl is
end;

architecture arch of tb_si5340_ctrl is
  constant g_dac_bits : natural := 16;
  signal clk               : std_logic;
  signal pl_reset_n        : std_logic;
  signal rst_n             : std_logic;
  signal wb_in             : t_wishbone_slave_in;
  signal wb_out            : t_wishbone_slave_out;
  signal dac_h_o             : std_logic_vector (g_dac_bits - 1 downto 0);
  signal dac_h_wr_o          : std_logic;
  signal dac_d_o             : std_logic_vector (g_dac_bits - 1 downto 0);
  signal dac_d_wr_o          : std_logic;
  signal dac_h_i             : std_logic_vector(g_dac_bits - 1 downto 0);
  signal dac_h_p_i           : std_logic;
  signal dac_d_i             : std_logic_vector(g_dac_bits - 1 downto 0);
  signal dac_d_p_i           : std_logic;
  signal bypass_spi_mosi   : std_logic;
  signal bypass_spi_miso   : std_logic;
  signal bypass_spi_clk    : std_logic;
  signal bypass_spi_h_cs_n : std_logic;
  signal bypass_spi_d_cs_n : std_logic;
  signal spi_mosi          : std_logic;
  signal spi_miso          : std_logic;
  signal spi_clk           : std_logic;
  signal spi_h_cs_n        : std_logic;
  signal spi_d_cs_n        : std_logic;

  signal spi_dat : std_logic_vector(127 downto 0);
begin
  process
  begin
    clk <= '0';
    wait for 8 ns;
    clk <= '1';
    wait for 8 ns;
  end process;

  process (spi_clk)
  begin
    if rising_edge (spi_clk) then
      if spi_h_cs_n = '0' or spi_d_cs_n = '0' then
        spi_dat <= spi_dat(spi_dat'left - 1 downto 0) & spi_mosi;
      else
        spi_dat <= (others => 'X');
      end if;
    end if;
  end process;
  
  DUT: entity work.si5340_ctrl
    generic map (
      g_spi_div  => 32,
      g_dac_bits => 16)
    port map (
      clk_i               => clk,
      pl_reset_n_i        => pl_reset_n,
      rst_n_i             => rst_n,
      wb_i                => wb_in,
      wb_o                => wb_out,
      dac_h_o             => dac_h_o,
      dac_h_wr_o          => dac_h_wr_o,
      dac_d_o             => dac_d_o,
      dac_d_wr_o          => dac_d_wr_o,
      dac_h_i             => dac_h_i,
      dac_h_p_i           => dac_h_p_i,
      dac_d_i             => dac_d_i,
      dac_d_p_i           => dac_d_p_i,
      bypass_spi_mosi_i   => bypass_spi_mosi,
      bypass_spi_miso_o   => bypass_spi_miso,
      bypass_spi_clk_i    => bypass_spi_clk,
      bypass_spi_h_cs_n_i => bypass_spi_h_cs_n,
      bypass_spi_d_cs_n_i => bypass_spi_d_cs_n,
      spi_mosi_o          => spi_mosi,
      spi_miso_i          => spi_miso,
      spi_clk_o           => spi_clk,
      spi_h_cs_n_o        => spi_h_cs_n,
      spi_d_cs_n_o        => spi_d_cs_n);

  process
    procedure write_wb(addr: natural; val: std_logic_vector(31 downto 0)) is
    begin
      wb_in.adr <= std_logic_vector(to_unsigned(addr, 32));
      wb_in.dat <= val;
      wb_in.sel <= x"f";
      wb_in.we <= '1';
      wb_in.stb <= '1';
      wb_in.cyc <= '1';
      loop
        wait until rising_edge(clk);
        exit when wb_out.ack = '1';
      end loop;
      wb_in.cyc <= '0';
      wb_in.stb <= '0';
    end write_wb;
  begin
    pl_reset_n <= '0';
    rst_n <= 'X';
    dac_d_p_i <= '0';

    --  While the fpga is in reset, SPI is bypassed.
    bypass_spi_clk <= '0';
    bypass_spi_h_cs_n <= '1';
    bypass_spi_d_cs_n <= '1';
    bypass_spi_mosi <= '0';
    wait for 1 ns;

    bypass_spi_clk <= '1';
    bypass_spi_h_cs_n <= '1';
    bypass_spi_d_cs_n <= '1';
    bypass_spi_mosi <= '1';
    wait for 1 ns;

    --  Do the reset.
    wb_in.stb <= '0';
    wb_in.cyc <= '0';
    wait until rising_edge(clk);
    rst_n <= '0';
    wait until rising_edge(clk);
    rst_n <= '1';
    pl_reset_n <= '1';

    --  Write DAC, update, enable
    write_wb(ADDR_SI5340_CTRL_MAP_PLL_1_DAC, x"0000_8000");
    write_wb(ADDR_SI5340_CTRL_MAP_PLL_0_DAC, x"0000_8000");
    write_wb(ADDR_SI5340_CTRL_MAP_CTRL, x"0000_0033");

    if false then
      --  Write bias, mul.
      --  1: main pll 10Mhz +/- 10ppm
      --  Num = 0x8_4000_0000  +/- 0x42000
      --      = 0x8_3ffb_e000  + 2*0x42000
      write_wb(ADDR_SI5340_CTRL_MAP_PLL_1_BIAS_H, x"0000_0008");
      write_wb(ADDR_SI5340_CTRL_MAP_PLL_1_BIAS_L, x"3ffb_e000");
      write_wb(ADDR_SI5340_CTRL_MAP_PLL_1_MUL, x"0004_2004");

      dac_d_i <= x"ffff";
      dac_d_p_i <= '1';
      wait until rising_edge(clk);
      dac_d_p_i <= '0';
    end if;

    if true then
      --  Write bias, mul
      --  0: helper pll 25Mhz +/- 100ppm
      --  Num = 0x14_a880_0000 +/- 0x52a200
      --      = 0x14_a82d_5e00 + 2*0x52a200
      write_wb(ADDR_SI5340_CTRL_MAP_PLL_0_BIAS_H, x"0000_0014");
      write_wb(ADDR_SI5340_CTRL_MAP_PLL_0_BIAS_L, x"a82d_5e00");
      write_wb(ADDR_SI5340_CTRL_MAP_PLL_0_MUL, x"0052_a252");

      dac_h_i <= x"0000" or x"ffff";
      dac_h_p_i <= '1';
      wait until rising_edge(clk);
      dac_h_p_i <= '0';
    end if;

    report "Done";
    wait;
  end process;
end arch;
